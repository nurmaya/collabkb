class KitBuildApp {

  constructor(canvas) {
    this.canvas = canvas;
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.analyzer = new AnalyzerLib();
    this.ajax = Ajax.ins(this.gui);
    this.controller = 'wibisono';
    this.page = 'kb';
    this.nextPage = 'postmapping';

    this.material = {
      mid: null,
      name: null,
      content: null
    }

    this.goalmap = {
      gmid: null,
      name: null
    }

    this.eventListeners = [];

    this.kit = null; // holder for kit socket communication
    this.handleEvent();
    this.canvas.attachEventListener(this);

  }

  getCanvas() {
    return this.canvas;
  }

  getGUI() {
    return this.gui;
  }

  getSession() {
    return this.session;
  }

  handleEvent() {
    let app = this;

    // $('#bt-login').on('click', function (e) {
    //   $('#modal-chat-login').modal('show');
    //   $('#modal-chat-login .input-username').focus();
    // });
    // let doSignIn = function () {
    //   let username = $('#modal-chat-login .input-username').val().trim();
    //   let password = $('#modal-chat-login .input-password').val().trim();
    //   app.ajax.post(
    //     baseUrl + 'kitbuildApi/signIn', {
    //       username: username,
    //       password: password
    //     }
    //   ).then(function (user) {
    //     let uid = user.uid;
    //     let rid = user.role_id;
    //     let gids = user.gids ? user.gids.split(",") : [];
    //     app.kit.setUserWithName(uid, username, user.name, rid, gids);
    //     app.signIn(uid, username, user.name, rid, gids);
    //     $('#modal-chat-login').modal('hide');
    //     $('#input-username').val('');
    //     $('#input-password').val('');
    //   });
    // }
    // $('#modal-chat-login').on('click', '.bt-ok', doSignIn);
    // $('#modal-chat-login .input-password').on('keyup', function (e) {
    //   if (e.keyCode == 13) doSignIn();
    // });
    $('#bt-logout').on('click', function (e) {
      let confirmDialog = app.gui.confirm('Do you want to logout?', {
        'positive-callback': function () {
          app.signOut();
          confirmDialog.modal('hide');
          window.location.href = baseUrl + app.controller + '/signOut';
        }
      });
    });
    // $('#bt-sync').on('click', (e) => {
    //   if (this.canvas.getCy().nodes().length) {
    //     let text = 'You currently have concept map on canvas.<br>If you sync the map, it will be <strong class="text-danger">replaced</strong>.<br>Continue?';
    //     let dialog = this.gui.confirm(text, {
    //       'positive-callback': function () {
    //         app.kit.syncMap();
    //         app.eventListeners.forEach(listener => {
    //           if (listener && typeof listener.onAppEvent == 'function') {
    //             listener.onAppEvent('request-sync');
    //           }
    //         })
    //         dialog.modal('hide');
    //       }
    //     })
    //   }
    // });
    // $('#bt-open-kit').on('click', function () {
    //   app.ajax.post(
    //     baseUrl + 'kitbuildApi/getMaterialsWithGids', {
    //       gids: app.user.gids
    //     }
    //   ).then(function (materials) {
    //     let materialList = (!materials || materials.length == 0) ?
    //       '<em class="text-secondary">No material available</em>' :
    //       '';
    //     materials.forEach((material) => {
    //       materialList += '<div class="row align-items-center list list-hover list-pointer justify-content-between pt-2 pb-2 pl-2 pr-2" data-mid="' + material.mid + '" data-name="' + material.name + '">'
    //       materialList += material.name
    //       materialList += '<i class="fas fa-check text-primary" style="display:none"></i>'
    //       materialList += '</div>'
    //     });
    //     $('#modal-kit .material-list').html(materialList);
    //     $('#modal-kit').modal('show', {
    //       'width': '200px'
    //     });
    //   })
    // });
    // $('#modal-kit .material-list').on('click', '.row', function () {
    //   if ($(this).hasClass('selected')) return;
    //   $('#modal-kit .material-list .row').find('i').hide();
    //   $('#modal-kit .material-list .row').removeClass('selected');
    //   $(this).find('i').show();
    //   $(this).addClass('selected')
    //   let mid = $(this).data('mid');
    //   let name = $(this).data('name');
    //   $('#modal-kit .bt-open').data('mid', mid);
    //   $('#modal-kit .bt-open').data('name', name);
    //   $('#modal-kit .bt-open').data('gmid', null);
    //   $('#modal-kit .bt-open').data('gmname', null);
    //   $('#modal-kit .goalmap-list').html('<em class="text-secondary">Loading goalmaps...</em>');
    //   app.ajax.get(
    //     baseUrl + 'kitbuildApi/getCollabGoalmaps/' + mid + '/' + app.kit.room.rid,
    //   ).then(function (goalmaps) {
    //     let goalmapList = (!goalmaps || goalmaps.length == 0) ?
    //       '<em class="text-secondary">No goalmap available</em>' :
    //       '';
    //     goalmaps.forEach((goalmap) => {
    //       goalmapList += '<div class="row align-items-center list list-hover list-pointer justify-content-between pt-2 pb-2 pl-2 pr-2" data-gmid="' + goalmap.gmid + '" data-name="' + goalmap.name + '">'
    //       goalmapList += goalmap.name
    //       goalmapList += '<i class="fas fa-check text-primary" style="display:none"></i>'
    //       goalmapList += '</div>'
    //     });
    //     $('#modal-kit .goalmap-list').html(goalmapList);
    //   })
    // });
    // $('#modal-kit .goalmap-list').on('click', '.row', function () {
    //   if ($(this).hasClass('selected')) return;
    //   $('#modal-kit .goalmap-list .row').find('i').hide();
    //   $('#modal-kit .goalmap-list .row').removeClass('selected');
    //   $(this).find('i').show();
    //   $(this).addClass('selected')
    //   let gmid = $(this).data('gmid');
    //   let name = $(this).data('name');
    //   $('#modal-kit .bt-open').data('gmid', gmid);
    //   $('#modal-kit .bt-open').data('gmname', name);
    // });
    // $('#modal-kit').on('click', '.bt-open', function () {
    //   let name = ($(this).data('name'))
    //   let mid = ($(this).data('mid'))
    //   let gmid = ($(this).data('gmid'))
    //   let gmname = ($(this).data('gmname'))
    //   if (!gmid) {
    //     app.gui.dialog('Please select a Kit.', {
    //       width: '300px'
    //     });
    //     return;
    //   }
    //   let confirm = app.gui.confirm('Open kit <strong>\'' + gmname + '\' of \'' + name + '\'</strong>?', {
    //     'positive-callback': function () {
    //       app.loadKit(gmid, function () {
    //         confirm.modal('hide');
    //         $('#modal-kit').modal('hide');
    //       });
    //     },
    //     'width': '300px'
    //   })
    // });
    $('#bt-show-material').on('click', function () {
      $('#popup-material').fadeToggle({
        duration: 200,
        complete: function () {
          let shown = $(this).is(':visible');
          app.eventListeners.forEach(listener => {
            if (listener && typeof listener.onAppEvent == 'function') {
              if (shown) listener.onAppEvent('show-material');
              else listener.onAppEvent('hide-material');
            }
          })
        }
      });
    });
    $('#popup-material .bt-close').on('click', function () {
      $('#popup-material .check-open').prop('checked', false);
      $('#popup-material').fadeOut({
        duration: 200,
        complete: function () {
          app.eventListeners.forEach(listener => {
            if (listener && typeof listener.onAppEvent == 'function') {
              listener.onAppEvent('hide-material');
            }
          })
        }
      });
    });
    $('#bt-open-map').on('click', function () {
      let gmid = app.goalmap.gmid;
      app.ajax.get(baseUrl + 'kitbuildApi/getUserLearnermaps/' + gmid + '/' + uid).then(function (learnermaps) {
        console.log(learnermaps);
        let mapList = '<em>No map found on database.</em>';
        if (learnermaps && learnermaps.length) {
          mapList = '';
          learnermaps.forEach(lm => { // console.log(lm);
            let badge = 'badge-info'
            switch (lm.type) {
              case 'draft':
                badge = 'badge-warning';
                break;
              case 'fix':
                badge = 'badge-success';
                break;
            }
            mapList += '<div class="row align-items-center list list-hover list-pointer justify-content-between pt-2 pb-2 pl-5 pr-5" data-lmid="' + lm.lmid + '" data-name="' + lm.name + '" data-created="' + lm.create_time + '">'
            mapList += ' <span>saved map <span class="badge ' + badge + '">' + lm.type + '</span></span>' + '<div><span class="badge badge-primary">Saved: ' + lm.create_time + '</span></div>'
            mapList += '</div>'
          });
        }
        $('#modal-open-map .map-list').html(mapList);
      });
      $('#modal-open-map').modal('show');
    });
    $('#modal-open-map .map-list').on('click', '.row', function () {
      let lmid = $(this).data('lmid');
      let created = $(this).data('created');
      if (app.canvas.getCy().nodes().length) {
        let dialog = app.gui.confirm('You have map on canvas. Opening the map will replace existing map on canvas. Open map created on "' + created + '"?', {
          'positive-callback': function () {
            app.loadSavedLearnermap(lmid, function () {
              dialog.modal('hide');
              $('#modal-open-map').modal('hide');
            });
          }
        });
        return;
      } else {
        app.loadSavedLearnermap(lmid, function () {
          $('#modal-open-map').modal('hide');
        });
      }
    })
    $('#bt-save').on('click', function () {
      let gmid = app.goalmap.gmid;
      let uid = app.user.uid;
      if (gmid != null && uid != null) {
        app.saveLearnermapDraft(gmid, uid, function (lmid) {
          if (lmid == null) {
            app.gui.notify('Unable to save empty map', {
              type: 'warning'
            });
            return;
          }
          app.gui.notify('Map saved.', {
            type: 'success'
          })
        });
      } else {
        if (!gmid) {
          app.gui.dialog('Please open a kit to save the map into.<br>A map should refer to a kit.');
          return;
        }
        // if (!rid) {
        //   app.gui.dialog('Please join a room to save the map into.<br>In a collaboration environment, a saved maps should relate with a collaboration group. Therefore, you need to join a room before you create and save a concept map.');
        //   return;
        // }
      }
    });
    // $('#bt-load-draft').on('click', function () {
    //   let gmid = app.goalmap.gmid;
    //   let rid = app.kit.room.rid;
    //   console.log(gmid, rid);
    //   if (gmid != null && rid != null) {
    //     let currentEles = app.getCanvas().getCy().nodes();
    //     if (currentEles.length) {
    //       let confirm = app.gui.confirm('You currently have existing map data on canvas. Loading saved draft map data will <strong class="text-danger">REPLACE</strong> current map on canvas and across <strong class="text-danger">ALL users</strong> in current room.<br>Continue?', {
    //         'positive-callback': function () {
    //           app.loadSavedLearnermapDraft(gmid, rid, function () {
    //             confirm.modal('hide');
    //           });
    //         }
    //       })
    //     } else app.loadSavedLearnermapDraft(gmid, rid);
    //   } else {
    //     if (!gmid) app.gui.dialog('Please select a kit. A concept map saved draft is related with a kit.')
    //     if (!rid) app.gui.dialog('Please select a room. A collaborative concept map saved draft is related with a collaboration room.')
    //   }
    // });
    let buildLearnermap = function () {
      let learnermap = {
        concepts: [],
        links: []
      }
      let concepts = app.canvas.getCy().nodes('[type="concept"]');
      let links = app.canvas.getCy().nodes('[type="link"]');
      for (let c of concepts) {
        learnermap.concepts.push({
          cid: c.id().substr(2),
          label: c.data('name')
        })
      }
      for (let l of links) {
        let left = l.connectedEdges('[type="left"]');
        let right = l.connectedEdges('[type="right"]');
        // console.log(left, right);
        let source = null,
          target = null;
        if (left.length == 1) source = left.connectedNodes('[type="concept"]')
        if (right.length == 1) target = right.connectedNodes('[type="concept"]')
        // console.log(source, target);
        learnermap.links.push({
          lid: l.id().substr(2),
          label: l.data('name'),
          source: source ? source.id().substr(2) : null,
          target: target ? target.id().substr(2) : null
        });
      }
      // console.log(concepts, links);
      return learnermap;
    }
    $('#bt-feedback').on('click', function () {
      // console.warn('feedback')
      app.learnermap = buildLearnermap();
      // console.log(app.learnermap, app.goalmap);
      let compare = app.analyzer.compare(app.learnermap, app.goalmap);
      
      let message = '';
      if (compare.excess.length > 0) {
        message += '<p>You have ' + compare.excess.length + ' unmatched propositions with teacher\'s map.<br>Unmatched propositions are marked with red dashed line.</p>';
        for (let e of compare.excess) {
          let link = app.canvas.getCy().nodes('#l-' + e.lid);
          if (link.length) {
            link.connectedEdges().data('match', 5);
            link.connectedEdges().data('link', 'miss');
          }
        }
      }

      app.canvas.getCy().remove('edge[type="feedback"]');
      if(compare.miss.length > 0) {
        let missingPropositions = [];
        for (let m of compare.miss) {
          let miss = {
            group: 'edges',
            data: {
              source: 'c-' + m.source,
              target: 'c-' + m.target,
              type: 'feedback'
            }
          }
          missingPropositions.push(miss);
        }
        app.canvas.getCy().add(missingPropositions);
      }

      if (compare.leave.length) message += '<p>You have ' + compare.leave.length + ' unconnected links.</p>';
      else message += '<p><i class="fas fa-check text-success"></i> Great! All links are connected.</p>';

      if (message.trim() != '') app.gui.dialog(message);
      app.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('get-feedback', {
            excess: compare.excess.length,
            leave: compare.leave.length            
          });
        }
      })
    });
    $('#bt-finalize').on('click', function () {
      let gmid = app.goalmap.gmid;
      let uid = app.user.uid;
      if (gmid != null && uid != null) {
        let confirm = app.gui.confirm('Save and finalize the map? <br>Any previously saved draft concept maps will be deleted, preserving the finalized one. <br><span class="text-danger">After finalizing the map, your concept mapping session will be ended and you will be redirected to the next page.</span>', {
          'positive-callback': function () {
            app.saveLearnermapFinalize(gmid, uid, function (lmid) {
              confirm.modal('hide');
              if (lmid == null) {
                app.gui.notify('Unable to save empty map', {
                  type: 'warning'
                });
                return;
              }
              app.eventListeners.forEach(listener => {
                if (listener && typeof listener.onAppEvent == 'function') {
                  listener.onAppEvent('finalize-map', {
                    lmid: lmid
                  });
                }
              })
              app.gui.notify('Concept map finalized.', {
                type: 'success'
              });
              app.session.setMulti({
                page: app.nextPage,
                mid: app.material.mid,
              }, function () {
                setTimeout(function () {
                  window.location.href = baseUrl + app.controller + '/' + app.nextPage;
                }, 1000)
              });
            });
          },
          'negative-callback': function () {
            confirm.modal('hide');
          }
        })
      } else {
        if (!gmid) {
          app.gui.dialog('Please open a kit to save the map into.<br>A map should refer to a kit.');
          return;
        }
        // if (!rid) {
        //   app.gui.dialog('Please join a room to save the map into.<br>In a collaboration environment, a saved maps should relate with a collaboration group. Therefore, you need to join a room before you create and save a concept map.');
        //   return;
        // }
      }
    });

  }

  attachEventListener(listener) {
    this.eventListeners.push(listener);
  }

  // attachKit(kit) { // console.log(kit);
  //   this.kit = kit;
  //   this.kit.attachEventListener(this);
  //   this.kit.enableRoom(true);
  //   this.kit.enableChannel(true);
  // }

  autoSave() {
    try {
      let app = this;
      let gmid = app.goalmap.gmid;
      let uid = app.user.uid;
      if (gmid && uid) this.saveLearnermapAuto(gmid, uid, function (data) {
        if (data != null) console.log('autosaving... ' + data);
      })
    } catch (error) {
      console.log('autosave error: ' + error);
    }
    setTimeout(this.autoSave.bind(this), 300000);
  }

  // onKitEvent(event, data) {
  //   let gui = this.gui;
  //   let kit = this.kit;
  //   let canvas = this.canvas;
  //   let app = this;

  //   let gmid = app.goalmap.gmid;
  //   let uid = app.user.uid;

  //   switch (event) {
  //     case 'join-room':
  //       this.session.set('room', data.room);
  //       // this.canvas.clearCanvas();
  //       kit.rooms.forEach(room => {
  //         // console.log(room, kit)
  //         if (room.name == kit.room.name && room.users.length) {
  //           kit.syncMap();
  //           kit.syncMessages();
  //           return;
  //         }
  //       })
  //       $('#bt-sync').prop('disabled', false)
  //       break;
  //     case 'leave-room':
  //       this.session.unset('room');
  //       break;
  //     case 'room-update':
  //       kit.rooms = data;
  //       break;
  //     case 'user-sign-in':
  //       this.session.set('user', data);
  //       break;
  //     case 'user-sign-out':
  //       this.session.unset('user');
  //       break;
  //     case 'command-sign-out':
  //       this.signOut();
  //       this.kit.leaveRoom(this.kit.room);
  //       this.kit.enableRoom(false)
  //       this.kit.enableMessage(false)
  //       this.gui.dialog('You have been signed out');
  //       break;
  //     case 'command-leave-room':
  //       // this.signOut();
  //       // this.kit.leaveRoom(this.kit.room);
  //       // this.kit.enableRoom(false)
  //       // this.kit.enableMessage(false)
  //       // this.gui.dialog('You have been signed out');
  //       break;
  //     case 'request-map-data':
  //       let nodes = this.canvas.getCy().nodes();
  //       let edges = this.canvas.getCy().edges();
  //       let elesData = [];
  //       nodes.forEach(n => {
  //         elesData.push({
  //           group: 'nodes',
  //           data: n.data(),
  //           position: n.position()
  //         });
  //       });
  //       edges.forEach(n => {
  //         elesData.push({
  //           group: 'edges',
  //           data: n.data(),
  //           position: n.position()
  //         });
  //       });
  //       return elesData;
  //     case 'request-material-data':
  //       return this.material;
  //     case 'request-goalmap-data':
  //       return this.goalmap;
  //     case 'sync-messages-data':
  //       break;
  //     case 'sync-map-data': //console.log(data);
  //       if(data.length == 0) break;
  //       console.log('replace map');
  //       canvas.reset();
  //       canvas.clearCanvas();
  //       canvas.showMap(data);
  //       canvas.centerOneToOne();
  //       gui.notify('Map data synchronized', {
  //         type: 'success'
  //       })
  //       $('#bt-center').click();
  //       break;
  //     case 'sync-material-data':
  //       if (data.mid == null) return;
  //       this.setMaterial(data.mid, data.name, data.content);
  //       this.enableMaterial();
  //       break;
  //     case 'sync-goalmap-data':
  //       if (data.gmid == null) return;
  //       this.setGoalmap(data.gmid, data.name);
  //       break;
  //     case 'sync-map-data-error': // console.log('sync-map-data-error received')
  //       this.gui.notify(data.error, {
  //         type: 'danger'
  //       })
  //       break;
  //     case 'chat-message-open':
  //       this.session.set('chat-window-open', data)
  //       break;
  //     case 'receive-create-node':
  //       let empty = (this.canvas.getCy().nodes().length == 0);
  //       this.canvas.createNodeFromJSON(data.node);
  //       if (empty) $('#bt-center').click();
  //       break;
  //     case 'receive-move-node':
  //       this.canvas.moveNode(data.node.id, data.node.x, data.node.y);
  //       break;
  //     case 'receive-move-node-group': // including from move-drag-group event
  //       data.nodes.forEach(n => {
  //         this.canvas.moveNode(n.id, n.x, n.y);
  //       });
  //       break;
  //     case 'receive-connect-node':
  //       this.canvas.connect(data.connection);
  //       break;
  //     case 'receive-disconnect-node':
  //       this.canvas.disconnect(data.connection);
  //       break;
  //     case 'receive-duplicate-node':
  //       this.canvas.createNodeFromJSON(data.node);
  //       break;
  //     case 'receive-delete-node':
  //       this.canvas.deleteNode(data.node);
  //       break;
  //     case 'receive-delete-node-group':
  //       this.canvas.deleteNodes(data.nodes);
  //       break;
  //     case 'receive-update-node':
  //       this.canvas.updateNode(data.node.nodeId, data.node.value);
  //       break;
  //     case 'receive-center-link':
  //       console.log(data);
  //       this.canvas.moveNode(data.link.id, data.link.x, data.link.y);
  //       break;
  //     case 'receive-switch-direction': // data is linkId
  //       this.canvas.switchDirection(data);
  //       break;
  //     case 'change-page':
  //       if(!data.from || data.from != app.page) break;
  //       if (gmid != null && uid != null && rid != null) {
  //         app.saveLearnermapFinalize(gmid, uid, function (lmid) {
  //           app.eventListeners.forEach(listener => {
  //             if (listener && typeof listener.onAppEvent == 'function') {
  //               listener.onAppEvent('finalize-map', {
  //                 lmid: lmid
  //               });
  //             }
  //           })
  //           app.gui.notify('Concept map finalized.', {
  //             type: 'success'
  //           });    
  //           app.gui.dialog('Your map has been saved and finalized. Redirecting to the next page in 5 seconds.');
  //           app.session.set('page', data.page, function () {
  //             setTimeout(function(){
  //               window.location.href = baseUrl + app.controller + '/' + data.page;
  //               return;
  //             }, 5000)
  //           }, function (error) {
  //             app.gui.dialog('Error: ' + error);
  //           });
  //         });
  //       }
  //       break;
  //     case 'admin-message':
  //       app.gui.notify("<strong>Admin:</strong> " + data.message, data.options);
  //       break;
  //     case 'save-map-draft':
  //       $('#bt-save').click();
  //       break;
  //     case 'save-map-finalize':
  //       if (gmid != null && uid != null && rid != null) {
  //         app.saveLearnermapFinalize(gmid, rid, uid, function (lmid) {
  //           confirm.modal('hide');
  //           app.eventListeners.forEach(listener => {
  //             if (listener && typeof listener.onAppEvent == 'function') {
  //               listener.onAppEvent('finalize-map', {
  //                 lmid: lmid
  //               });
  //             }
  //           })
  //           app.gui.notify('Concept map finalized.', {
  //             type: 'success'
  //           })
  //         });
  //       }
  //       break;

  //       // Collab Event From Kit

  //     case 'send-message':
  //       this.eventListeners.forEach(listener => {
  //         if (listener && typeof listener.onCollabEvent == 'function') {
  //           listener.onCollabEvent('send-room-message', data.message);
  //         }
  //       })
  //       break;
  //     case 'close-discuss-channel':
  //       this.eventListeners.forEach(listener => {
  //         if (listener && typeof listener.onCollabEvent == 'function') {
  //           listener.onCollabEvent(event, data);
  //         }
  //       })
  //       break;
  //     case 'send-channel-message':
  //       this.eventListeners.forEach(listener => {
  //         if (listener && typeof listener.onCollabEvent == 'function') {
  //           listener.onCollabEvent(event, {
  //             channel: data.channel,
  //             message: data.message
  //           });
  //         }
  //       })
  //       break;
  //   }
  // }

  onCanvasEvent(event, data) { // console.log(event, data);
    let app = this;
    let edges = null;
    switch (event) {
      case 'move-link':
        edges = app.canvas.getCy().nodes('#' + data.id).connectedEdges();
        edges.data('link', 'match');
        break;
      case 'connect-left':
      case 'connect-right':
      case 'disconnect-left':
      case 'disconnect-right':
        edges = app.canvas.getCy().nodes('#' + data.source).connectedEdges();
        edges.data('link', 'match');
        app.canvas.getCy().remove('edge[type="feedback"]');
        break;
      case 'change-connect-left':
      case 'change-connect-right':
        edges = app.canvas.getCy().nodes('#' + data.from.source).connectedEdges();
        edges.data('link', 'match');
        app.canvas.getCy().remove('edge[type="feedback"]');
        break;
    }
  }
  //   if (!this.kit) return;
  //   switch (event) {
  //     case 'create-concept':
  //       this.kit.createNode(data);
  //       break;
  //     case 'create-link':
  //       this.kit.createNode(data);
  //       break;
  //     case 'drag-concept':
  //       this.kit.dragNode(data);
  //       break;
  //     case 'drag-link':
  //       this.kit.dragNode(data);
  //       break;
  //     case 'drag-node-group':
  //       this.kit.dragNodeGroup(data);
  //       break;
  //     case 'move-concept':
  //       this.kit.moveNode(data);
  //       break;
  //     case 'move-link':
  //       this.kit.moveNode(data);
  //       break;
  //     case 'move-node-group':
  //       this.kit.moveNodeGroup(data);
  //       break;
  //     case 'connect-left':
  //       this.kit.connectNode(data);
  //       break;
  //     case 'connect-right':
  //       this.kit.connectNode(data);
  //       break;
  //     case 'disconnect-left':
  //       this.kit.disconnectNode(data);
  //       break;
  //     case 'disconnect-right':
  //       this.kit.disconnectNode(data);
  //       break;
  //     case 'duplicate-node':
  //       this.kit.duplicateNode(data);
  //       break;
  //     case 'delete-node':
  //       this.kit.deleteNode(data);
  //       break;
  //     case 'update-node':
  //       this.kit.updateNode(data);
  //       break;
  //     case 'center-link':
  //       this.kit.centerLink(data);
  //       break;
  //     case 'switch-direction':
  //       this.kit.switchDirection(data);
  //       break;
  //       // case 'canvas-tool-clicked':
  //       //   console.log(data);
  //       //   break;
  //   }
  // }

  // onCanvasToolClicked(tool, node) { // console.log(tool, node);

  //   // for listener purposes
  //   let action = null;
  //   let data = null;
  //   let listenerHandled = false;
  //   let app = this;

  //   switch (tool.type) {
  //     case 'discuss-channel':
  //       let nodeId = node.id();
  //       let nodeType = node.data('type');
  //       let nodeLabel = node.data('name');
  //       app.kit.openChannel(nodeId, nodeType, nodeLabel, function (channel) {
  //         action = 'open-discuss-channel';
  //         app.eventListeners.forEach(listener => {
  //           if (listener && typeof listener.onCollabEvent == 'function') {
  //             listener.onCollabEvent(action, {
  //               cid: channel.cid,
  //               rid: channel.rid,
  //               node_id: channel.node_id,
  //               node_type: channel.node_type,
  //               node_label: channel.node_label
  //             });
  //           }
  //         })
  //       });
  //       listenerHandled = true;
  //       break;
  //     case 'center-link':
  //     case 'switch-direction':
  //       listenerHandled = true;
  //       break;
  //   }

  //   if (listenerHandled) return;
  //   this.eventListeners.forEach(listener => {
  //     if (listener && typeof listener.onCollabEvent == 'function') {
  //       listener.onCollabEvent(action, data);
  //     }
  //   })
  // }


  // Commanding Kit-Build App Interface

  signIn(uid, username, name, gids) {
    $('#bt-login').hide();
    $('#bt-logout').show();
    $('#bt-logout-username').html(": " + username);
    $('#bt-open-kit').prop('disabled', false);
    this.kit.enableMessage();
    this.enableSaveDraft();
    this.enableFinalize();
  };

  signOut() {
    $('#bt-login').show();
    $('#bt-logout').hide();
    $('#bt-open-kit').prop('disabled', true);
    $('#bt-show-material').prop('disabled', true);
    this.enableSaveDraft(false);
    this.enableFinalize(false);
    this.unsetMaterial();
  }

  enableMaterial(enabled = true) {
    $('#bt-show-material').prop('disabled', !enabled);
  }

  enableSaveDraft(enable = true) {
    $('#bt-save').prop('disabled', !enable);
    $('#bt-load-draft').prop('disabled', !enable);
  }

  enableFinalize(enable = true) {
    $('#bt-finalize').prop('disabled', !enable);
  }

  loadMaterial(mid, callback) {
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/getMaterialCollabByMid/' + mid
    ).then(function (material) {
      app.session.set('mid', material.mid);
      app.setMaterial(material.mid, material.name, material.content);
      // app.kit.sendMaterial(material.mid, material.name, material.content);
      $('#modal-kit').modal('hide');
      if (typeof callback == 'function') callback(material);
    })
    $('#bt-show-material').prop('disabled', false);
  }

  setMaterial(mid, name, content) {
    this.material.mid = mid;
    this.material.name = name;
    this.material.content = content;
    $('#popup-material .material-title').html(this.material.name)
    // /<\/?[a-z][\s\S]*>/i.test(this.material.content);
    $('#popup-material .material-content').html(this.material.content);
  }

  unsetMaterial() {
    this.material.mid = null;
    this.material.name = null;
    this.material.content = null;
  }

  setGoalmap(gmid, name) {
    this.goalmap.gmid = gmid;
    this.goalmap.name = name;
  }

  unsetGoalmap() {
    this.goalmap.gmid = null;
    this.goalmap.name = null;
  }

  saveLearnermap(gmid, uid, type, callback) {
    let app = this;
    let learnermaps = {
      type: type,
      gmid: gmid,
      uid: uid,
      concepts: [],
      links: []
    }

    let cs = app.canvas.getNodes('[type="concept"]');
    let ls = app.canvas.getNodes('[type="link"]');
    let es = app.canvas.getEdges();

    if (es.length == 0) {
      if (typeof callback == 'function') callback(null);
      return;
    };

    // console.log(cs, ls, es);
    cs.forEach(c => {
      learnermaps.concepts.push({
        cid: c.data.id.substr(2),
        gmid: gmid,
        locx: c.position.x,
        locy: c.position.y
      });
    });
    ls.forEach(l => {
      let tl = {
        lid: l.data.id.substr(2),
        gmid: gmid,
        locx: l.position.x,
        locy: l.position.y,
        source: null,
        target: null
      };
      es.forEach((e) => {
        if (e.data.source == l.data.id) {
          if (e.data.type == 'right') tl.target = e.data.target.substr(2);
          if (e.data.type == 'left') tl.source = e.data.target.substr(2);
          return;
        }
      });
      learnermaps.links.push(tl);
    })
    app.ajax.post(
      baseUrl + 'kitbuildApi/saveLearnermap',
      learnermaps
    ).then(function (lmid) {
      app.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('map-save-' + type, {
            lmid: lmid
          });
        }
      })
      if (callback) callback(lmid)
    })
  }

  saveLearnermapDraft(gmid, uid, callback) {
    this.saveLearnermap(gmid, uid, 'draft', callback)
  }

  saveLearnermapFinalize(gmid, uid, callback) {
    this.saveLearnermap(gmid, uid, 'fix', callback)
  }

  saveLearnermapAuto(gmid, uid, callback) {
    this.saveLearnermap(gmid, uid, 'auto', callback)
  }

  // checkAvailableLearnermapDraft(gmid, rid) {
  //   let app = this;
  //   app.ajax.get(
  //     baseUrl + 'kitbuildApi/getLastDraftLearnermap/' + gmid + '/' + rid
  //   ).then(function (lmid) {
  //     if (lmid) $('#bt-load-draft').prop('disabled', false);
  //   })
  // }

  // loadSavedLearnermapDraft(gmid, rid, callback) { // console.log(mid, uid, rid);
  //   let app = this;
  //   app.ajax.get(
  //     baseUrl + 'kitbuildApi/loadLastDraftLearnermapCollab/' + gmid + "/" + rid
  //   ).then(function (draft) {
  //     if (draft == null) {
  //       app.gui.notify('Nothing to load. No previously saved draft maps found.')
  //       return;
  //     }
  //     let learnermap = draft.learnermap;
  //     let concepts = draft.concepts;
  //     let links = draft.links;
  //     let eles = [];
  //     concepts.forEach((c) => {
  //       eles.push({
  //         group: 'nodes',
  //         data: {
  //           id: 'c-' + c.cid,
  //           name: c.label,
  //           type: 'concept'
  //         },
  //         position: {
  //           x: parseInt(c.locx),
  //           y: parseInt(c.locy)
  //         }
  //       })
  //     })
  //     links.forEach((l) => {
  //       eles.push({
  //         group: 'nodes',
  //         data: {
  //           id: 'l-' + l.lid,
  //           name: l.label,
  //           type: 'link'
  //         },
  //         position: {
  //           x: parseInt(l.locx),
  //           y: parseInt(l.locy)
  //         }
  //       })
  //       if (l.source != null) {
  //         eles.push({
  //           group: 'edges',
  //           data: {
  //             source: 'l-' + l.lid,
  //             target: 'c-' + l.source,
  //             type: 'left'
  //           }
  //         })
  //       }
  //       if (l.target != null) {
  //         eles.push({
  //           group: 'edges',
  //           data: {
  //             source: 'l-' + l.lid,
  //             target: 'c-' + l.target,
  //             type: 'right'
  //           }
  //         })
  //       }
  //     })
  //     // let currentEles = app.canvas.getCy().nodes();
  //     // if (currentEles.length) {
  //     //   let confirm = app.gui.confirm('You currently have existing map data on canvas. Loading saved draft map data will <strong class="text-danger">REPLACE</strong> current map on canvas and across <strong class="text-danger">ALL users</strong> in current room.<br>Continue?', {
  //     //     'positive-callback': function () {
  //     //       confirm.modal('hide');
  //     //       app.canvas.clearCanvas();
  //     //       app.canvas.getCy().add(eles);
  //     //       app.gui.notify('Concept map loaded from last saved drafts.')
  //     //       app.kit.sendMap(eles);
  //     //       $('#bt-center').click();
  //     //     }
  //     //   })
  //     // } else {
  //     app.canvas.clearCanvas();
  //     app.canvas.getCy().add(eles);
  //     app.gui.notify('Concept map loaded from last saved drafts.')
  //     app.kit.sendMap(eles);
  //     app.kit.sendKit(app.goalmap)
  //     $('#bt-center').click();
  //     app.eventListeners.forEach(listener => {
  //       if (listener && typeof listener.onAppEvent == 'function') {
  //         listener.onAppEvent('load-last-draft', {
  //           lmid: parseInt(learnermap.lmid)
  //         });
  //       }
  //     })
  //     if (typeof callback == "function") callback();
  //     // }
  //   })
  // }

  loadKit(gmid, callback) {
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/openKit/' + gmid
    ).then(function (kit) { // console.log(kit);
      let goalmap = kit.goalmap;
      let concepts = kit.concepts;
      let links = kit.links;
      app.setGoalmap(goalmap.gmid, goalmap.name);
      app.session.set('gmid', app.goalmap.gmid);
      let nodes = [];
      concepts.forEach(c => {
        nodes.push({
          group: "nodes",
          data: {
            id: "c-" + c.cid,
            name: c.label,
            type: 'concept'
          },
          position: {
            x: parseInt(c.locx),
            y: parseInt(c.locy)
          }
        })
      })
      links.forEach(l => {
        nodes.push({
          group: "nodes",
          data: {
            id: "l-" + l.lid,
            name: l.label,
            type: 'link'
          },
          position: {
            x: parseInt(l.locx),
            y: parseInt(l.locy)
          }
        })
      })
      // console.log(nodes)
      app.getCanvas().getCy().elements().remove();
      app.getCanvas().getCy().add(nodes);
      app.getCanvas().getCy().layout({
        name: 'grid',
        fit: false,
        condense: true,
        stop: function () {
          let nodes = app.getCanvas().getCy().nodes().toArray(); // console.log(nodes);
          let eles = [];
          nodes.forEach(node => {
            eles.push({
              group: 'nodes',
              data: {
                id: node.id(),
                name: node.data('name'),
                type: node.data('type')
              },
              position: {
                x: node.position().x,
                y: node.position().y
              }
            })
          })
          //app.kit.sendKit(goalmap);
          //app.kit.sendMap(eles);
          $('#bt-center').click();
        }
      }).run();
      if (typeof callback == 'function') callback(kit);
    })
  }

  loadSavedLearnermap(lmid, callback) { // console.log(mid, uid, rid);
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/getLearnermap/' + lmid
    ).then(function (cmap) {
      if (cmap == null) {
        app.gui.notify('Error loading concept map.')
        return;
      }
      let goalmap = cmap.goalmap;
      let concepts = cmap.concepts;
      let links = cmap.links;
      let eles = [];
      concepts.forEach((c) => {
        app.canvas.getCy().nodes('#c-' + c.cid).position({
          x: parseInt(c.locx),
          y: parseInt(c.locy)
        })
        // eles.push({
        //   group: 'nodes',
        //   data: {
        //     id: 'c-' + c.cid,
        //     name: c.label,
        //     type: 'concept'
        //   },
        //   position: {
        //     x: parseInt(c.locx),
        //     y: parseInt(c.locy)
        //   }
        // })
      })
      links.forEach((l) => {
        app.canvas.getCy().nodes('#l-' + l.lid).position({
          x: parseInt(l.locx),
          y: parseInt(l.locy)
        })
        // eles.push({
        //   group: 'nodes',
        //   data: {
        //     id: 'l-' + l.lid,
        //     name: l.label,
        //     type: 'link'
        //   },
        //   position: {
        //     x: parseInt(l.locx),
        //     y: parseInt(l.locy)
        //   }
        // })
        if (l.source != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.source,
              type: 'left'
            }
          })
        }
        if (l.target != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.target,
              type: 'right'
            }
          })
        }
      })
      // app.canvas.clearCanvas();
      app.canvas.getCy().edges().remove();
      app.canvas.getCy().add(eles);
      app.canvas.getToolbar().getAction().clearStack();
      app.canvas.getCanvasTool().clear();
      app.gui.notify('Concept map loaded.')
      $('#bt-center').click();
      app.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('load-concept-map', {
            lmid: parseInt(cmap.lmid)
          });
        }
      })
      if (typeof callback == 'function') callback(cmap);
    })
  }

}

jQuery.fn.center = function (parent) {
  parent = parent ? $(parent) : window
  this.css({
    "position": "absolute",
    "top": ((($(parent).height() - this.outerHeight()) / 2) + $(parent).scrollTop() + "px"),
    "left": ((($(parent).width() - this.outerWidth()) / 2) + $(parent).scrollLeft() + "px")
  });
  return this;
}

var BRIDGE = {};

$(function () {

  var canvas = new Canvas('cy', {
    // isDirected: false,
    // enableToolbar: false,
    enableNodeCreation: false,
    enableConceptCreation: false,
    enableLinkCreation: false,
    // enableUndoRedo: false,
    // enableZoom: false,
    // enableAutoLayout: false,
    // enableSaveImage: false,
    enableClearCanvas: false,
    // enableNodeTools: false,
    enableNodeModificationTools: false,
    // enableConnectionTools: false,
  }).init();

  let app = new KitBuildApp(canvas); // the app
  let logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null);
  let eventListener = new EventListener(logger, canvas);

  canvas.attachEventListener(eventListener);
  app.attachEventListener(eventListener);

  BRIDGE.app = app;
  BRIDGE.logger = eventListener.getLogger();
  app.session.getAll(function(sess){
    app.user = sess.user ? sess.user : null;
    mid = sess.mid ? sess.mid : null;
    gmid = sess.gmid ? sess.gmid : null;
    logger.setGmid(gmid);
    app.loadMaterial(mid, function (material) {
      app.setMaterial(material.mid, material.name, material.content);
      app.loadKit(gmid, function (cmap) { // console.log(cmap);
        app.goalmap = cmap;
        app.setGoalmap(cmap.goalmap.gmid, cmap.goalmap.name); 
        app.autoSave();
        app.enableSaveDraft(true);
        app.enableFinalize(true);
      })
    });
  });
});