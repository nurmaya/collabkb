class Session {

  constructor(baseURL) {
    this.baseURL = baseURL;
    this.error = null;
    this.sessions = null;
  }

  getCookie() {
    return document.cookie;
  }

  set(sessionKey, sessionData, callback = null, errorCallback = null) {
    let session = this;
    $.ajax({
        url: this.baseURL + "session/set",
        data: {
          key: sessionKey,
          data: sessionData
        },
        method: 'post'
      })
      .done(function(response){ 
        if(typeof callback == 'function') callback(JSON.parse(response.result))
      })
      .fail(function (e) { 
        session.error = e;
        if (typeof errorCallback == 'function') errorCallback(session);
      });
  }

  setMulti(keyDataPairs, callback = null, errorCallback = null) {
    let session = this;
    $.ajax({
        url: this.baseURL + "session/set",
        data: keyDataPairs,
        method: 'post'
      })
      .done(function(response){ 
        if(typeof callback == 'function') callback(JSON.parse(response.result))
      })
      .fail(function (e) {
        session.error = e;
        if (typeof errorCallback == 'function') errorCallback(session);
      });
  }

  unset(sessionKey, callback = null, errorCallback = null) {
    let session = this;
    $.ajax({
        url: this.baseURL + "session/unset",
        data: {
          key: sessionKey
        },
        method: 'post'
      })
      .done(function(response){
        if(typeof callback == 'function') callback(JSON.parse(response))
      })
      .fail(function (e) {
        session.error = e;
        if (typeof errorCallback == 'function') errorCallback(session);
      });
  }

  get(sessionKey, callback = null, errorCallback = null) {
    let session = this;
    $.ajax({
        url: this.baseURL + "session/get",
        data: {
          key: sessionKey
        },
        method: 'post'
      })
      .done(function(response){ 
        if(typeof callback == 'function') callback(JSON.parse(response))
      })
      .fail(function (e) {
        session.error = e;
        if (typeof errorCallback == 'function') errorCallback(session);
      });
  }

  getAll(callback, errorCallback) {
    this.get(null, function(sessions){
      this.sessions = sessions;
      callback(sessions);
    }.bind(this), errorCallback);
  }

  destroy(callback = null, errorCallback = null) {
    let session = this;
    $.ajax({
        url: this.baseURL + "session/destroy",
        method: 'post'
      })
      .done(function(response){
        if(typeof callback == 'function') callback(JSON.parse(response))
      })
      .fail(function (e) {
        session.error = e;
        if (typeof errorCallback == 'function') errorCallback(session);
      });
  }

}