var BRIDGE = {};

$(function(){

  var canvas = new Canvas('cy', {
    // isDirected: false,
    // enableToolbar: false,
    // enableNodeCreation: false,
    // enableConceptCreation: false,
    // enableLinkCreation: false,
    enableUndoRedo: false,
    // enableZoom: false,
    enableAutoLayout: false,
    // enableSaveImage: false,
    enableClearCanvas: false
  }).init();

  let kitbuild = new CmapKitBuild(canvas); // the app
  let eventListener = new EventListener(new Logger());
  let collabChannel = new CollabChannel().init(canvas);
  let collabKit = new CollabKit();
  
  collabKit.attachGUI(kitbuild.getGUI());
  collabKit.attachChannelTool(collabChannel);

  canvas.attachEventListener(eventListener);
  kitbuild.attachKit(collabKit);
  kitbuild.getCanvas().attachTool(collabChannel);

  BRIDGE.kitbuild = kitbuild;
  BRIDGE.collabKit = collabKit;
  BRIDGE.logger = eventListener.getLogger();

});