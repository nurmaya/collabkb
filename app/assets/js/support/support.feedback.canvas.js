  /* Moved from kbui.canvas.js */
  
  /* Reflection Analysis */

  /* Deprecated */
  addProposition(subjectText, relationText, objectText, sentenceIndex) {
    // console.log(subjectText);
    // console.log(relationText);
    // console.log(objectText);
    // console.log($(this));
    // TODO: Add check whether kit to be added is already exists.

    // let sentenceIndex = $(this).data('sentence');

    let subjectNode = this.findNode(sentenceIndex, subjectText);
    let selfProposition = (objectText == subjectText);
    let objectNode = selfProposition ?
      subjectNode : this.findNode(sentenceIndex, objectText);
    let relationNode = this.findNode(
      sentenceIndex, relationText, subjectNode, objectNode
    );

    // console.log(subjectNode);
    // console.log(relationNode);
    // console.log(objectNode);

    if (subjectNode && relationNode && objectNode) {
      // TODO: Add check whether relation node is connected 
      // to subject and object node
      // console.log(relationNode.edges('[type="left"]').sourceEndPoint());
      // console.log(relationNode.edges('[type="right"]').targetEndPoint());
      // console.log(relationNode.connectedEdges('[type="right"]'));
      return;
    }

    let nodes = [];

    // let kitsToAdd = [];
    if (!subjectNode) {
      subjectNode = this.createNode({
        type: 'concept',
        proposition: 'subject',
        label: subjectText,
        sentence: [sentenceIndex]
      });
      nodes.push(subjectNode);
    }
    if (!relationNode) {
      relationNode = this.createNode({
        type: 'link',
        proposition: 'relation',
        label: relationText,
        sentence: [sentenceIndex]
      });
      nodes.push(relationNode);
    }
    if (!objectNode && !selfProposition) {
      objectNode = this.createNode({
        type: 'concept',
        proposition: 'object',
        label: objectText,
        sentence: [sentenceIndex]
      });
      nodes.push(objectNode);
    }

    let subject = subjectNode;
    let relation = relationNode;
    let object = selfProposition ? subjectNode : objectNode;

    // console.log(subject, relation, object);

    let collection = this.cy.collection();

    if (selfProposition) object = subject;

    // console.log(relation);

    let edges = [];
    edges.push(this.addEdge({
      group: "edges",
      data: {
        source: relation.data('id'),
        target: subject.data('id'),
        type: 'left'
      }
    }));
    edges.push(this.addEdge({
      group: "edges",
      data: {
        source: relation.data('id'),
        target: object.data('id'),
        type: 'right'
      }
    }));

    this.centerLinkPosition(relation);

    /* Preparing Undo and Log */

    let undoNodes = [];
    let undoEdges = [];

    for (let i = 0; i < nodes.length; i++) {
      let nodeData = nodes[i].json();
      delete nodeData.classes;
      delete nodeData.grabbable;
      delete nodeData.locked;
      delete nodeData.removed;
      delete nodeData.selectable;
      delete nodeData.selected;
      nodeData.position.x = parseInt(nodeData.position.x);
      nodeData.position.y = parseInt(nodeData.position.y);

      nodeData = Object.assign(nodeData, {
        type: 'concept',
        proposition: 'subject',
        label: subjectText,
        sentence: [sentenceIndex]
      });

      undoNodes.push(nodeData);
    }

    for (let i = 0; i < edges.length; i++) {
      let edgesData = edges[i].json();
      // console.log(edgesData);
      undoEdges.push({
        group: "edges",
        data: {
          id: edgesData.data.id,
          source: edgesData.data.source,
          target: edgesData.data.target,
          type: edgesData.data.type
        }
      });
    }

    // console.log(nodeData);
    this.toolbar.action.push(new Proposition(undoNodes, undoEdges));

    let edgesCollection = this.getCy().collection();
    let nodesCollection = this.getCy().collection();

    edges.forEach(e => {
      collection = collection.union(e);
      edgesCollection = edgesCollection.union(e);
    })
    nodes.forEach(n => {
      nodesCollection = nodesCollection.union(n);
      collection = collection.union(n);
    });

    return {
      nodes: nodesCollection,
      edges: edgesCollection,
      collection: collection
    }

  }

  getRootNodes() {
    let concepts = this.cy.nodes('[type="concept"]');
    let rootConcepts = [];
    for (let i = 0; i < concepts.length; i++) {
      // console.log(concepts[i].data('name'));
      let concept = concepts[i];
      let edges = concept.connectedEdges();
      let isRoot = true;
      // console.log(concept.data('name'));
      for (let j = 0; j < edges.length; j++) {
        let edge = edges[j];
        if (edge.data('type') == 'right') {
          isRoot = false;
          break;
        }
        // console.log(edge.data('type'));
      }
      if (isRoot) rootConcepts.push(concept);
    }

    // console.log(rootConcepts);
    // for(let i = 0; i < rootConcepts.length; i++) {
    //   let concept = rootConcepts[i];
    //   console.log(concept.data('name'));
    // }

    return rootConcepts;
  }

  getLongNodes() {
    let nodes = this.cy.nodes();
    let concepts = [];
    let links = [];
    for (let i = 0; i < nodes.length; i++) {
      let type = nodes[i].data('type');
      let label = nodes[i].data('name');
      let tokens = label ? label.split(/(\s+)/) : [];
      for (let x = 0; x < tokens.length; x++) {
        if (tokens[x] == ' ') {
          tokens.splice(x, 1);
        }
      }
      if (tokens.length > 6) {
        switch (type) {
          case 'concept':
            // console.log(tokens);
            // console.log("concept:", nodes[i].data('name'));
            concepts.push(nodes[i]);
            break;
          case 'link':
            // console.log(tokens);
            // console.log("link:", nodes[i].data('name'));
            links.push(nodes[i]);
            break;
        }
      }
    }
    // console.log(concepts, links);
    return {
      concepts: concepts,
      links: links
    }
  }

  getClusters(options) {
    let defaults = {
      colorize: true
    }

    let settings = Object.assign({}, defaults, options);

    let cluster = 0;
    let clusters = [];

    do {

      let unvisitedNodes = this.cy.nodes('[!visited]');
      if (unvisitedNodes.length == 0) break;

      let randomIndex = Math.floor(Math.random() * Math.floor(unvisitedNodes.length));
      let concepts = [];
      let links = [];
      let propositions = [];


      cluster++;
      let bgIndex = cluster % 5;
      let canvas = this;

      this.cy.elements().bfs({
        roots: unvisitedNodes[randomIndex],
        visit: function (v, e, u, i, depth) {
          v.data('visited', true);
          v.data('cluster', cluster);
          v.data('color', canvas.colors[bgIndex]);
          v.data('class', 'c' + (bgIndex + 1));
          if (v.data('type') == 'concept') {
            // v.classes('c' + (bgIndex + 1));
            concepts.push(v);
            if (settings.colorize) {
              // // v.style({ 'background-color': canvas.colors[bgIndex] });
              // v.addClass('c' + (bgIndex + 1));
              // console.log(v.hasClass('c2'));
              // console.log(v.data('name'));
            }
            v.data('o', v.connectedEdges('[type="left"]').length);
            v.data('i', v.connectedEdges('[type="right"]').length);
          } else if (v.data('type') == 'link') {
            links.push(v);
            let lEdge = v.connectedEdges('[type="left"]')[0];
            let rEdge = v.connectedEdges('[type="right"]')[0];
            propositions.push({
              subject: lEdge ? lEdge.target() : null,
              relation: v,
              object: rEdge ? rEdge.target() : null
            });
          }
        }
      });

      let edges = this.cy.nodes('[cluster=' + cluster + ']').connectedEdges();
      // this.cy.nodes('[cluster=' + cluster + ']').classes('c3');

      if (concepts.length > 0) {

        let roots = [];
        for (let i = 0; i < concepts.length; i++) {
          let concept = concepts[i];
          let edges = concept.connectedEdges();
          // console.log(concept.data('name'));
          let isRoot = true;
          for (let j = 0; j < edges.length; j++) {
            let edge = edges[j];
            // console.log(edge.data('type'));
            if (edge.data('type') == 'right') {
              isRoot = false;
              break;
            }
          }
          if (isRoot) roots.push(concept);
        }

        if (roots.length == 0) roots.push(concepts[0]);
        // console.log(concepts);
        // for (let i = 0; i < concepts.length; i++){
        //   concepts[i].classes('c3');
        // }
        // console.log(JSON.stringify(this.cy.json()));
        // this.cy.nodes().addClass('c2');

        clusters.push({
          id: cluster,
          concepts: concepts,
          links: links,
          color: this.colors[bgIndex],
          propositions: propositions,
          edges: edges,
          roots: roots
        });
      }

    } while (this.cy.nodes('[!visited]').length > 0 && cluster < 100)

    this.cy.nodes().removeData('visited');

    // kb._analysis.numClusters = cluster;
    return clusters;
  }

  getUnconnectedNodes() {
    let nodes = this.cy.nodes();
    let unconnectedConcepts = [];
    let unconnectedLinks = [];
    for (let i = 0; i < nodes.length; i++) {
      // console.log(concepts[i].data('name'));
      let node = nodes[i];
      let edges = node.connectedEdges();
      switch (node.data('type')) {
        case 'concept':
          if (edges.length < 1) unconnectedConcepts.push(node);
          break;
        case 'link':
          if (edges.length < 2) unconnectedLinks.push(node);
          break;
      }
    }
    return {
      concepts: unconnectedConcepts,
      links: unconnectedLinks
    };
  }

  getDuplicatePropositions() {
    let links = this.cy.nodes('[type="link"]');
    let duplicatePropositions = [];
    // console.log(links);
    for (let i = 0; i < links.length; i++) {
      let linkA = links[i];
      let edgesAL = linkA.connectedEdges('[type="left"]');
      let edgesAR = linkA.connectedEdges('[type="right"]');
      let leftNodeA = edgesAL.target(); //(edgesAL.connectedNodes());
      let rightNodeA = edgesAR.target(); //(edgesAR.connectedNodes());
      // console.log(leftNodeA, rightNodeA);
      // console.log(leftNodeA.data('id'), linkA.data('name'), rightNodeA.data('id'));
      for (let j = i + 1; j < links.length; j++) {
        let linkB = links[j];
        let edgesBL = linkB.connectedEdges('[type="left"]');
        let edgesBR = linkB.connectedEdges('[type="right"]');
        let leftNodeB = edgesBL.target();
        let rightNodeB = edgesBR.target();
        if (!leftNodeA || !leftNodeB || !rightNodeA || !rightNodeB)
          continue;
        if (leftNodeA.data('id') == leftNodeB.data('id') &&
          rightNodeA.data('id') == rightNodeB.data('id') &&
          linkA.data('name').toString().trim() == linkB.data('name').toString().trim()) {
          // console.log(leftNodeA.data('name'), linkA.data('name'), rightNodeA.data('name'));
          duplicatePropositions.push({
            a: {
              s: leftNodeA,
              l: linkA,
              o: rightNodeA
            },
            b: {
              s: leftNodeB,
              l: linkB,
              o: rightNodeB
            }
          });
        }
        // console.log(leftNodeB, rightNodeB);
      }
    }
    return duplicatePropositions;
  }

  getSimilarConcepts(nlp, idfs, documents) {

    let queries = [];
    let nodes = this.cy.nodes('[type="concept"]');
    // console.log(nlp);
    let tokens = [];
    for (let i = 0; i < nlp.sentences.length; i++) {
      let sent = nlp.sentences[i];
      tokens = tokens.concat(sent.tokens);
    }
    // console.log(tokens);
    for (let i = 0; i < nodes.length; i++) {
      let label = nodes[i].data('name');
      let lemmas = Nlp.getLemmasFromTokens(label, tokens);
      // console.log(lemmas);
      // let words = label.split(/(\s+)/);
      let query = {
        originalText: label, // triple.subject,
        text: lemmas.join(" "),
        lemmas: lemmas, // subjectLemmas,
        sentenceIndex: 0 //i
      }
      Cosim.calculateTfIdf(query, idfs, documents.length);
      queries.push(query);
    }
    // console.log(queries);

    let similarityPairs = [];
    for (let i = 0; i < queries.length; i++) {
      let a = queries[i];
      for (let j = i + 1; j < queries.length; j++) {
        let b = queries[j];
        let sim = Cosim.cosineSimilarity(a, b);
        let simr = Cosim.cosineSimilarity(b, a);
        let sima = (sim + simr) / 2;
        if (sima > 0.85) {
          similarityPairs.push({
            a: a,
            b: b,
            sim: sim,
            simr: simr,
            sima: sima
          });
          // console.log(i, j, a.originalText, " vs ", b.originalText, "sim", sim);
        }
      }
    }

    return similarityPairs;

  }