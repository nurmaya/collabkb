class Reflection {

  constructor(nlp) {
    this.nlp = nlp;
    this.tokens = [];
    this.idfs = {};
    this.documents = [];
  }

  initializeSentences() {
    this.sentences = this.nlp.sentences;
    // list of concept from all sentences and empty it!
    this.documents = [];
    // this.keywords = []; // empty bag of keywords!
    for (let i in this.sentences) {
      // console.log(this.sentences[i]);
      let sent = this.sentences[i];
      // sent.read = false;
      let triples = sent.openie;
      // this.keywords.push(...sent.keywords);

      for (let t in sent.tokens) {
        let token = sent.tokens[t];
        token.sentenceIndex = i;
        this.tokens.push(token);
      }

      for (let j in triples) {
        let triple = triples[j];

        // Process subject's lemmas
        let subjectLemmas = [];
        let subjectWords = triple.subject.toLowerCase().split(" ");
        for (let k = triple.subjectSpan[0]; k < triple.subjectSpan[1]; k++) {
          if (CDM.stopLists.includes(sent.tokens[k].lemma.toLowerCase()))
            continue;
          if (subjectWords.includes(sent.tokens[k].word.toLowerCase()))
            subjectLemmas.push(sent.tokens[k].lemma.toLowerCase());
        }
        this.documents.push({
          originalText: triple.subject,
          text: subjectLemmas.join(" "),
          lemmas: subjectLemmas,
          sentenceIndex: i
        });

        // Process object's lemmas
        let objectLemmas = [];
        let objectWords = triple.object.toLowerCase().split(" ");
        for (let k = triple.objectSpan[0]; k < triple.objectSpan[1]; k++) {
          if (CDM.stopLists.includes(sent.tokens[k].lemma.toLowerCase()))
            continue;
          if (objectWords.includes(sent.tokens[k].word.toLowerCase()))
            objectLemmas.push(sent.tokens[k].lemma.toLowerCase());
        }
        this.documents.push({
          originalText: triple.object,
          text: objectLemmas.join(" "),
          lemmas: objectLemmas,
          sentenceIndex: i
        });
      } // end triples loop
    } // end sentences loop
    // console.log(this.documents);
    // for(let i in this.documents) {
    //   if(this.documents[i].originalText == 'located')
    //     console.log(this.documents[i]);
    // }

    // let queries = [];
    // for (let i in this.keywords) {

    //     let keyword = this.keywords[i];
    //     keyword.lemmas = [];
    //     for (let j in keyword.words) {
    //         // skip word which is in stopwords list
    //         if (this.stopwords.includes(keyword.words[j].lemma))
    //             continue;
    //         keyword.lemmas.push(keyword.words[j].lemma);
    //     }
    //     // change text value to joined lemmas  
    //     keyword.text = keyword.lemmas.join(" ");

    //     // add keyword to list of this.documents
    //     this.documents.push(keyword);

    //     // // add keyword to list of keywords
    //     queries.push(keyword);
    // }


    // extract words from this.documents
    let uwords = [];
    for (let i in this.documents) uwords.push(...this.documents[i].lemmas);

    // remove all duplicate words
    uwords = Array.from(new Set(uwords)); // console.log(uwords);

    // calculate idfs for all (unique) words in all this.documents
    this.idfs = Cosim.getIdfs(uwords, this.documents);
    //console.log(this.idfs);
    let numDocuments = this.documents.length;

    // Calculate all tf and tf-idf on all documents
    // Including keywords (queries)
    for (let i in this.documents) {
      this.documents[i].id = i;
      Cosim.calculateTf(this.documents[i]);
      Cosim.calculateIdf(this.documents[i], this.idfs, numDocuments);
    }

    // Calculate similarity with other documents
    // for (let i in this.documents) {
    //     let doc = this.documents[i];
    //     doc.sims = [];
    //     for (let j in queries) {
    //         let que = queries[j];
    //         // Only calculate documents similarity 
    //         // which is in the same sentence as keywords (queries)
    //         if (que.sentenceIndex != doc.sentenceIndex) continue;

    //         let similarity = Cosim.cosineSimilarity(que, doc);
    //         if (similarity > 0) {
    //             doc.sims.push({
    //                 query: que,
    //                 sim: similarity
    //             });
    //         }
    //     }
    // }

    // console.log(this.keywords);
    // console.log(this.documents);

    // console.log(this.keywords);
  }

  analyze(canvas) {

    let clusters = canvas.getClusters(); // musti duluan, coloring
    let rootConcepts = canvas.getRootNodes();
    let longNodes = canvas.getLongNodes();
    let unconnectedNodes = canvas.getUnconnectedNodes();
    let duplicatePropositions = canvas.getDuplicatePropositions();
    let similarConcepts = canvas.getSimilarConcepts(this.nlp, this.idfs, this.documents);

    return {
      clusters: clusters,
      rootConcepts: rootConcepts,
      longNodes: longNodes,
      unconnectedNodes: unconnectedNodes,
      duplicatePropositions: duplicatePropositions,
      similarConcepts: similarConcepts
    }
    
  }
}