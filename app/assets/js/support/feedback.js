class Feedback {

  constructor() {
    // let modal = e.target;
    // if (typeof Logger != 'undefined') Logger.log('show-reflection');
    // if (wiz.nlp) wiz.analyzeConceptMap(wiz.nlp, modal, canvas);
    // else {
    // 	$.ajax({
    // 		url: baseUrl + 'kitBuildApi/getReading/' + Canvas.meta.mid,
    // 		method: 'get'
    // 	}).done(function (r) {
    // 		// console.log(r);
    // 		Canvas.meta.mid = r.mid;
    // 		wiz.material.mid = r.mid;
    // 		wiz.analyzeConceptMap(r.nlp, modal, canvas);
    // 	}).fail(function (e) {
    // 		console.error(e);
    // 	});
    // }
    this.reflection = null;

  }

  attachLogger(logger) {
    this.logger = logger;
    return this;
  }

  setNlp(nlp) {
    this.nlp = nlp;
  }

  analyze(canvas) { // console.log(this.nlp);
    try {
      this.reflection = new Reflection(JSON.parse(this.nlp));
      this.reflection.initializeSentences();
      // console.log(this.reflection);
      let results = this.reflection.analyze(canvas);
      this.showResults(
        results.clusters, results.rootConcepts,
        results.longNodes, results.unconnectedNodes,
        results.duplicatePropositions, results.similarConcepts
      )
      return true;
    } catch (err) {
      console.log(err);
      return null;
    }
  }

  showResults(clusters, rootConcepts,
    longNodes, unconnectedNodes,
    duplicatePropositions, similarConcepts) {

    let logger = this.logger;

    let rMultiCluster = $('#reflect-multi-cluster');
    let rUnconnectedNodes = $('#reflect-unconnected-nodes');
    let rLongNodeLabel = $('#reflect-long-node-label');
    let rMultiRoot = $('#reflect-multi-root');
    let rSimilarConcept = $('#reflect-similar-concept');
    let rDuplicatePropositions = $('#reflect-duplicate-propositions');

    rMultiCluster.hide();
    rUnconnectedNodes.hide();
    rLongNodeLabel.hide();
    rMultiRoot.hide();
    rSimilarConcept.hide();
    rDuplicatePropositions.hide();

    let content = '';
    let feedbackCount = 0;

    if (unconnectedNodes.concepts.length || unconnectedNodes.links.length) {
      rUnconnectedNodes.show();
      feedbackCount++;
      if (unconnectedNodes.concepts.length) content += '<div>Nodes: ';
      for (let i = 0; i < unconnectedNodes.concepts.length; i++) {
        let concept = unconnectedNodes.concepts[i];
        content += '<li class="badge badge-warning reflection-node reflection-concept">' + concept.data('name') + '</li>';
      }
      if (unconnectedNodes.concepts.length) content += '</div>';
      if (unconnectedNodes.links.length) content += '<div>Links: ';
      for (let i = 0; i < unconnectedNodes.links.length; i++) {
        let link = unconnectedNodes.links[i];
        content += '<li class="badge badge-secondary reflection-node reflection-link">' + link.data('name') + '</li>';
      }
      if (unconnectedNodes.links.length) content += '</div>';
      rUnconnectedNodes.find('.reflection-concept-list').html(content);
    } else {
      rUnconnectedNodes.hide();
    }


    if (longNodes.concepts.length || longNodes.links.length) {
      rLongNodeLabel.show();
      feedbackCount++;
      content = '';
      if (longNodes.concepts.length) content += '<div>Nodes: ';
      for (let i = 0; i < longNodes.concepts.length; i++) {
        let concept = longNodes.concepts[i];
        content += '<li class="badge badge-warning reflection-node reflection-concept">' + concept.data('name') + '</li>';
      }
      if (longNodes.concepts.length) content += '</div>';
      if (longNodes.links.length) content += '<div>Links: ';
      for (let i = 0; i < longNodes.links.length; i++) {
        let link = longNodes.links[i];
        content += '<li class="badge badge-secondary reflection-node reflection-link">' + link.data('name') + '</li>';
      }
      if (longNodes.links.length) content += '</div>';
      rLongNodeLabel.find('.reflection-concept-list').html(content);
    } else {
      rLongNodeLabel.hide();
    }


    if (rootConcepts.length > 1) {
      rMultiRoot.show();
      feedbackCount++;
      content = '';
      if (rootConcepts.length) content += '<div>Nodes: ';
      for (let i = 0; i < rootConcepts.length; i++) {
        let concept = rootConcepts[i];
        content += '<li class="badge badge-warning reflection-node reflection-concept">' + concept.data('name') + '</li>';
      }
      if (rootConcepts.length) content += '</div>';
      rMultiRoot.find('.reflection-concept-list').html(content);
    } else rMultiRoot.hide();

    if (clusters.length > 1) {
      rMultiCluster.show();
      feedbackCount++;
      content = '';
      rMultiCluster.find('.reflection-concept-list').html(content);
      for (let i = 0; i < clusters.length; i++) {
        let cluster = clusters[i];
        let concepts = cluster.roots;
        for (let j = 0; j < concepts.length; j++) {
          let concept = concepts[j];
          content += '<li class="badge badge-warning reflection-node reflection-concept" style="background-color:' + concept.data('color') + '">' + concept.data('name') + '</li>';
        }
      }
      rMultiCluster.find('.reflection-concept-list').html(content);
    } else rMultiCluster.hide();

    if (duplicatePropositions.length > 0) {
      rDuplicatePropositions.show();
      feedbackCount++;
      content = '';
      for (let i = 0; i < duplicatePropositions.length; i++) {
        let proposition = duplicatePropositions[i];
        content += '<li><span class="badge badge-warning">' + proposition.a.s.data('name') + '</span> - <span class="badge badge-secondary">' + proposition.a.l.data('name') + '</span> - <span class="badge badge-warning">' + proposition.a.o.data('name') + '</span></li>';
      }
      rDuplicatePropositions.find('.reflection-concept-list').html(content);
    } else rDuplicatePropositions.hide();

    if (similarConcepts.length > 0) {
      rSimilarConcept.show();
      feedbackCount++;
      content = '';
      for (let i = 0; i < similarConcepts.length; i++) {
        let similarConcept = similarConcepts[i];
        let a = similarConcept.a;
        let b = similarConcept.b;
        let sim = similarConcept.sim * 100;
        let simr = similarConcept.simr * 100;
        let sima = similarConcept.sima * 100;
        // console.log(a, b);
        content += '<div>';
        content += '<span class="badge badge-warning reflection-node reflection-concept">' + a.originalText + '</span>';
        content += '<span class="text-sm"> &times; </span>';
        content += '<span class="badge badge-info reflection-node reflection-concept">' + b.originalText + '</span>';
        content += '<button class="badge badge-primary reflection-node reflection-concept reflection-similarity" data-tippy-content="Similarity ' + sim.toFixed(0) + '%-' + simr.toFixed(0) + '%">' + sima.toFixed(0) + '%</button>';
        // content += '<button class="badge badge-secondary reflection-node reflection-concept reflection-similarity" data-tippy-content="Similarity">' + sim.toFixed(0) + '%</button>';
        // content += '<button class="badge badge-secondary reflection-node reflection-concept reflection-similarity" data-tippy-content="Similarity">' + simr.toFixed(0) + '%</button>';
        content += '</div>';
      }
      rSimilarConcept.find('.reflection-concept-list').html(content);
      let ts = tippy('.reflection-similarity', {
        arrow: true,
        arrowType: 'round', // or 'sharp' (default)
        animation: 'fade',
      });
    } else rSimilarConcept.hide();

    if (feedbackCount == 0) {
      $('#reflect-ok').show();
      $('#reflect-ok').css('border', 'none');

      // if (typeof Logger != 'undefined') Logger.log('show-reflection', {
      //   m: 'no error'
      // });
      if (logger != null) logger.log('show-reflection', "OK");
    } else {
      $('#reflect-ok').hide();
      // if (typeof Logger != 'undefined') Logger.log('show-reflection', {
      //   'lg-c': longNodes.concepts.length,
      //   'lg-l': longNodes.links.length,
      //   'clus': clusters.length,
      //   'root': rootConcepts.length,
      //   'un-c': unconnectedNodes.concepts.length,
      //   'un-l': unconnectedNodes.links.length,
      //   'simi': similarConcepts.length,
      //   'dupp': duplicatePropositions.length
      // });

      if (logger != null) logger.log('show-reflection', {
        'lg-c': longNodes.concepts.length,
        'lg-l': longNodes.links.length,
        'clus': clusters.length,
        'root': rootConcepts.length,
        'un-c': unconnectedNodes.concepts.length,
        'un-l': unconnectedNodes.links.length,
        'simi': similarConcepts.length,
        'dupp': duplicatePropositions.length
      });

      let refls = $('.reflection:visible');
      let lastRef = refls[refls.length - 1];
      $(lastRef).css('border', 'none'); 
      // console.log(refls.length);
    }
  }

}