class Support {
  constructor(kitbuild) {

    this.kitbuild = kitbuild;
    this.logger = kitbuild.getLogger();
    console.log(this.logger);

    this.nlp = null;
    this.sentences = null;
    this.sentenceIndex = 0;

    this.stopwords = ["a", "about", "above", "above", "across", "after", "afterwards",
      "again", "against", "all", "almost", "alone", "along", "already", "also",
      "although", "always", "am", "among", "amongst", "amoungst", "amount", "an",
      "and", "another", "any", "anyhow", "anyone", "anything", "anyway", "anywhere", "are",
      "around", "as", "at", "back", "be", "became", "because", "become", "becomes",
      "becoming", "been", "before", "beforehand", "behind", "being", "below", "beside",
      "besides", "between", "beyond", "bill", "both", "bottom", "but", "by", "call", "can",
      "cannot", "cant", "co", "con", "could", "couldnt", "cry", "de", "describe", "detail",
      "do", "done", "down", "due", "during", "each", "eg", "eight", "either", "eleven",
      "else", "elsewhere", "empty", "enough", "etc", "even", "ever", "every", "everyone",
      "everything", "everywhere", "except", "few", "fifteen", "fify", "fill", "find",
      "fire", "first", "five", "for", "former", "formerly", "forty", "found", "four",
      "from", "front", "full", "further", "get", "give", "go", "had", "has", "hasnt", "have",
      "he", "hence", "her", "here", "hereafter", "hereby", "herein", "hereupon", "hers",
      "herself", "him", "himself", "his", "how", "however", "hundred", "ie", "if", "in",
      "inc", "indeed", "interest", "into", "is", "it", "its", "itself", "keep", "last",
      "latter", "latterly", "least", "less", "ltd", "made", "many", "may", "me", "meanwhile",
      "might", "mill", "mine", "more", "moreover", "most", "mostly", "move", "much", "must",
      "my", "myself", "name", "namely", "neither", "never", "nevertheless", "next", "nine",
      "no", "nobody", "none", "noone", "nor", "not", "nothing", "now", "nowhere", "of",
      "off", "often", "on", "once", "one", "only", "onto", "or", "other", "others",
      "otherwise", "our", "ours", "ourselves", "out", "over", "own", "part", "per",
      "perhaps", "please", "put", "rather", "re", "same", "see", "seem", "seemed", "seeming",
      "seems", "serious", "several", "she", "should", "show", "side", "since", "sincere",
      "six", "sixty", "so", "some", "somehow", "someone", "something", "sometime", "sometimes",
      "somewhere", "still", "such", "system", "take", "ten", "than", "that", "the", "their",
      "them", "themselves", "then", "thence", "there", "thereafter", "thereby", "therefore",
      "therein", "thereupon", "these", "they", "thickv", "thin", "third", "this", "those",
      "though", "three", "through", "throughout", "thru", "thus", "to", "together", "too",
      "top", "toward", "towards", "twelve", "twenty", "two", "un", "under", "until", "up",
      "upon", "us", "very", "via", "was", "we", "well", "were", "what", "whatever", "when",
      "whence", "whenever", "where", "whereafter", "whereas", "whereby", "wherein",
      "whereupon", "wherever", "whether", "which", "while", "whither", "who", "whoever",
      "whole", "whom", "whose", "why", "will", "with", "within", "without", "would", "yet",
      "you", "your", "yours", "yourself", "yourselves", "the", "-lrb-", "-rrb-"
    ];
    this.documents = [];
    this.keywords = [];
    this.tokens = [];

    this.handleEvent();

  }

  setNlp(nlp) {
    try {
      this.nlp = null;
      this.sentences = null;
      this.sentenceIndex = 0;
      this.documents = [];
      this.keywords = [];
      this.tokens = [];
      this.nlp = JSON.parse(nlp);
      this.sentences = null;
    } catch (err) {
      this.nlp = null;
      console.log(err);
    }
  }

  getNlp() {
    return this.nlp;
  }

  getSentences() {
    return this.sentences;
  }

  getSentenceAt(index) {
    if (this.sentences != null)
      return this.sentences[index];
  }

  handleEvent() { // console.log('support event handled');

    let support = this;
    let ui = this.kitbuild.getUI();
    let canvas = this.kitbuild.getCanvas();
    let logger = this.kitbuild.getLogger();

    $('#bt-sentences-list').off('click').on('click', function () {
      support.showSentences();
    });

    $('#bt-manual-proposition').off('click').on('click', function () {
      support.showManualProposition();
    });

    $('#bt-add-manual-proposition').off('click').on('click', function (e) {

      e.preventDefault();

      let subject = $('#exp-manual-konsep-a').val().toString().trim();
      let relation = $('#exp-manual-link').val().toString().trim();
      let object = $('#exp-manual-konsep-b').val().toString().trim();

      if (subject == '') {
        $('#exp-manual-konsep-a').addClass('is-invalid');
      } else $('#exp-manual-konsep-a').removeClass('is-invalid');
      if (object == '') {
        $('#exp-manual-konsep-b').addClass('is-invalid');
      } else $('#exp-manual-konsep-b').removeClass('is-invalid');
      if (relation == '') {
        $('#exp-manual-link').addClass('is-invalid');
      } else $('#exp-manual-link').removeClass('is-invalid');

      if (subject == '' || object == '' || relation == '') {
        ui.getGui().notify('First concept, second concept, and link may not be empty', {
          type: 'warning'
        });
        return false;
      }

      let result = support.addProposition(subject, relation, object, 0);
      if (!result) {
        ui.getGui().notify('Proposition <strong>' + subject + ' - ' + relation + ' - ' + object + '</strong> already exist.', {
          type: 'warning'
        });
      } else { // console.log(result);
        // console.log(logger);
        if(logger != null) logger.log('add-manual-proposition', {
          subject: subject,
          relation: relation,
          object: object
        });
        $('#modal-proposition').modal('hide');
        canvas.centerCamera();
      }

      // if (typeof Logger != 'undefined') Logger.log('add-manual-proposition', {
      //   s: subject, r: relation, o: object
      // });

    });

    $('#exp-collab-sentence-selection').on('click', '.nav-next', function () {
      if (support.sentenceIndex < support.sentences.length - 1) {
        support.sentenceIndex++;
        support.goToSentence(support.sentenceIndex);
        if(logger != null) logger.log('sentence-select', support.sentenceIndex);
        // if (typeof Logger != 'undefined') Logger.log('click-bt-next-sentence', {
        //   sidx: support.sentenceIndex
        // });
      } else ui.getGui().notify('You have reached the end of the reading.', {
        type: 'info'
      });
    });

    $('#exp-collab-sentence-selection').on('click', '.nav-prev', function () {
      if (support.sentenceIndex > 0) {
        support.sentenceIndex--;
        support.goToSentence(support.sentenceIndex);
        if(logger != null) logger.log('sentence-select', support.sentenceIndex);
        // if (typeof Logger != 'undefined') Logger.log('click-bt-prev-sentence', {
        //   sidx: support.sentenceIndex
        // });
      } else ui.getGui().notify('You have reached the beginning of the reading.', {
        type: 'info'
      });
    });

    $('#exp-keyword').on('click', '.bt-add-keyword-as-concept', function (e) {

      e.preventDefault();
      // TODO: Add check whether kit to be added is already exists.
      let existsNode = canvas.findNode(null, $(this).data('label'));
      if (existsNode != null) {
        ui.getGui().notify('Keyword <strong>' + $(this).data('label') + '</strong> is already exists on the map.', {
          type: 'warning'
        });
        return;
      }

      let node = canvas.createNode({
        type: $(this).data('type'),
        label: $(this).data('label')
      });
      ui.getGui().notify('Keyword <strong>' + $(this).data('label') + '</strong> added to the map as concept', {
        type: 'success'
      });

      // /* Preparing Undo and Log */

      let nodeData = node.json();
      delete nodeData.classes;
      delete nodeData.grabbable;
      delete nodeData.locked;
      delete nodeData.removed;
      delete nodeData.selectable;
      delete nodeData.selected;
      nodeData.position.x = parseInt(nodeData.position.x);
      nodeData.position.y = parseInt(nodeData.position.y);
      // console.log(nodeData);
      canvas.getToolbar().action.push(new Create(nodeData));

      if(logger != null) logger.log('add-keyword-as-concept', {
        mid: support.mid,
        type: $(this).data('type'),
        label: $(this).data('label')
      });

      // if (typeof Logger != 'undefined') Logger.log("add-keyword-to-map", {
      //   mid: support.mid,
      //   type: $(this).data('type'),
      //   label: $(this).data('label')
      // });

    });

    $('#exp-collab').on('click', '.bt-add-proposition', function (e) {
      e.preventDefault();
      let sentenceIndex = $(this).data('sentence');

      // let subject = $(this).data('subject');
      // let object = $(this).data('object');
      let relation = $(this).data('relation');
      let ttype = $(this).data('ttype');
      // console.log($(this).siblings('.prop-group'));
      let s = $(this).siblings('.prop-group').find('.concept-subject')[0];
      let o = $(this).siblings('.prop-group').find('.concept-object')[0];
      let subject = $(s).html();
      let object = $(o).html();
      // console.log($(s).html(), relation, $(o).html());

      let results =
        support.addProposition(subject, relation, object, sentenceIndex);
      if (results != null) {
        canvas.getCy().nodes().lock();
        results.nodes.unlock();
        canvas.getCy().nodes().layout({
          name: 'cose',
          fit: false
        }).on('layoutstop', function () {
          canvas.getCy().nodes().unlock();
          canvas.getCy().animate({
            center: {
              eles: results.nodes
            },
            duration: 300
          });
        }).run();
        // console.log(results.collection);

        // canvas.getCy().center(results.collection);
        // cy.animate({ fit: { eles: cy, padding: 30 } });
        // if (typeof Logger != 'undefined') Logger.log("add-proposition-" + ttype, {
        //   mid: support.mid,
        //   subject: subject,
        //   relation: relation,
        //   object: object,
        //   sentenceIndex: sentenceIndex,
        //   ttype: ttype
        // });

        if(logger != null) logger.log("add-proposition-" + ttype, {
          mid: support.mid,
          subject: subject,
          relation: relation,
          object: object,
          sentenceIndex: sentenceIndex,
          ttype: ttype
        });

      } else {
        ui.getGui().notify('Proposition <strong>"' + subject + '" - "' + relation + '" - "' + object + '"</strong> is already exists', {
          type: 'warning'
        });
      }

    });

    $('#exp-collab').on('click', '.bt-edit-proposition', function () {
      let propGroup = $(this).siblings('.prop-group');
      let conceptA = $(propGroup.find('.concept-subject')[0]).html().toString().trim();
      let conceptB = $(propGroup.find('.concept-object')[0]).html().toString().trim();
      let relation = $(propGroup.find('.relation')[0]).html().toString().trim();

      let modal = support.showManualProposition({
        subject: conceptA,
        relation: relation,
        object: conceptB
      });

      // // console.log(propGroup, conceptA, relation, conceptB);

      // $('#exp-manual-konsep-a').val(conceptA);
      // $('#exp-manual-konsep-b').val(conceptB);
      // $('#exp-manual-link').val(relation);

      // if (typeof Logger != 'undefined') Logger.log('edit-proposition', {
      //   ca: conceptA,
      //   cb: conceptB,
      //   rel: relation
      // });

      if(logger != null) logger.log('edit-proposition', {
        ca: conceptA,
        cb: conceptB,
        rel: relation
      });

      // // var elm = $('#form-exp-proposition-manual');
      // // console.log(elm);
      // // var newone = elm[0].cloneNode(true);
      // // elm[0].parentNode.replaceChild(newone, elm[0]);

      // // $(newone)
      // $('#form-exp-proposition-manual')
      //   .css({ 'background-color': '#ffc107' })
      //   .animate({ 'background-color': 'transparent' }, 150, function () {
      //     $(this).css({ 'background-color': '#ffc107' })
      //       .animate({ 'background-color': 'transparent' }, 500);
      //   });

    });

    $('#exp-collab').on('click', '#bt-show-hide-more-suggestions', function () {
      // if (typeof Logger != 'undefined') Logger.log('toggle-more-suggestions');
      if (this.logger != null) logger.log('toggle-more-suggestions');
    });
  }

  postProcessSentence() {
    if (this.nlp != null)
      this.sentences = Nlp.postProcess(this.nlp.sentences);
  }

  extractKeywords() {
    //this.sentences = sentences;
    this.documents = []; // list of concept from all sentences and empty it!
    this.keywords = []; // empty bag of keywords!
    for (let i in this.sentences) {
      // console.log(this.sentences[i]);
      let sent = this.sentences[i];
      // sent.read = false;
      let triples = sent.openie;
      this.keywords.push(...sent.keywords);

      for (let t in sent.tokens) {
        let token = sent.tokens[t];
        token.sentenceIndex = i;
        this.tokens.push(token);
      }

      for (let j in triples) {
        let triple = triples[j];

        // Process subject's lemmas
        let subjectLemmas = [];
        let subjectWords = triple.subject.toLowerCase().split(" ");
        for (let k = triple.subjectSpan[0]; k < triple.subjectSpan[1]; k++) {
          if (this.stopwords.includes(sent.tokens[k].lemma.toLowerCase()))
            continue;
          if (subjectWords.includes(sent.tokens[k].word.toLowerCase()))
            subjectLemmas.push(sent.tokens[k].lemma.toLowerCase());
        }
        this.documents.push({
          originalText: triple.subject,
          text: subjectLemmas.join(" "),
          lemmas: subjectLemmas,
          sentenceIndex: i
        });

        // Process object's lemmas
        let objectLemmas = [];
        let objectWords = triple.object.toLowerCase().split(" ");
        for (let k = triple.objectSpan[0]; k < triple.objectSpan[1]; k++) {
          if (this.stopwords.includes(sent.tokens[k].lemma.toLowerCase()))
            continue;
          if (objectWords.includes(sent.tokens[k].word.toLowerCase()))
            objectLemmas.push(sent.tokens[k].lemma.toLowerCase());
        }
        this.documents.push({
          originalText: triple.object,
          text: objectLemmas.join(" "),
          lemmas: objectLemmas,
          sentenceIndex: i
        });
      } // end triples loop
    } // end sentences loop
    // console.log(this.documents);
    // for(let i in this.documents) {
    //   if(this.documents[i].originalText == 'located')
    //     console.log(this.documents[i]);
    // }

    let queries = [];
    for (let i in this.keywords) {

      let keyword = this.keywords[i];
      keyword.lemmas = [];
      for (let j in keyword.words) {
        // skip word which is in stopwords list
        if (this.stopwords.includes(keyword.words[j].lemma))
          continue;
        keyword.lemmas.push(keyword.words[j].lemma);
      }
      // change text value to joined lemmas  
      keyword.text = keyword.lemmas.join(" ");

      // add keyword to list of this.documents
      this.documents.push(keyword);

      // // add keyword to list of keywords
      queries.push(keyword);
    }


    // extract words from this.documents
    let uwords = [];
    for (let i in this.documents) uwords.push(...this.documents[i].lemmas);

    // remove all duplicate words
    uwords = Array.from(new Set(uwords)); // console.log(uwords);

    // calculate idfs for all (unique) words in all this.documents
    this.idfs = Cosim.getIdfs(uwords, this.documents); //console.log(idfs);
    let numDocuments = this.documents.length;

    // Calculate all tf and tf-idf on all documents
    // Including keywords (queries)
    for (let i in this.documents) {
      this.documents[i].id = i;
      Cosim.calculateTf(this.documents[i]);
      Cosim.calculateIdf(this.documents[i], this.idfs, numDocuments);
    }

    // Calculate similarity with other documents
    for (let i in this.documents) {
      let doc = this.documents[i];
      doc.sims = [];
      for (let j in queries) {
        let que = queries[j];
        // Only calculate documents similarity 
        // which is in the same sentence as keywords (queries)
        if (que.sentenceIndex != doc.sentenceIndex) continue;

        let similarity = Cosim.cosineSimilarity(que, doc);
        if (similarity > 0) {
          doc.sims.push({
            query: que,
            sim: similarity
          });
        }
      }
    }

    // console.log(this.keywords);
    // console.log(this.documents);
    // console.log(this.keywords);
  }

  getSentenceText(tokens) {
    let text = '';
    for (let i = 0; i < tokens.length; i++) {
      text += tokens[i].originalText + tokens[i].after;
    }
    return text;
  }

  getSimilarTriples(triples) {

    // console.log(triples);

    let keywordSimTriples = [];
    for (let i in triples) {
      let triple = triples[i];
      // console.log(triple);
      triple.subjectReplacement =
        Cosim.getSimilarConceptKeyword(triple.subject, this.documents);
      triple.objectReplacement =
        Cosim.getSimilarConceptKeyword(triple.object, this.documents);

      if (triple.subjectReplacement == null ||
        triple.objectReplacement == null) continue;

      let subSame, preSame, objSame;
      let conSameTriple, index = -1;
      for (let j in keywordSimTriples) {
        conSameTriple = null;
        let keywordSimTriple = keywordSimTriples[j];
        subSame = keywordSimTriple.subjectReplacement.originalText ==
          triple.subjectReplacement.originalText;
        objSame = keywordSimTriple.objectReplacement.originalText ==
          triple.objectReplacement.originalText;
        preSame = keywordSimTriple.relation == triple.relation;
        if (subSame && preSame && objSame) break;
        if (subSame && objSame) {
          conSameTriple = keywordSimTriple;
          index = j;
          break;
        }
        // console.log(subSame);
        // console.log(preSame);
        // console.log(objSame);
        // console.log(index);
      }

      // if not equal in any way with previous triple...
      if (!(subSame && preSame && objSame)) {

        // if only both concepts are equal
        if (conSameTriple != null) {
          if (conSameTriple.relation.length < triple.relation.length) {
            keywordSimTriples.splice(index, 1);
            keywordSimTriples.push(triple);
          }
        } else { // otherwise... add to triples list
          keywordSimTriples.push(triple);
        }

      }

      // console.log(triple);
    }
    return keywordSimTriples;
  }

  processSentence(sent, index) {
    // let sentences = this.getSentences();
    // let index = $(this).find(":selected").data('index');
    // let sent = sentences[index];
    let keywords = sent.keywords;
    sent.read = true;

    // referencing UI list containers
    let keywordList = $('#exp-keyword-list');
    let tripleList = $('#exp-triple-list');
    let tripleKeywordSimList = $('#exp-triple-keyword-sim-list');
    let tripleReverbList = $('#exp-triple-reverb-list');

    $('#selected-sentence').html('' + sent.string + '');
    // console.log(localStorage);

    keywordList.html('');
    for (let i in keywords) {
      let keyword = keywords[i];
      keywordList.append('<li>' + keyword.originalText + ' - <span class="badge badge-info keyword-score" data-tippy-content="Keyword score">' + keyword.score.toFixed(2) + '</span> &nbsp; <a class="bt-add-keyword-as-concept badge badge-warning" href="#" data-type="concept" data-label="' + keyword.originalText + '"><i class="fas fa-plus"></i> Add as Concept</a></li>');
    }

    let ts = tippy('.keyword-score', {
      arrow: true,
      arrowType: 'round', // or 'sharp' (default)
      animation: 'fade',
    });

    let triples = sent.openie;
    tripleList.html(triples.length ? '' : '<li style="text-align:center; color:#999999; padding:0; margin: 0; list-style:none"><em>Sorry, we could not find any more suggestions in this sentence for you.</em></li>');
    tripleKeywordSimList.html('');
    tripleReverbList.html('');

    for (let i in triples) {
      let triple = triples[i];

      tripleList.append('<li><span class="prop-group" data-subject="' + triple.subject + '" data-relation="' + triple.relation + '" data-object="' + triple.object + '" data-sentence="' + index + '"><span class="concept-ref"><span class="concept concept-subject" data-triple-type="subject">' + triple.subject + '</span> <i class="fas fa-caret-up text-primary"></i></span> &nbsp; <i class="fas fa-long-arrow-alt-right text-success"></i> &nbsp; <span class="relation">' + triple.relation + '</span> &nbsp; <i class="fas fa-long-arrow-alt-right text-success"></i> &nbsp; <span class="concept-ref"><span class="concept concept-object" data-triple-type="object">' + triple.object + '</span> <i class="fas fa-caret-up text-primary"></i></span></span> &nbsp; <a class="bt-edit-proposition badge badge-warning"><i class="fas fa-pen"></i> Edit</a> <a class="bt-add-proposition badge badge-primary" href="#" data-ttype="openie" data-type="concept" data-subject="' + triple.subject + '" data-relation="' + triple.relation + '" data-object="' + triple.object + '" data-sentence="' + index + '"><i class="fas fa-plus"></i> Add Proposition</a></li>');

    }

    let keywordSimTriples = this.getSimilarTriples(triples);

    for (let i in keywordSimTriples) {

      let triple = keywordSimTriples[i]; // console.log(triple);
      tripleKeywordSimList.append('<li><span class="prop-group" data-subject="' + triple.subjectReplacement.originalText + '" data-relation="' + triple.relation + '" data-object="' + triple.objectReplacement.originalText + '" data-sentence="' + index + '"><span class="concept-ref"><span class="concept concept-subject" data-triple-type="subject">' + triple.subjectReplacement.originalText + '</span> <i class="fas fa-caret-up text-primary"></i></span> &nbsp; <i class="fas fa-long-arrow-alt-right text-success"></i> &nbsp; <span class="relation">' + triple.relation + '</span> &nbsp; <i class="fas fa-long-arrow-alt-right text-success"></i> &nbsp; <span class="concept-ref"><span class="concept concept-object" data-triple-type="object">' + triple.objectReplacement.originalText + '</span> <i class="fas fa-caret-up text-primary"></i></span></span> &nbsp; <a class="bt-edit-proposition badge badge-warning"><i class="fas fa-pen"></i> Edit</a> <a class="bt-add-proposition badge badge-primary" href="#"  data-ttype="openie-sim" data-type="concept" data-subject="' + triple.subjectReplacement.originalText + '" data-relation="' + triple.relation + '" data-object="' + triple.objectReplacement.originalText + '" data-sentence="' + index + '"><i class="fas fa-plus"></i> Add Proposition</a></li>');

    }

    let reverbTriples = ReVerb.getTriples(sent, index, this.idfs, this.documents.length);
    // console.log(reverbTriples);

    for (let i in reverbTriples) {
      let triple = reverbTriples[i];
      // console.log(triple);
      tripleReverbList.append('<li><span class="prop-group" data-subject="' + triple.subject.originalText + '" data-relation="' + triple.relation + '" data-object="' + triple.object.originalText + '" data-sentence="' + index + '"><span class="concept-ref"><span class="concept concept-subject" data-triple-type="subject">' + triple.subject.originalText + '</span> <i class="fas fa-caret-up text-primary"></i></span> &nbsp; <i class="fas fa-long-arrow-alt-right text-success"></i> &nbsp; <span class="relation">' + triple.relation + '</span> &nbsp; <i class="fas fa-long-arrow-alt-right text-success"></i> &nbsp; <span class="concept-ref"><span class="concept concept-object" data-triple-type="object">' + triple.object.originalText + '</span> <i class="fas fa-caret-up text-primary"></i></span></span> &nbsp; <a class="bt-edit-proposition badge badge-warning"><i class="fas fa-pen"></i> Edit</a> <a class="bt-add-proposition badge badge-primary" href="#" data-ttype="reverb" data-type="concept" data-subject="' + triple.subject.originalText + '" data-relation="' + triple.relation + '" data-object="' + triple.object.originalText + '" data-sentence="' + index + '"><i class="fas fa-plus"></i> Add Proposition</a></li>');

    }

    if (reverbTriples.length > 0) $('#exp-triple-reverb').show();
    else $('#exp-triple-reverb').hide();

    if (keywordSimTriples.length > 0) $('#exp-triple-keyword-sim').show();
    else $('#exp-triple-keyword-sim').hide();

    // if(triples.length > 0) $('#exp-triple').show(); 
    // else $('#exp-triple').hide();


    // console.log(a);
    // console.log($('.concept'), $('#alt-concept'));
    // $('.concept').click(function(){
    //   $('#alt-concept').toggle();
    //   var popper = new Popper($('.concept'), $('#alt-concept'), {
    //     placement: 'top'
    //   });
    //   popper.scheduleUpdate();
    //   // console.log(popper);
    // });

    // console.log('sentence-processed', index);
    // if (typeof Logger != 'undefined') Logger.log('sentence-processed', {mid: this.mid});

    let animateShowHideButton = function () {
      $('#bt-show-hide-more-suggestions')
        .removeClass('animated pulse')
        .addClass('animated pulse');

      var elm = $('#bt-show-hide-more-suggestions'); // console.log(elm);
      var newone = elm[0].cloneNode(true);
      elm[0].parentNode.replaceChild(newone, elm[0]);
      setTimeout(animateShowHideButton, 5000);
    }
    if (!$('#bt-show-hide-more-suggestions').hasClass('animated'))
      animateShowHideButton();
    // setTimeout(animateShowHideButton, 5000);

  }

  addProposition(subject, relation, object, sentenceIndex) {

    let canvas = this.kitbuild.getCanvas();
    let ui = this.kitbuild.getUI();
    let logger = this.logger;

    if (subject == '') ui.getGui().notify('First concept can not empty', {
      type: 'warning'
    });
    if (relation == '') ui.getGui().notify('Link can not empty', {
      type: 'warning'
    });
    if (object == '') ui.getGui().notify('Second concept can not empty', {
      type: 'warning'
    });

    if (subject == '' || relation == '' || object == '') return null;

    let results = canvas.addProposition(subject, relation, object, sentenceIndex);


    if (results != null) {
      // results.collection.layout({
      //   name: 'cose-bilkent'
      // }).run();
      logger.log('add-proposition', {
        subject: subject,
        relation: relation,
        object: object,
        sentenceIndex: sentenceIndex
      })
      ui.getGui().notify('Proposition of <strong>"' + subject + '" - "' + relation + '" - "' + object + '"</strong> is successfully added.', {
        type: 'success'
      });
    }
    // canvas.updateCanvasInfo();
    return results;
  }

  handleTripleAlterationPopup() { // console.log('triple alteration handled');
    let ref = '.concept-ref';
    let canvas = this.kitbuild.getCanvas();
    let logger = this.logger;

    let a = tippy(ref, {
      // theme: 'light',
      arrow: true,
      arrowType: 'round', // or 'sharp' (default)
      boundary: 'viewport',
      // animation: 'fade',
      content: '-',
      interactive: true,
      distance: 5,
      animateFill: false,
      trigger: 'click',
      maxWidth: 650,
      onShow: function (e) {

        let concepts = canvas.getNodes('[type="concept"]');
        // console.log(concepts);
        let concept = $(e.reference).find('.concept');
        // console.log(concept);
        let type = $(concept).data('triple-type');
        let def = $(e.reference).parent('.prop-group').data(type);
        // console.log(def);
        let contents = '<a class="dropdown-item bt-replace" data-name="' + def + '">' + def + ' <span class="badge badge-success">&nbsp;</span></a>';
        for (let i = 0; i < concepts.length; i++) {
          if (def == concepts[i].data.name) continue;
          contents = '<a class="dropdown-item bt-replace" data-id="' + concepts[i].data.id + '" data-name="' + concepts[i].data.name + '">' + concepts[i].data.name + '</a>' +
            contents;
        }
        for (let i = 0; i < a.length; i++) {
          a[i].setContent('<div class="tippy-wrapper" style="max-height: calc(vh - 1rem); overflow: scroll">' + contents + '</div>');
        }

        // console.log(e.reference._tippy);

        $('body').off('click').on('click', '.bt-replace', function () {
          let c = $(e.reference).find('.concept');
          // console.log($(c).html());
          $(c).html($(this).data('name'));
          if(logger != null) logger.log('replace-concept', {
            t: type,
            df: def,
            to: $(this).data('name')
          });
          // if (typeof Logger != 'undefined') Logger.log('replace-concept', {
          //   t: type,
          //   df: def,
          //   to: $(this).data('name')
          // });
          e.reference._tippy.hide();
        });

        //$(a)._tippy.setContent('Yo!');
      }
    });
  }

  updateProgress() {
    let max = this.sentences.length;
    let read = 0;
    for (let i = 0; i < this.sentences.length; i++)
      if (this.sentences[i].read) read++;

    let now = this.sentenceIndex + 1;
    let valeur = parseInt(read / max * 100);
    $('#exp-collab .exp-progress .progress-bar').css('width', valeur + '%').attr('aria-valuenow', valeur);
    $('#exp-progress-text').html('<span>Sentence ' + now + ' / ' + max + '</span> <span>' + read + " sentence(s) read (" + valeur + "%)</span>");
    if (valeur >= 99) $('#bt-finalize').removeClass('disabled');
  }

  showSentences() {

    let logger = this.logger;

    let sentencesList = $('#collab-sentences-list').html('');
    // console.log(collab.sentences.length);
    for (let i = 0; i < this.sentences.length; i++) {
      let sent = this.sentences[i]; // console.log(i, sent.read);
      sentencesList.append('<tr>' +
        '<td>' + this.getSentenceText(sent.tokens) + '</td>' +
        '<td>' + (sent.read ? '<span class="badge badge-success">Read</span>' : '<span class="badge badge-warning">Unread</span>') + '</td>' +
        '<td><button id="bt-select-sentence" data-index="' + i + '" class="btn btn-sm btn-outline-primary">Read</button></td>' +
        '</tr>');
    }
    let collab = this;
    $('#modal-sentences').off('click').on('click', '#bt-select-sentence', function () {
      // console.log(this);
      collab.goToSentence($(this).data('index'));
      if(logger != null) logger.log('select-sentence', {
        sidx: collab.sentenceIndex
      });
      // if (typeof Logger != 'undefined') Logger.log('select-sentence', {
      //   sidx: collab.sentenceIndex
      // });
      $('#modal-sentences').modal('hide');
    });
    $('#modal-sentences').modal('show');
  }

  showManualProposition(proposition = null) {
    if (proposition) {
      $('#modal-proposition .modal-title').html('Edit Proposition');
      $('#modal-proposition #exp-manual-konsep-a').val(proposition.subject);
      $('#modal-proposition #exp-manual-link').val(proposition.relation);
      $('#modal-proposition #exp-manual-konsep-b').val(proposition.object);
    } else {
      $('#modal-proposition .modal-title').html('Create Proposition');
      $('#modal-proposition #exp-manual-konsep-a').val('');
      $('#modal-proposition #exp-manual-link').val('');
      $('#modal-proposition #exp-manual-konsep-b').val('');
    }
    let modal = $('#modal-proposition').modal('show');
    return modal;
  }

  goToSentence(sentenceIndex) {
    let logger = this.logger;
    let sent = this.sentences[sentenceIndex];

    this.processSentence(sent, sentenceIndex);
    this.updateProgress();
    this.handleTripleAlterationPopup();
    this.sentenceIndex = sentenceIndex;

    $('#exp-collab-sentence').text(this.getSentenceText(sent.tokens));

    if(logger != null) logger.log('begin-construct-sentence', 
      { mid: this.mid, sentenceIndex: sentenceIndex });
    // if (typeof Logger != 'undefined') Logger.log('begin-construct-sentence', 
    //   { mid: this.mid, sentenceIndex: sentenceIndex });
  }

}