class HomePage {

  constructor() {
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.ajax = Ajax.ins(this.gui);
    this.user = null;

    this.controller = 'hanifah';
    this.nextPage = 'delaytest';

    // console.log(session.getCookie());
    this.logger = new Logger(null, seq ? seq : null, sessId ? sessId : null);
    console.log(this.logger);
    this.handleEvent(this);
  }

  handleEvent(app) {

    // $('#bt-teacher').on('click', function () {
    //   $('#modal-login .bt-ok').data('role_id', 'TEA')
    //   $('#modal-login').modal('show');
    // });

    $('#bt-student').on('click', function () {
      $('#modal-login .bt-ok').data('role_id', 'HST')
      $('#modal-login').modal('show');
    });

    let doLogin = function () {

      let username = $('#modal-login .input-username').val();
      let password = $('#modal-login .input-password').val();
      let role_id = $('#modal-login .bt-ok').data('role_id');
      let mid = $('#bt-student').data('mid');
      let qsid = $('#bt-student').data('qsid');
      if(!mid || !qsid) {
        console.log(mid, qsid);
        app.gui.notify('Invalid ID', {type: 'danger', delay: 0});
        return;
      }
      app.ajax.post(baseUrl + 'kitbuildApi/signInRole', {
        username: username,
        password: password,
        role_id: role_id
      }).then(function (user) { console.log(user)
        if (user.seq) {
          app.logger.setSeq(user.seq);
          seq = user.seq;
        }
        app.logger.setUid(user.uid);
        user.gids = user.gids ? user.gids.split(",") : [];
        user.grups = user.grups ? user.grups.split(",") : [];
        user.fids = user.fids ? user.fids.split(",") : [];
        if (user.fids.indexOf('HGA') >= 0 || user.fids.indexOf('HGB') >= 0 || user.fids.indexOf('HMGA') >= 0 || user.fids.indexOf('HMGB') >= 0) {
          // console.log('OK')
          app.ajax.post(baseUrl + 'testApi/getAttempts', {
            type: 'delay',
            uid: user.uid,
            qsid: qsid
          }).then(function (taken) { console.log(taken)
            // return;
            if (taken.length) {
              app.gui.notify('You cannot take this test as you had already took it before. Thank you for your kind participation.', {
                delay: 5000,
                type: 'danger'
              });
              return;
            }
            let confirm = app.gui.confirm('Start delayed measurement test?', {
              'positive-callback': function(){
                confirm.modal('hide');
                app.logger.log('sign-in-delay', user);
                app.session.setMulti({
                  user: user,
                  page: app.nextPage,
                  qsid: qsid,
                  mid: mid,
                  seq: app.logger.seq + 1 // because of 'sign-in'
                }, function () {
                  window.location.href = baseUrl + app.controller + '/' + app.nextPage;
                }, function (error) {
                  gui.dialog(error, {
                    width: '300px'
                  });
                })
              },
              'negative-callback': function(){
                confirm.modal('hide');
              }
            });
          });
        } else {
          app.gui.notify('Invalid Group ID');
          return;
        }
      });
    }

    $('#modal-login').on('click', '.bt-ok', function () {
      doLogin();
    });
    $('#modal-login').on('keyup', '.input-password', function (e) {
      if (e.keyCode == 13) doLogin();
    })
  }
}

$(function () {
  let homePage = new HomePage();
});