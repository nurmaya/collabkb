let BRIDGE = {}

class Selection {
  constructor() {
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.ajax = Ajax.ins(this.gui);
    this.user = null;
    this.mid = null;
    this.rid = null;
    this.controller = 'e3'
    this.nextPage = 'read'
    this.logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null);
    console.log(this.logger);
    this.handleEvent(this);
  }

  handleEvent(app) {
    $('#list-material').on('click', '.row', function () {
      app.mid = $(this).data('mid');
      $('#list-material .row i').hide();
      $('#list-material .row[data-mid="' + app.mid + '"] i').show();
      $('#bt-continue').attr('data-mid', app.mid);
      $('#bt-continue').removeData('gmid').removeAttr('data-gmid');
      $('#list-goalmap').html('<em class="pl-5 text-secondary">Loading kit...</em>');
      app.ajax.get(baseUrl + 'kitbuildApi/getGoalmaps/' + app.mid).then(function (goalmaps) { // console.log(goalmaps);
        if (goalmaps.length == 0) {
          $('#list-goalmap').html('<em class="text-danger">No kits found for selected topic...</em>');
          return;
        } else {
          let list = '';
          goalmaps.forEach(gm => {
            list += '<div class="goalmap row" data-gmid="' + gm.gmid + '"> <span>' + gm.name +
              ' <span class="badge badge-primary"> ' + gm.concepts_count + 'C </span>' +
              ' <span class="badge badge-primary"> ' + gm.links_count + 'L </span></span>' +
              ' <i class="fas fa-check text-success" style="display: none"></i></div>';
          });
          $('#list-goalmap').html(list);
        }
      })
    })

    $('#list-goalmap').on('click', '.row', function () { //console.log('click')
      app.gmid = $(this).data('gmid');
      // /console.log(rid);
      $('#list-goalmap .row i').hide();
      $('#list-goalmap .row[data-gmid="' + app.gmid + '"] i').show();
      $('#bt-continue').attr('data-gmid', app.gmid);
    })

    $('#list-room').on('click', '.row', function () { //console.log('click')
      app.rid = $(this).data('rid');
      // /console.log(rid);
      $('#list-room .row i').hide();
      $('#list-room .row[data-rid="' + app.rid + '"] i').show();
      $('#bt-continue').attr('data-rid', app.rid);
    })

    $('#bt-continue').on('click', function () {
      app.mid = $(this).attr('data-mid');
      app.gmid = $(this).attr('data-gmid');
      app.rid = $(this).attr('data-rid');
      if (!app.mid) {
        app.gui.dialog("Please select a material topic.", {
          width: '300px'
        })
        return;
      }
      if(app.user.grups) {
        if (!app.gmid && app.user.grups.indexOf('E3B') >= 0) {
          app.gui.dialog("Please select a kit.", {
            width: '300px'
          })
          return;
        }
      }
      if (!app.rid) {
        app.gui.dialog("Please select a destined collaboration room.", {
          width: '400px'
        })
        return;
      }
      app.logger.log('begin-read', {
        mid: app.mid,
        gmid: app.gmid,
        rid: app.rid
      });
      app.session.setMulti({
        mid: app.mid,
        gmid: app.gmid,
        rid: app.rid,
        page: app.nextPage,
        seq: app.logger.seq + 1
      }, function () {
        window.location.href = baseUrl + app.controller + '/' + app.nextPage;
      }, function (error) {
        app.gui.dialog(error, {
          width: '300px'
        })
      })
    });
  }
}

$(function () {
  BRIDGE.app = new Selection();
});