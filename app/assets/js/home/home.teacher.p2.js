var BRIDGE = {}

class Selection {
  constructor() {
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.user = null;
    this.mid = null;
    this.rid = null;
    this.controller = 'home';
    this.nextPage = 'cmap'
    this.logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null);
    console.log(this.logger);
    this.support = null;
    this.handleEvent(this);
  }

  handleEvent(app) {
    $('#list-material').on('click', '.row', function () {
      let mid = $(this).data('mid');
      $('#list-material .row i').hide();
      $('#list-material .row[data-mid="' + mid + '"] i').show();
      $('#bt-continue').attr('data-mid', mid);
    })

    $('#list-room').on('click', '.row', function () { //console.log('click')
      let rid = $(this).data('rid');
      $('#list-room .row i').hide();
      $('#list-room .row[data-rid="' + rid + '"] i').show();
      $('#bt-continue').attr('data-rid', rid);
    })

    $('#bt-continue').on('click', function () {
      let mid = $(this).attr('data-mid');
      let rid = $(this).attr('data-rid');
      if (mid == null) {
        app.gui.dialog("Please select a material topic.", {
          width: '300px'
        })
        return;
      }
      if (rid == null) {
        app.gui.dialog("Please select a destined collaboration room.", {
          width: '400px'
        })
        return;
      }
      app.logger.log('begin-cmapping', {
        mid: mid,
        rid: rid
      });
      app.session.setMulti({
        mid: mid,
        rid: rid,
        page: app.nextPage,
        seq: app.logger.seq + 1
      }, function () {
        window.location.href = baseUrl + app.controller + '/' + app.nextPage;
      }, function (error) {
        app.gui.dialog(error, {
          width: '300px'
        })
      })
    });
  }
}

$(function () {
  BRIDGE.app = new Selection();
  BRIDGE.app.support = new CollabKitSupport();
  BRIDGE.app.support.attachEventListener(BRIDGE.app);
});