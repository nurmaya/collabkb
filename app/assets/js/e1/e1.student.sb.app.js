class ScratchBuildApp {

  constructor(canvas) {
    this.canvas = canvas;
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.ajax = Ajax.ins(this.gui);
    this.controller = 'e1'
    this.nextPage = 'postsb'

    this.material = {
      mid: null,
      name: null,
      content: null
    }

    this.user = {
      uid: null,
      username: null,
      rid: null,
      gids: []
    };

    this.eventListeners = [];

    this.kit = null; // holder for kit socket communication
    this.handleEvent();
    this.canvas.attachEventListener(this);
    //this.canvas.getToolbar().enableNodeCreation(false);
  }

  setUser(uid, username, rid, gids) {
    this.user = {
      uid: uid,
      username: username,
      role_id: rid,
      gids: gids
    }
    return this;
  }

  getCanvas() {
    return this.canvas;
  }

  getGUI() {
    return this.gui;
  }

  getSession() {
    return this.session;
  }

  attachEventListener(listener) {
    this.eventListeners.push(listener);
  }

  handleEvent() {
    let kitbuild = this;

    $('#bt-login').on('click', function (e) {
      $('#modal-chat-login').modal('show');
      $('#modal-chat-login .input-username').focus();
    });
    let doSignIn = function () {
      let username = $('#modal-chat-login .input-username').val().trim();
      let password = $('#modal-chat-login .input-password').val().trim();
      kitbuild.ajax.post(
        baseUrl + 'kitbuildApi/signIn', {
          username: username,
          password: password
        }
      ).then(function (user) {
        let uid = user.uid;
        let rid = user.role_id;
        let gids = user.gids ? user.gids.split(",") : [];
        kitbuild.kit.setUser(uid, username, rid, gids);
        kitbuild.signIn(uid, username, rid, gids);
        $('#modal-chat-login').modal('hide');
        $('#input-username').val('');
        $('#input-password').val('');
      })
    }
    $('#modal-chat-login').on('click', '.bt-ok', doSignIn);
    $('#modal-chat-login .input-password').on('keyup', function (e) {
      if (e.keyCode == 13) doSignIn();
    });
    $('#bt-logout').on('click', function (e) {
      let confirmDialog = kitbuild.gui.confirm('Do you want to logout?', {
        'positive-callback': function () {
          kitbuild.session.destroy(() => {
            kitbuild.signOut();
            kitbuild.kit.leaveRoom(kitbuild.kit.room);
            confirmDialog.modal('hide');
            window.location.href = baseUrl + 'home';
          })
        }
      });
    });
    $('#bt-sync').on('click', (e) => {
      if (this.canvas.getCy().nodes().length) {
        let text = 'You currently have concept map on canvas.<br>If you sync the map, it will be <strong class="text-danger">replaced</strong>.<br>Continue?';
        let dialog = this.gui.confirm(text, {
          'positive-callback': function () {
            kitbuild.kit.syncMap();
            kitbuild.eventListeners.forEach(listener => {
              if (listener && typeof listener.onAppEvent == 'function') {
                listener.onAppEvent('request-sync');
              }
            })
            dialog.modal('hide');
          }
        })
      }
    });
    $('#bt-open-kit').on('click', function () {
      kitbuild.ajax.post(
        baseUrl + 'kitbuildApi/getMaterialsWithGids', {
          gids: kitbuild.kit.user.gids
        }
      ).then(function (materials) {
        let materialList = (!materials || materials.length == 0) ?
          '<em class="text-secondary">No material available</em>' :
          '';
        materials.forEach((material) => {
          materialList += '<div class="row align-items-center list list-hover list-pointer justify-content-between pt-2 pb-2 pl-5 pr-5" data-mid="' + material.mid + '" data-name="' + material.name + '">'
          materialList += material.name
          materialList += '<i class="fas fa-check text-primary" style="display:none"></i>'
          materialList += '</div>'
        });
        $('#modal-kit .material-list').html(materialList);
        $('#modal-kit').modal('show', {
          'width': '200px'
        });
      })
    });
    $('#modal-kit .material-list').on('click', '.row', function () {
      if ($(this).hasClass('selected')) return;
      $('#modal-kit .material-list .row').find('i').hide();
      $('#modal-kit .material-list .row').removeClass('selected');
      $(this).find('i').show();
      $(this).addClass('selected')
      let mid = $(this).data('mid');
      let name = $(this).data('name');
      $('#modal-kit .bt-open').data('mid', mid);
      $('#modal-kit .bt-open').data('name', name);
    });
    $('#modal-kit').on('click', '.bt-open', function () {
      let name = ($(this).data('name'))
      let mid = ($(this).data('mid'))
      let confirm = kitbuild.gui.confirm('Open material <strong>\'' + name + '\'</strong>?', {
        'positive-callback': function () {
          kitbuild.loadMaterial(mid, confirm)
        },
        'width': '300px'
      })
    });
    $('#bt-show-material').on('click', function () {
      $('#popup-material').fadeToggle({
        duration: 200,
        complete: function () {
          let shown = $(this).is(':visible');
          kitbuild.eventListeners.forEach(listener => {
            if (listener && typeof listener.onAppEvent == 'function') {
              if (shown) listener.onAppEvent('show-material');
              else listener.onAppEvent('hide-material');
            }
          })
        }
      });
    });
    $('#popup-material .bt-close').on('click', function () {
      $('#popup-material .check-open').prop('checked', false);
      $('#popup-material').fadeOut({
        duration: 200,
        complete: function () {
          kitbuild.eventListeners.forEach(listener => {
            if (listener && typeof listener.onAppEvent == 'function') {
              listener.onAppEvent('hide-material');
            }
          })
        }
      });
    });
    $('#bt-save').on('click', function () {
      let mid = kitbuild.material.mid;
      let uid = kitbuild.user.uid;
      if (!mid) {
        kitbuild.gui.dialog('Please open a material to save the map into.<br>A map should refer to a material.');
        return;
      }
      if (mid != null && uid != null) {
        kitbuild.saveGoalmapDraft(mid, uid);
      }
    });
    $('#bt-load-draft').on('click', function () {
      let mid = kitbuild.material.mid;
      let uid = kitbuild.user.uid;
      // let rid = kitbuild.kit.room.rid;
      if (mid != null) {
        let currentEles = kitbuild.canvas.getCy().nodes();
        if (currentEles.length) {
          let confirm = kitbuild.gui.confirm('You currently have existing map data on canvas. Loading saved draft map data will <strong class="text-danger">REPLACE</strong> current map on canvas and across <strong class="text-danger">ALL users</strong> in current room.<br>Continue?', {
            'positive-callback': function () {
              kitbuild.loadSavedGoalmapDraft(mid, uid, function () {
                confirm.modal('hide');
              });
            }
          })
        } else kitbuild.loadSavedGoalmapDraft(mid, uid);
      } else {
        if (!mid) kitbuild.gui.dialog('Please select a material. A concept map saved draft is related with a material.')
        if (!rid) kitbuild.gui.dialog('Please select a room. A collaborative concept map saved draft is related with a collaboration room.')
      }
    });
    $('#bt-finalize').on('click', function () {
      if (kitbuild.getCanvas().getCy().nodes().length != 0) {
        let imageData = kitbuild.getCanvas().getCy().png({
          scale: 2,
          full: true
        });
        $('#popup-map').find('img.cmap').attr('src', imageData);
        setTimeout(function () {
          $('#popup-map').show().center().center();
        }, 100);
      } else {
        confirmFinalize();
      }
    });

    $('#popup-map').on('click', '.bt-cancel', function () {
      $('#popup-map').hide();
    });

    $('#popup-map').on('click', '.bt-download', function () {
      let mid = kitbuild.material.mid;
      let uid = kitbuild.user.uid;
      // let rid = kitbuild.kit.room.rid;
      var a = document.createElement("a"); //Create <a>
      a.href = $('#popup-map').find('img.cmap').attr('src');
      a.download = "concept-map-m" + mid + "-u" + uid + ".png"; //File name Here
      a.click(); //Downloaded file
    })

    let confirmFinalize = function () {
      let mid = kitbuild.material.mid;
      let uid = kitbuild.user.uid;
      // let rid = kitbuild.kit.room.rid;
      if (mid != null && uid != null) {
        let confirm = kitbuild.gui.confirm('Save and finalize the map? <br>Any previously saved draft concept maps will be deleted, preserving the finalized one.', {
          'positive-callback': function () {
            confirm.modal('hide');
            $('#modal-map-name').modal('show');
            $('#modal-map-name .map-name-input').val('final-m' + mid + '-u' + uid + '-' + kitbuild.user.username).focus().select();
          },
          'negative-callback': function () {
            confirm.modal('hide');
          }
        })
      } else {
        if (!mid) {
          kitbuild.gui.dialog('Please open a material to save the map into.<br>A map should refer to a material.');
          return;
        }
        // if (!rid) {
        //   kitbuild.gui.dialog('Please join a room to save the map into.<br>In a collaboration environment, a saved maps should relate with a collaboration group. Therefore, you need to join a room before you create and save a concept map.');
        //   return;
        // }
      }
    }

    $('#popup-map').on('click', '.bt-continue', function () {
      confirmFinalize();
    });

    $('#modal-map-name').on('click', '.bt-ok', function () {
      let mid = kitbuild.material.mid;
      let uid = kitbuild.user.uid;
      // let rid = kitbuild.kit.room.rid;
      if (mid != null && uid != null) {
        let name = $('#modal-map-name .map-name-input').val().trim()
        if (name == '')
          kitbuild.gui.dialog('Concept map name should not be empty.')
        else {
          kitbuild.saveGoalmapFinalize(mid, uid, name, function (gmid) {
            $('#modal-map-name').modal('hide');
            kitbuild.eventListeners.forEach(listener => {
              if (listener && typeof listener.onAppEvent == 'function') {
                listener.onAppEvent('finalize-map', {
                  gmid: gmid
                });
              }
            })
            kitbuild.session.set('page', kitbuild.nextPage, function () {
              window.location.href = baseUrl + kitbuild.controller + '/' + kitbuild.nextPage;
              return;
            }, function (error) {
              kitbuild.gui.dialog('Error: ' + error);
            })
          });
        }
      } else {
        if (!mid) {
          kitbuild.gui.dialog('Please open a material to save the map into.<br>A map should refer to a material.');
          return;
        }
        if (!rid) {
          kitbuild.gui.dialog('Please join a room to save the map into.<br>In a collaboration environment, a saved maps should relate with a collaboration group. Therefore, you need to join a room before you create and save a concept map.');
          return;
        }
      }
    });
  }

  attachKit(kit) { // console.log(kit);
    this.kit = kit;
    this.kit.attachEventListener(this);
  }

  // onKitEvent(event, data) {
  //   let gui = this.gui;
  //   let kit = this.kit;
  //   let canvas = this.canvas;
  //   switch (event) {
  //     case 'join-room':
  //       this.session.set('room', data.room);
  //       this.canvas.clearCanvas();
  //       this.canvas.getToolbar().enableNodeCreation(true);
  //       kit.rooms.forEach(room => {
  //         // console.log(room, kit)
  //         if (room.name == kit.room.name && room.users.length) {
  //           kit.syncMap();
  //           kit.syncMessages();
  //           return;
  //         }
  //       })
  //       $('#bt-sync').prop('disabled', false)
  //       break;
  //     case 'leave-room':
  //       this.session.unset('room');
  //       this.canvas.getToolbar().enableNodeCreation(false);
  //       break;
  //     case 'room-update':
  //       kit.rooms = data;
  //       break;
  //       // case 'user-sign-in':
  //       //   this.session.set('user', data);
  //       //   break;
  //       // case 'user-sign-out':
  //       //   this.session.unset('user');
  //       //   break;
  //     case 'command-sign-out':
  //       this.signOut();
  //       this.kit.leaveRoom(this.kit.room);
  //       this.kit.enableRoom(false)
  //       this.kit.enableMessage(false)
  //       this.gui.dialog('You have been signed out');
  //       break;
  //     case 'command-leave-room':
  //       // this.signOut();
  //       // this.kit.leaveRoom(this.kit.room);
  //       // this.kit.enableRoom(false)
  //       // this.kit.enableMessage(false)
  //       // this.gui.dialog('You have been signed out');
  //       break;
  //     case 'request-map-data':
  //       let nodes = this.canvas.getCy().nodes();
  //       let edges = this.canvas.getCy().edges();
  //       let elesData = [];
  //       nodes.forEach(n => {
  //         elesData.push({
  //           group: 'nodes',
  //           data: n.data(),
  //           position: n.position()
  //         });
  //       });
  //       edges.forEach(n => {
  //         elesData.push({
  //           group: 'edges',
  //           data: n.data(),
  //           position: n.position()
  //         });
  //       });
  //       return elesData;
  //     case 'request-material-data':
  //       return this.material;
  //     case 'sync-messages-data':
  //       break;
  //     case 'sync-map-data':
  //       canvas.reset();
  //       canvas.clearCanvas();
  //       canvas.showMap(data);
  //       canvas.centerOneToOne();
  //       gui.notify('Map data synchronized', {
  //         type: 'success'
  //       })
  //       $('#bt-center').click();
  //       break;
  //     case 'sync-material-data':
  //       if (data.mid == null) return;
  //       this.setMaterial(data.mid, data.name, data.content);
  //       this.enableMaterial();
  //       break;
  //     case 'sync-map-data-error': // console.log('sync-map-data-error received')
  //       this.gui.notify(data.error, {
  //         type: 'danger'
  //       })
  //       break;
  //     case 'chat-message-open':
  //       this.session.set('chat-window-open', data)
  //       break;
  //     case 'receive-create-node':
  //       let empty = (this.canvas.getCy().nodes().length == 0);
  //       this.canvas.createNodeFromJSON(data.node);
  //       if (empty) $('#bt-center').click();
  //       break;
  //     case 'receive-move-node':
  //       this.canvas.moveNode(data.node.id, data.node.x, data.node.y);
  //       break;
  //     case 'receive-connect-node':
  //       this.canvas.connect(data.connection);
  //       break;
  //     case 'receive-disconnect-node':
  //       this.canvas.disconnect(data.connection);
  //       break;
  //     case 'receive-duplicate-node':
  //       this.canvas.createNodeFromJSON(data.node);
  //       break;
  //     case 'receive-delete-node':
  //       this.canvas.deleteNode(data.node);
  //       break;
  //     case 'receive-update-node':
  //       this.canvas.updateNode(data.node.nodeId, data.node.value);
  //       break;
  //     case 'receive-center-link':
  //       this.canvas.moveNode(data.link.id, data.link.x, data.link.y);
  //       break;
  //     case 'receive-switch-direction': // data is linkId
  //       this.canvas.switchDirection(data);
  //       break;
  //     case 'change-page':
  //       this.session.set('page', data.page, function () {
  //         window.location.href = baseUrl + 'home/' + data.page;
  //         return;
  //       }, function (error) {
  //         this.gui.dialog('Error: ' + error);
  //       })
  //       break;


  //       // Collab Event From Kit

  //     case 'send-message':
  //       this.eventListeners.forEach(listener => {
  //         if (listener && typeof listener.onCollabEvent == 'function') {
  //           listener.onCollabEvent('send-room-message', data.message);
  //         }
  //       })
  //       break;
  //     case 'close-discuss-channel':
  //       this.eventListeners.forEach(listener => {
  //         if (listener && typeof listener.onCollabEvent == 'function') {
  //           listener.onCollabEvent(event, data);
  //         }
  //       })
  //       break;
  //     case 'send-channel-message':
  //       this.eventListeners.forEach(listener => {
  //         if (listener && typeof listener.onCollabEvent == 'function') {
  //           listener.onCollabEvent(event, {
  //             channel: data.channel,
  //             message: data.message
  //           });
  //         }
  //       })
  //       break;
  //   }
  // }

  onCanvasEvent(event, data) {
    // console.log(event, data);
    if (!this.kit) return;
    switch (event) {
      case 'create-concept':
        this.kit.createNode(data);
        break;
      case 'create-link':
        this.kit.createNode(data);
        break;
      case 'drag-concept':
        this.kit.dragNode(data);
        break;
      case 'drag-link':
        this.kit.dragNode(data);
        break;
      case 'drag-node-group':
        this.kit.dragNodeGroup(data);
        break;
      case 'move-concept':
        this.kit.moveNode(data);
        break;
      case 'move-link':
        this.kit.moveNode(data);
        break;
      case 'move-node-group':
        this.kit.moveNodeGroup(data);
        break;
      case 'connect-left':
        this.kit.connectNode(data);
        break;
      case 'connect-right':
        this.kit.connectNode(data);
        break;
      case 'disconnect-left':
        this.kit.disconnectNode(data);
        break;
      case 'disconnect-right':
        this.kit.disconnectNode(data);
        break;
      case 'change-connect-left':
      case 'change-connect-right':
        this.kit.changeConnection(data);
        break;
      case 'duplicate-concept':
      case 'duplicate-link':
        this.kit.duplicateNode(data);
        break;
      case 'delete-node':
      case 'delete-link':
      case 'delete-concept':
        this.kit.deleteNode(data);
        break;
      case 'delete-node-group':
        this.kit.deleteNodeGroup(data);
        break;
      case 'update-concept-name':
      case 'update-link-name':
        this.kit.updateNode(data);
        break;
      case 'center-link':
        this.kit.centerLink(data);
        break;
      case 'switch-direction':
        this.kit.switchDirection(data);
        break;
    }
  }

  onCanvasToolClicked(tool, node) { // console.log(tool, node);

    // for listener purposes
    let action = null;
    let data = null;
    let listenerHandled = false;
    let app = this;

    switch (tool.type) {
      case 'discuss-channel':
        let nodeId = node.id();
        let nodeType = node.data('type');
        let nodeLabel = node.data('name');
        app.kit.openChannel(nodeId, nodeType, nodeLabel, function (channel) {
          action = 'open-discuss-channel';
          app.eventListeners.forEach(listener => {
            if (listener && typeof listener.onCollabEvent == 'function') {
              listener.onCollabEvent(action, {
                cid: channel.cid,
                rid: channel.rid,
                node_id: channel.node_id,
                node_type: channel.node_type,
                node_label: channel.node_label
              });
            }
          })
        });
        listenerHandled = true;
        break;
      case 'center-link':
      case 'switch-direction':
        listenerHandled = true;
        break;
    }
    if (listenerHandled) return;
    this.eventListeners.forEach(listener => {
      if (listener && typeof listener.onCollabEvent == 'function') {
        listener.onCollabEvent(action, data);
      }
    })
  }


  // Commanding Kit-Build App Interface

  signIn(uid, username, rid, gids) {
    $('#bt-login').hide();
    $('#bt-logout').show();
    $('#bt-logout-username').html(": " + username);
    $('#bt-open-kit').prop('disabled', false);
    // this.kit.enableMessage();
    // this.kit.enableRoom();
    this.enableSaveDraft();
    this.enableFinalize();
    // this.kit.setUser(uid, username, rid, gids);
    // this.initiateGoalmap();
  };

  signOut() {
    $('#bt-login').show();
    $('#bt-logout').hide();
    $('#bt-open-kit').prop('disabled', true);
    $('#bt-show-material').prop('disabled', true);
    this.kit.unsetUser();
    this.enableSaveDraft(false);
    this.enableFinalize(false);
    this.unsetMaterial();
  }

  enableMaterial(enabled = true) {
    $('#bt-show-material').prop('disabled', !enabled);
  }

  enableSaveDraft(enable = true) {
    $('#bt-save').prop('disabled', !enable);
    $('#bt-load-draft').prop('disabled', !enable);
  }

  enableFinalize(enable = true) {
    $('#bt-finalize').prop('disabled', !enable);
  }

  loadMaterial(mid, confirmDialog) {
    let kitbuild = this;
    kitbuild.ajax.get(
      baseUrl + 'kitbuildApi/getMaterialCollabByMid/' + mid

    ).then(function (material) {
      if (confirmDialog) confirmDialog.modal('hide');
      kitbuild.session.set('mid', material.mid);
      kitbuild.setMaterial(material.mid, material.name, material.content);
      // kitbuild.kit.sendMaterial(material.mid, material.name, material.content);
      $('#modal-kit').modal('hide');
    })
    $('#bt-show-material').prop('disabled', false);
  }

  setMaterial(mid, name, content) {
    this.material.mid = mid;
    this.material.name = name;
    this.material.content = content;
    $('#popup-material .material-title').html(this.material.name)
    $('#popup-material .material-content').html(this.material.content);
  }

  unsetMaterial() {
    this.material.mid = null;
    this.material.name = null;
    this.material.content = null;
  }

  saveGoalmap(mid, uid, type, name, callback) { // console.log(name);
    let kitbuild = this;
    let goalmaps = {
      name: name ? name : type + '-m' + mid + '-u' + uid,
      type: type,
      mid: mid,
      creator_id: uid,
      updater_id: uid,
      concepts: [],
      links: []
    }
    let cs = kitbuild.canvas.getNodes('[type="concept"]');
    let ls = kitbuild.canvas.getNodes('[type="link"]');
    let es = kitbuild.canvas.getEdges();
    // console.log(cs, ls, es);
    cs.forEach(c => {
      goalmaps.concepts.push({
        cid: c.data.id.substr(2),
        gmid: null,
        label: c.data.name,
        locx: c.position.x,
        locy: c.position.y
      });
    });
    ls.forEach(l => {
      let tl = {
        lid: l.data.id.substr(2),
        gmid: null,
        label: l.data.name,
        locx: l.position.x,
        locy: l.position.y,
        source: null,
        target: null
      };
      es.forEach((e) => {
        if (e.data.source == l.data.id) {
          if (e.data.type == 'right') tl.target = e.data.target.substr(2);
          if (e.data.type == 'left') tl.source = e.data.target.substr(2);
          return;
        }
      });
      goalmaps.links.push(tl);
    })
    kitbuild.ajax.post(baseUrl + 'kitbuildApi/saveGoalmap', goalmaps).then(function (gmid) {
      let message = type == 'fix' ? 'Map finalized' : 'Map saved';
      if (type != 'auto')
        kitbuild.gui.notify(message, {
          type: 'success'
        })
      kitbuild.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('map-save-' + type, {
            gmid: gmid
          });
        }
      })
      if (callback) callback(gmid)
    })
  }

  saveGoalmapDraft(mid, uid, callback) {
    this.saveGoalmap(mid, uid, 'draft', null, callback)
  }

  saveGoalmapFinalize(mid, uid, name, callback) {
    this.saveGoalmap(mid, uid, 'fix', name, callback)
  }

  saveGoalmapAuto(mid, uid, callback) {
    this.saveGoalmap(mid, uid, 'auto', null, callback)
  }

  checkAvailableGoalmapDraft(mid, rid) {
    let kitbuild = this;
    kitbuild.ajax.get(baseUrl + 'kitbuildApi/getLastDraftGoalmap/' + mid + '/' + rid).then(function (gmid) {
      if (gmid) $('#bt-load-draft').prop('disabled', false);
    })

  }

  loadSavedGoalmapDraft(mid, uid, callback) {
    let kitbuild = this;
    kitbuild.ajax.get(baseUrl + 'kitbuildApi/loadLastDraftGoalmap/' + mid + '/' + uid).then(function (draft) {
      if (draft == null) {
        kitbuild.gui.notify('Nothing to load. No previously saved draft maps found.')
        return;
      }
      let goalmap = draft.goalmap;
      let concepts = draft.concepts;
      let links = draft.links;
      let eles = [];
      concepts.forEach((c) => {
        eles.push({
          group: 'nodes',
          data: {
            id: 'c-' + c.cid,
            name: c.label,
            type: 'concept'
          },
          position: {
            x: parseInt(c.locx),
            y: parseInt(c.locy)
          }
        })
      })
      links.forEach((l) => {
        eles.push({
          group: 'nodes',
          data: {
            id: 'l-' + l.lid,
            name: l.label,
            type: 'link'
          },
          position: {
            x: parseInt(l.locx),
            y: parseInt(l.locy)
          }
        })
        if (l.source != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.source,
              type: 'left'
            }
          })
        }
        if (l.target != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.target,
              type: 'right'
            }
          })
        }
      })
      kitbuild.canvas.clearCanvas();
      kitbuild.canvas.getCy().add(eles);
      kitbuild.gui.notify('Concept map loaded from last saved drafts.')
      // kitbuild.kit.sendMap(eles);
      $('#bt-center').click();
      kitbuild.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('load-last-draft', {
            gmid: parseInt(goalmap.gmid)
          });
        }
      })
      if (typeof callback == 'function') callback();
    })
  }

}

jQuery.fn.center = function (parent) {
  parent = parent ? $(parent) : window
  this.css({
    "position": "absolute",
    "top": ((($(parent).height() - this.outerHeight()) / 2) + $(parent).scrollTop() + "px"),
    "left": ((($(parent).width() - this.outerWidth()) / 2) + $(parent).scrollLeft() + "px")
  });
  return this;
}