var BRIDGE = {};

class StudentPostSBCollab {

  constructor() {
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.controller = 'e1'
    this.nextPage = 'posttest'
    this.logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null);
    console.log(this.logger);
    
    this.handleEvent(this);
    // this.loadUser(uid);
  }

  handleEvent(app) {
    $('#bt-continue').on('click', function () {
      let confirm = app.gui.confirm('Mulai mengerjakan post test?', {
        'positive-callback': function () {
          confirm.modal('hide');
          app.logger.log('begin-post-test');
          app.session.setMulti({
            page: app.nextPage,
            seq: app.logger.seq + 1
          }, function () {
            window.location.href = baseUrl + app.controller + '/' + app.nextPage;
          }, function (error) {
            app.gui.notify('Unable to prepare kit. Failed to set Gmid. ' + error, {
              type: 'danger'
            })
          })
        }
      })
    });
  }

}

$(function () {
  BRIDGE.studentPostSBCollab = new StudentPostSBCollab();
});