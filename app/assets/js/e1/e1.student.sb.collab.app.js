class ScratchBuildCollabApp {

  constructor(canvas) {
    this.canvas = canvas;
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.ajax = Ajax.ins(this.gui);
    this.analyzer = new AnalyzerLib();

    this.controller = 'e1';
    this.postFinalizeMethod = 'postsbcollab'

    this.material = {
      mid: null,
      name: null,
      content: null
    }

    this.goalmaps = [];
    this.learnermaps = [];
    this.gids = [];
    this.grups = [];

    this.eventListeners = [];

    this.kit = null; // holder for kit socket communication
    this.handleEvent();
    this.canvas.attachEventListener(this);
    this.canvas.getToolbar().enableNodeCreation(false);

    this.enableSaveDraft();
    this.enableFinalize();

  }

  getCanvas() {
    return this.canvas;
  }

  getGUI() {
    return this.gui;
  }

  getSession() {
    return this.session;
  }

  attachEventListener(listener) {
    this.eventListeners.push(listener);
  }

  handleEvent() {
    let app = this;

    $('#bt-sync').on('click', (e) => {
      // console.log('sync')
      if (this.canvas.getCy().nodes().length) {
        let text = 'You currently have concept map on canvas.<br>If you sync the map, it will be <strong class="text-danger">replaced</strong>.<br>Continue?';
        let dialog = this.gui.confirm(text, {
          'positive-callback': function () {
            app.kit.syncMap();
            app.eventListeners.forEach(listener => {
              if (listener && typeof listener.onAppEvent == 'function') {
                listener.onAppEvent('request-sync');
              }
            })
            dialog.modal('hide');
          }
        })
      } else app.kit.syncMap();
    });
    $('#bt-show-material').on('click', function () {
      $('#popup-material').fadeToggle({
        duration: 200,
        complete: function () {
          let shown = $(this).is(':visible');
          app.eventListeners.forEach(listener => {
            if (listener && typeof listener.onAppEvent == 'function') {
              if (shown) listener.onAppEvent('show-material');
              else listener.onAppEvent('hide-material');
            }
          })
        }
      });
    });
    $('#popup-material .bt-close').on('click', function () {
      $('#popup-material .check-open').prop('checked', false);
      $('#popup-material').fadeOut({
        duration: 200,
        complete: function () {
          app.eventListeners.forEach(listener => {
            if (listener && typeof listener.onAppEvent == 'function') {
              listener.onAppEvent('hide-material');
            }
          })
        }
      });
    });
    $('#bt-init').on('click', function () {
      if(app.canvas.getCy().nodes().length > 0) {
        let confirm = app.gui.confirm('You have existing map on canvas. If you initialize the map, the canvas will be <strong class="text-danger">CLEARED and RESET</strong> to initial state. Your <strong class="text-danger">friends map</strong> on the same room <strong class="text-danger">WILL ALSO RESET</strong>. Continue?', {
          'positive-callback': function() {
            confirm.modal('hide');
            let mid = app.material.mid;
            let rid = app.kit.room.rid;
            app.initializeGroupMap(rid, mid);
          }
        });
        return;
      }
    });
    $('#bt-save').on('click', function () {
      let mid = app.material.mid;
      let uid = app.kit.user.uid;
      let rid = app.kit.room.rid;
      if (mid != null && uid != null && rid != null) {
        app.saveGoalmapDraft(mid, rid, uid);
      } else {
        if (!mid) {
          app.gui.dialog('Please open a material to save the map into.<br>A map should refer to a material.');
          return;
        }
        if (!rid) {
          app.gui.dialog('Please join a room to save the map into.<br>In a collaboration environment, a saved maps should relate with a collaboration group. Therefore, you need to join a room before you create and save a concept map.');
          return;
        }
      }
    });
    $('#bt-load-draft').on('click', function () {
      let mid = app.material.mid;
      let rid = app.kit.room.rid;
      if (mid != null && rid != null) {
        let currentEles = app.canvas.getCy().nodes();
        if (currentEles.length) {
          let confirm = app.gui.confirm('You currently have existing map data on canvas. Loading saved draft map data will <strong class="text-danger">REPLACE</strong> current map on canvas and across <strong class="text-danger">ALL users</strong> in current room.<br>Continue?', {
            'positive-callback': function () {
              app.loadSavedGoalmapDraft(mid, rid, function () {
                confirm.modal('hide');
                app.hideExcessiveLinks();
                if ($('#show-excessive-links').is(':checked'))
                  app.showExcessiveLinks();
              });
            }
          })
        } else app.loadSavedGoalmapDraft(mid, rid, function () {
          app.hideExcessiveLinks();
          if ($('#show-excessive-links').is(':checked'))
            app.showExcessiveLinks();
        });
      } else {
        if (!mid) app.gui.dialog('Please select a material. A concept map saved draft is related with a material.')
        if (!rid) app.gui.dialog('Please select a room. A collaborative concept map saved draft is related with a collaboration room.')
      }
    });
    $('#bt-finalize').on('click', function () {
      if($('#show-excessive-links').is(':checked')) $('#show-excessive-links').click();

      if (app.getCanvas().getCy().nodes().length != 0) {
        let imageData = app.getCanvas().getCy().png({
          scale: 2,
          full: true
        });
        $('#popup-map').find('img.cmap').attr('src', imageData);
        setTimeout(function () {
          $('#popup-map').show().center().center();
        }, 100);
      } else {
        confirmFinalize();
      }
    });

    $('#popup-map').on('click', '.bt-cancel', function () {
      $('#popup-map').hide();
    });

    $('#popup-map').on('click', '.bt-download', function () {
      let mid = app.material.mid;
      let uid = app.kit.user.uid;
      let rid = app.kit.room.rid;
      var a = document.createElement("a"); //Create <a>
      a.href = $('#popup-map').find('img.cmap').attr('src');
      a.download = "concept-map-m" + mid + "-r" + rid + "-u" + uid + ".png"; //File name Here
      a.click(); //Downloaded file
    })

    let confirmFinalize = function () {
      let mid = app.material.mid;
      let uid = app.kit.user.uid;
      let rid = app.kit.room.rid;
      if (mid != null && uid != null && rid != null) {
        let confirm = app.gui.confirm('Save and finalize the map? <br>Any previously saved draft concept maps will be deleted, preserving the finalized one.', {
          'positive-callback': function () {
            confirm.modal('hide');
            $('#modal-map-name').modal('show');
            $('#modal-map-name .map-name-input').val('final-m' + mid + '-r' + rid + '-u' + uid + '-' + app.kit.user.username).focus().select();
          },
          'negative-callback': function () {
            confirm.modal('hide');
          }
        })
      } else {
        if (!mid) {
          app.gui.dialog('Please open a material to save the map into.<br>A map should refer to a material.');
          return;
        }
        if (!rid) {
          app.gui.dialog('Please join a room to save the map into.<br>In a collaboration environment, a saved maps should relate with a collaboration group. Therefore, you need to join a room before you create and save a concept map.');
          return;
        }
      }
    }

    $('#popup-map').on('click', '.bt-continue', function () {
      confirmFinalize();
    });

    $('#modal-map-name').on('click', '.bt-ok', function () {
      let mid = app.material.mid;
      let uid = app.kit.user.uid;
      let rid = app.kit.room.rid;
      if (mid != null && uid != null && rid != null) {
        let name = $('#modal-map-name .map-name-input').val().trim()
        if (name == '')
          app.gui.dialog('Concept map name should not be empty.')
        else {
          app.saveGoalmapFinalize(mid, rid, uid, name, function (gmid) {
            $('#modal-map-name').modal('hide');
            app.eventListeners.forEach(listener => {
              if (listener && typeof listener.onAppEvent == 'function') {
                listener.onAppEvent('finalize-map', {
                  gmid: gmid
                });
              }
            })
            app.kit.sendRoomMessage('change-page', null, {
              page: app.postFinalizeMethod
            }, function () {
              app.session.set('page', app.postFinalizeMethod, function () {
                window.location.href = baseUrl + app.controller + "/" + app.postFinalizeMethod;
                return;
              }, function (error) {
                app.gui.dialog('Error: ' + error);
              })
            });
          });
        }
      } else {
        if (!mid) {
          app.gui.dialog('Please open a material to save the map into.<br>A map should refer to a material.');
          return;
        }
        if (!rid) {
          app.gui.dialog('Please join a room to save the map into.<br>In a collaboration environment, a saved maps should relate with a collaboration group. Therefore, you need to join a room before you create and save a concept map.');
          return;
        }
      }
    });

    $('#show-excessive-links').on('change', function () {
      if ($(this).is(':checked')) app.showExcessiveLinks();
      else app.hideExcessiveLinks();
    })
  }

  getPairGoalmaps(rid, mid, callback) {
    let app = this;
    app.ajax.get(baseUrl + 'experimentApi/getPairGoalmaps/' + rid + '/' + mid).then(function (goalmaps) {
      if (typeof callback == 'function') callback(goalmaps)
    });
  }

  getPairworkLearnermaps(goalmaps, callback) {
    let app = this;
    let gmids = [];
    for (let gm of goalmaps)
      gmids.push(gm.gmid); // console.log(gmids);
    app.ajax.post(baseUrl + 'experimentApi/getPairworkLearnermaps', {
      gmids: gmids
    }).then(function (learnermaps) {
      learnermaps.forEach(learnermap => {
        for (let goalmap of app.goalmaps) {
          if (learnermap.gmid == goalmap.gmid) {
            learnermap.compare = app.analyzer.compare(learnermap, goalmap);
            break;
          } else
            continue;
        }
      });
      if(typeof callback == 'function') callback(learnermaps);
    });
  }

  initializeGroupMap(rid, mid) {
    let app = this;
    let canvas = this.canvas;
    app.getPairGoalmaps(rid, mid, function (goalmaps) {
      app.goalmaps = goalmaps;
      canvas.clearCanvas();
      
      let gmaps = [];
      let sumx = 0;
      let sumy = 0;
      goalmaps.forEach(gm => {
        let concepts = gm.concepts;
        for (let c of concepts) {
          let n = {
            group: 'nodes',
            data: {
              id: "c-" + canvas.getConceptId(),
              name: c.label,
              type: 'concept',
              state: '',
              cid: c.cid,
              gmid: gm.gmid
            },
            position: {
              x: parseInt(c.locx),
              y: parseInt(c.locy)
            }
          };
          let node = canvas.addNode(n);
        }
        let links = gm.links;
        for (let l of links) {
          let lid = "l-" + canvas.getConceptId();
          let n = {
            group: 'nodes',
            data: {
              id: lid,
              name: l.label,
              type: 'link',
              state: '',
              lid: l.lid,
              gmid: gm.gmid
            },
            position: {
              x: parseInt(l.locx),
              y: parseInt(l.locy)
            }
          };
          let link = canvas.addNode(n);
          if (l.source && parseInt(l.source) > 0) {
            let selector = '[cid="' + parseInt(l.source) + '"][gmid="' + gm.gmid + '"]'; // console.log(selector);
            let c = (canvas.getCy().nodes(selector));
            let e = {
              group: "edges",
              data: {
                source: lid,
                target: c.id(),
                type: 'left'
              }
            };
            canvas.addEdge(e);
          }
          if (l.target && parseInt(l.target) > 0) {
            let selector = '[cid="' + parseInt(l.target) + '"][gmid="' + gm.gmid + '"]'; // console.log(selector);
            let c = (canvas.getCy().nodes(selector));
            let e = {
              group: "edges",
              data: {
                source: lid,
                target: c.id(),
                type: 'right'
              }
            };
            canvas.addEdge(e);
          }
        }
        let nodes = canvas.getCy().nodes('[gmid="' + gm.gmid + '"]');
        let bb = nodes.boundingBox();
        let gmap = {
          nodes: nodes,
          bb: bb,
          centerx: (bb.x1 + bb.x2) / 2,
          centery: (bb.y1 + bb.y2) / 2
        };
        sumx += gmap.centerx;
        sumy += gmap.centery;
        gmaps.push(gmap);
      });

      // align center-x
      let avgx = sumx/gmaps.length;
      let avgy = sumy/gmaps.length;
      for(let gmap of gmaps) {
        gmap.nodes.shift({
          x: avgx - gmap.centerx,
          y: avgy - gmap.centery
        })
      }

      let index = 0;
      for(let gmap of gmaps) {
        // console.log(index, gmap.nodes);
        gmap.nodes.shift({
          x: (index == 0) ? -gmap.bb.w/2 - 10 : gmap.bb.w/2 + 10
        });
        index++;
      }

      let njsons = canvas.getCy().nodes().jsons();
      let ejsons = canvas.getCy().edges().jsons();
      let eles = [];
      for(let nj of njsons) {
        eles.push({
          group: nj.group,
          data: nj.data,
          position: nj.position
        })
      }
      for(let ej of ejsons) {
        eles.push({
          group: ej.group,
          data: ej.data,
          position: ej.position
        })
      }
      app.kit.sendMap(eles);
      // console.log(njsons, ejsons);
      if(app.grups.indexOf('E1A') >= 0) {
        app.getPairworkLearnermaps(goalmaps, function(learnermaps) {
          app.learnermaps = learnermaps;
          app.hideExcessiveLinks();
          if ($('#show-excessive-links').is(':checked')) app.showExcessiveLinks();
        });
      }
      $('#bt-fit').click();
    });
  }

  attachKit(kit) { // console.log(kit);
    this.kit = kit;
    this.kit.attachEventListener(this);
  }

  onKitEvent(event, data) {
    let gui = this.gui;
    let kit = this.kit;
    let canvas = this.canvas;
    let app = this;
    switch (event) {
      case 'join-room': // console.log(data.room)
        this.session.set('room', data.room);
        this.canvas.getToolbar().enableNodeCreation(true);
        
        let mid = app.material.mid;
        let rid = data.room.rid;
        
        if (data.room.users.length == 1) {
          app.initializeGroupMap(rid, mid);
        } else if (data.room.users.length > 1) {
          kit.syncMap();
          kit.syncMessages();
          app.getPairGoalmaps(rid, mid, function(goalmaps) {
            app.goalmaps = goalmaps;
            app.getPairworkLearnermaps(goalmaps, function(learnermaps){
              app.learnermaps = learnermaps;
              app.hideExcessiveLinks();
              if ($('#show-excessive-links').is(':checked')) app.showExcessiveLinks();
            })
          })
        }
        $('#bt-sync').prop('disabled', false)
        break;
      case 'leave-room':
        this.session.unset('room');
        this.canvas.getToolbar().enableNodeCreation(false);
        break;
      case 'room-update':
        kit.rooms = data;
        break;
        // case 'user-sign-in':
        //   this.session.set('user', data);
        //   break;
        // case 'user-sign-out':
        //   this.session.unset('user');
        //   break;
      case 'command-sign-out':
        this.signOut();
        this.kit.leaveRoom(this.kit.room);
        this.kit.enableRoom(false)
        this.kit.enableMessage(false)
        this.gui.dialog('You have been signed out');
        break;
      case 'command-leave-room':
        // this.signOut();
        // this.kit.leaveRoom(this.kit.room);
        // this.kit.enableRoom(false)
        // this.kit.enableMessage(false)
        // this.gui.dialog('You have been signed out');
        break;
      case 'request-map-data':
        let nodes = this.canvas.getCy().nodes();
        let edges = this.canvas.getCy().edges();
        let elesData = [];
        nodes.forEach(n => {
          elesData.push({
            group: 'nodes',
            data: n.data(),
            position: n.position()
          });
        });
        edges.forEach(n => {
          elesData.push({
            group: 'edges',
            data: n.data(),
            position: n.position()
          });
        });
        return elesData;
      case 'request-material-data':
        return this.material;
      case 'sync-messages-data':
        break;
      case 'sync-map-data': //console.log(data);
        if(data.length == 0) break;
        console.log('replace map');
        canvas.reset();
        canvas.clearCanvas();
        canvas.showMap(data);
        canvas.centerOneToOne();
        app.hideExcessiveLinks();
        if($('#show-excessive-links').is(':checked')) app.showExcessiveLinks();
        gui.notify('Map data synchronized', {
          type: 'success'
        })
        $('#bt-center').click();
        break;
      case 'sync-material-data':
        if (data.mid == null) return;
        this.setMaterial(data.mid, data.name, data.content);
        this.enableMaterial();
        break;
      case 'sync-map-data-error': // console.log('sync-map-data-error received')
        this.gui.notify(data.error, {
          type: 'danger'
        })
        break;
      case 'chat-message-open':
        this.session.set('chat-window-open', data)
        break;
      case 'receive-create-node':
        let empty = (this.canvas.getCy().nodes().length == 0);
        this.canvas.createNodeFromJSON(data.node);
        if (empty) $('#bt-center').click();
        break;
      case 'receive-move-node':
        this.canvas.moveNode(data.node.id, data.node.x, data.node.y);
        break;
      case 'receive-move-node-group': // including from move-drag-group event
        data.nodes.forEach(n => {
          this.canvas.moveNode(n.id, n.x, n.y);
        });
        break;
      case 'receive-connect-node':
        this.canvas.connect(data.connection);
        break;
      case 'receive-disconnect-node':
        this.canvas.disconnect(data.connection);
        break;
      case 'receive-change-connect':
        this.canvas.connect(data.to);
        this.canvas.disconnect(data.from);
        break;
      case 'receive-duplicate-node':
        this.canvas.createNodeFromJSON(data.node);
        break;
      case 'receive-delete-node':
        this.canvas.deleteNode(data.node);
        break;
      case 'receive-delete-node-group':
        this.canvas.deleteNodes(data.nodes);
        break;
      case 'receive-update-node':
        this.canvas.updateNode(data.node.id, data.node.value);
        break;
      case 'receive-center-link':
        this.canvas.moveNode(data.link.id, data.link.x, data.link.y);
        break;
      case 'receive-switch-direction': // data is linkId
        this.canvas.switchDirection(data);
        break;
      case 'change-page':
        this.session.set('page', data.page, function () {
          window.location.href = baseUrl + app.controller + '/' + data.page;
          return;
        }, function (error) {
          this.gui.dialog('Error: ' + error);
        })
        break;


        // Collab Event From Kit

      case 'send-message':
        this.eventListeners.forEach(listener => {
          if (listener && typeof listener.onCollabEvent == 'function') {
            listener.onCollabEvent('send-room-message', data.message);
          }
        })
        break;
      case 'close-discuss-channel':
        this.eventListeners.forEach(listener => {
          if (listener && typeof listener.onCollabEvent == 'function') {
            listener.onCollabEvent(event, data);
          }
        })
        break;
      case 'send-channel-message':
        this.eventListeners.forEach(listener => {
          if (listener && typeof listener.onCollabEvent == 'function') {
            listener.onCollabEvent(event, {
              channel: data.channel,
              message: data.message
            });
          }
        })
        break;
    }
  }

  onCanvasEvent(event, data) {
    // console.log(event, data);
    if (!this.kit) return;
    switch (event) {
      case 'create-concept':
        this.kit.createNode(data);
        break;
      case 'create-link':
        this.kit.createNode(data);
        break;
      case 'drag-concept':
        this.kit.dragNode(data);
        break;
      case 'drag-link':
        this.kit.dragNode(data);
        break;
      case 'drag-node-group':
        this.kit.dragNodeGroup(data);
        break;
      case 'move-concept':
        this.kit.moveNode(data);
        break;
      case 'move-link':
        this.kit.moveNode(data);
        break;
      case 'move-node-group':
        this.kit.moveNodeGroup(data);
        break;
      case 'connect-left':
        this.kit.connectNode(data);
        break;
      case 'connect-right':
        this.kit.connectNode(data);
        break;
      case 'disconnect-left':
        this.kit.disconnectNode(data);
        break;
      case 'disconnect-right':
        this.kit.disconnectNode(data);
        break;
      case 'change-connect-left':
      case 'change-connect-right':
        this.kit.changeConnection(data);
        break;
      case 'duplicate-concept':
      case 'duplicate-link':
        this.kit.duplicateNode(data);
        break;
      case 'delete-node':
      case 'delete-link':
      case 'delete-concept':
        this.kit.deleteNode(data);
        break;
      case 'delete-node-group':
        this.kit.deleteNodeGroup(data);
        break;
      case 'update-concept-name':
      case 'update-link-name':
        this.kit.updateNode(data);
        break;
      case 'center-link':
        this.kit.centerLink(data);
        break;
      case 'switch-direction':
        this.kit.switchDirection(data);
        break;
    }
  }

  onCanvasToolClicked(tool, node) { // console.log(tool, node);

    // for listener purposes
    let action = null;
    let data = null;
    let listenerHandled = false;
    let app = this;

    switch (tool.type) {
      case 'discuss-channel':
        let nodeId = node.id();
        let nodeType = node.data('type');
        let nodeLabel = node.data('name');
        let nodeJson = node.json();
        let extras = {};
        let primaryKeys = ['id','name','type','state']
        for(let key in nodeJson.data) {
          if(primaryKeys.indexOf(key) == -1) extras[key] = nodeJson.data[key];
        }
        // console.log(nodeJson);
        app.kit.openChannelWithExtra(nodeId, nodeType, nodeLabel, extras, function (channel) {
          action = 'open-discuss-channel';
          app.eventListeners.forEach(listener => {
            if (listener && typeof listener.onCollabEvent == 'function') {
              listener.onCollabEvent(action, {
                cid: channel.cid,
                rid: channel.rid,
                node_id: channel.node_id,
                node_type: channel.node_type,
                node_label: channel.node_label,
                extra: extras
              });
            }
          })
        });
        listenerHandled = true;
        break;
      case 'center-link':
      case 'switch-direction':
        listenerHandled = true;
        break;
    }
    if (listenerHandled) return;
    this.eventListeners.forEach(listener => {
      if (listener && typeof listener.onCollabEvent == 'function') {
        listener.onCollabEvent(action, data);
      }
    })
  }


  // Commanding Kit-Build App Interface

  enableMaterial(enabled = true) {
    $('#bt-show-material').prop('disabled', !enabled);
  }

  enableSaveDraft(enable = true) {
    $('#bt-save').prop('disabled', !enable);
    $('#bt-load-draft').prop('disabled', !enable);
  }

  enableFinalize(enable = true) {
    $('#bt-finalize').prop('disabled', !enable);
  }

  loadMaterial(mid, callback) {
    let kitbuild = this;
    kitbuild.ajax.get(
      baseUrl + 'kitbuildApi/getMaterialCollabByMid/' + mid).then(function (material) {
      kitbuild.session.set('mid', material.mid);
      kitbuild.setMaterial(material.mid, material.name, material.content);
      // kitbuild.kit.sendMaterial(material.mid, material.name, material.content);
      $('#modal-kit').modal('hide');
      if (typeof callback == 'function') callback(material);
    })
    $('#bt-show-material').prop('disabled', false);
  }

  setMaterial(mid, name, content) {
    this.material.mid = mid;
    this.material.name = name;
    this.material.content = content;
    $('#popup-material .material-title').html(this.material.name)
    $('#popup-material .material-content').html(this.material.content);
  }

  unsetMaterial() {
    this.material.mid = null;
    this.material.name = null;
    this.material.content = null;
  }

  saveGoalmap(mid, rid, uid, type, name, callback) { // console.log(name);
    let kitbuild = this;
    let goalmaps = {
      name: name ? name : type + '-m' + mid + '-r' + rid + '-u' + uid,
      type: type,
      mid: mid,
      creator_id: uid,
      updater_id: uid,
      concepts: [],
      links: [],
      rid: rid
    }
    let cs = kitbuild.canvas.getNodes('[type="concept"]');
    let ls = kitbuild.canvas.getNodes('[type="link"]');
    let es = kitbuild.canvas.getEdges('[link!="miss"]');
    // console.log(cs, ls, es);
    let primaryKeys = ['id', 'name', 'type', 'state'];
    cs.forEach(c => { //console.log(c)
      let extra = {};
      for (let key in c.data) {
        // console.log(key);
        if (primaryKeys.indexOf(key) == -1) extra[key] = c.data[key];
      }
      goalmaps.concepts.push({
        cid: c.data.id.substr(2),
        gmid: null,
        label: c.data.name,
        locx: c.position.x,
        locy: c.position.y,
        extra: extra
      });
    });
    ls.forEach(l => {
      let extra = {};
      for (let key in l.data) {
        if (primaryKeys.indexOf(key) == -1) extra[key] = l.data[key];
      }
      let tl = {
        lid: l.data.id.substr(2),
        gmid: null,
        label: l.data.name,
        locx: l.position.x,
        locy: l.position.y,
        source: null,
        target: null,
        extra: extra
      };
      es.forEach((e) => {
        if (e.data.source == l.data.id) {
          if (e.data.type == 'right') tl.target = e.data.target.substr(2);
          if (e.data.type == 'left') tl.source = e.data.target.substr(2);
          return;
        }
      });
      goalmaps.links.push(tl);
    })
    // console.log(goalmaps);return;
    kitbuild.ajax.post(
      baseUrl + 'kitbuildApi/saveGoalmapCollab',
      goalmaps
    ).then(function (gmid) {
      let message = type == 'fix' ? 'Map finalized' : 'Map saved';
      if (type != 'auto')
        kitbuild.gui.notify(message, {
          type: 'success'
        })
      kitbuild.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('map-save-' + type, {
            gmid: gmid
          });
        }
      })
      if (callback) callback(gmid)
    })
  }

  saveGoalmapDraft(mid, rid, uid, callback) {
    this.saveGoalmap(mid, rid, uid, 'draft', null, callback)
  }

  saveGoalmapFinalize(mid, rid, uid, name, callback) {
    this.saveGoalmap(mid, rid, uid, 'fix', name, callback)
  }

  saveGoalmapAuto(mid, rid, uid, callback) {
    this.saveGoalmap(mid, rid, uid, 'auto', null, callback)
  }

  checkAvailableGoalmapDraft(mid, rid) {
    let kitbuild = this;
    kitbuild.ajax.get(
      baseUrl + 'kitbuildApi/getLastDraftGoalmap/' + mid + '/' + rid).then(function (gmid) {
      if (gmid) $('#bt-load-draft').prop('disabled', false);
    })
  }

  loadSavedGoalmapDraft(mid, rid, callback) {
    // console.log(mid, uid, rid);
    let kitbuild = this;
    kitbuild.ajax.get(
      baseUrl + 'kitbuildApi/loadLastDraftGoalmapCollab/' + mid + "/" + rid).then(function (draft) {
      if (draft == null) {
        kitbuild.gui.notify('Nothing to load. No previously saved draft maps found.')
        return;
      }
      let goalmap = draft.goalmap;
      let concepts = draft.concepts;
      let links = draft.links;
      let eles = [];
      concepts.forEach((c) => {
        let cn = {
          group: 'nodes',
          data: {
            id: 'c-' + c.cid,
            name: c.label,
            type: 'concept'
          },
          position: {
            x: parseInt(c.locx),
            y: parseInt(c.locy)
          }
        }
        let extras = JSON.parse(c.extra);
        for (let e in extras)
          cn.data[e] = extras[e];
        eles.push(cn);
      })
      links.forEach((l) => {
        let ln = {
          group: 'nodes',
          data: {
            id: 'l-' + l.lid,
            name: l.label,
            type: 'link'
          },
          position: {
            x: parseInt(l.locx),
            y: parseInt(l.locy)
          }
        };
        let extras = JSON.parse(l.extra);
        for (let e in extras)
          ln.data[e] = extras[e];
        eles.push(ln)
        if (l.source != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.source,
              type: 'left'
            }
          })
        }
        if (l.target != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.target,
              type: 'right'
            }
          })
        }
      })
      kitbuild.canvas.clearCanvas();
      kitbuild.canvas.getCy().add(eles);
      kitbuild.gui.notify('Concept map loaded from last saved drafts.')
      kitbuild.kit.sendMap(eles);
      $('#bt-center').click();
      kitbuild.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('load-last-draft', {
            gmid: parseInt(goalmap.gmid)
          });
        }
      })
      if (typeof callback == 'function') callback(draft, eles);
    })

  }

  showExcessiveLinks() {
    let app = this;
    app.learnermaps.forEach(learnermap => {
      learnermap.compare.excess.forEach(ex => { //console.log(ex);
        if (ex.source && parseInt(ex.source) > 0) {
          let selector = '[cid="' + parseInt(ex.source) + '"][gmid="' + ex.gmid + '"]'; // console.log(selector);
          let c = (app.canvas.getCy().nodes(selector))
          selector = '[lid="' + parseInt(ex.lid) + '"][gmid="' + ex.gmid + '"]';
          let l = (app.canvas.getCy().nodes(selector))
          if (l.id() && c.id()) {
            let e = {
              group: "edges",
              data: {
                source: l.id(),
                target: c.id(),
                type: 'left',
                link: 'miss',
                match: 5
              }
            }
            app.canvas.addEdge(e);
          }
        }
        if (ex.target && parseInt(ex.target) > 0) {
          let selector = '[cid="' + parseInt(ex.target) + '"][gmid="' + ex.gmid + '"]'; // console.log(selector);
          let c = (app.canvas.getCy().nodes(selector))
          selector = '[lid="' + parseInt(ex.lid) + '"][gmid="' + ex.gmid + '"]';
          let l = (app.canvas.getCy().nodes(selector))
          if (l.id() && c.id()) {
            let e = {
              group: "edges",
              data: {
                source: l.id(),
                target: c.id(),
                type: 'right',
                link: 'miss',
                match: 5
              }
            }
            app.canvas.addEdge(e);
          }
        }
      });
    });
  }

  hideExcessiveLinks() {
    let app = this;
    app.canvas.getCy().edges('[link="miss"]').remove();
  }

}

jQuery.fn.center = function (parent) {
  parent = parent ? $(parent) : window
  this.css({
    "position": "absolute",
    "top": ((($(parent).height() - this.outerHeight()) / 2) + $(parent).scrollTop() + "px"),
    "left": ((($(parent).width() - this.outerWidth()) / 2) + $(parent).scrollLeft() + "px")
  });
  return this;
}