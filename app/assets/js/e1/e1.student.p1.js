class TopicRoomSelection {

  constructor() {
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.ajax = Ajax.ins(this.gui);
    this.mid = null;
    this.rid = null;
    this.logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null);
    console.log(this.logger);

    this.controller = 'e1';
    this.nextPage = 'read';

    this.handleEvent(this);
  }

  handleEvent(app) {
    $('#list-material').on('click', '.row', function () {
      app.mid = $(this).data('mid');
      $('#list-material .row i').hide();
      $('#list-material .row[data-mid="' + app.mid + '"] i').show();
      $('#bt-continue').attr('data-mid', app.mid);
    })

    $('#list-room').on('click', '.row', function () {
      app.rid = $(this).data('rid');
      let roomName = $(this).find('.room-name').html();
      $('#list-room .row i').hide();
      $('#list-room .row[data-rid="' + app.rid + '"] i').show();
      $('#bt-continue').attr('data-rid', app.rid);
      console.log(uid, app.rid);
      app.ajax.post(baseUrl + 'experimentApi/joinRoom', {
        uid: uid,
        rid: app.rid
      }).then(function(result){
        app.gui.notify("Room '" + roomName + "' joined");
      });
    })

    $('#bt-continue').on('click', function () {
      app.mid = $(this).attr('data-mid');
      app.rid = $(this).attr('data-rid');
      if (app.mid == null) {
        app.gui.dialog("Please select a material topic.", {
          width: '300px'
        })
        return;
      }
      if (app.rid == null) {
        app.gui.dialog("Please select a destined collaboration room.", {
          width: '400px'
        })
        return;
      }
      app.logger.log('begin-read', {
        mid: app.mid,
        rid: app.rid
      });
      app.session.setMulti({
        mid: app.mid,
        rid: app.rid,
        page: app.nextPage,
        seq: app.logger.seq + 1
      }, function () {
        window.location.href = baseUrl + app.controller + '/' + app.nextPage;
      }, function (error) {
        gui.dialog(error, {
          width: '300px'
        })
      })
    });
  }

}

$(function () {
  let topicRoomSelection = new TopicRoomSelection();
});