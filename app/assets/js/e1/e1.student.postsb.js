var BRIDGE = {};

class StudentPostSB {

  constructor() {
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.ajax = Ajax.ins(this.gui);
    this.controller = 'e1'
    this.nextPage = null;
    this.logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null); console.log(this.logger);
    this.user = null;
    this.rid = null;
    this.mid = null;
    
    this.handleEvent(this);
    this.loadUser(uid);
  }

  handleEvent(app) {
    $('#bt-continue').on('click', function () {
      app.nextPage = $(this).data('next');
      let confirm = app.gui.confirm('Lanjutkan proses selanjutnya?', {
        'positive-callback': function () {
          let uid = app.user.uid;
          let rid = app.rid;
          let mid = app.mid;
          if (!rid) {
            app.gui.notify('Invalid Room!')
            return;
          }
          if (!mid) {
            app.gui.notify('Invalid Topic/Material!')
            return;
          }
          app.ajax.get(baseUrl + 'experimentApi/getPairGmid/' + uid + '/' + rid + '/' + mid).then(function (gmid) {
            confirm.modal('hide');
            app.logger.log('begin-kb');
            app.session.setMulti({
              gmid: gmid, // pair gmid
              page: app.nextPage,
              seq: app.logger.seq + 1
            }, function () {
              window.location.href = baseUrl + app.controller + '/' + app.nextPage;
            }, function (error) {
              app.gui.notify('Unable to prepare kit. Failed to set Gmid. ' + error, {
                type: 'danger'
              })
            })
          })
        }
      })
    });
  }

  loadUser(uid) {
    let app = this;
    app.ajax.get(baseUrl + 'kitbuildApi/loadUser/' + uid).then(function (user) {
      app.user = user;
      if (app.user.gids) app.user.gids = app.user.gids.split(",");
      if (app.user.grups) app.user.grups = app.user.grups.split(",");
      // console.log(app.user);
    });
  }

}

$(function () {
  BRIDGE.studentPostSB = new StudentPostSB();
});