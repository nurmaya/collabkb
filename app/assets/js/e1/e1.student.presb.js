var BRIDGE = {};

class StudentPreSB {

  constructor() {
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.controller = 'e1'
    this.nextPage = 'sb'
    this.logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null);
    this.user = null;
    console.log(this.logger);
    this.handleEvent(this);
  }

  handleEvent(app) {

    $('#bt-continue').on('click', function () {
      let confirm = app.gui.confirm('Mulai Kit-Building?', {
        'positive-callback': function () {
          confirm.modal('hide');
          app.session.setMulti({
            page: app.nextPage,
            seq: app.logger.seq + 1
          }, function () {
            window.location.href = baseUrl + app.controller + '/' + app.nextPage;
          }, function (error) {
            app.gui.dialog(error, {
              width: '300px'
            })
          })
        }
      })
    });
  }

}

$(function () {
  BRIDGE.studentPreSB = new StudentPreSB();
});