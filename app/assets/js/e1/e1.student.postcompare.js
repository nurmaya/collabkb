var BRIDGE = {};

class PostCompare {
  constructor(canvas) {
    this.canvas = canvas;
    this.gui = new GUI();
    this.ajax = Ajax.ins(this.gui);
    this.analyzerLib = new AnalyzerLib();
    this.session = new Session(baseUrl);
    this.logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null); console.log(this.logger);
    
    this.controller = 'e1'
    this.nextPage = 'sbcollab'
    this.goalmaps = [];

    this.mid = null;
    this.rid = null;

    this.handleEvent(this);
  }

  getCanvas() {
    return this.canvas;
  }

  handleEvent(app) {

    $('#bt-continue').on('click', function() {
      let confirm = app.gui.confirm("Lanjutkan ke tahap selanjutnya?", {
        'positive-callback': function() {

          let gmids = [];
          app.goalmaps.forEach((gm) => {
            gmids.push(gm.gmid)
          });
          app.logger.log('begin-scratch-collab', {
            gmids: gmids,
            mid: app.mid,
            rid: app.rid,
            uid: uid
          });
          app.session.setMulti({
            page: app.nextPage,
            seq: app.logger.seq + 1 // app.logger.seq + 1
          },function(){
            window.location.href = baseUrl + app.controller + '/' + app.nextPage;
          },function(error){
            app.gui.dialog(error);
          })
          confirm.modal('hide')
        }
      })
    });

  }

  loadPairGoalmaps(rid, mid) {
    let app = this;
    app.ajax.get(baseUrl + 'experimentApi/getPairGoalmaps/' + rid + '/' + mid).then(function(goalmaps){
      app.goalmaps = goalmaps;
      app.canvas.clearCanvas();
      let index = 0;
      let gmaps = [];
      let sumx = 0;
      goalmaps.forEach(gm => {
        let concepts = gm.concepts;
        for (let c of concepts) {
          let n = {
            group: 'nodes',
            data: {
              id: "c-" + app.canvas.getConceptId(),
              name: c.label,
              type: 'concept',
              state: '',
              cid: c.cid,
              gmid: gm.gmid
            },
            position: {
              x: parseInt(c.locx),
              y: parseInt(c.locy)
            }
          };
          let node = app.canvas.addNode(n);
        }
        let links = gm.links;
        for (let l of links) {
          let lid = "l-" + app.canvas.getConceptId();
          let n = {
            group: 'nodes',
            data: {
              id: lid,
              name: l.label,
              type: 'link',
              state: '',
              lid: l.lid,
              gmid: gm.gmid
            },
            position: {
              x: parseInt(l.locx),
              y: parseInt(l.locy)
            }
          };
          let link = app.canvas.addNode(n);
          if (l.source && parseInt(l.source) > 0) {
            let selector = '[cid="' + parseInt(l.source) + '"][gmid="' + gm.gmid + '"]'; // console.log(selector);
            let c = (app.canvas.getCy().nodes(selector));
            let e = {
              group: "edges",
              data: {
                source: lid,
                target: c.id(),
                type: 'left'
              }
            };
            app.canvas.addEdge(e);
          }
          if (l.target && parseInt(l.target) > 0) {
            let selector = '[cid="' + parseInt(l.target) + '"][gmid="' + gm.gmid + '"]'; // console.log(selector);
            let c = (app.canvas.getCy().nodes(selector));
            let e = {
              group: "edges",
              data: {
                source: lid,
                target: c.id(),
                type: 'right'
              }
            };
            app.canvas.addEdge(e);
          }
        }
        let nodes = canvas.getCy().nodes('[gmid="' + gm.gmid + '"]');
        let bb = nodes.boundingBox();
        let gmap = {
          nodes: nodes,
          bb: bb,
          centerx: (bb.x1 + bb.x2) / 2,
          centery: (bb.y1 + bb.y2) / 2
        };
        sumx += gmap.centerx;
        sumy += gmap.centery;
        gmaps.push(gmap);
      });

      let avgx = sumx/gmaps.length;
      let avgy = sumy/gmaps.length;
      for(let gmap of gmaps) {
        gmap.nodes.shift({
          x: avgx - gmap.centerx,
          y: avgy - gmap.centery
        })
      }

      let index = 0;
      for(let gmap of gmaps) {
        console.log(index, gmap.nodes);
        gmap.nodes.shift({
          y: (index == 0) ? -gmap.bb.h/2 - 10 : gmap.bb.h/2 + 10
        });
        index++;
      }

      $('#bt-fit').click();
    });
  }

}

$(function () {

  var canvas = new Canvas('cy', {
    // isDirected: false,
    // enableToolbar: false,
    enableNodeCreation: false,
    // enableConceptCreation: false,
    // enableLinkCreation: false,
    enableUndoRedo: false,
    // enableZoom: false,
    enableAutoLayout: false,
    // enableSaveImage: false,
    enableClearCanvas: false,
    enableNodeTools: false,
    enableNodeModificationTools: false,
    enableConnectionTools: false,
  }).init();

  let app = new PostCompare(canvas);

  // create bridges to server-side javascript session handler 
  BRIDGE.app = app;

});