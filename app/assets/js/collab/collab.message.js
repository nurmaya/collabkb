var Message = function (app, data) {
  if (app != null)
    return {
      socketId: app.getSocketId(),
      userId: app.getUserId(),
      room: app.room,
      message: data
    }
  else return {
    socketId: null,
    userId: null,
    room: null,
    message: data
  }
}