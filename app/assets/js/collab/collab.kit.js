class CollabKit {

  // This class act as a BRIDGE between clients and socket.io server
  // Responsible in communication among clients
  // This class also features a simple single-room chatting app 

  constructor(options) {

    // Default settings
    let defaultSettings = {
      port: 3000
    }

    // Override default settings by options
    this.settings = Object.assign({}, defaultSettings, options);

    // Set collaboration server URL
    let url = new URL(window.location.origin);
    url.port = this.settings.port;

    // Setup socket.io
    var socket = io(url.toString() + 'kit', {
      extraHeaders: {
        SameSite: "Lax",
        Secure: false
      }
    });

    // Client identifiers
    this.socketId = null;
    this.user = {
      uid: null,
      username: null,
      name: null,
      rid: null,
      gids: []
    };
    this.room = {
      rid: null,
      name: null
    };
    this.rooms = [];

    // Get client's socket ID
    let kit = this;
    socket.on('connect', function () { console.log(socket)
      kit.socketId = socket.io.engine.id;
    });
    this.socket = socket;

    // Holder for socket event listeners
    this.eventListeners = [];

    this.gui = (typeof GUI) ? new GUI() : null;

    this.handleEvent();
    this.handleSocketEvent();
    this.getRooms();

    // global variables for chat features
    this.unreadMessageCount = 0;
    this.chatWindowOpen = false;

    // disable room and chat initially
    this.enableRoom(false)
    this.enableMessage(false)
    this.enableCreateRoom(false)
    this.enableLeaveRoom(false)
    this.enableChannel(false)
  }

  attachEventListener(listener) {
    this.eventListeners.push(listener);
  }

  attachGUI(gui) {
    // Provide access to standard global UI features, such as notification 
    this.gui = gui;
  }

  setUser(uid, username, rid, gids) {
    return this.setUserWithName(uid, username, username, rid, gids);
  }

  setUserWithName(uid, username, name, rid, gids, page = null) {
    this.user = {
      uid: uid,
      username: username,
      name: name ? name : username,
      role_id: rid,
      gids: gids,
      page: page
    }
    this.socket.emit('set-user', this.user, this.onSetUserCallback);
    this.eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('user-sign-in', this.user);
      }
    });
    return this;
  }

  unsetUser() {
    this.socket.emit('unset-user', this.user, this.onSetUserCallback);
    this.eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('user-sign-out', this.user);
      }
    });
    this.user = {
      uid: null,
      username: null,
      name: null,
      rid: null,
      gids: []
    };
    return this;
  }

  onSetUserCallback(data) {
    //console.log(this);
    //console.log(data);
  }

  setRoom(rid, name) {
    this.room.rid = rid;
    this.room.name = name;
  }

  unsetRoom() {
    this.room.rid = null;
    this.room.name = null;
  }

  handleEvent() {

    let kit = this;
    let eventListeners = kit.eventListeners;
    $('.popup').hide();

    $(window).on('resize', function (e) { // console.log('resize', $('#bt-room').offset());
      kit.movePopupChat();
      kit.movePopupRooms();
      kit.movePopupChannel();
      kit.movePopupUsers();
    });

    $('#bt-room').on('click', function (e) {
      console.log('click');
      $('.popup').not('#popup-users').hide()
      if (!$('#popup-users').is(':visible')) kit.getUsers();
      else $('#popup-users').hide();
    });

    $('#bt-channel').on('click', function (e) {
      $('.popup').not('#popup-channel').hide()
      $('#popup-channel').fadeToggle({
        duration: 100,
        start: kit.movePopupChannel, // callback
        complete: function () {
          if ($(this).is(':visible')) {
            kit.updateChannelList(kit.channelTool.indicators);
            eventListeners.forEach(listener => {
              if (listener && typeof listener.onKitEvent == 'function') {
                listener.onKitEvent('update-channel-list', {
                  indicators: kit.channelTool.indicators
                });
              }
            });
          }
        }
      });
    });

    $('#bt-message').on('click', function (e) { // console.log('clicked');
      if (!kit.room.rid) {
        if (kit.gui) kit.gui.notify('Please enter or create a room to send and receive messages', {
          type: 'warning'
        });
        return;
      }
      $('#room-name').html('<i class="fas fa-comment text-info"></i> &nbsp;' + kit.room.name);
      $('.popup').not('#popup-chat').hide()
      kit.movePopupChat();
      $('#popup-chat').fadeToggle(100, function () {
        kit.chatWindowOpen = $(this).is(':visible');
        if (kit.chatWindowOpen) {
          $('#chat-message-input').focus();
          kit.scrollMessageContainer();
          kit.loadMessages(kit.room.rid);
        }
        eventListeners.forEach(listener => {
          if (listener && typeof listener.onKitEvent == 'function') {
            listener.onKitEvent('chat-message-open', kit.chatWindowOpen);
          }
        });
      });
      $(this).html('Message'); // clear any unread message indicator
    })

    $('#bt-create-room').on('click', function () { // console.log('create-room')
      $('#modal-create-room').modal('show');
      $('#input-room-name').val('').focus();
    })

    // $('#modal-create-room').on('click', '.bt-ok', function () {
    //   let name = $('#input-room-name').val();
    //   if (name.trim().length > 0) {
    //     kit.joinRoom(name);
    //     $('#popup-rooms').fadeOut(100);
    //     $('#bt-message').trigger('click');
    //   }
    // });

    // $('#input-room-name').on('keyup', function (e) {
    //   if (e.keyCode === 13) {
    //     let name = $(this).val();
    //     if (name.trim().length > 0) {
    //       kit.joinRoom(name);
    //     }
    //   }
    // });

    $('#room-list').on('click', '.room', function () {
      let name = $(this).data('name');
      let rid = $(this).data('rid');
      if (!rid) return;
      if (rid == kit.room.rid) {
        if (kit.gui) kit.gui.notify('You already in room: ' + name + '. To sync data from others in room, please click \'Sync\' button.');
        return;
      }
      if (kit.gui) {
        let joinDialog = kit.gui.confirm('Enter room: ' + name + '?<br>Any maps created and messages will be cleared. Continue?', {
          'positive-callback': function () {
            kit.joinRoom({
              rid: rid,
              name: name
            });
            joinDialog.modal('hide')
          }
        })
      } else kit.joinRoom({
        rid: uid,
        name: name
      });
    });

    $('#bt-leave-room').on('click', function () {
      if (kit.gui) {
        let joinDialog = kit.gui.confirm('Leave room: ' + kit.room.name + '?<br>You will stop receiving updates from this room.', {
          'positive-callback': function () {
            kit.leaveRoom(kit.room);
            joinDialog.modal('hide')
          }
        })
      } else kit.leaveRoom(kit.room);
    });

    $('#chat-message-input').on('keyup', function (e) {
      let input = $(this);
      let m = input.val();
      if (e.keyCode === 13) {
        if (m.trim().length) kit.sendMessage(m.trim());
      } else {
        kit.isTyping(kit.user);
      }
    });

    $('#bt-close-popup-chat').on('click', function () {
      $('#popup-chat').fadeOut(100);
      kit.chatWindowOpen = false;
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('chat-message-open', kit.chatWindowOpen);
        }
      });
    });

    // Channel

    $('#modal-channel').on('keyup', '.channel-message-input', function (e) {
      let input = $(this);
      let m = input.val();
      let cid = $(this).closest('#modal-channel').attr('data-cid');
      let node_id = $(this).closest('#modal-channel').attr('data-node_id');
      let node_type = $(this).closest('#modal-channel').attr('data-node_type');
      // console.log(cid, node_id, node_type);
      if (e.keyCode === 13) {
        if (m.trim().length) kit.sendChannelMessage(m.trim(), {
          // cid: cid,
          // node_id: node_id,
          // node_type: node_type,
          cid: $('#modal-channel').attr('data-cid'),
          rid: $('#modal-channel').attr('data-rid'),
          node_id: $('#modal-channel').attr('data-node_id'),
          node_type: $('#modal-channel').attr('data-node_type'),
          node_label: $('#modal-channel').attr('data-node_label')
        });
      } else {
        kit.isTypingInChannel(cid, node_id, node_type);
      }
    })

    $('#channel-list').on('click', '.list-item', function () {
      let id = $(this).data('id');
      console.log(id);
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('channel-item-clicked', id);
        }
      });
    });

  }

  handleSocketEvent() {

    let connectionOK = function (isOK = true, e = 'Connection OK') {
      if (isOK) {
        $('#bt-error').addClass('btn-outline-secondary').removeClass('btn-warning').prop('title', e);
        $('#bt-error i').addClass('fa-check').removeClass('fa-exclamation-triangle');
      } else {
        $('#bt-error').removeClass('btn-outline-secondary').addClass('btn-warning').prop('title', e);
        $('#bt-error i').removeClass('fa-check').addClass('fa-exclamation-triangle')
      }
      $('[data-toggle="tooltip"]').tooltip()
    }

    let kit = this;
    let socket = this.socket;
    let eventListeners = this.eventListeners;

    socket.on('error', (e) => {
      console.log(e)
      $('#bt-error').removeClass('btn-outline-secondary').addClass('btn-warning');
      $('#bt-error i').removeClass('fas-check').addClass('fas-exclamation-triangle');
    });

    socket.on('connect_error', (e) => {
      connectionOK(false, e)
    });

    socket.on('pong', (latency) => {
      console.log('pong', latency)
      connectionOK();
    });

    socket.on('reconnect', function (attemptNumber) {
      console.log('reconnect', attemptNumber, socket.io.engine.id, kit.user)
      kit.socketId = socket.io.engine.id;
      if (kit.room.rid != null) kit.joinRoom(kit.room);
      if (kit.user.uid != null) kit.setUserWithName(kit.user.uid, kit.user.username, kit.user.name, kit.user.rid, kit.user.gids)
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('reconnect', attemptNumber);
        }
      });
      connectionOK();
    });

    socket.on('room-update', function (rooms) {
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('room-update', rooms);
        }
      });
      // if (!$('#popup-rooms').is(':visible')) {
      //   $('#bt-room')
      //     .addClass('animated flash')
      //     .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
      //       $(this).removeClass('animated flash');
      //     });
      // }
      kit.updateRooms(rooms);
    });

    socket.on('room-users-update', function (roomUsers) {
      kit.roomUsers = roomUsers;
      if ($('#popup-users').is(':visible')) {
        let userList = '';
        roomUsers.forEach(roomUser => {
          // console.log(roomUser);
          userList += '<div class="row list-item" data-key="' + roomUser.socketId + '" data-username="' + roomUser.username + '" data-uid="' + roomUser.uid + '"><small>' + roomUser.name + '</small></div>';
        })
        $('#user-list').html(userList);
      }
    });

    socket.on('user-join-info', function (user) {
      kit.appendInfoMessage(user.username + " joined.")
    })

    socket.on('user-leave-info', function (user) {
      kit.appendInfoMessage(user.username + " left the room.")
    })

    socket.on('command-sign-out', function () {
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('command-sign-out');
        }
      });
    })

    socket.on('command-join-room', function (room) {
      kit.room.name = room.name;
      $('#modal-create-room').modal('hide');
      $('#bt-room').html('Room: ' + room.name);
      $('#room-name').html('<i class="fas fa-comment text-info"></i> &nbsp;' + kit.room.name);
      $('#popup-rooms').fadeOut(100);
      if (kit.chatWindowOpen) $('#bt-message').trigger('click');

      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('command-join-room');
        }
      });
    })

    socket.on('command-leave-room', function () {
      kit.room.rid = null;
      kit.room.name = null;
      kit.enableLeaveRoom(kit.room.rid)
      $('.pop-up').hide();
      $('#bt-room').html('Room');
      $('#room-name').html('<i class="fas fa-comment text-info"></i> &nbsp;');
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('command-leave-room');
        }
      });
    })

    socket.on('user-update', function (users) { // console.log(users)
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('user-update', users);
        }
      });
      kit.updateUsers(users);
    });

    socket.on('is-typing', function (user) {
      kit.appendIsTyping(user);
    })

    socket.on('is-typing-channel', function (data) {
      kit.appendIsTypingChannel(data.user, data.channel);
    })

    socket.on('chat-message', function (data) {
      let message = data.message;
      let sender = data.sender == null ? data.socketId : data.sender;
      kit.appendMessage(message, {
        from: 'other',
        sender: sender
      });
      kit.updateUnreadMessageCount();
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('chat-message', data);
        }
      });
    });

    socket.on('chat-channel-message', function (data) {
      let message = data.message;
      let sender = data.sender == null ? data.socketId : data.sender;
      // console.log($('#modal-channel[data-cid="' + data.channel.cid + '"]'),
      // $('#modal-channel').data('cid'), $('#modal-channel').attr('data-cid'))
      kit.appendChannelMessage(message, {
        from: 'other',
        sender: sender,
        channel: data.channel
      });
      let currentDisplayedChannelId = $('#modal-channel').attr('data-cid');
      if (currentDisplayedChannelId != data.channel.cid || !$('#modal-channel').is(':visible')) {
        kit.updateUnreadChannelMessageIndicator(data.channel);
        kit.updateChannelList(kit.channelTool.indicators);
      }

      let popupChannel = $('#popup-channel');
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') { // console.log('B')
          listener.onKitEvent('update-channel-list', {
            indicators: kit.channelTool.indicators,
            message: data
          });
        }
      });
      // console.log(kit.channelTool.indicators);
      kit.updateChannelList(kit.channelTool.indicators);
      kit.updateChannelButtonBadge();

      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('chat-message', data);
        }
      });
    });

    socket.on('request-map-data', function (data, callback) {
      console.log('request-map-data received');
      // console.log(data)
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          // let kit-build app know that map data are requested
          let mapData = listener.onKitEvent('request-map-data');
          callback(mapData); // send back map data to server
        }
      });
    });

    socket.on('request-material-data', function (data, callback) {
      console.log('request-material-data received');
      // console.log(data)
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          // let kit-build app know that map data are requested
          let materialData = listener.onKitEvent('request-material-data');
          callback(materialData); // send back map data to server
        }
      });
    });

    socket.on('request-goalmap-data', function (data, callback) {
      console.log('request-goalmap-data received');
      // console.log(data)
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          // let kit-build app know that map data are requested
          let goalmapData = listener.onKitEvent('request-goalmap-data');
          // console.log(goalmapData)
          callback(goalmapData); // send back map data to server
        }
      });
    });

    socket.on('sync-messages-data', function (data) {
      console.log('sync-messages-data received');
      $('#chat-message-container').html('')
      data.forEach((m) => {
        kit.appendMessage(m.message, {
          from: m.uid == kit.user.uid ? 'self' : 'other',
          sender: {
            uid: m.uid,
            username: m.username
          },
          doScroll: false
        });
      })
      kit.scrollMessageContainer();
      $('#bt-message')
        .addClass('animated flash')
        .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
          $(this).removeClass('animated flash');
        });
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('sync-messages-data', data);
        }
      });
    })

    socket.on('sync-map-data', function (data) {
      console.log('sync-map-data received');
      //console.log(data)
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('sync-map-data', data);
        }
      });
    })

    socket.on('sync-material-data', function (data) {
      console.log('sync-material-data received');
      //console.log(data)
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('sync-material-data', data);
        }
      });
    })

    socket.on('sync-goalmap-data', function (goalmap) {
      console.log('sync-goalmap-data received');
      //console.log(goalmap)
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('sync-goalmap-data', goalmap);
        }
      });
    })

    socket.on('sync-map-data-error', function (data) {
      console.log('sync-map-data-error received');
      // console.log(data)
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('sync-map-data-error', data); // { error: 'message' }
        }
      });
    })

    socket.on('create-node', function (data) {
      console.log('create-node received');
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('receive-create-node', data);
        }
      });
    });

    socket.on('move-node', function (data) {
      console.log('move-node received');
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('receive-move-node', data);
        }
      });
    });

    socket.on('move-node-group', function (data) {
      console.log('move-node-group received');
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('receive-move-node-group', data);
        }
      });
    });

    socket.on('connect-node', function (data) {
      console.log('connect-node received');
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('receive-connect-node', data);
        }
      });
    });

    socket.on('disconnect-node', function (data) {
      console.log('disconnect-node received');
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('receive-disconnect-node', data);
        }
      });
    });

    socket.on('change-connect', function (fromTo) {
      console.log('change-connect received');
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('receive-change-connect', fromTo);
        }
      });
    });

    socket.on('duplicate-node', function (data) {
      console.log('duplicate-node received');
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('receive-duplicate-node', data);
        }
      });
    });

    socket.on('delete-node', function (data) {
      console.log('delete-node received');
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('receive-delete-node', data);
        }
      });
    });

    socket.on('delete-node-group', function (data) {
      console.log('delete-node-group received');
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('receive-delete-node-group', data);
        }
      });
    });

    socket.on('update-node', function (data) {
      console.log('update-node received');
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('receive-update-node', data);
        }
      });
    });

    socket.on('center-link', function (data) {
      console.log('center-link received');
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('receive-center-link', data);
        }
      });
    });

    socket.on('switch-direction', function (data) {
      console.log('switch-direction received');
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('receive-switch-direction', data.linkId);
        }
      });
    });


    // Other functionalities

    socket.on('change-page', function (data) {
      console.log('change-page received');
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('change-page', data);
        }
      });
    })

    socket.on('session-set', function (data) {
      console.log('session-set received');
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('session-set', data);
        }
      });
    })

    socket.on('admin-message', function(data) { // { message, options }
      console.log('admin-message-received', data);
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('admin-message', data);
        }
      });
    })

    socket.on('send-command', function(data) { // { message, options }
      console.log('send-command-received', data);
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent(data.command, data.data);
        }
      });
    })

    socket.on('send-feedback', function(data){
      console.log('send-feedback-received', data);
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('send-feedback', data? data.data : null);
        }
      });
    })

  }

  getSessionId() {
    return /SESS\w*ID=([^;]+)/i.test(document.cookie) ? RegExp.$1 : false;
  }

  // Kit Request Action

  syncMap() {

    let kit = this;
    let socket = this.socket;
    let eventListeners = this.eventListeners;
    socket.emit('sync-map', {
      room: kit.room,
    }, function (status, reply) {
      // console.log(status);
    });
    eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('on-sync-map-request', {
          socketId: socket.id,
          room: kit.room,
          // message: m
        });
      }
    });
  }

  syncMessages() {
    let kit = this;
    let socket = this.socket;
    let eventListeners = this.eventListeners;
    socket.emit('sync-messages', {
      room: kit.room,
    }, function (status, reply) {
      // console.log(status);
    });
    eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('on-sync-messages-request', {
          socketId: socket.id,
          room: kit.room
        });
      }
    });
  }

  sendMap(eles) {
    let kit = this;
    let socket = this.socket;
    let eventListeners = this.eventListeners;
    socket.emit('send-map', {
      room: kit.room,
      eles: eles
    }, function (status, reply) {
      // console.log(status);
    });
    eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('on-send-map', {
          socketId: socket.id,
          room: kit.room,
          eles: eles
        });
      }
    });
  }

  sendMaterial(mid, name, content) {
    let kit = this;
    let socket = this.socket;
    let eventListeners = this.eventListeners;
    socket.emit('send-material', {
      room: kit.room,
      material: {
        mid: mid,
        name: name,
        content: content
      }
    }, function (status, reply) {
      // console.log(status);
    });
    eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('on-send-material', {
          socketId: socket.id,
          room: kit.room,
          material: {
            mid: mid,
            name: name,
            content: content
          }
        });
      }
    });
  }

  sendKit(goalmap) {
    let kit = this;
    let socket = this.socket;
    let eventListeners = this.eventListeners;
    socket.emit('send-kit', {
      room: kit.room,
      goalmap: goalmap
    }, function (status, reply) {
      // console.log(status);
    });
    eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('on-send-kit', {
          socketId: socket.id,
          room: kit.room,
          goalmap: goalmap
        });
      }
    });
  };

  // Kit Command Action

  createNode(node) {
    let kit = this;
    let eventListeners = this.eventListeners;
    let data = {
      room: kit.room,
      node: node
    };
    this.socket.emit('create-node', data);
    eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('send-create-node', data);
      }
    });
  }

  dragNode(node) {
    let kit = this;
    let eventListeners = this.eventListeners;
    let data = {
      room: kit.room,
      node: node
    };
    this.socket.emit('drag-node', data);
    eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('send-drag-node', data);
      }
    });
  }

  dragNodeGroup(nodes) {
    let kit = this;
    let eventListeners = this.eventListeners;
    let data = {
      room: kit.room,
      nodes: nodes
    };
    this.socket.emit('drag-node-group', data);
    eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('send-drag-node-group', data);
      }
    });
  }

  moveNode(node) {
    let kit = this;
    let eventListeners = this.eventListeners;
    let data = {
      room: kit.room,
      node: node
    };
    this.socket.emit('move-node', data);
    eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('send-move-node', data);
      }
    });
  }

  moveNodeGroup(nodes) {
    let kit = this;
    let eventListeners = this.eventListeners;
    let data = {
      room: kit.room,
      nodes: nodes
    };
    this.socket.emit('move-node-group', data);
    eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('send-move-node-group', data);
      }
    });
  }

  connectNode(connection) {
    let kit = this;
    let eventListeners = this.eventListeners;
    let data = {
      room: kit.room,
      connection: connection
    };
    this.socket.emit('connect-node', data);
    eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('send-connect-node', data);
      }
    });
  }

  disconnectNode(connection) {
    let kit = this;
    let eventListeners = this.eventListeners;
    let data = {
      room: kit.room,
      connection: connection
    };
    this.socket.emit('disconnect-node', data);
    eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('send-disconnect-node', data);
      }
    });
  }

  duplicateNode(duplicateData) {
    let kit = this;
    let eventListeners = this.eventListeners;
    let data = {
      room: kit.room,
      fromNode: duplicateData.fromNode,
      node: duplicateData.node
    };
    this.socket.emit('duplicate-node', data); // console.log(eventListeners)
    eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('send-duplicate-node', data);
      }
    });
  }

  deleteNode(node) {
    let kit = this;
    let eventListeners = this.eventListeners;
    let data = {
      room: kit.room,
      node: node
    };
    this.socket.emit('delete-node', data); // console.log(eventListeners)
    eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('send-delete-node', data);
      }
    });
  }

  deleteNodeGroup(nodes) {
    let kit = this;
    let eventListeners = this.eventListeners;
    let data = {
      room: kit.room,
      nodes: nodes
    };
    this.socket.emit('delete-node-group', data); // console.log(eventListeners)
    eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('send-delete-node-group', data);
      }
    });
  }

  updateNode(updateData) {
    let kit = this;
    let eventListeners = this.eventListeners;
    let data = {
      room: kit.room,
      node: updateData
    };
    this.socket.emit('update-node', data); // console.log(eventListeners)
    eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('send-update-node', data);
      }
    });
  }

  centerLink(link) {
    let kit = this;
    let eventListeners = this.eventListeners;
    let data = {
      room: kit.room,
      link: link
    };
    this.socket.emit('center-link', data); // console.log(eventListeners)
    eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('send-center-link', data);
      }
    });
  }

  switchDirection(link) {
    // console.log(link);
    let kit = this;
    let eventListeners = this.eventListeners;
    let data = {
      room: kit.room,
      linkId: link.id
    };
    this.socket.emit('switch-direction', data); // console.log(eventListeners)
    eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('send-switch-direction', data);
      }
    });
  }

  changeConnection(fromTo) {
    // console.log(link);
    let kit = this;
    let eventListeners = this.eventListeners;
    let data = {
      room: kit.room,
      fromTo: fromTo
    };
    this.socket.emit('change-connect', data); // console.log(eventListeners)
    eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('change-connect', data);
      }
    });
  }


  // Chat Command to Server Action

  getUsers() {
    let kit = this;
    this.socket.emit('get-room-users', {
      room: kit.room
    }, function (roomUsers) {
      this.roomUsers = roomUsers;
      kit.showRoomUsers(this.roomUsers);
    });
  }

  getRooms() {
    this.socket.emit('get-rooms', '');
  }

  loadRoom(rid, callback) {
    let kitbuild = this;
    $.ajax({
      url: baseUrl + 'kitbuildApi/getRoomByRid/' + rid,
      method: 'get'
    }).done(function (response) {
      if (!response.status) {
        kitbuild.gui.notify(response.error ? response.error : response, {
          type: 'danger'
        });
        return;
      }
      let room = response.result; // console.log(response)
      kitbuild.setRoom(room.rid, room.name);
      $('#modal-kit').modal('hide');
      if (typeof callback == 'function') callback(room);
    }).fail(function (response) {
      kitbuild.gui.notify(response, {
        type: 'danger'
      });
    })
    // $('#bt-show-room').prop('disabled', false);
  }

  joinRoom(room) {
    let kit = this;
    let eventListeners = this.eventListeners;

    kit.room.rid = room.rid;
    this.socket.emit('join-room', {
        room: room,
        sessionId: kit.getSessionId(),
        user: kit.user
      },
      function (status, rooms) { // callback reply from server
        // console.log(status, rooms);
        if (status) {
          kit.room.rid = room.rid;
          kit.room.name = room.name;
          kit.enableLeaveRoom(kit.room.rid);
          for (let r of rooms) {
            if (r.rid == room.rid) {
              room.users = r.users;
              break;
            }
          }
          $('#modal-create-room').modal('hide');
          $('#bt-room').html('Room: ' + room.name);
          $('#room-name').html('<i class="fas fa-comment text-info"></i> &nbsp;' + kit.room.name);
          $('#popup-rooms').fadeOut(100);
          if (kit.chatWindowOpen) $('#bt-message').trigger('click');
        }
        eventListeners.forEach(listener => {
          if (listener && typeof listener.onKitEvent == 'function') {
            listener.onKitEvent('join-room', {
              room: room,
              status: status
            });
          }
        });
        // if(typeof(callback) != 'undefined') callback(status);
      });
  }

  leaveRoom(room) {
    let kit = this;
    let eventListeners = this.eventListeners;
    this.socket.emit('leave-room', {
        room: room,
        sessionId: kit.getSessionId(),
        user: kit.user
      },
      function (status, message) { // callback reply from server
        // console.log(status, message);
        if (status) {
          kit.room.rid = null;
          kit.room.name = null;
          kit.enableLeaveRoom(kit.room.rid)
          $('.pop-up').hide();
          $('#bt-room').html('Room');
          $('#room-name').html('<i class="fas fa-comment text-info"></i> &nbsp;');
        }
        eventListeners.forEach(listener => {
          if (listener && typeof listener.onKitEvent == 'function') {
            listener.onKitEvent('leave-room', {
              name: name,
              status: status,
              message: message
            });
          }
        });
        // if(typeof(callback) != 'undefined') callback(status);
      });
  }

  isTyping(user) {
    this.socket.emit('is-typing', {
      room: this.room,
      user: user
    });
  }

  isTypingInChannel(cid, node_id, node_type) {
    this.socket.emit('is-typing-channel', {
      room: this.room,
      user: this.user,
      channel: {
        cid: cid,
        node_id: node_id,
        node_type: node_type
      }
    })
  }

  sendMessage(m) {
    let kit = this;
    let socket = this.socket;
    let eventListeners = this.eventListeners;
    socket.emit('chat-message', {
      room: kit.room,
      message: m,
      uid: kit.user.uid
    }, function (status, reply) {
      if (status) {
        kit.appendMessage(m, {
          from: 'self'
        });
        $('#chat-message-input').val('');
      } else kit.appendInfoMessage('Server error: ' + reply);
    });
    eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('send-message', {
          socketId: socket.id,
          room: kit.room,
          message: m
        });
      }
    });
  }

  sendChannelMessage(m, channel) {
    let kit = this;
    let socket = this.socket;
    let eventListeners = this.eventListeners;
    socket.emit('chat-channel-message', {
      room: kit.room,
      user: kit.user,
      channel: channel,
      message: m
    }, function (status, reply) {
      if (status) {
        kit.appendChannelMessage(m, {
          from: 'self'
        });
        $('.channel-message-input').val('');
      } else kit.appendInfoMessage('Server error: ' + reply);
    });
    eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('send-channel-message', {
          socketId: socket.id,
          room: kit.room,
          message: m,
          channel: channel
        });
      }
    });
  }

  // Chat UI

  enableMessage(enabled = true) {
    $('#bt-message').prop('disabled', !enabled);
    $('#chat-message-input').prop('disabled', !enabled);
  }

  enableChannel(enabled = true) {
    $('#bt-channel').prop('disabled', !enabled);
  }

  enableRoom(enabled = true) {
    $('#bt-room').prop('disabled', !enabled);
  }

  enableCreateRoom(enabled = true) {
    $('#bt-create-room').prop('disabled', !enabled);
  }

  enableLeaveRoom(enabled = true) {
    $('#bt-leave-room').prop('disabled', !enabled);
  }

  updateRooms(rooms) {
    // console.log(rooms)
    let count = 0;
    let kit = this;
    kit.rooms = rooms;
    if (rooms.length) {
      let roomList = '';
      if (kit.user.uid) {
        rooms.forEach(room => { // console.log(room);
          if (room.name.match(/\/kit#.+/)) return;
          if (!room.gid) return;
          if (kit.user.gids.indexOf(room.gid.toString()) == -1) return;
          let active = (room.name == kit.room.name) ? ' text-white bg-primary' : ' text-secondary';
          roomList += '<div class="room' + active + '" data-rid="' + room.rid + '" data-name="' + room.name + '" data-gid="' + room.gid + '">';
          roomList += room.name + ' &nbsp;<span class="badge-pill badge-info">' + room.users.length + '</span> ';
          if (room.name == kit.room)
            roomList += ' &nbsp; <i class="fas fa-check text-success"></i>';
          roomList += '</div>';
          count++;
        });
      }
      $('#room-list').html(roomList);
    }
    if (!count) {
      let emptyLabel = '<li class="room text-secondary text-sm">' +
        '<em>No room available</em>' +
        '</li>';
      $('#room-list').html(emptyLabel);
    }
    if ($('#popup-rooms').is(':visible')) {
      let h = $('#popup-rooms').outerHeight();
      let w = $('#popup-rooms').outerWidth();
      $('#popup-rooms').css('top', $('#bt-room').offset().top - h - 6);
      $('#popup-rooms').css('left', $('#bt-room').offset().left + $('#bt-room').outerWidth() - w);
      $('#popup-rooms').show();
    }
  }

  updateUsers(users) {
    for (let [key, value] of Object.entries(users)) { // console.log(`${key}: ${value}`);
      let user = users[key]
      $('#chat-message-container .sender:contains(' + user.socketId + ')').html(user.username);
    }
    // console.log(users);
  }

  openChatWindow() {
    $('#bt-message').trigger('click')
  }

  loadMessages(rid) {
    let kit = this;
    $.ajax({
      url: baseUrl + 'channelApi/getMessages/' + rid,
      method: 'post',
      data: {
        rid: rid
      }
    }).done(function (response) {
      console.log(response)
      if (!response.status) {
        kit.gui.notify(response.error ? response.error : response, {
          type: 'danger',
          delay: 0
        });
        return;
      }
      let messages = response.result;
      $('#chat-message-container').html('');
      console.warn(kit);
      for(let message of messages) { console.log(message)
        kit.appendMessage(message.message, {
          sender: {
            uid: message.uid,
            username: message.username
          },
          from: kit.user.uid == message.uid ? 'self' : 'other'
        });
      }
      // let channel = response.result;
      // kit.channelTool.clearChannelIndicator(channel);
      // kit.updateChannelButtonBadge();
      // eventListeners.forEach(listener => {
      //   if (listener && typeof listener.onKitEvent == 'function') {
      //     listener.onKitEvent('update-channel-list', {
      //       indicators: kit.channelTool.indicators
      //     });
      //     kit.updateChannelList(kit.channelTool.indicators);
      //   }
      // });
      // // console.log(channel.cid, $('#modal-channel').attr('data-cid'))
      // if ($('#modal-channel').attr('data-cid') != channel.cid) {
      //   $('#modal-channel .channel-message-input').html('');
      //   $('#modal-channel .channel-message-container').html('<em class="text-secondary mt-2 mb-2">loading channel messages...</em>');
      // }
      // $('#modal-channel').attr('data-cid', channel.cid);
      // $('#modal-channel').attr('data-rid', channel.rid);
      // $('#modal-channel').attr('data-node_id', channel.node_id);
      // $('#modal-channel').attr('data-node_type', channel.node_type);
      // $('#modal-channel').attr('data-node_label', channel.node_label);
      // $('#modal-channel .channel-name').html(nodeLabel + '<br><small class="text-secondary text-smaller"><em>#c' + channel.cid + "-r" + channel.rid + '-' + channel.node_type + '-' + channel.node_id + '-' + channel.node_label + '</em></small>');
      // if (callback && typeof callback == 'function') callback(channel);
      // $('#modal-channel').fadeIn({
      //   duration: 100,
      //   complete: function () {
      //     $('#modal-channel .channel-message-input').focus();
      //     $('#modal-channel .channel-message-container').html('');
      //     kit.loadChannelMessages(channel.messages);
      //   }
      // }).draggable({
      //   handle: '.modal-header'
      // }).resizable({
      //   maxWidth: 500,
      //   maxHeight: 700
      // }).center().center().css('top', '8em');
      // $('#modal-channel .bt-close').off('click').on('click', function () {
      //   $(this).closest('#modal-channel').hide();
      //   kit.eventListeners.forEach(listener => {
      //     if (listener && typeof listener.onKitEvent == 'function') {
      //       listener.onKitEvent('close-discuss-channel', {
      //         cid: $('#modal-channel').attr('data-cid'),
      //         rid: $('#modal-channel').attr('data-rid'),
      //         node_id: $('#modal-channel').attr('data-node_id'),
      //         node_type: $('#modal-channel').attr('data-node_type'),
      //         node_label: $('#modal-channel').attr('data-node_label')
      //       });
      //     }
      //   });
      // });
      // console.log($('#modal-channel[data-cid="' + channel.cid + '"]'));
    }).fail(function (response) { // console.log(response)
      kit.gui.notify(response.responseText, {
        type: 'danger',
        delay: 0
      });
    })
  }

  appendInfoMessage(message) {
    $('#chat-message-container').append('<p class="server"><span class="wrap">' + message + '</span></p>');
    this.scrollMessageContainer();
  }

  appendIsTyping(user) {
    let isTypingIndicator = $('#chat-message-container').find('.is-typing[data-uid="' + user.uid + '"]');
    if (this.clearId) clearInterval(this.clearId);
    if (isTypingIndicator.length == 0) {
      let typingText = '<div class="is-typing mt-2 mb-2 ml-3 mr-3" data-uid="' + user.uid + '" data-time="' + (new Date().getTime()) + '">'
      typingText += '<div class="wave mr-2">'
      typingText += '<span class="dot"></span>'
      typingText += '<span class="dot"></span>'
      typingText += '<span class="dot"></span>'
      typingText += '</div>'
      typingText += '<em>' + user.username + ' is typing... </em></div>';
      $('#chat-message-container').append(typingText);
      this.scrollMessageContainer();
    }
    isTypingIndicator = $('#chat-message-container').find('.is-typing[data-uid="' + user.uid + '"]');
    this.clearId = setTimeout(function () {
      $(isTypingIndicator).remove();
    }, 5000)
  }

  appendMessage(message, options) {
    let settings = Object.assign({}, {
      from: 'other',
      doScroll: true
    }, options);
    if (settings.from == 'self')
      $('#chat-message-container').append('<p class="me"><span class="wrap">' + message + '</span></p>');
    else if (settings.from == 'other') {
      if (this.clearId) clearInterval(this.clearId);
      let isTypingIndicator = $('#chat-message-container').children('.is-typing[data-uid="' + options.sender.uid + '"]');
      if (isTypingIndicator.length > 0) $(isTypingIndicator).remove();
      $('#chat-message-container').append('<p class="other"><span class="wrap"><span class="sender text-info">' + settings.sender.username + '</span><br>' + message + '</span></p>');
    }
    if (settings.doScroll) this.scrollMessageContainer();
  }

  scrollMessageContainer() {
    $('#chat-container').animate({
      scrollTop: $('#chat-message-container').height()
    });
  }

  updateUnreadMessageCount() {
    if ($('#popup-chat').is(':hidden')) {
      this.unreadMessageCount++;
      $('#bt-message').html('Message <span class="badge-pill badge-danger">&nbsp;' + this.unreadMessageCount + '&nbsp;</span>')
        .addClass('animated flash')
        .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
          $(this).removeClass('animated flash');
        });
    }
  }

  // Channel Discussion

  attachChannelTool(channelTool) {
    this.channelTool = channelTool;
  }

  openChannel(nodeId, nodeType, nodeLabel, callback) {
    this.openChannelWithExtra(nodeId, nodeType, nodeLabel, null, callback);
  }

  openChannelWithExtra(nodeId, nodeType, nodeLabel, extra, callback) {
    let rid = this.room.rid;
    let kit = this;
    let eventListeners = kit.eventListeners;
    $.ajax({
      url: baseUrl + 'channelApi/open',
      method: 'post',
      data: {
        name: 'channel-r' + rid + '-' + nodeType + '-' + nodeId + '-' + nodeLabel,
        rid: rid,
        node_id: nodeId,
        node_type: nodeType,
        node_label: nodeLabel,
        extra: extra
      }
    }).done(function (response) {
      // console.log(response)
      if (!response.status) {
        kit.gui.notify(response.error ? response.error : response, {
          type: 'danger',
          delay: 0
        });
        return;
      }
      let channel = response.result;
      kit.channelTool.clearChannelIndicator(channel);
      kit.updateChannelButtonBadge();
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('update-channel-list', {
            indicators: kit.channelTool.indicators
          });
          kit.updateChannelList(kit.channelTool.indicators);
        }
      });
      // console.log(channel.cid, $('#modal-channel').attr('data-cid'))
      if ($('#modal-channel').attr('data-cid') != channel.cid) {
        $('#modal-channel .channel-message-input').html('');
        $('#modal-channel .channel-message-container').html('<em class="text-secondary mt-2 mb-2">loading channel messages...</em>');
      }
      $('#modal-channel').attr('data-cid', channel.cid);
      $('#modal-channel').attr('data-rid', channel.rid);
      $('#modal-channel').attr('data-node_id', channel.node_id);
      $('#modal-channel').attr('data-node_type', channel.node_type);
      $('#modal-channel').attr('data-node_label', channel.node_label);
      $('#modal-channel .channel-name').html(nodeLabel + '<br><small class="text-secondary text-smaller"><em>#c' + channel.cid + "-r" + channel.rid + '-' + channel.node_type + '-' + channel.node_id + '-' + channel.node_label + '</em></small>');
      if (callback && typeof callback == 'function') callback(channel);
      $('#modal-channel').fadeIn({
        duration: 100,
        complete: function () {
          $('#modal-channel .channel-message-input').focus();
          $('#modal-channel .channel-message-container').html('');
          kit.loadChannelMessages(channel.messages);
        }
      }).draggable({
        handle: '.modal-header'
      }).resizable({
        maxWidth: 500,
        maxHeight: 700
      }).center().center().css('top', '8em');
      $('#modal-channel .bt-close').off('click').on('click', function () {
        $(this).closest('#modal-channel').hide();
        kit.eventListeners.forEach(listener => {
          if (listener && typeof listener.onKitEvent == 'function') {
            listener.onKitEvent('close-discuss-channel', {
              cid: $('#modal-channel').attr('data-cid'),
              rid: $('#modal-channel').attr('data-rid'),
              node_id: $('#modal-channel').attr('data-node_id'),
              node_type: $('#modal-channel').attr('data-node_type'),
              node_label: $('#modal-channel').attr('data-node_label')
            });
          }
        });
      });
      // console.log($('#modal-channel[data-cid="' + channel.cid + '"]'));
    }).fail(function (response) { // console.log(response)
      kit.gui.notify(response.responseText, {
        type: 'danger',
        delay: 0
      });
    })
  }

  appendIsTypingChannel(user, channel) {
    if ($('#modal-channel[data-cid="' + channel.cid + '"]').length == 0) return;
    let isTypingIndicator = $('.channel-message-container').find('.is-typing[data-uid="' + user.uid + '"]');
    if (this.clearChannelId) clearInterval(this.clearChannelId);
    if (isTypingIndicator.length == 0) {
      let typingText = '<div class="is-typing mt-2 mb-2 ml-3 mr-3" data-uid="' + user.uid + '" data-time="' + (new Date().getTime()) + '">'
      typingText += '<div class="wave mr-2">'
      typingText += '<span class="dot"></span>'
      typingText += '<span class="dot"></span>'
      typingText += '<span class="dot"></span>'
      typingText += '</div>'
      typingText += '<em>' + user.username + ' is typing... </em></div>';
      $('.channel-message-container').append(typingText);
      this.scrollChannelMessageContainer();
    }
    isTypingIndicator = $('.channel-message-container').find('.is-typing[data-uid="' + user.uid + '"]');
    this.clearChannelId = setTimeout(function () {
      $(isTypingIndicator).remove();
    }, 5000)
  }

  appendChannelMessage(message, options) {
    let settings = Object.assign({}, {
      from: 'other',
      doScroll: true
    }, options);
    if (settings.from == 'self')
      $('.channel-message-container').append('<p class="me"><span class="wrap">' + message + '</span></p>');
    else if (settings.from == 'other') {
      if ($('#modal-channel[data-cid="' + options.channel.cid + '"]').length == 0) return;
      if (this.clearChannelId) clearInterval(this.clearChannelId);
      let isTypingIndicator = $('.channel-message-container').children('.is-typing[data-uid="' + options.sender.uid + '"]');
      if (isTypingIndicator.length > 0) $(isTypingIndicator).remove();
      $('.channel-message-container').append('<p class="other"><span class="wrap"><span class="sender text-info">' + settings.sender.username + '</span><br>' + message + '</span></p>');
    }
    if (settings.doScroll) this.scrollChannelMessageContainer();
  }

  loadChannelMessages(messages) {
    let kit = this;
    messages.forEach(m => {
      this.appendChannelMessage(m.message, {
        from: m.uid == kit.user.uid ? 'self' : 'other',
        channel: {
          cid: m.cid,
          rid: m.rid,
        },
        sender: {
          uid: m.uid,
          username: m.username,
          name: m.name
        },
        doScroll: false
      });
    });
    kit.scrollChannelMessageContainer();
  }

  scrollChannelMessageContainer() {
    $('.channel-container').animate({
      scrollTop: $('.channel-message-container').height()
    });
  }

  updateUnreadChannelMessageIndicator(channel) {
    this.channelTool.notify(channel);
  }

  updateChannelList(indicators) {
    // console.log(indicators);
    let kit = this;
    let channelList = ''
    for (let indicator of indicators) {
      let id = (indicator.node_type == 'concept') ? 'c-' + indicator.node_id : 'l-' + indicator.node_id;
      let label = indicator.node_label; // cy.nodes('#' + id).data('name'); // console.log(label);
      let blink = indicator.blinkEnabled ? '<span class="badge badge-warning">New</span>' : '';
      channelList += '<div class="list-item pb-1 pt-1" data-id="' + id + '" style="overflow:hidden; text-overflow: ellipsis; white-space:nowrap">' + blink + ' <small>' + indicator.node_type + ': ' + label + '</small></div>'
    }
    $('#channel-list').html(channelList);
    kit.movePopupChannel();
  }

  updateChannelButtonBadge() {
    let kit = this;
    let enabledChannels = 0;
    for (let indicator of kit.channelTool.indicators)
      if (indicator.blinkEnabled) enabledChannels++;
    if (enabledChannels > 0) {
      $('#bt-channel').html('Channel <span class="badge badge-danger">' + enabledChannels + '</span');
      $('#bt-channel')
        .addClass('animated flash')
        .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
          $(this).removeClass('animated flash');
        });
    } else $('#bt-channel').html('Channel');
  }

  // General Function

  sendFeedback(callback) {
    let kit = this;
    let socket = this.socket;
    let eventListeners = this.eventListeners;
    socket.emit('send-feedback', {
      room: kit.room,
    }, function (status, reply) {
      // console.log(status);
    });
    eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('on-send-feedback', {
          socketId: socket.id,
          room: kit.room
        });
      }
    });
  }

  sendRoomMessage(message, room, data, callback) {
    let kit = this;
    let socket = this.socket;
    let eventListeners = this.eventListeners;
    socket.emit(message, {
      room: room ? room : kit.room,
      data: data
    }, function (status, reply) {
      if (typeof callback == 'function') callback(status, reply);
    });
  }

  sendCommand(command, room, data, callback) {
    let kit = this;
    let socket = this.socket;
    let eventListeners = this.eventListeners;
    socket.emit('send-command', {
      command: command,
      room: room != null ? room : kit.room,
      data: data
    }, callback);
  }

  movePopupRooms() { // maintaining pop-up rooms position
    let h = $('#popup-rooms').outerHeight();
    let w = $('#popup-rooms').outerWidth();
    $('#popup-rooms').css('top', $('#bt-room').offset().top - h - 6);
    $('#popup-rooms').css('left', $('#bt-room').offset().left + $('#bt-room').outerWidth() - w);
  }

  movePopupUsers() { // maintaining pop-up rooms position
    let h = $('#popup-users').outerHeight();
    let w = $('#popup-users').outerWidth();
    $('#popup-users').css('top', $('#bt-room').offset().top - h - 6);
    $('#popup-users').css('left', $('#bt-room').offset().left + $('#bt-room').outerWidth() - w);
  }

  movePopupChat() { // maintaining pop-up chat position
    let h = $('#popup-chat').outerHeight();
    let w = $('#popup-chat').outerWidth();
    $('#popup-chat').css('top', $('#bt-message').offset().top - h - 6);
    $('#popup-chat').css('left', $('#bt-message').offset().left + $('#bt-message').outerWidth() - w);
  }

  movePopupChannel() { // maintaining pop-up chat position
    let h = $('#popup-channel').outerHeight();
    let w = $('#popup-channel').outerWidth();
    $('#popup-channel').css('top', $('#bt-channel').offset().top - h - 6);
    $('#popup-channel').css('left', $('#bt-channel').offset().left + $('#bt-channel').outerWidth() - w);
  }

  showRoomUsers(roomUsers) {
    let kit = this;
    let userList = '';
    roomUsers.forEach(roomUser => {
      // console.log(roomUser);
      userList += '<div class="row list-item" data-key="' + roomUser.socketId + '" data-username="' + roomUser.username + '" data-uid="' + roomUser.uid + '"><small>' + roomUser.name + '</small></div>';
    })
    $('#user-list').html(userList);
    $('#popup-users').fadeToggle({
      duration: 100,
      start: kit.movePopupUsers, // callback
      complete: function () {}
    });
  }

}