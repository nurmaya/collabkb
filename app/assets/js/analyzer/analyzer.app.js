class AnalyzerApp {

  constructor(canvas) {

    this.canvas = canvas;
    this.gui = new GUI();
    this.analyzerLib = new AnalyzerLib();
    this.ajax = Ajax.ins(this.gui);
    this.goalmap = {
      goalmap: null,
      concepts: [],
      links: []
    }
    this.learnermaps = [];

    this.handleEvent();

    this.learnermap = null;
    this.compareResult = null;
    this.gCompare = null;

    let app = this;

    $('input[type="range"]').rangeslider();
    $('input[type="range"]').on('input', function (e) {
      let minVal = $('input[data-id="min-range"]').val();
      let maxVal = $('input[data-id="max-range"]').val();
      let id = $(this).data('id');
      let over = false;
      if (id == 'min-range' && parseInt(this.value) > parseInt(maxVal)) {
        $(this).val(maxVal).change();
        over = true;
      } else if (id == 'max-range' && parseInt(this.value) < parseInt(minVal)) {
        $(this).val(minVal).change();
        over = true;
      }
      if (over) return;
      // console.log(minVal, maxVal);
      $('.min-val').html(minVal);
      $('.max-val').html(maxVal);
      app.updateGroupAnalysis();
    });

  }

  getCanvas() {
    return this.canvas;
  }

  handleEvent() {

    let app = this;

    $('#bt-open-kit').on('click', function () {
      $.ajax({
        url: baseUrl + 'kitbuildApi/getMaterials',
        method: 'post'
      }).done(function (response) { // console.log(response)
        if (!response.status) {
          app.gui.notify(response.error ? response.error : response, {
            type: 'danger'
          });
          return;
        }
        let materials = response.result;
        let materialList = (!materials || materials.length == 0) ?
          '<em class="text-secondary">No material available</em>' :
          '';
        materials.forEach((material) => {
          materialList += '<div class="row align-items-center list list-hover list-pointer justify-content-between pt-2 pb-2 pl-2 pr-2" data-mid="' + material.mid + '" data-name="' + material.name + '">'
          materialList += material.name
          materialList += '<i class="fas fa-check text-primary" style="display:none"></i>'
          materialList += '</div>'
        });
        $('#modal-kit .material-list').html(materialList);
        app.gui.modal('#modal-kit', {
          'width': '600px'
        });
      }).fail(function (response) {
        app.gui.notify(response, {
          type: 'danger'
        });
      })
    });
    $('#modal-kit .material-list').on('click', '.row', function () {
      if ($(this).hasClass('selected')) return;
      $('#modal-kit .material-list .row').find('i').hide();
      $('#modal-kit .material-list .row').removeClass('selected');
      $(this).find('i').show();
      $(this).addClass('selected')
      let mid = $(this).data('mid');
      let name = $(this).data('name');
      $('#modal-kit .bt-open').data('mid', mid);
      $('#modal-kit .bt-open').data('name', name);
      $('#modal-kit .bt-open').data('gmid', null);
      $('#modal-kit .bt-open').data('gmname', null);
      $('#modal-kit .goalmap-list').html('<em class="text-secondary">Loading goalmaps...</em>');
      $.ajax({
        url: baseUrl + 'kitbuildApi/getGoalmaps/' + mid + '/' + 'fix',
      }).done(function (response) { // console.log(response)
        if (!response.status) {
          app.gui.notify(response.error ? response.error : response.responseText, {
            type: 'danger'
          })
          return;
        }
        let goalmaps = response.result;
        let goalmapList = (!goalmaps || goalmaps.length == 0) ?
          '<em class="text-secondary">No goalmap available</em>' :
          '';
        goalmaps.forEach((goalmap) => {
          goalmapList += '<div class="row align-items-center list list-hover list-pointer justify-content-between pt-2 pb-2 pl-2 pr-2" data-gmid="' + goalmap.gmid + '" data-name="' + goalmap.name + '">'
          goalmapList += '<span>' + goalmap.name + ' '
          goalmapList += '<span class="badge badge-info" title="Learnermaps Count">' + goalmap.learnermaps_count + '</span>'
          goalmapList += '</span>'
          goalmapList += '<i class="fas fa-check text-primary" style="display:none"></i>'
          goalmapList += '</div>'
        });
        $('#modal-kit .goalmap-list').html(goalmapList);
      }).fail(function (response) { // console.log(response)
        app.gui.notify(response.responseText, {
          type: 'danger'
        })
      })
    });
    $('#modal-kit .goalmap-list').on('click', '.row', function () {
      if ($(this).hasClass('selected')) return;
      $('#modal-kit .goalmap-list .row').find('i').hide();
      $('#modal-kit .goalmap-list .row').removeClass('selected');
      $(this).find('i').show();
      $(this).addClass('selected')
      let gmid = $(this).data('gmid');
      let name = $(this).data('name');
      $('#modal-kit .bt-open').data('gmid', gmid);
      $('#modal-kit .bt-open').data('gmname', name);
    });
    $('#modal-kit').on('click', '.bt-open', function () {
      let name = ($(this).data('name'))
      let mid = ($(this).data('mid'))
      let gmid = ($(this).data('gmid'))
      let gmname = ($(this).data('gmname'))
      if (!gmid) {
        app.gui.dialog('Please select a Kit.', {
          width: '300px'
        });
        return;
      }
      let confirm = app.gui.confirm('Open kit <strong>\'' + gmname + '\' of \'' + name + '\'</strong>?', {
        'positive-callback': function () {
          app.loadGoalmap(gmid, function (goalmap, concepts, links) {
            app.setGoalmap(goalmap, concepts, links);
            app.analyzerLib.showGoalmap(app.canvas.getCy(), concepts, links, function () {
              app.canvas.getCy().fit(app.canvas.getCy().nodes(), 50);
            });
            app.loadLearnermaps(gmid, function (learnermaps) {
              app.learnermaps = learnermaps;
              app.gCompare = app.analyzerLib.groupCompare(app.learnermaps, app.goalmap);
              app.showLearnermapList(learnermaps);
              // console.log(learnermaps);
              // console.log(app.gCompare);
              let range = app.analyzerLib.getRange(app.gCompare);
              // console.log(range, $('input[type="range"]'), $('input[type="range"][data-id="max-range"]'));
              $('input[type="range"]').attr('max', range.max);
              $('input[type="range"]').attr('min', range.min);
              $('input[type="range"][data-id="min-range"]').val(range.min).change();
              $('input[type="range"][data-id="max-range"]').val(range.max).change();
              $('input[type="range"]').change();
              $('.min-val').html(range.min);
              $('.max-val').html(range.max);
            });
            confirm.modal('hide');
            $('#modal-kit').modal('hide');
          });
        },
        'width': '300px'
      })
    });
    $('#learner-maps-list-container').on('click', '.row', function () {
      let lmid = $(this).data('lmid');
      app.learnermap = app.getLearnermap(lmid); // console.log(learnermap);
      app.compareResult = app.analyzerLib.compare(app.learnermap, app.goalmap); // console.log(compareResult);
      app.analyzerLib.drawAnalysisLinks(app.canvas.getCy(),
        app.compareResult.match, app.compareResult.leave, app.compareResult.excess, app.compareResult.miss, {
          match: $('#bt-matching-links').is(':checked'),
          leave: $('#bt-leaving-links').is(':checked'),
          excess: $('#bt-excessive-links').is(':checked'),
          miss: $('#bt-missing-links').is(':checked')
        },
        function () {
          if (app.analyzerLib.isGraphOutOfView(app.canvas.getCy())) {
            $('#bt-fit').click();
          };
        })
      // app.drawIndividualMap(compareResult.match, compareResult.leave, compareResult.excess, compareResult.miss);
    })
    $('#bt-matching-links').on('change', function () {
      if (app.learnermap) app.updateAnalysisDrawing();
      else app.updateGroupAnalysis();
    })
    $('#bt-leaving-links').on('change', function () {
      if (app.learnermap) app.updateAnalysisDrawing();
      else app.updateGroupAnalysis();
    })
    $('#bt-excessive-links').on('change', function () {
      if (app.learnermap) app.updateAnalysisDrawing();
      else app.updateGroupAnalysis();
    })
    $('#bt-missing-links').on('change', function () {
      if (app.learnermap) app.updateAnalysisDrawing();
      else app.updateGroupAnalysis();
    })
    $('#bt-show-groups-map').on('click', function () {
      if (!app.gCompare) return;
      app.learnermap = null;
      app.updateGroupAnalysis();
    })
    $('#bt-show-teacher-map').on('click', function () {
      // console.log('click')
      if (app.goalmap.goalmap) {
        app.showGoalmap();
        // app.analyzerLib.showGoalmap(app.canvas.getCy(), app.goalmap.concepts, app.goalmap.links, function () {
        //   app.canvas.getCy().fit(app.canvas.getCy().nodes(), 50);
        // });
      }
      else $('#bt-open-kit').click();
    })
  }

  updateAnalysisDrawing() {
    let app = this;
    if (app.compareResult)
      app.analyzerLib.drawAnalysisLinks(app.canvas.getCy(),
        app.compareResult.match, app.compareResult.leave, app.compareResult.excess, app.compareResult.miss, {
          match: $('#bt-matching-links').is(':checked'),
          leave: $('#bt-leaving-links').is(':checked'),
          excess: $('#bt-excessive-links').is(':checked'),
          miss: $('#bt-missing-links').is(':checked')
        })
  }

  updateGroupAnalysis() {
    let app = this;
    if (app.gCompare)
      app.analyzerLib.drawGroupAnalysis(app.canvas.getCy(),
        app.gCompare.match, app.gCompare.leave, app.gCompare.excess, app.gCompare.miss, {
          match: $('#bt-matching-links').is(':checked'),
          leave: $('#bt-leaving-links').is(':checked'),
          excess: $('#bt-excessive-links').is(':checked'),
          miss: $('#bt-missing-links').is(':checked'),
          min: $('input[data-id="min-range"]').val(),
          max: $('input[data-id="max-range"]').val()
        },
        function () {})
  }

  loadGoalmap(gmid, callback) {
    let app = this;
    $.ajax({
      url: baseUrl + 'kitbuildApi/openKit/' + gmid,
    }).done(function (response) { // console.log(response)
      if (!response.status) {
        app.gui.notify(response.error ? response.error : response.responseText, {
          type: 'danger'
        })
        return;
      }
      let goalmap = response.result.goalmap;
      let concepts = response.result.concepts;
      let links = response.result.links;
      // app.showGoalmap(concepts, links);
      // app.setGoalmap(goalmap, concepts, links);
      if (typeof callback == 'function') callback(goalmap, concepts, links);
    }).fail(function (response) { // console.log(response)
      app.gui.notify(response.responseText, {
        type: 'danger'
      })
    })
  }

  setGoalmap(goalmap, concepts, links) {
    this.goalmap.goalmap = goalmap;
    this.goalmap.concepts = concepts;
    this.goalmap.links = links;
    this.goalmap.propositionCount = 0;
    links.forEach(l => {
      if (l.source && l.target) this.goalmap.propositionCount++;
    })
  }

  showLearnermapList(learnermaps = []) {
    this.learnermaps = learnermaps;
    let learnermapList = learnermaps.length == 0 ? '<em class="text-secondary">No learnermaps available</em>' : '';
    learnermaps.forEach(learnermap => {
      // console.log(learnermap);
      learnermapList += '<div class="row align-items-center list list-hover list-pointer justify-content-between pt-2 pb-2 pl-2 pr-2" data-lmid="' + learnermap.lmid + '">'
      learnermapList += learnermap.username
      learnermapList += '<span>' + (learnermap.compare.match.length / this.goalmap.propositionCount * 100).toFixed(2) + '%</span>'
      learnermapList += '</div>'
    })
    $('#learner-maps-list-container').html(learnermapList);
  }

  loadLearnermaps(gmid, callback) {
    let app = this;
    let learnermaps = [];
    $.ajax({
      url: baseUrl + 'kitbuildApi/getLearnermaps/' + gmid,
    }).done(function (response) { // console.log(response)
      if (!response.status) {
        app.gui.notify(response.error ? response.error : response.responseText, {
          type: 'danger'
        })
        return;
      }
      learnermaps = response.result;
      if (typeof callback == 'function') callback(learnermaps);
    }).fail(function (response) { // console.log(response)
      app.gui.notify(response.responseText, {
        type: 'danger'
      })
    })
  }

  getLearnermap(lmid) {
    for (let learnermap of this.learnermaps) {
      if (learnermap.lmid == lmid) return learnermap;
    };
  }

  showGoalmap() {
    let app = this;
    let links = app.goalmap.links;
    let edges = [];
    links.forEach(l => {
      if (l.source)
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.source,
            type: 'left',
            label: ''
          }
        })
      if (l.target)
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.target,
            type: 'right',
            label: ''
          }
        })
    })

    app.getCanvas().getCy().remove('edge');
    app.getCanvas().getCy().remove('[link="leave"]');
    app.getCanvas().getCy().add(edges);

  }

}