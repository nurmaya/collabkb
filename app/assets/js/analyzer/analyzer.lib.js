class AnalyzerLib {

  constructor() {}

  compare(learnermap, goalmap) {
    let match = [];
    let leave = [];
    let excess = [];
    let miss = [];

    var clone = function(o) {
      return JSON.parse(JSON.stringify(o));
    }

    // look for matching link for every goalmap's links
    for (let glink of goalmap.links) {
      let found = false;
      for (let link of learnermap.links) {
        // Compare by label, incase two or more different links share the same label.
        if (link.label.trim().toLowerCase() != glink.label.trim().toLowerCase()) {
          continue;
        }

        // If one of source or target is not connected
        if (!link.source || !link.target) { // target is leaving links
          continue;
        }
        // If source and target are the same, reversed source and target also count
        if ((link.source == glink.source && link.target == glink.target) ||
          (link.source == glink.target && link.target == glink.source)) {
          found = true;
          match.push(clone(link));
          break;
        }
      }
      if (!found) miss.push(clone(glink));
    }

    // look for excessive links for every learnermap's links
    for (let link of learnermap.links) {
      if (!link.source || !link.target) { // it is leaving links
        // add to leaving links list
        leave.push(clone(link));
        // skip searching!
        continue;
      }
      let found = false;
      for (let glink of goalmap.links) {
        // Compare by label, incase two or more different links share the same label.
        if (link.label.trim().toLowerCase() != glink.label.trim().toLowerCase()) {
          // different label, this is not what are we looking for, skip! 
          continue;
        }
        // If source and target are the same, reversed source and target also count
        if ((link.source == glink.source && link.target == glink.target) ||
          (link.source == glink.target && link.target == glink.source)) {
          // found matching links
          found = true;
          // stop searching, continue to next learnermap's link
          break;
        }
      }
      // could not find any, then it must be excessive links.
      if (!found) excess.push(clone(link));
    }

    function linkInLinks(link, links, by = 'id') {
      let founds = [];
      for (let l of links) {
        switch (by) {
          case 'label':
            if (link.label.toLowerCase().trim() == l.label.toLowerCase().trim()) founds.push(l);
            break;
          default:
            if (link.lid == l.lid) founds.push(l);
            break;
        }
      }
      return founds;
    }

    for (let mi of miss) {
      // ada nggak missing link di matching link; merah di hijau?

      let missInMatches = linkInLinks(mi, match);
      if (missInMatches.length) { // ada

        for (let missInMatch of missInMatches) {

          // dari merah yang ada di hijau, ada nggak yang labelnya ada di biru

          let inExcesses = linkInLinks(missInMatch, excess, 'label');

          let foundInBlue = false;
          if (inExcesses.length) {
            for (let inExcess of inExcesses) {

              // kalau ada cek, ada nggak yang biru tadi di merah
              let inMisses = linkInLinks(inExcess, miss)

              if (inMisses.length == 0) { // yes! pindah missing link ke excessive link
                // console.log('miss di match yang harusnya ke excessive');
                foundInBlue = true;
                mi.lid = inExcess.lid;
                break;
              }
            }
          }

          if (!foundInBlue) {
            // cari di leaving links  
            let inLeaves = linkInLinks(missInMatch, leave, 'label');

            if (inLeaves.length) {
              for (let inLeave of inLeaves) {
                // ada, check sudah ada di missing link belum? 
                // karena habis dipindah sebelumnya.
                let inMisses = linkInLinks(inLeave, miss);

                if (inMisses.length == 0) { // yes! pindah missing link ke leaving link
                  // console.log('miss di match yang harusnya ke leave');
                  mi.lid = inLeave.lid;
                  break;
                }
              }
            }
          }
        }
      }
    }

    learnermap.compare = {
      match: match,
      leave: leave,
      excess: excess,
      miss: miss
    }
    return learnermap.compare;
  }

  groupCompare(learnermaps, goalmap) {
    let app = this;
    let gmatches = [];
    let gleaves = [];
    let gexcesses = [];
    let gmisses = [];
    learnermaps.forEach(learnermap => {
      let compare = app.compare(learnermap, goalmap);

      for (let match of compare.match) {
        let exists = false;
        for (let gmatch of gmatches) {
          if (match.label == gmatch.label && (match.source == gmatch.source && match.target == gmatch.target) ||
            (match.source == gmatch.target && match.target == gmatch.source)) {
            exists = true;

            gmatch.count++;
            break;
          }
        }
        if (!exists) {
          match.count = 1;
          gmatches.push(match);
        }
      }

      for (let leave of compare.leave) {
        let exists = false;
        for (let gleave of gleaves) {
          // if (leave.label == gleave.label && (leave.source == gleave.source && leave.target == gleave.target) ||
          //   (leave.source == gleave.target && leave.target == gleave.source)) {
          if (leave.label == gleave.label) {
            exists = true;

            gleave.count++;
            break;
          }
        }
        if (!exists) {
          leave.count = 1;
          gleaves.push(leave);
        }
      }

      for (let excess of compare.excess) {
        let exists = false;
        for (let gexcess of gexcesses) {
          if (excess.label == gexcess.label && (excess.source == gexcess.source && excess.target == gexcess.target) ||
            (excess.source == gexcess.target && excess.target == gexcess.source)) {
            exists = true;

            gexcess.count++;
            break;
          }
        }
        if (!exists) {
          excess.count = 1;
          gexcesses.push(excess);
        }
      }

      for (let miss of compare.miss) {
        let exists = false;
        for (let gmiss of gmisses) {
          if (miss.label == gmiss.label && (miss.source == gmiss.source && miss.target == gmiss.target) ||
            (miss.source == gmiss.target && miss.target == gmiss.source)) {
            exists = true;

            gmiss.count++;
            break;
          }
        }
        if (!exists) {
          miss.count = 1;
          gmisses.push(miss);
        }
      }

    });


    return {
      match: gmatches,
      leave: gleaves,
      excess: gexcesses,
      miss: gmisses
    }
  }

  showGoalmapKit(cy, concepts, links, callback) {
    // concepts and links in database format
    let nodes = [];
    let edges = [];
    concepts.forEach(c => {
      nodes.push({
        group: "nodes",
        data: {
          id: "c-" + c.cid,
          name: c.label,
          type: 'concept'
        },
        position: {
          x: parseInt(c.locx),
          y: parseInt(c.locy)
        }
      })
    })
    links.forEach(l => {
      nodes.push({
        group: "nodes",
        data: {
          id: "l-" + l.lid,
          name: l.label,
          type: 'link'
        },
        position: {
          x: parseInt(l.locx),
          y: parseInt(l.locy)
        }
      })
    })

    cy.reset();
    cy.clear();
    cy.elements().remove();
    cy.add(nodes);

    cy.layout({
      name: 'preset',
      fit: false,
      zoom: 1.0,
      stop: (typeof callback == 'function') ? callback : function () {}
    }).run();
  }

  showGoalmap(cy, concepts, links, callback) {
    // concepts and links in database format
    // let app = this;
    let nodes = [];
    let edges = [];
    concepts.forEach(c => {
      let extras = c.data ? JSON.parse(c.data) : null;
      nodes.push({
        group: "nodes",
        data: {
          id: "c-" + c.cid,
          name: c.label,
          type: 'concept',
          color: extras ? extras.color : undefined,
          textColor: extras ? extras.textColor : undefined
        },
        position: {
          x: parseInt(c.locx),
          y: parseInt(c.locy)
        }
      })
    })
    links.forEach(l => {
      nodes.push({
        group: "nodes",
        data: {
          id: "l-" + l.lid,
          name: l.label,
          type: 'link'
        },
        position: {
          x: parseInt(l.locx),
          y: parseInt(l.locy)
        }
      })
      if (l.source)
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.source,
            type: 'left'
          }
        })
      if (l.target)
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.target,
            type: 'right'
          }
        })
    })

    cy.reset();
    cy.clear();
    cy.elements().remove();
    cy.add(nodes);
    cy.add(edges);

    cy.layout({
      name: 'preset',
      fit: false,
      zoom: 1.0,
      stop: (typeof callback == 'function') ? callback : function () {}
    }).run();
  }

  getRange(links) {
    let max = 1;
    if (links.match && links.match.length) {
      links.match.forEach(l => {
        if (l.count > max) max = l.count
      })
    }
    if (links.leave && links.leave.length) {
      links.leave.forEach(l => {
        if (l.count > max) max = l.count
      })
    }
    if (links.excess && links.excess.length) {
      links.excess.forEach(l => {
        if (l.count > max) max = l.count
      })
    }
    if (links.miss && links.miss.length) {
      links.miss.forEach(l => {
        if (l.count > max) max = l.count
      })
    }
    return {
      min: 1,
      max: max
    }
  }

  drawAnalysisLinks(cy, match, leave, excess, miss, options, callback) {

    let settings = Object.assign({
      match: true,
      leave: true,
      excess: true,
      miss: true
    }, options);

    let edges = [];
    let nodes = [];
    if (settings.excess) excess.forEach(l => {
      if (l.source)
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.source,
            type: 'left',
            link: 'ex',
            match: 5,
            label: ''
          }
        })
      if (l.target)
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.target,
            type: 'right',
            link: 'ex',
            match: 5,
            label: ''
          }
        })
    });
    if (settings.miss) miss.forEach(l => {
      if (l.source)
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.source,
            type: 'left',
            link: 'miss',
            match: 5,
            label: ''
          }
        })
      if (l.target)
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.target,
            type: 'right',
            link: 'miss',
            match: 5,
            label: ''
          }
        })
    });
    if (settings.match) match.forEach(l => {
      if (l.source)
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.source,
            type: 'left',
            link: 'match',
            match: 5,
            label: ''
          }
        })
      if (l.target)
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.target,
            type: 'right',
            link: 'match',
            match: 5,
            label: ''
          }
        })
    });
    if (settings.leave) leave.forEach(l => {
      nodes.push({
        group: "nodes",
        data: {
          id: "ll-" + l.lid,
          name: l.label,
          type: 'link',
          link: 'leave'
        },
        position: {
          x: parseInt(l.locx),
          y: parseInt(l.locy)
        }
      })
    })
    cy.edges().remove();
    cy.add(edges);
    // get bounding box BEFORE adding leaving links
    cy.remove('[link="leave"]');
    let bb = cy.nodes().boundingBox();
    let leavingNodes = cy.add(nodes);

    // positioning leaving nodes
    leavingNodes.layout({
      name: 'grid',
      fit: false,
      condense: true,
      cols: 1,
      boundingBox: {
        x1: bb.x2,
        y1: bb.y1,
        w: 100,
        h: bb.y2 - bb.y1
      },
      stop: function () {
        if (callback) callback();
      }
    }).run();
  }

  drawGroupAnalysis(cy, match, leave, excess, miss, options, callback) {

    let range = this.getRange({
      match: match,
      leave: leave,
      excess: excess,
      miss: miss
    })

    let settings = Object.assign({
      match: true,
      leave: true,
      excess: true,
      miss: true,
      min: range.min,
      max: range.max,
      minRange: range.min,
      maxRange: range.max
    }, options);

    let edges = [];
    let nodes = [];
    if (settings.excess) excess.forEach(l => {
      if (l.source && l.count >= settings.min && l.count <= settings.max)
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.source,
            type: 'left',
            link: 'ex',
            match: l.count,
            label: l.count
          }
        })
      if (l.target && l.count >= settings.min && l.count <= settings.max)
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.target,
            type: 'right',
            link: 'ex',
            match: l.count,
            label: l.count
          }
        })
    });
    if (settings.miss) miss.forEach(l => {
      if (l.source && l.count >= settings.min && l.count <= settings.max)
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.source,
            type: 'left',
            link: 'miss',
            match: l.count,
            label: l.count
          }
        })
      if (l.target && l.count >= settings.min && l.count <= settings.max)
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.target,
            type: 'right',
            link: 'miss',
            match: l.count,
            label: l.count
          }
        })
    });
    if (settings.match) match.forEach(l => {
      if (l.source && l.count >= settings.min && l.count <= settings.max)
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.source,
            type: 'left',
            link: 'match',
            match: l.count,
            label: l.count
          }
        })
      if (l.target && l.count >= settings.min && l.count <= settings.max)
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.target,
            type: 'right',
            link: 'match',
            match: l.count,
            label: l.count
          }
        })
    });
    if (settings.leave) leave.forEach(l => {
      if (l.count >= settings.min && l.count <= settings.max)
        nodes.push({
          group: "nodes",
          data: {
            id: "ll-" + l.lid,
            name: l.label + ' (' + l.count + ')',
            type: 'link',
            link: 'leave'
          },
          position: {
            x: parseInt(l.locx),
            y: parseInt(l.locy)
          }
        })
    })
    cy.edges().remove();
    cy.add(edges);
    // get bounding box BEFORE adding leaving links
    cy.remove('[link="leave"]');
    let bb = cy.nodes().boundingBox();
    let leavingNodes = cy.add(nodes);

    // positioning leaving nodes
    leavingNodes.layout({
      name: 'grid',
      fit: false,
      condense: true,
      cols: 1,
      boundingBox: {
        x1: bb.x2,
        y1: bb.y1,
        w: 100,
        h: bb.y2 - bb.y1
      },
      stop: function () {
        if (callback) callback();
      }
    }).run();
  }

  buildLearnermap(cy) {
    if (!cy) return null;
    let learnermap = {
      concepts: [],
      links: []
    }
    let concepts = cy.nodes('[type="concept"]');
    let links = cy.nodes('[type="link"]');
    for (let c of concepts) {
      learnermap.concepts.push({
        cid: c.id().substr(2),
        label: c.data('name')
      })
    }
    for (let l of links) {
      let left = l.connectedEdges('[type="left"]');
      let right = l.connectedEdges('[type="right"]');
      let source = null,
        target = null;
      if (left.length == 1) source = left.connectedNodes('[type="concept"]')
      if (right.length == 1) target = right.connectedNodes('[type="concept"]')
      learnermap.links.push({
        lid: l.id().substr(2),
        label: l.data('name'),
        source: source ? source.id().substr(2) : null,
        target: target ? target.id().substr(2) : null
      });
    }
    return learnermap;
  }

  isGraphOutOfView(cy) {
    let width = cy.width();
    let height = cy.height();
    let rbb = cy.elements().renderedBoundingbox();
    return rbb.x1 < 0 || rbb.y1 < 0 || rbb.x2 > width || rbb.y2 > height;
  }

}