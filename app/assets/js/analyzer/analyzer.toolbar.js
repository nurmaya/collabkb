class AnalyzerToolbar {

  constructor(kitbuild) {
    
    // this.chat = chat;
    // this.kit = kit;
    this.kitbuild = kitbuild;    
    this.handleToolbarEvent(kitbuild);
  }

  handleToolbarEvent(kitbuild) {
    let app = kitbuild.getApp();
    let ui = kitbuild.getUI();
    let toolbar = this;
    //let chat = this.chat;
    //let kit = this.kit;

    $('#bt-sign-in').on('click', function () {
      ui.showSignInDialog();
    });

    $('.bt-signIn').on('click', function (e) {
      e.preventDefault();
      let bt = $(this);
      let url = baseUrl + "kitBuildApi/signIn";
      bt.attr('disabled', true); // return false;
      $('#tx-sign-in').slideDown();
      $.ajax({
          url: url,
          method: 'post',
          data: {
            'kb-username': $('#kb-username').val(),
            'kb-password': $('#kb-password').val()
          }
        }).done(function (result) {
          //console.log(result);
          bt.attr('disabled', false);
          if (typeof result == 'object' && result.status == true) {
            app.setUser(result.result);
            $('#tx-sign-in').slideUp();
            $('#bt-sign-out').show();
            // if (typeof Logger != 'undefined') Logger.log('login-ok', {
            //   uid: Canvas.meta.user.uid,
            // })
            ui.hideSignInDialog();
            ui.notify('Welcome, ' + result.result.fullname + '.', {
              type: 'success'
            })
            ui.signIn(result.result);
          } else { // console.log(ui);
            ui.notify('Invalid User ID and/or Key', {
              type: 'danger'
            });
            // if (typeof Logger != 'undefined') Logger.log('login-nok', {
            //   uid: Canvas.meta.user.uid,
            //   uname: $('#kb-username').val(),
            //   ukey: $('#kb-password').val()
            // });
            $('#tx-sign-in').slideUp();
          }
        })
        .fail(function (e) {
          console.error(e);
          $('#tx-sign-in').slideUp();
          bt.prop("disabled", false);
          // TODO: Show error message
          ui.notify('Sign in problem. Server error.', {
            type: 'danger'
          });
        });
    });

    $('#bt-sign-out').on('click', function () {
      let confirm = ui.dialog("If you sign out, any unsaved progress will be lost.<br>Continue sign-out?", {
        'positive-callback': function () {
          $.ajax({
              url: baseUrl + "kitBuildApi/signOut"
            })
            .done(function (e) {
              if (e instanceof Object) {
                if (e.status == false) notify.notify(e.error, {
                  type: 'danger'
                });
                else if (e.status == true) { // console.log(e);

                  app.setUser(null);
                  // if (typeof Logger != 'undefined') Logger.log('sign-out', Canvas.meta);
                  // Logger.reset();
                  // console.log(Canvas.meta);
                  ui.notify("You have been successfully signed out.", {
                    type: 'success'
                  });
                  ui.signOut();
                }
                return;
              }
              notify.notify(e, {
                type: 'danger'
              })
            })
            .fail(function (e) {
              notify.notify(e.error, {
                type: 'danger'
              });
            })
            .always(function () {
              confirm.modal('hide');
            });
        }
      });
    });

    $('#bt-kit-open').on('click', function () {
      ui.showOpenKitMaterialDialog();
    });

    $('#modal-open-kit-material').on('click', '.bt-close', function () {
      ui.hideOpenKitMaterialDialog();
    })

    $('#modal-open-kit-material').on('click', '.material', function () {
      let mid = $(this).data('mid');
      ui.showOpenKitGoalmapDialog(mid);
    })

    $('#modal-open-kit-goalmap').on('click', '.bt-back', function () {
      ui.hideOpenKitGoalmapDialog();
    });

    $('#modal-open-kit-goalmap').on('click', '.goalmap', function () {
      ui.hideOpenKitGoalmapDialog();
      ui.hideOpenKitMaterialDialog();

      // let mid = $(this).data('mid');
      let gmid = $(this).data('gmid'); // console.log(mid, gmid);
      
      $.ajax({
        url: baseUrl + 'analyzerApi/getGoalmapById/' + gmid
      }).done(function (r) { // console.log(r);
        ui.hideOpenKitGoalmapDialog();
        let options = {
          showMatchingLinks: $('#bt-matching-links').prop('checked'),
          showExcessiveLinks: $('#bt-excessive-links').prop('checked'),
          showLackingLinks: $('#bt-lacking-links').prop('checked'),
          showLeavingLinks: $('#bt-leaving-links').prop('checked')
        };
        kitbuild.showGoalmap(r.goalmap, r.concepts, r.links, r.elinks, options); // in JSON array
        app.setGoalmapId(gmid);
        app.setGoalmap(r.goalmap, r.concepts, r.links, r.elinks);
      }).fail(function (e) {
        console.error(e);
      });

      $.ajax({
        url: baseUrl + 'analyzerApi/getLearnerFinalMaps/' + gmid
      }).done(function (r) { // console.log(r);
        kitbuild.showLearnerMapList(r); // in JSON array
      }).fail(function (e) {
        console.error(e);
      });

    });

    $('#bt-show-groups-map').on('click', function() {
      let options = {
        showMatchingLinks: $('#bt-matching-links').prop('checked'),
        showExcessiveLinks: $('#bt-excessive-links').prop('checked'),
        showLackingLinks: $('#bt-lacking-links').prop('checked'),
        showLeavingLinks: $('#bt-leaving-links').prop('checked')
      };
      console.log(options);
      kitbuild.showGroupsMap(options);
    });

    $('#learner-maps-list-container').on('click', '.learnermap', function(){
      let lmid = $(this).data('lmid');
      let gmid = $(this).data('gmid');
      $.ajax({
        url: baseUrl + 'analyzerApi/compareGoalmapLearnermap/' + gmid + '/' + lmid
      }).done(function (r) { // console.log(r);
        let links = r.links;
        let matchLinks = r.matchLinks;
        let learnerMatchLinks = r.learnerMatchLinks;
        let missingLinks = r.missingLinks;
        let excessiveLinks = r.excessiveLinks;
        let leavingLinks = r.leavingLinks;
        kitbuild.compareMaps(
          links, matchLinks, learnerMatchLinks, missingLinks, 
          excessiveLinks, leavingLinks
        );
      }).fail(function (e) {
        console.error(e);
      });
    });

    $('#bt-lacking-links').on('click', function(){ // console.log('toggle');
      let checked = $(this).prop('checked');
      toolbar.showLackingLinks(checked);
    });
    $('#bt-matching-links').on('click', function(){ // console.log('toggle');
      let checked = $(this).prop('checked');
      toolbar.showMatchingLinks(checked);
    });
    $('#bt-excessive-links').on('click', function(){ // console.log('toggle');
      let checked = $(this).prop('checked');
      toolbar.showExcessiveLinks(checked);
    });
    $('#bt-leaving-links').on('click', function(){ // console.log('toggle');
      let checked = $(this).prop('checked');
      toolbar.showLeavingLinks(checked);
    });

  }

  init() {
    return this;
  }

  showMatchingLinks(show) {
    if(!show) this.kitbuild.getCanvas().getCy().nodes('[type="link"][link="match"]').hide();
    else {
      this.kitbuild.getCanvas().getCy().nodes('[type="link"][link="match"]').show();
      this.kitbuild.getCanvas().getCy().edges('[link="match"]').show();
    }
    
  }

  showLackingLinks(show) {
    if(!show) this.kitbuild.getCanvas().getCy().nodes('[type="link"][link="miss"]').hide();
    else {
      this.kitbuild.getCanvas().getCy().nodes('[type="link"][link="miss"]').show();
      this.kitbuild.getCanvas().getCy().edges('[link="miss"]').show();
    }
  }

  showExcessiveLinks(show) {
    if(!show) this.kitbuild.getCanvas().getCy().nodes('[type="link"][link="ex"]').hide();
    else {
      this.kitbuild.getCanvas().getCy().nodes('[type="link"][link="ex"]').show();
      this.kitbuild.getCanvas().getCy().edges('[link="ex"]').show();
    }
  }

  showLeavingLinks(show) {
    if(!show) this.kitbuild.getCanvas().getCy().nodes('[type="link"][link="lv"]').hide();
    else {
      this.kitbuild.getCanvas().getCy().nodes('[type="link"][link="lv"]').show();
      this.kitbuild.getCanvas().getCy().edges('[link="lv"]').show();
    }
  }

}