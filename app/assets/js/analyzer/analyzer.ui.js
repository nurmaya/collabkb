class UI {

  constructor(app) {
    this.app = app;
    this.notification = new Notify();
  }

  init() {
    $('#tx-sign-in').hide();
    return this;
  }

  notify(message, options) {
    this.notification.notify(message, options);
  }

  dialog(text, options) {

    let dialog = $('#modal-dialog');

    let defaults = {
      'text-positive': 'Yes',
      'text-negative': 'No',
      'text': 'Do you want to continue?',
      'positive-callback': undefined,
      'negative-callback': undefined,
      'shown-callback': undefined,
      'hidden-callback': undefined,
      // 'backdrop': 'static',
      // 'keyboard': false,
      'width': 'normal',
      'selector': '.modal-dialog-dialog'
    }

    let message = text || defaults['text'];

    let settings = Object.assign({}, defaults, options);

    // this.setModalWidth(settings);

    dialog.find('.btn-positive').text(settings['text-positive']);
    dialog.find('.btn-negative').text(settings['text-negative']);
    dialog.find('#modal-dialog-content').html(message);

    if (settings['negative-callback'] == undefined) {
      dialog.find('.btn-negative')
        .off('click')
        .on('click', function () {
          dialog.modal('hide');
        }); //.hide();
    } else dialog.find('.btn-negative')
      .off('click')
      .on('click', settings['negative-callback'])
      .show();
    if (settings['positive-callback'] != undefined) {
      dialog.find('.btn-positive')
        .off('click')
        .on('click', settings['positive-callback']);
    } else dialog.find('.btn-positive')
      .off('click');

    if (settings['shown-callback'] != undefined) {
      dialog.on('shown.bs.modal', settings['shown-callback']);
    }
    if (settings['hidden-callback'] != undefined) {
      dialog.on('hidden.bs.modal', settings['hidden-callback']);
    }
    // console.log(settings);
    return dialog.modal(settings);

  }

  showSignInDialog() {
    $('#modal-sign-in').modal('show');
  }

  hideSignInDialog() {
    $('#modal-sign-in').modal('hide');
  }

  signIn(user) {
    // TODO: Do sign in procedure here...
    $('#bt-sign-in').hide();
    $('#bt-sign-out').show();
  }

  signOut() {
    $('#bt-sign-in').show();
    $('#bt-sign-out').hide();
  }

  showOpenKitMaterialDialog() {
    $('#modal-open-kit-material').modal('show');
    $('#modal-open-kit-material-content .list-container').html('<p style="text-align:center" class="text-secondary"><em>Loading...</em></p>');
    $.ajax({
      url: baseUrl + 'kitBuildApi/getMaterialsWithMetaByUid',
      method: 'post'
    }).done(function (r) { // console.log(r);
      let list = (r.length == 0) ? '<p style="text-align:center" class="text-secondary"><em>No materials found on database.</em></p>' : '';
      r.forEach(m => {
        list += '<div class="material list-item" data-mid="' + m.mid + '">' + m.name + '</div>';
      });
      $('#modal-open-kit-material-content .list-container').html(list);
    }).fail(function (e) {
      console.error(e);
    });
  }

  hideOpenKitMaterialDialog() {
    $('#modal-open-kit-material').modal('hide');
  }

  showOpenKitGoalmapDialog(mid) {
    let notification = this.notification;
    $('#modal-open-kit-goalmap').modal('show');
    $('#modal-open-kit-goalmap-content .list-container').html('<p style="text-align:center" class="text-secondary"><em>Loading...</em></p>');
    $.ajax({
      url: baseUrl + 'kitBuildApi/getGoalMaps/' + mid
    }).done(function (r) { // console.log(r);
      let list = '<p style="text-align:center" class="text-secondary"><em>No maps found on database.</em></p>';
      if (r.result != null) {
        if(r.result.length > 0) list = '';
        r.result.forEach(m => {
          list += '<div class="goalmap list-item" ';
          list += 'data-gmid="' + m.gmid + '" ';
          list += 'data-mid="' + m.mid + '" ';
          list += 'data-nc="' + m.num_concepts + '" ';
          list += 'data-nl="' + m.num_links + '">';
          list += m.name;
          list += ' <span class="badge badge-info">' + m.num_concepts + 'C</span>';
          list += ' <span class="badge badge-info">' + m.num_links + 'L</span>';
          list += '</div>';
        });
      }
      $('#modal-open-kit-goalmap-content .list-container').html(list);
    }).fail(function (e) {
      if (typeof e == 'object') {
        notification.notify('e.error', {
          type: "danger"
        });
      } else notification.notify(e);
    });
  }

  hideOpenKitGoalmapDialog() {
    $('#modal-open-kit-goalmap').modal('hide');
  }

  showLearnerMapList(maps) {
    let mapList = '';
    if (maps != null) {
      maps.forEach(m => { // lmid, gmid, uid, fullname
        mapList += '<div class="learnermap" ';
        mapList += 'data-lmid="' + m.lmid + '" data-gmid="' + m.gmid + '" >';
        mapList += '<span class="wrap">' + m.fullname + '</span></wrap>';
        mapList += '<span class="wrap">' + parseInt(m.score * 10000) / 100 + '%</span></wrap>';
        mapList += '</div>';
      });
    }
    $('#learner-maps-list-container').html(mapList);
  }

  showSidebar(show = true) {
    console.log(show);
    if (show == 'toggle') $('#sidebar-content').toggle();
    else {
      if (show) $('#sidebar-content').show();
      else $('#sidebar-content').hide();
    }
  }




}