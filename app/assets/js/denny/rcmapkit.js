class KitBuildApp {

  constructor(canvas) {
    this.canvas = canvas;
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.ajax = Ajax.ins(this.gui);
    this.analyzer = new AnalyzerLib();
    this.controller = 'denny';
    this.page = 'rcmapkit';
    this.nextPage = 'postmapping';

    this.material = {
      mid: null,
      name: null,
      content: null
    }

    this.goalmap = {
      gmid: null,
      name: null
    }

    this.eventListeners = [];

    this.kit = null; // holder for kit socket communication
    this.handleEvent();
    this.canvas.attachEventListener(this);

  }

  getCanvas() {
    return this.canvas;
  }

  getGUI() {
    return this.gui;
  }

  getSession() {
    return this.session;
  }

  handleEvent() {
    let app = this;

    $('#bt-login').on('click', function (e) {
      $('#modal-chat-login').modal('show');
      $('#modal-chat-login .input-username').focus();
    });
    let doSignIn = function () {
      let username = $('#modal-chat-login .input-username').val().trim();
      let password = $('#modal-chat-login .input-password').val().trim();
      app.ajax.post(
        baseUrl + 'kitbuildApi/signIn', {
          username: username,
          password: password
        }
      ).then(function (user) {
        let uid = user.uid;
        let rid = user.role_id;
        let gids = user.gids ? user.gids.split(",") : [];
        app.kit.setUserWithName(uid, username, user.name, rid, gids);
        app.signIn(uid, username, user.name, rid, gids);
        $('#modal-chat-login').modal('hide');
        $('#input-username').val('');
        $('#input-password').val('');
      });
    }
    $('#modal-chat-login').on('click', '.bt-ok', doSignIn);
    $('#modal-chat-login .input-password').on('keyup', function (e) {
      if (e.keyCode == 13) doSignIn();
    });
    $('#bt-logout').on('click', function (e) {
      let confirmDialog = app.gui.confirm('Do you want to logout?', {
        'positive-callback': function () {
          app.kit.leaveRoom(app.kit.room);
          app.signOut();
          confirmDialog.modal('hide');
          window.location.href = baseUrl + app.controller + '/signOut';
        }
      });
    });
    $('#bt-sync').on('click', (e) => {
      if (this.canvas.getCy().nodes().length) {
        let text = 'You currently have concept map on canvas.<br>If you sync the map, it will be <strong class="text-danger">replaced</strong>.<br>Continue?';
        let dialog = this.gui.confirm(text, {
          'positive-callback': function () {
            app.kit.syncMap();
            app.eventListeners.forEach(listener => {
              if (listener && typeof listener.onAppEvent == 'function') {
                listener.onAppEvent('request-sync');
              }
            })
            dialog.modal('hide');
          }
        })
      }
    });
    $('#bt-open-kit').on('click', function () {

      if (!app.kit.room.rid) {
        app.gui.dialog('Please join a room before opening a kit.');
        return;
      }

      app.ajax.post(
        baseUrl + 'kitbuildApi/getMaterialsWithGids', {
          gids: app.kit.user.gids
        }
      ).then(function (materials) {
        let materialList = (!materials || materials.length == 0) ?
          '<em class="text-secondary">No material available</em>' :
          '';
        materials.forEach((material) => {
          materialList += '<div class="row align-items-center list list-hover list-pointer justify-content-between pt-2 pb-2 pl-2 pr-2" data-mid="' + material.mid + '" data-name="' + material.name + '">'
          materialList += material.name
          materialList += '<i class="fas fa-check text-primary" style="display:none"></i>'
          materialList += '</div>'
        });
        $('#modal-kit .material-list').html(materialList);
        $('#modal-kit').modal('show', {
          'width': '200px'
        });
      })
    });
    $('#modal-kit .material-list').on('click', '.row', function () {
      if ($(this).hasClass('selected')) return;
      $('#modal-kit .material-list .row').find('i').hide();
      $('#modal-kit .material-list .row').removeClass('selected');
      $(this).find('i').show();
      $(this).addClass('selected')
      let mid = $(this).data('mid');
      let name = $(this).data('name');
      $('#modal-kit .bt-open').data('mid', mid);
      $('#modal-kit .bt-open').data('name', name);
      $('#modal-kit .bt-open').data('gmid', null);
      $('#modal-kit .bt-open').data('gmname', null);
      $('#modal-kit .goalmap-list').html('<em class="text-secondary">Loading goalmaps...</em>');
      app.ajax.get(
        baseUrl + 'kitbuildApi/getCollabGoalmaps/' + mid + '/' + app.kit.room.rid,
      ).then(function (goalmaps) {
        let goalmapList = (!goalmaps || goalmaps.length == 0) ?
          '<em class="text-secondary">No goalmap available</em>' :
          '';
        goalmaps.forEach((goalmap) => {
          goalmapList += '<div class="row align-items-center list list-hover list-pointer justify-content-between pt-2 pb-2 pl-2 pr-2" data-gmid="' + goalmap.gmid + '" data-name="' + goalmap.name + '">'
          goalmapList += goalmap.name
          goalmapList += '<i class="fas fa-check text-primary" style="display:none"></i>'
          goalmapList += '</div>'
        });
        $('#modal-kit .goalmap-list').html(goalmapList);
      })
    });
    $('#modal-kit .goalmap-list').on('click', '.row', function () {
      if ($(this).hasClass('selected')) return;
      $('#modal-kit .goalmap-list .row').find('i').hide();
      $('#modal-kit .goalmap-list .row').removeClass('selected');
      $(this).find('i').show();
      $(this).addClass('selected')
      let gmid = $(this).data('gmid');
      let name = $(this).data('name');
      $('#modal-kit .bt-open').data('gmid', gmid);
      $('#modal-kit .bt-open').data('gmname', name);
    });
    $('#modal-kit').on('click', '.bt-open', function () {
      let name = ($(this).data('name'))
      let mid = ($(this).data('mid'))
      let gmid = ($(this).data('gmid'))
      let gmname = ($(this).data('gmname'))
      if (!gmid) {
        app.gui.dialog('Please select a Kit.', {
          width: '300px'
        });
        return;
      }
      let confirm = app.gui.confirm('Open kit <strong>\'' + gmname + '\' of \'' + name + '\'</strong>?', {
        'positive-callback': function () {
          app.loadKit(gmid, function () {
            confirm.modal('hide');
            $('#modal-kit').modal('hide');
          });
        },
        'width': '300px'
      })
    });
    $('#bt-show-material').on('click', function () {
      $('#popup-material').fadeToggle({
        duration: 200,
        complete: function () {
          let shown = $(this).is(':visible');
          app.eventListeners.forEach(listener => {
            if (listener && typeof listener.onAppEvent == 'function') {
              if (shown) listener.onAppEvent('show-material');
              else listener.onAppEvent('hide-material');
            }
          })
        }
      });
    });
    $('#popup-material .bt-close').on('click', function () {
      $('#popup-material .check-open').prop('checked', false);
      $('#popup-material').fadeOut({
        duration: 200,
        complete: function () {
          app.eventListeners.forEach(listener => {
            if (listener && typeof listener.onAppEvent == 'function') {
              listener.onAppEvent('hide-material');
            }
          })
        }
      });
    });
    $('#bt-reset').on('click', function() {
      let confirmDialog = app.gui.confirm('Reset concept to initial group map? Your map and your friends map will also be <strong class="text-danger">RESET</strong> and synchronized. Unsaved work on map will be <strong class="text-danger">LOST</strong>.', {
        'positive-callback': function() {
          confirmDialog.modal('hide');
          let gmid = app.gmid;
          let mid = app.mid;
          let rid = app.rid;
          app.canvas.clearCanvas();
          app.loadKit(gmid, function() {
            app.loadIndividualMaps(mid, rid);
          });
        }
      })
    });
    $('#bt-open-map').on('click', function () {
      // let gmid = app.goalmap.gmid;
      // let rid = app.kit.room.rid;
      // app.ajax.get(baseUrl + 'kitbuildApi/getCollabLearnermaps/' + gmid + '/' + rid).then(function (learnermaps) { // console.log(learnermaps);
      //   let mapList = '<em>No saved map found on database.</em>';
      //   if (learnermaps && learnermaps.length) {
      //     mapList = '';
      //     learnermaps.forEach(lm => { // console.log(lm);
      //       mapList += '<div class="row align-items-center list list-hover list-pointer justify-content-between pt-2 pb-2 pl-5 pr-5" data-lmid="' + lm.lmid + '" data-name="' + lm.name + '" data-created="' + lm.create_time + '">'
      //       mapList += ' <span>saved map <span class="badge badge-info">' + lm.type + '</span></span>' + '<div><span class="badge badge-primary">Saved: ' + lm.create_time + '</span></div>'
      //       mapList += '</div>'
      //     });
      //   }
      //   $('#modal-open-map .map-list').html(mapList);
      // });
      // $('#modal-open-map').modal('show');
      let mid = app.material.mid;
      // let uid = app.kit.user.uid;
      let rid = app.kit.room.rid;
      app.ajax.get(baseUrl + 'kitbuildApi/getCollabGoalmaps/' + mid + '/' + rid).then(function (goalmaps) {
        // console.log(goalmaps);
        let mapList = '<em>No finalized map found on database.</em>';
        if (goalmaps && goalmaps.length) {
          mapList = '';
          goalmaps.forEach(gm => {
            // console.log(gm);
            mapList += '<div class="row align-items-center list list-hover list-pointer justify-content-between pt-2 pb-2 pl-5 pr-5" data-gmid="' + gm.gmid + '" data-name="' + gm.name + '">'
            mapList += '<span>' + gm.name + ' <span class="badge badge-info">' + gm.type + '</span></span>' + '<div><span class="badge badge-warning">Concepts:' + gm.concepts_count + ' Links:' + gm.links_count + '</span>' + ' <span class="badge badge-primary">Saved: ' + gm.create_time + '</span> <span class="badge badge-secondary">' + gm.username + '</span></div>'
            mapList += '</div>'
          });
        }
        $('#modal-open-map .map-list').html(mapList);
      });
      $('#modal-open-map').modal('show');
    });
    $('#modal-open-map .map-list').on('click', '.row', function () {
      let gmid = $(this).data('gmid');
      let name = $(this).data('name');
      // console.log(gmid, name);
      if (app.canvas.getCy().nodes().length) {
        let dialog = app.gui.confirm('You have map on canvas. Opening the map will replace existing map on canvas. Open map "' + name + '"?', {
          'positive-callback': function () {
            app.loadSavedGoalmap(gmid, function () {
              dialog.modal('hide');
              $('#modal-open-map').modal('hide');
            });
          }
        });
        return;
      } else {
        app.loadSavedGoalmap(gmid, function () {
          $('#modal-open-map').modal('hide');
        });
      }
    })
    $('#bt-save').on('click', function () {
      let gmid = app.goalmap.gmid;
      let rid = app.kit.room.rid;
      let uid = app.kit.user.uid;
      let mid = app.material.mid;
      if (mid != null && uid != null && rid != null) {
        app.saveGoalmapDraft(mid, rid, uid, function(gmid) {
        // app.saveLearnermapDraft(gmid, rid, uid, function (lmid) {
          if (gmid == null) {
            app.gui.notify('Unable to save map', {
              type: 'warning'
            });
            return;
          }
          app.gui.notify('Map saved.', {
            type: 'success'
          })
        });
      } else {
        app.gui.notify('Invalid map data', {type: 'danger'});
      }
    });
    $('#bt-load-draft').on('click', function () {
      let mid = app.material.mid;
      let rid = app.kit.room.rid;
      if (mid != null && rid != null) {
        let currentEles = app.canvas.getCy().nodes();
        if (currentEles.length) {
          let confirm = app.gui.confirm('You currently have existing map data on canvas. Loading saved draft map data will <strong class="text-danger">REPLACE</strong> current map on canvas and across <strong class="text-danger">ALL users</strong> in current room.<br>Continue?', {
            'positive-callback': function () {
              app.loadSavedGoalmapDraft(mid, rid, function () {
                confirm.modal('hide');
              });
            }
          })
        } else app.loadSavedGoalmapDraft(mid, rid);
      } else {
        if (!mid) app.gui.dialog('Please select a material. A concept map saved draft is related with a material.')
        if (!rid) app.gui.dialog('Please select a room. A collaborative concept map saved draft is related with a collaboration room.')
      }
    });
    // $('#bt-feedback').on('click', function () {
    //   // console.warn('feedback')
    //   app.kit.sendFeedback();
    //   app.learnermap = app.buildLearnermap();
    //   // console.log(app.learnermap, app.goalmap);
    //   let compare = app.analyzer.compare(app.learnermap, app.goalmap);

    //   let message = '';
    //   if (compare.excess.length > 0) {
    //     message += '<p>You have ' + compare.excess.length + ' unmatched propositions with teacher\'s map.<br>Unmatched propositions are marked with red dashed line.</p>';
    //     for (let e of compare.excess) {
    //       let link = app.canvas.getCy().nodes('#l-' + e.lid);
    //       if (link.length) {
    //         link.connectedEdges().data('match', 5);
    //         link.connectedEdges().data('link', 'miss');
    //       }
    //     }
    //   }
    //   if (compare.leave.length) message += '<p>You have ' + compare.leave.length + ' unconnected links.</p>';
    //   if (message.trim() != '') app.gui.dialog(message);
    //   app.eventListeners.forEach(listener => {
    //     if (listener && typeof listener.onAppEvent == 'function') {
    //       listener.onAppEvent('get-feedback', {
    //         excess: compare.excess.length,
    //         leave: compare.leave.length
    //       });
    //     }
    //   })
    // });
    $('#bt-finalize').on('click', function () {
      if (app.getCanvas().getCy().nodes().length != 0) {
        let imageData = app.getCanvas().getCy().png({
          scale: 2,
          full: true
        });
        $('#popup-map').find('img.cmap').attr('src', imageData);
        setTimeout(function () {
          console.log('click')
          $('#popup-map').show().center().center();
        }, 100);
      } else {
        confirmFinalize();
      }
    });
    $('#popup-map').on('click', '.bt-cancel', function () {
      $('#popup-map').hide();
    });

    $('#popup-map').on('click', '.bt-download', function () {
      let mid = app.material.mid;
      let uid = app.kit.user.uid;
      let rid = app.kit.room.rid;
      var a = document.createElement("a"); //Create <a>
      a.href = $('#popup-map').find('img.cmap').attr('src');
      a.download = "concept-map-m" + mid + "-r" + rid + "-u" + uid + ".png"; //File name Here
      a.click(); //Downloaded file
    })

    let confirmFinalize = function () {
      let mid = app.material.mid;
      let uid = app.kit.user.uid;
      let rid = app.kit.room.rid;
      if (mid != null && uid != null && rid != null) {
        let confirm = app.gui.confirm('Save and finalize the map? <br>Any previously saved draft concept maps will be deleted, preserving the finalized one.', {
          'positive-callback': function () {
            confirm.modal('hide');
            $('#modal-map-name').modal('show');
            $('#modal-map-name .map-name-input').val('final-m' + mid + '-r' + rid + '-u' + uid + '-' + app.kit.user.username).focus().select();
          },
          'negative-callback': function () {
            confirm.modal('hide');
          }
        })
      } else {
        if (!mid) {
          app.gui.dialog('Please open a material to save the map into.<br>A map should refer to a material.');
          return;
        }
        if (!rid) {
          app.gui.dialog('Please join a room to save the map into.<br>In a collaboration environment, a saved maps should relate with a collaboration group. Therefore, you need to join a room before you create and save a concept map.');
          return;
        }
      }
    }

    $('#popup-map').on('click', '.bt-continue', function () {
      confirmFinalize();
    });

    $('#modal-map-name').on('click', '.bt-ok', function () {
      let mid = app.material.mid;
      let uid = app.kit.user.uid;
      let rid = app.kit.room.rid;
      if (mid != null && uid != null && rid != null) {
        let name = $('#modal-map-name .map-name-input').val().trim()
        if (name == '')
          app.gui.dialog('Concept map name should not be empty.')
        else {
          app.saveGoalmapFinalize(mid, rid, uid, name, function (gmid) {
            $('#modal-map-name').modal('hide');
            $('#popup-map').hide();
            app.gui.notify('Map finalized', {
              type: 'success'
            })
            app.eventListeners.forEach(listener => {
              if (listener && typeof listener.onAppEvent == 'function') {
                listener.onAppEvent('finalize-map', {
                  gmid: gmid
                });
              }
            })
            app.kit.sendRoomMessage('change-page', null, {
              page: app.nextPage,
              from: app.page
            }, function () {
              app.session.set('page', app.nextPage, function () {
                window.location.href = baseUrl + app.controller + '/' + app.nextPage;
                return;
              }, function (error) {
                app.gui.dialog('Error: ' + error);
              })
            });
          });
        }
      } else {
        if (!mid) {
          app.gui.dialog('Please open a material to save the map into.<br>A map should refer to a material.');
          return;
        }
        if (!rid) {
          app.gui.dialog('Please join a room to save the map into.<br>In a collaboration environment, a saved maps should relate with a collaboration group. Therefore, you need to join a room before you create and save a concept map.');
          return;
        }
      }
    });
    $('.material-content').on('mouseup', function (e) {
      let s = window.getSelection();
      if(s.anchorNode == null) return;
      let oRange = s.getRangeAt(0); //get the text range
      let text = oRange.toString().trim();
      if (text.length) {
        let oRect = oRange.getBoundingClientRect();
        // console.log(s, oRange, oRect);
        console.log(app.canvas.settings);
        if(!app.canvas.settings.enableConceptCreation)
          $('#popup-selection .menu-item[data-type="concept"]').hide();
        else $('#popup-selection .menu-item[data-type="concept"]').show();
        if(!app.canvas.settings.enableLinkCreation)
        $('#popup-selection .menu-item[data-type="link"]').hide(); 
        else $('#popup-selection .menu-item[data-type="link"]').show();
        $('#popup-selection .selected-text').html(text);
        $('#popup-selection').show()
          .css('top', oRect.top)
          .css('left', oRect.left + oRect.width + 10);
        e.stopImmediatePropagation();
      } else $('#popup-selection').hide();
    });
    $('#popup-selection').on('click', '.menu-item', function () {
      let text = $(this).find('strong').text();
      let type = $(this).data('type');
      $('#input-node-label').val(text);
      app.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('new-' + type + '-from-selection', {
            type: type,
            text: text
          });
        }
      })
      switch (type) {
        case 'concept':
          $('#bt-new-concept').click();
          $('#input-node-label').val(text);
          break;
        case 'link':
          $('#bt-new-link').click();
          $('#input-node-label').val(text);
          break;
      }
      $('#popup-selection').hide()
    });
    $('.material-content').on('scroll', function () {
      let s = window.getSelection();
      if(s.anchorNode == null) return;
      let oRange = s.getRangeAt(0); //get the text range
      let oRect = oRange.getBoundingClientRect();
      if (oRect.top - $(this).offset().top > -24 &&
        oRect.top < $(this).offset().top + $(this).height()) {
        $('#popup-selection').show();
      } else $('#popup-selection').hide();
      if ($('#popup-selection').is(':visible'))
        $('#popup-selection').css('top', oRect.top).css('left', oRect.left + oRect.width + 10);
    });
    $('#popup-material').on('mouseup', function () {
      $('#popup-selection').hide();
    });
    $(document).on('mouseup', function (e) {
      var container = $("#popup-material");
      var button = $('#bt-show-material')
      if (!container.is(e.target) && container.has(e.target).length === 0 &&
        !button.is(e.target) && button.has(e.target).length === 0) {
        $('#popup-selection').hide();
        if ($('#popup-material .check-open').prop('checked')) return;
        if (container.is(':visible')) {
          container.fadeOut();
          app.eventListeners.forEach(listener => {
            if (listener && typeof listener.onAppEvent == 'function') {
              listener.onAppEvent('hide-material');
            }
          });
        }
      }
    });
  }

  attachEventListener(listener) {
    this.eventListeners.push(listener);
  }

  attachKit(kit) { // console.log(kit);
    this.kit = kit;
    this.kit.attachEventListener(this);
    this.kit.enableRoom(true);
    this.kit.enableChannel(true);
    this.autoSave();
  }

  autoSave() {
    try {
      let app = this;
      let mid = app.material.mid;
      let rid = app.kit.room.rid;
      let uid = app.kit.user.uid;
      if(mid && rid && uid) this.saveGoalmapAuto(mid, rid, uid, function(data){
        if(data != null) console.log('autosaving... ' + data);
      })
    } catch(error) {
      console.log('autosave error: ' + error);
    }
    setTimeout(this.autoSave.bind(this), 300000);
  }

  onKitEvent(event, data) {
    let gui = this.gui;
    let kit = this.kit;
    let canvas = this.canvas;
    let app = this;

    let gmid = app.goalmap.gmid;
    let mid = app.material.mid;
    let uid = app.kit.user.uid;
    let rid = app.kit.room.rid;

    switch (event) {
      case 'join-room':
        this.session.set('room', data.room);
        // this.canvas.clearCanvas();
        kit.rooms.forEach(room => {
          // console.log(room, kit)
          if (room.name == kit.room.name && room.users.length) {
            kit.syncMap();
            kit.syncMessages();
            return;
          }
        })
        $('#bt-sync').prop('disabled', false)
        break;
      case 'leave-room':
        this.session.unset('room');
        break;
      case 'room-update':
        kit.rooms = data;
        break;
      // case 'user-sign-in':
      //   this.session.set('user', data);
      //   break;
      // case 'user-sign-out':
      //   this.session.unset('user');
      //   break;
      case 'command-sign-out':
        this.signOut();
        this.kit.leaveRoom(this.kit.room);
        this.kit.enableRoom(false)
        this.kit.enableMessage(false)
        this.gui.dialog('You have been signed out');
        break;
      case 'command-leave-room':
        // this.signOut();
        // this.kit.leaveRoom(this.kit.room);
        // this.kit.enableRoom(false)
        // this.kit.enableMessage(false)
        // this.gui.dialog('You have been signed out');
        break;
      case 'request-map-data':
        let nodes = this.canvas.getCy().nodes();
        let edges = this.canvas.getCy().edges();
        let elesData = [];
        nodes.forEach(n => {
          elesData.push({
            group: 'nodes',
            data: n.data(),
            position: n.position()
          });
        });
        edges.forEach(n => {
          elesData.push({
            group: 'edges',
            data: n.data(),
            position: n.position()
          });
        });
        return elesData;
      case 'request-material-data':
        return this.material;
      case 'request-goalmap-data':
        return this.goalmap;
      case 'sync-messages-data':
        break;
      case 'sync-map-data': //console.log(data);
        if (data.length == 0) break;
        console.log('replace map');
        canvas.reset();
        canvas.clearCanvas();
        canvas.showMap(data);
        canvas.centerOneToOne();
        gui.notify('Map data synchronized', {
          type: 'success'
        })
        $('#bt-center').click();
        break;
      case 'sync-material-data':
        if (data.mid == null) return;
        this.setMaterial(data.mid, data.name, data.content);
        this.enableMaterial();
        break;
      case 'sync-goalmap-data':
        if (data.gmid == null) return;
        // this.setGoalmap(data.gmid, data.name);
        if (this.goalmap) {
          this.goalmap.gmid = data.gmid;
          this.goalmap.name = data.name;
        }
        break;
      case 'sync-map-data-error': // console.log('sync-map-data-error received')
        this.gui.notify(data.error, {
          type: 'danger'
        })
        break;
      case 'chat-message-open':
        this.session.set('chat-window-open', data)
        break;
      case 'receive-create-node':
        let empty = (this.canvas.getCy().nodes().length == 0);
        this.canvas.createNodeFromJSON(data.node);
        if (empty) $('#bt-center').click();
        break;
      case 'receive-move-node':
        this.canvas.moveNode(data.node.id, data.node.x, data.node.y);
        break;
      case 'receive-move-node-group': // including from move-drag-group event
        data.nodes.forEach(n => {
          this.canvas.moveNode(n.id, n.x, n.y);
        });
        break;
      case 'receive-connect-node':
        this.canvas.connect(data.connection);
        break;
      case 'receive-disconnect-node':
        this.canvas.disconnect(data.connection);
        break;
      case 'receive-change-connect':
        this.canvas.connect(data.to);
        this.canvas.disconnect(data.from);
        break;
      case 'receive-duplicate-node':
        this.canvas.createNodeFromJSON(data.node);
        break;
      case 'receive-delete-node':
        this.canvas.deleteNode(data.node);
        break;
      case 'receive-delete-node-group':
        this.canvas.deleteNodes(data.nodes);
        break;
      case 'receive-update-node':
        this.canvas.updateNode(data.node.nodeId, data.node.value);
        break;
      case 'receive-center-link':
        // console.log(data);
        this.canvas.moveNode(data.link.id, data.link.x, data.link.y);
        break;
      case 'receive-switch-direction': // data is linkId
        this.canvas.switchDirection(data);
        break;
      case 'change-page':
        if (!data.from || data.from != app.page) break;
        if (mid != null && uid != null && rid != null) {
          let name = 'final-m' + mid + '-r' + rid + '-u' + uid + '-' + app.kit.user.username;
          app.saveGoalmapFinalize(mid, rid, uid, name, function (gmid) {
            if(gmid == null) {
              app.gui.notify('Unable to save empty map', {type: 'warning'});
              return;
            }
            $('#modal-map-name').modal('hide');
            $('#popup-map').hide();
            app.gui.notify('Map finalized', {
              type: 'success'
            })
            app.eventListeners.forEach(listener => {
              if (listener && typeof listener.onAppEvent == 'function') {
                listener.onAppEvent('finalize-map', {
                  gmid: gmid
                });
              }
            })
            app.session.set('page', data.page, function () {
              app.gui.dialog('Your friend have finalized the map. You will be redirected to the next page in 3s');
              setTimeout(function(){
                window.location.href = baseUrl + app.controller + '/' + data.page;
              }, 3000);
              return;
            }, function (error) {
              this.gui.dialog('Error: ' + error);
            })
          });
        }
        break;
      case 'admin-message':
        app.gui.notify("<strong>Admin:</strong> " + data.message, data.options);
        break;
      case 'save-map-draft':
        $('#bt-save').click();
        break;
      case 'save-map-finalize':
        if (mid != null && uid != null && rid != null) {
          let name = 'final-m' + mid + '-r' + rid + '-u' + uid + '-' + app.kit.user.username;
          app.saveGoalmapFinalize(mid, rid, uid, name, function (gmid) {
            if(gmid == null) {
              app.gui.notify('Unable to save empty map', {type: 'warning'});
              return;
            }
            $('#modal-map-name').modal('hide');
            $('#popup-map').hide();
            app.gui.notify('Map finalized', {
              type: 'success'
            })
            app.eventListeners.forEach(listener => {
              if (listener && typeof listener.onAppEvent == 'function') {
                listener.onAppEvent('finalize-map', {
                  gmid: gmid
                });
              }
            })
          });
        }
        break;
        // Collab Event From Kit
      case 'send-message':
        this.eventListeners.forEach(listener => {
          if (listener && typeof listener.onCollabEvent == 'function') {
            listener.onCollabEvent('send-room-message', data.message);
          }
        })
        break;
      case 'close-discuss-channel':
        this.eventListeners.forEach(listener => {
          if (listener && typeof listener.onCollabEvent == 'function') {
            listener.onCollabEvent(event, data);
          }
        })
        break;
      case 'send-channel-message':
        this.eventListeners.forEach(listener => {
          if (listener && typeof listener.onCollabEvent == 'function') {
            listener.onCollabEvent(event, {
              channel: data.channel,
              message: data.message
            });
          }
        })
        break;
      case 'update-channel-list': // { data.indicators, data.message: nullable }
        if (data.message) {
          console.log(data.message.channel, $('#modal-channel').attr('data-cid'))
          if ($('#modal-channel').attr('data-cid') != data.message.channel.cid) {
            app.gui.notify('New discussion message is coming for ' + data.message.channel.node_type + ': ' + data.message.channel.node_label, {
              delay: 2000
            });
          }
        }
        break;
      case 'channel-item-clicked':
        let id = data;
        app.getCanvas().getCy().animate({
          center: {
            eles: '#' + id
          },
          duration: 200
        })
        break;
      case 'send-feedback':
        app.learnermap = app.buildLearnermap();
        // console.log(app.learnermap, app.goalmap);
        let compare = app.analyzer.compare(app.learnermap, app.goalmap);
        if (compare.excess.length > 0) {
          for (let e of compare.excess) {
            let link = app.canvas.getCy().nodes('#l-' + e.lid);
            if (link.length) {
              link.connectedEdges().data('match', 5);
              link.connectedEdges().data('link', 'miss');
            }
          }
        }
        break;

    }
  }

  onCanvasEvent(event, data) { // console.log(event, data);
    if (!this.kit) return;
    switch (event) {
      case 'create-concept':
        this.kit.createNode(data);
        break;
      case 'create-link':
        this.kit.createNode(data);
        break;
      case 'drag-concept':
        this.kit.dragNode(data);
        break;
      case 'drag-link':
        this.kit.dragNode(data);
        break;
      case 'drag-node-group':
        this.kit.dragNodeGroup(data);
        break;
      case 'move-concept':
        this.kit.moveNode(data);
        break;
      case 'move-link':
        this.kit.moveNode(data);
        break;
      case 'move-node-group':
        this.kit.moveNodeGroup(data);
        break;
      case 'connect-left':
        this.kit.connectNode(data);
        break;
      case 'connect-right':
        this.kit.connectNode(data);
        break;
      case 'disconnect-left':
        this.kit.disconnectNode(data);
        break;
      case 'disconnect-right':
        this.kit.disconnectNode(data);
        break;
      case 'change-connect-left':
      case 'change-connect-right':
        this.kit.changeConnection(data);
        break;
      case 'duplicate-concept':
      case 'duplicate-link':
        this.kit.duplicateNode(data);
        break;
      case 'delete-node':
      case 'delete-link':
      case 'delete-concept':
        this.kit.deleteNode(data);
        break;
      case 'delete-node-group':
        this.kit.deleteNodeGroup(data);
        break;
      case 'update-concept-name':
      case 'update-link-name':
        this.kit.updateNode(data);
        break;
      case 'center-link':
        this.kit.centerLink(data);
        break;
      case 'switch-direction':
        this.kit.switchDirection(data);
        break;
      case 'canvas-layout':
        let n = this.canvas.getCy().nodes();
        n = n.union(this.canvas.getCy().edges());
        this.kit.sendMap(n.jsons());
        break;
    }
  }

  onCanvasToolClicked(tool, node) { // console.log(tool, node);

    // for listener purposes
    let action = null;
    let data = null;
    let listenerHandled = false;
    let app = this;

    switch (tool.type) {
      case 'discuss-channel':
        let nodeId = node.id();
        let nodeType = node.data('type');
        let nodeLabel = node.data('name');
        app.kit.openChannel(nodeId, nodeType, nodeLabel, function (channel) {
          action = 'open-discuss-channel';
          app.eventListeners.forEach(listener => {
            if (listener && typeof listener.onCollabEvent == 'function') {
              listener.onCollabEvent(action, {
                cid: channel.cid,
                rid: channel.rid,
                node_id: channel.node_id,
                node_type: channel.node_type,
                node_label: channel.node_label
              });
            }
          })
        });
        listenerHandled = true;
        break;
      case 'center-link':
      case 'switch-direction':
        listenerHandled = true;
        break;
    }

    if (listenerHandled) return;
    this.eventListeners.forEach(listener => {
      if (listener && typeof listener.onCollabEvent == 'function') {
        listener.onCollabEvent(action, data);
      }
    })
  }


  // Commanding Kit-Build App Interface

  signIn(uid, username, name, rid, gids) {
    $('#bt-login').hide();
    $('#bt-logout').show();
    $('#bt-logout-username').html(": " + username);
    $('#bt-open-kit').prop('disabled', false);
    this.kit.enableMessage();
    this.enableSaveDraft();
    this.enableFinalize();
  };

  signOut() {
    $('#bt-login').show();
    $('#bt-logout').hide();
    $('#bt-open-kit').prop('disabled', true);
    $('#bt-show-material').prop('disabled', true);
    this.enableSaveDraft(false);
    this.enableFinalize(false);
    this.unsetMaterial();
  }

  enableMaterial(enabled = true) {
    $('#bt-show-material').prop('disabled', !enabled);
  }

  enableSaveDraft(enable = true) {
    $('#bt-save').prop('disabled', !enable);
    $('#bt-load-draft').prop('disabled', !enable);
  }

  enableFinalize(enable = true) {
    $('#bt-finalize').prop('disabled', !enable);
  }

  loadMaterial(mid, confirmDialog) {
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/getMaterialCollabByMid/' + mid
    ).then(function (material) {
      if (confirmDialog) confirmDialog.modal('hide');
      app.session.set('mid', material.mid);
      app.setMaterial(material.mid, material.name, material.content);
      app.kit.sendMaterial(material.mid, material.name, material.content);
      $('#modal-kit').modal('hide');
    })
    $('#bt-show-material').prop('disabled', false);
  }

  setMaterial(mid, name, content) {
    this.material.mid = mid;
    this.material.name = name;
    this.material.content = content;
    $('#popup-material .material-title').html(this.material.name)
    // /<\/?[a-z][\s\S]*>/i.test(this.material.content);
    $('#popup-material .material-content').html(this.material.content);
  }

  unsetMaterial() {
    this.material.mid = null;
    this.material.name = null;
    this.material.content = null;
  }

  setGoalmap(goalmap) {
    this.goalmap = goalmap;
    this.goalmap.gmid = goalmap.goalmap.gmid;
    this.goalmap.name = goalmap.goalmap.name;
  }

  unsetGoalmap() {
    this.goalmap.gmid = null;
    this.goalmap.name = null;
  }

  // saveLearnermap(gmid, rid, uid, type, callback) {
  //   let app = this;
  //   let learnermaps = {
  //     type: type,
  //     gmid: gmid,
  //     uid: uid,
  //     concepts: [],
  //     links: [],
  //     rid: rid
  //   }

  //   let cs = app.canvas.getNodes('[type="concept"]');
  //   let ls = app.canvas.getNodes('[type="link"]');
  //   let es = app.canvas.getEdges();

  //   if (cs.length == 0 && ls.length == 0) {
  //     if (typeof callback == 'function') callback(null);
  //     return;
  //   };

  //   // console.log(cs, ls, es);
  //   cs.forEach(c => {
  //     learnermaps.concepts.push({
  //       cid: c.data.id.substr(2),
  //       gmid: gmid,
  //       locx: c.position.x,
  //       locy: c.position.y
  //     });
  //   });
  //   ls.forEach(l => {
  //     let tl = {
  //       lid: l.data.id.substr(2),
  //       gmid: gmid,
  //       locx: l.position.x,
  //       locy: l.position.y,
  //       source: null,
  //       target: null
  //     };
  //     es.forEach((e) => {
  //       if (e.data.source == l.data.id) {
  //         if (e.data.type == 'right') tl.target = e.data.target.substr(2);
  //         if (e.data.type == 'left') tl.source = e.data.target.substr(2);
  //         return;
  //       }
  //     });
  //     learnermaps.links.push(tl);
  //   })
  //   app.ajax.post(
  //     baseUrl + 'kitbuildApi/saveLearnermapCollab',
  //     learnermaps
  //   ).then(function (lmid) {
  //     app.eventListeners.forEach(listener => {
  //       if (listener && typeof listener.onAppEvent == 'function') {
  //         listener.onAppEvent('map-save-' + type, {
  //           lmid: lmid
  //         });
  //       }
  //     })
  //     if (callback) callback(lmid)
  //   })
  // }

  // saveLearnermapDraft(gmid, rid, uid, callback) {
  //   this.saveLearnermap(gmid, rid, uid, 'draft', callback)
  // }

  // saveLearnermapFinalize(gmid, rid, uid, callback) {
  //   this.saveLearnermap(gmid, rid, uid, 'fix', callback)
  // }

  // saveLearnermapAuto(gmid, rid, uid, callback) {
  //   this.saveLearnermap(gmid, rid, uid, 'auto', callback)
  // }

  saveGoalmap(mid, rid, uid, type, name, callback) { // console.log(name);
    let app = this;
    let goalmaps = {
      rid: rid,
      name: name ? name : type + '-m' + mid + '-r' + rid + '-u' + uid,
      type: type,
      mid: mid,
      creator_id: uid,
      updater_id: uid,
      concepts: [],
      links: []
    }
    let cs = app.canvas.getNodes('[type="concept"]');
    let ls = app.canvas.getNodes('[type="link"]');
    let es = app.canvas.getEdges();

    if(cs.length == 0 && ls.length == 0) {
      if(typeof callback == 'function') callback(null);
      return;
    };
    // console.log(cs, ls, es);
    cs.forEach(c => {
      goalmaps.concepts.push({
        cid: c.data.id.substr(2),
        gmid: null,
        label: c.data.name,
        locx: c.position.x,
        locy: c.position.y
      });
    });
    ls.forEach(l => {
      let tl = {
        lid: l.data.id.substr(2),
        gmid: null,
        label: l.data.name,
        locx: l.position.x,
        locy: l.position.y,
        source: null,
        target: null
      };
      es.forEach((e) => {
        if (e.data.source == l.data.id) {
          if (e.data.type == 'right') tl.target = e.data.target.substr(2);
          if (e.data.type == 'left') tl.source = e.data.target.substr(2);
          return;
        }
      });
      goalmaps.links.push(tl);
    })
    app.ajax.post(
      baseUrl + 'kitbuildApi/saveGoalmapCollab',
      goalmaps
    ).then(function (gmid) {
      app.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('map-save-' + type, {
            gmid: gmid
          });
        }
      })
      if (callback) callback(gmid)
    })
  }

  saveGoalmapDraft(mid, rid, uid, callback) {
    this.saveGoalmap(mid, rid, uid, 'draft', null, callback)
  }

  saveGoalmapFinalize(mid, rid, uid, name, callback) {
    this.saveGoalmap(mid, rid, uid, 'fix', name, callback)
  }

  saveGoalmapAuto(mid, rid, uid, callback) {
    this.saveGoalmap(mid, rid, uid, 'auto', null, callback)
  }

  checkAvailableLearnermapDraft(gmid, rid) {
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/getLastDraftLearnermap/' + gmid + '/' + rid
    ).then(function (lmid) {
      if (lmid) $('#bt-load-draft').prop('disabled', false);
    })
  }

  loadSavedLearnermapDraft(gmid, rid, callback) { // console.log(mid, uid, rid);
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/loadLastDraftLearnermapCollab/' + gmid + "/" + rid
    ).then(function (draft) {
      if (draft == null) {
        app.gui.notify('Nothing to load. No previously saved draft maps found.')
        return;
      }
      let learnermap = draft.learnermap;
      let concepts = draft.concepts;
      let links = draft.links;
      let eles = [];
      concepts.forEach((c) => {
        eles.push({
          group: 'nodes',
          data: {
            id: 'c-' + c.cid,
            name: c.label,
            type: 'concept'
          },
          position: {
            x: parseInt(c.locx),
            y: parseInt(c.locy)
          }
        })
      })
      links.forEach((l) => {
        eles.push({
          group: 'nodes',
          data: {
            id: 'l-' + l.lid,
            name: l.label,
            type: 'link'
          },
          position: {
            x: parseInt(l.locx),
            y: parseInt(l.locy)
          }
        })
        if (l.source != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.source,
              type: 'left'
            }
          })
        }
        if (l.target != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.target,
              type: 'right'
            }
          })
        }
      })
      // let currentEles = app.canvas.getCy().nodes();
      // if (currentEles.length) {
      //   let confirm = app.gui.confirm('You currently have existing map data on canvas. Loading saved draft map data will <strong class="text-danger">REPLACE</strong> current map on canvas and across <strong class="text-danger">ALL users</strong> in current room.<br>Continue?', {
      //     'positive-callback': function () {
      //       confirm.modal('hide');
      //       app.canvas.clearCanvas();
      //       app.canvas.getCy().add(eles);
      //       app.gui.notify('Concept map loaded from last saved drafts.')
      //       app.kit.sendMap(eles);
      //       $('#bt-center').click();
      //     }
      //   })
      // } else {
      app.canvas.clearCanvas();
      app.canvas.getCy().add(eles);
      app.gui.notify('Concept map loaded from last saved drafts.')
      app.kit.sendMap(eles);
      app.kit.sendKit(app.goalmap)
      $('#bt-center').click();
      app.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('load-last-draft', {
            lmid: parseInt(learnermap.lmid)
          });
        }
      })
      if (typeof callback == "function") callback();
      // }
    })
  }

  loadKit(gmid, callback) {
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/openKit/' + gmid
    ).then(function (kit) {
      console.log(kit);
      let goalmap = kit.goalmap;
      let concepts = kit.concepts;
      let links = kit.links;
      app.setGoalmap(kit);
      app.session.set('gmid', app.goalmap.gmid);
      let nodes = [];
      concepts.forEach(c => {
        nodes.push({
          group: "nodes",
          data: {
            id: "c-" + c.cid,
            name: c.label,
            type: 'concept'
          },
          position: {
            x: parseInt(c.locx),
            y: parseInt(c.locy)
          }
        })
      })
      // links.forEach(l => {
      //   nodes.push({
      //     group: "nodes",
      //     data: {
      //       id: "l-" + l.lid,
      //       name: l.label,
      //       type: 'link'
      //     },
      //     position: {
      //       x: parseInt(l.locx),
      //       y: parseInt(l.locy)
      //     }
      //   })
      // })
      // console.log(nodes)
      app.getCanvas().getCy().elements().remove();
      app.getCanvas().getCy().add(nodes);
      app.getCanvas().getCy().layout({
        name: 'grid',
        fit: false,
        condense: true,
        stop: function () {
          let nodes = app.getCanvas().getCy().nodes().toArray(); // console.log(nodes);
          let eles = [];
          nodes.forEach(node => {
            eles.push({
              group: 'nodes',
              data: {
                id: node.id(),
                name: node.data('name'),
                type: node.data('type')
              },
              position: {
                x: node.position().x,
                y: node.position().y
              }
            })
          })
          //app.kit.sendKit(goalmap);
          //app.kit.sendMap(eles);
          $('#bt-center').click();
        }
      }).run();
      if (typeof callback == 'function') callback();
    })
  }

  loadSavedLearnermap(lmid, callback) { // console.log(mid, uid, rid);
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/getLearnermap/' + lmid
    ).then(function (cmap) {
      if (cmap == null) {
        app.gui.notify('Error loading concept map.')
        return;
      }
      let goalmap = cmap.goalmap;
      let concepts = cmap.concepts;
      let links = cmap.links;
      let eles = [];
      concepts.forEach((c) => {
        eles.push({
          group: 'nodes',
          data: {
            id: 'c-' + c.cid,
            name: c.label,
            type: 'concept'
          },
          position: {
            x: parseInt(c.locx),
            y: parseInt(c.locy)
          }
        })
      })
      links.forEach((l) => {
        eles.push({
          group: 'nodes',
          data: {
            id: 'l-' + l.lid,
            name: l.label,
            type: 'link'
          },
          position: {
            x: parseInt(l.locx),
            y: parseInt(l.locy)
          }
        })
        if (l.source != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.source,
              type: 'left'
            }
          })
        }
        if (l.target != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.target,
              type: 'right'
            }
          })
        }
      })
      app.canvas.clearCanvas();
      app.canvas.getCy().add(eles);
      app.gui.notify('Concept map loaded.')
      app.kit.sendMap(eles);
      $('#bt-center').click();
      app.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('load-concept-map', {
            lmid: parseInt(cmap.lmid)
          });
        }
      })
      if (typeof callback == 'function') callback();
    })
  }

  buildLearnermap() {
    let app = this;
    let learnermap = {
      concepts: [],
      links: []
    }
    let concepts = app.canvas.getCy().nodes('[type="concept"]');
    let links = app.canvas.getCy().nodes('[type="link"]');
    for (let c of concepts) {
      learnermap.concepts.push({
        cid: c.id().substr(2),
        label: c.data('name')
      })
    }
    for (let l of links) {
      let left = l.connectedEdges('[type="left"]');
      let right = l.connectedEdges('[type="right"]');
      // console.log(left, right);
      let source = null,
        target = null;
      if (left.length == 1) source = left.connectedNodes('[type="concept"]')
      if (right.length == 1) target = right.connectedNodes('[type="concept"]')
      // console.log(source, target);
      learnermap.links.push({
        lid: l.id().substr(2),
        label: l.data('name'),
        source: source ? source.id().substr(2) : null,
        target: target ? target.id().substr(2) : null
      });
    }
    // console.log(concepts, links);
    return learnermap;
  }

  loadSavedGoalmapDraft(mid, rid, callback) { // console.log(mid, uid, rid);
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/loadLastDraftGoalmapCollab/' + mid + "/" + rid
    ).then(function (draft) {
      if (draft == null) {
        app.gui.notify('Nothing to load. No previously saved draft maps found.')
        return;
      }
      let goalmap = draft.goalmap;
      let concepts = draft.concepts;
      let links = draft.links;
      let eles = [];
      concepts.forEach((c) => {
        eles.push({
          group: 'nodes',
          data: {
            id: 'c-' + c.cid,
            name: c.label,
            type: 'concept'
          },
          position: {
            x: parseInt(c.locx),
            y: parseInt(c.locy)
          }
        })
      })
      links.forEach((l) => {
        eles.push({
          group: 'nodes',
          data: {
            id: 'l-' + l.lid,
            name: l.label,
            type: 'link'
          },
          position: {
            x: parseInt(l.locx),
            y: parseInt(l.locy)
          }
        })
        if (l.source != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.source,
              type: 'left'
            }
          })
        }
        if (l.target != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.target,
              type: 'right'
            }
          })
        }
      })
      app.canvas.clearCanvas();
      app.canvas.getCy().add(eles);
      app.gui.notify('Concept map loaded from last saved drafts.')
      app.kit.sendMap(eles);
      $('#bt-center').click();
      app.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('load-last-draft', {
            gmid: parseInt(goalmap.gmid)
          });
        }
      })
      if (typeof callback == 'function') callback();
    })
  }

  loadSavedGoalmap(gmid, callback) { // console.log(mid, uid, rid);
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/openKit/' + gmid
    ).then(function (cmap) {
      if (cmap == null) {
        app.gui.notify('Error loading concept map.')
        return;
      }
      let goalmap = cmap.goalmap;
      let concepts = cmap.concepts;
      let links = cmap.links;
      let eles = [];
      concepts.forEach((c) => {
        eles.push({
          group: 'nodes',
          data: {
            id: 'c-' + c.cid,
            name: c.label,
            type: 'concept'
          },
          position: {
            x: parseInt(c.locx),
            y: parseInt(c.locy)
          }
        })
      })
      links.forEach((l) => {
        eles.push({
          group: 'nodes',
          data: {
            id: 'l-' + l.lid,
            name: l.label,
            type: 'link'
          },
          position: {
            x: parseInt(l.locx),
            y: parseInt(l.locy)
          }
        })
        if (l.source != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.source,
              type: 'left'
            }
          })
        }
        if (l.target != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.target,
              type: 'right'
            }
          })
        }
      })
      app.canvas.clearCanvas();
      app.canvas.getCy().add(eles);
      app.gui.notify('Concept map loaded.')
      app.kit.sendMap(eles);
      $('#bt-center').click();
      app.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('load-concept-map', {
            gmid: parseInt(goalmap.gmid)
          });
        }
      })
      if (typeof callback == 'function') callback();
    })
  }

  loadIndividualMaps(mid, rid) {
    let app = this;
    console.log(mid, rid);
    app.ajax.get(baseUrl + 'kitbuildApi/getAllUserLastGoalmapOfRoom/' + mid + '/' + rid + '/fix')
      .then(function(goalmaps){
        console.log(goalmaps);
        goalmaps.forEach(goalmap => {
          let links = goalmap.links;
          links.forEach((l) => {
            let lid = app.canvas.getLinkId();
            let eles = [];
            eles.push({
              group: 'nodes',
              data: {
                id: 'l-' + lid,
                name: l.label,
                type: 'link',
                extra: {
                  gmid: goalmap.goalmap.gmid,
                  lid: l.lid
                }
              },
              position: {
                x: parseInt(l.locx),
                y: parseInt(l.locy)
              }
            })
            if (l.source != null) {
              eles.push({
                group: 'edges',
                data: {
                  source: 'l-' + lid,
                  target: 'c-' + l.source,
                  type: 'left'
                }
              })
            }
            if (l.target != null) {
              eles.push({
                group: 'edges',
                data: {
                  source: 'l-' + lid,
                  target: 'c-' + l.target,
                  type: 'right'
                }
              })
            }
            // console.log(eles);
            app.canvas.getCy().add(eles);
          })
        });

        app.canvas.getCy().layout({
          name: 'cose-bilkent',
          fit: false,
          stop: function() {
            $('#bt-center').click();
            let n = app.canvas.getCy().nodes();
            n = n.union(app.canvas.getCy().edges());
            app.kit.sendMap(n.jsons());
          }
        }).run();

      });
  }

  friendlyTime(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? (h < 10 ? "0" + h : h) + ":" : "";
    var mDisplay = m > 0 ? (m < 10 ? "0" + m : m) + ":" : "00:";
    var sDisplay = s > 0 ? (s < 10 ? "0" + s : s) : "00";
    return hDisplay + mDisplay + sDisplay; 
  }

  tick() {
    let app = this;
    this.remaining--;
    // console.log(this.remaining);
    $('#remaining').html(app.friendlyTime(this.remaining) + " left.");
    if (this.remaining > 0) setTimeout(this.tick.bind(this), 1000);
    else {
      let mid = app.mid;
      let uid = app.user.uid;
      let rid = app.rid;
      let username = app.user.username;
      let name = 'final-force-m' + mid + '-r' + rid + '-u' + uid + '-' + username;
      console.log(name);
      app.saveGoalmapFinalize(mid, rid, uid, name, function(gmid) {
        app.gui.dialog('Time up. Your map has been saved.<br>Session will be ended in 5s.');
        setTimeout(function() {
          app.logger.log('finalize-map', {
            gmid: gmid
          });
          app.session.setMulti({
            page: app.nextPage,
            seq: app.logger.seq + 1
          }, function () {
            window.location.href = baseUrl + app.controller + '/' + app.nextPage;
          }, function (error) {
            gui.dialog(error, {
              width: '300px'
            });
          });
        }, 5000);
      });
    }
  }

  countdown(remaining) {
    this.remaining = remaining;
    setTimeout(this.tick.bind(this), 1000);
  }

}

jQuery.fn.center = function (parent) {
  parent = parent ? $(parent) : window
  this.css({
    "position": "absolute",
    "top": ((($(parent).height() - this.outerHeight()) / 2) + $(parent).scrollTop() + "px"),
    "left": ((($(parent).width() - this.outerWidth()) / 2) + $(parent).scrollLeft() + "px")
  });
  return this;
}

var BRIDGE = {};

$(function () {

  var canvas = new Canvas('cy', {
    // isDirected: false,
    // enableToolbar: false,
    // enableNodeCreation: false,
    enableConceptCreation: false,
    // enableLinkCreation: false,
    enableUndoRedo: false,
    // enableZoom: false,
    // enableAutoLayout: false,
    // enableSaveImage: false,
    enableClearCanvas: false,
    // enableNodeTools: false,
    enableNodeModificationTools: false,
    // enableConnectionTools: false,
  }).init();

  let app = new KitBuildApp(canvas); // the app
  let logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null);
  let eventListener = new EventListener(logger, canvas);
  let collabChannel = new CollabChannel().init(canvas);
  let collabKit = new CollabKit();

  collabKit.attachGUI(app.getGUI());
  collabKit.attachChannelTool(collabChannel);

  canvas.attachEventListener(eventListener);
  app.attachKit(collabKit);
  app.attachEventListener(eventListener);
  app.getCanvas().attachTool(collabChannel);

  BRIDGE.app = app;
  BRIDGE.collabKit = collabKit;
  BRIDGE.logger = eventListener.getLogger();
  BRIDGE.app.logger = logger;

  BRIDGE.app.session.getAll(function(sess){
    console.log(sess);
    let user = sess.user ? sess.user : null;
    if(!user) return;

    BRIDGE.app.user = user;

    BRIDGE.app.kit.setUserWithName(user.uid, user.username, user.name, user.role_id, user.gids, app.page);
    BRIDGE.app.signIn(user.uid, user.username, user.name, user.role_id, user.gids);

    let chatOpen = sess['chat-window-open'] ? sess['chat-window-open'] : false;
    BRIDGE.collabKit.chatWindowOpen = chatOpen;

    let mid = sess.mid ? sess.mid : null;
    BRIDGE.app.mid = mid;
    BRIDGE.app.loadMaterial(mid);

    let rid = sess.rid ? sess.rid : null;
    BRIDGE.app.rid = rid;

    let gmid = sess.gmid ? sess.gmid : null;
    BRIDGE.app.gmid = gmid;
    BRIDGE.app.loadKit(gmid, function() {
      BRIDGE.app.loadIndividualMaps(mid, rid);
    });
    BRIDGE.logger.setGmid(gmid);
    
    BRIDGE.app.kit.loadRoom(rid, function(room){
      BRIDGE.app.kit.joinRoom(room);
      BRIDGE.logger.setRid(rid);
    });

    BRIDGE.app.countdown(sess.remaining ? sess.remaining : 0);
  });

});