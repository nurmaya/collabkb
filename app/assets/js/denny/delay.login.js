class HomePage {

  constructor() {
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.ajax = Ajax.ins(this.gui);
    this.user = null;

    this.controller = 'denny';
    this.nextPage = 'delaytest';

    this.logger = new Logger(null, seq ? seq : null, sessId ? sessId : null);
    console.log(this.logger);
    this.handleEvent(this);
  }

  handleEvent(app) {

    $('#bt-student').on('click', function () {
      $('#modal-login .bt-ok').data('role_id', 'WSTPTI,WSTSI')
      $('#modal-login').modal('show');
    });

    let doLogin = function () {

      let username = $('#modal-login .input-username').val();
      let password = $('#modal-login .input-password').val();
      var role_ids = $('#modal-login .bt-ok').data('role_id').split(",");
      let mid = $('#bt-student').data('mid');
      let qsid = $('#bt-student').data('qsid');
      if(!mid || !qsid) { // console.log(mid, qsid);
        app.gui.notify('Invalid ID', {type: 'danger', delay: 0});
        return;
      }

      var loginCallback = function(user) { console.log(user)
        if(!user) {
          let role_id = role_ids.shift(); 
          console.warn('logging in using: ' + role_id);
          if(role_id) {
            app.ajax.post(baseUrl + 'kitbuildApi/signInRole', {
              username: username,
              password: password,
              role_id: role_id
            }).then(function (u) {
              loginCallback(u);
            },
            function(e){ console.error(e);
              if(role_ids.length == 0) throw new Error(e);
              loginCallback();
            });
          }
          return;
        }
        if (user.seq) {
          app.logger.setSeq(user.seq);
          seq = user.seq;
        }
        app.logger.setUid(user.uid);
        user.gids = user.gids ? user.gids.split(",") : [];
        user.grups = user.grups ? user.grups.split(",") : [];
        user.fids = user.fids ? user.fids.split(",") : [];
        app.ajax.post(baseUrl + 'testApi/getAttempts', {
          uid: user.uid,
          qsid: qsid
        }).then(function (attempts) { console.log(attempts);
          if (attempts.length) {
            app.gui.notify('You cannot take this test as you had already took it before. Thank you for your kind participation.', {
              delay: 5000,
              type: 'danger'
            });
            return;
          } else {
            $('#modal-login').modal('hide');
            let confirm = app.gui.confirm('Start Post-Test?', {
              'positive-callback': function () {
                confirm.modal('hide');
                app.ajax.get(baseUrl + 'testApi/attemptTest/' + user.uid + '/' + qsid).then(function(result){ 
                  // console.log(result);
                  app.logger.log('begin-delay-test', { uid: user.uid, mid: mid, type: 'delay', qsid: qsid });
                  let sessData = {
                    user: user,
                    page: app.nextPage,
                    qsid: qsid,
                    mid: mid,
                    seq: app.logger.seq + 1 // because of 'sign-in'
                  };
                  console.log(sessData);
                  app.session.setMulti(sessData, function () {
                    window.location.href = baseUrl + app.controller + '/' + app.nextPage;
                  }, function (error) {
                    gui.dialog(error, {
                      width: '300px'
                    });
                  });
                })
              }
            });

          }
        });
      }

      loginCallback();
      
    }

    $('#modal-login').on('click', '.bt-ok', function () {
      doLogin();
    });
    $('#modal-login').on('keyup', '.input-password', function (e) {
      if (e.keyCode == 13) doLogin();
    })
  }
}

$(function () {
  let homePage = new HomePage();
});