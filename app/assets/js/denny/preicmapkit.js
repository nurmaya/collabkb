var BRIDGE = {};

class PreIndividualConceptMappingWithKit {

  constructor(canvas) {
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.ajax = Ajax.ins(this.gui);
    this.controller = 'denny';
    this.page = 'premapping';
    this.nextPage = null; // set by data-next of Continue
    this.skipPage = 'postmapping';

    this.eventListeners = [];
    this.handleEvent();
  }

  getCanvas() {
    return this.canvas;
  }

  getGUI() {
    return this.gui;
  }

  getSession() {
    return this.session;
  }

  handleEvent() {
    let app = this;

    $('#bt-continue').on('click', function (e) {
      let mid = app.mid;
      let uid = app.user.uid;
      
      if (mid == null || uid == null) {
        app.notify('Invalid ID', {
          type: 'danger'
        });
        return;
      }

      app.ajax.get(baseUrl + 'experimentApi/getUserGoalmaps/' + mid + '/' + uid + '/fix').then(function (goalmaps) {
        console.log(goalmaps);
        if (goalmaps.length > 0) {
          let skipDialog = app.gui.dialog('You have already created the map. Skip mapping?', {
            'positive-callback': function () {
              app.session.setMulti({
                page: app.skipPage
              }, function () {
                setTimeout(function () {
                  window.location.href = baseUrl + app.controller + '/' + app.skipPage;
                }, 500)
              });
            },
            'negative-callback': function () {
              skipDialog.modal('hide');
            }
          })
          return;
        }
        let startMapping = function (activity) {
          console.log('A')
          let lama = 60 * 60; // 60 menit
          app.ajax.get(baseUrl + 'experimentApi/getBeginTimeIndividualCmappingWithKit/' + app.user.uid)
            .then(function (beginTime) { console.log(beginTime);
              if (beginTime.begin != null) { // kalau sudah pernah dimulai
                let elapsed = beginTime.now - beginTime.begin;
                if (elapsed > lama) { // kalau sudah lewat waktunya...
                  let confirmDialog = app.gui.confirm('Waktu yang disediakan untuk concept mapping sudah habis. Lewati concept mapping?', {
                    'positive-callback': function () {
                      app.session.setMulti({
                        page: app.skipPage
                      }, function () {
                        setTimeout(function () {
                          window.location.href = baseUrl + app.controller + '/' + app.skipPage;
                        }, 500)
                      });
                    },
                    'negative-callback': function () {
                      confirmDialog.modal('hide');
                    }
                  });
                  return;
                } else { // kalau belum lewat waktunya...
                  let sisa = parseInt((lama - elapsed) / 60);
                  let confirmDialog = app.gui.confirm('Ada masih memiliki sisa waktu <strong class="text-danger">' + sisa + ' menit</strong> untuk melakukan concept mapping. Lanjutkan concept mapping?', {
                    'positive-callback': function () {
                      app.logger.log('continue-' + activity, {uid: app.user.uid});
                      // console.log(beginTime.begin);
                      app.session.setMulti({
                        page: app.nextPage,
                        'begin-icmapkit': beginTime.begin
                      }, function () {
                        setTimeout(function () {
                          window.location.href = baseUrl + app.controller + '/' + app.nextPage;
                        }, 500)
                      });
                    },
                    'negative-callback': function () {
                      confirmDialog.modal('hide');
                    }
                  });
                  return;
                }
              } else { // kalau belum pernah dimulai
                let confirmDialog = app.gui.confirm('Mulai concept mapping?', {
                  'positive-callback': function () {
                    app.ajax.get(baseUrl + 'experimentApi/beginIndividualCmappingWithKit/' + app.user.uid).then(function (begin) { console.log(begin);
                      confirmDialog.modal('hide');
                      app.logger.log('begin-' + activity)
                      app.session.setMulti({
                        page: app.nextPage
                      }, function () {
                        setTimeout(function () {
                          window.location.href = baseUrl + app.controller + '/' + app.nextPage;
                        }, 500)
                      });
                    })
                  }
                })
              }
            });
        }
        app.nextPage = $('#bt-continue').attr('data-next');
        let activity = app.nextPage;
        startMapping(activity);
      });
      return;
    });
    $('#bt-logout').on('click', function (e) {
      let confirm = app.gui.dialog('Do you want to sign out?', {
        'positive-callback': function () {
          window.location.href = baseUrl + app.controller + "/signOut";
        },
        'negative-callback': function () {
          confirm.modal('hide');
        }
      })
    });
  }

  attachEventListener(listener) {
    this.eventListeners.push(listener);
  }

}

$(function () {
  let app = new PreIndividualConceptMappingWithKit(); // the app
  var logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null);
  app.logger = logger;
  BRIDGE.app = app;
  app.session.getAll(function (sess) {
    let activity = sess.activity ? sess.activity : null;
    let mid = sess.mid ? sess.mid : null;
    let user = sess.user ? sess.user : null;
    app.mid = mid;
    app.user = user;
    logger.log('pre-icmap-page', {
      uid: uid,
      activity: activity,
      mid: mid
    });
    let eventListener = new EventListener(logger);
    BRIDGE.logger = eventListener.getLogger();
  })
});