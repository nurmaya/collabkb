class Modal {

  constructor() {

    this.newNodeLabel = null;

    $('#kb-modal-prompt').on('shown.bs.modal', function (e) {
      $('#input-node-label').focus();
    });

  }

  promptNode(type, value, settings, callback) { // console.log('prompt node')

    $('#input-node-label').removeClass('is-invalid').val(value);
    $('#kb-modal-prompt .text-error').html('');

    switch (type) {
      case 'concept':
        $('#kb-modal-prompt .modal-title').html(settings.action + ' Concept');
        $('#kb-modal-prompt .modal-input-label').html('Concept Label');
        $('#kb-modal-prompt .modal-helper-text').html('Concept label should be less than 6 words and cannot be empty.');
        break;
      case 'link':
        $('#kb-modal-prompt .modal-title').html(settings.action + ' Link');
        $('#kb-modal-prompt .modal-input-label').html('Link Label');
        $('#kb-modal-prompt .modal-helper-text').html('Link label should be less than 6 words and cannot be empty.');
        break;
    }

    if (typeof callback == 'function') {

      $('#kb-modal-prompt .bt-ok').show();

      let submitLabel = function () {
        let val = $('#input-node-label').val().toString().trim();
        // console.log('invalid');
        if (val != '') {
          // console.log('submit');
          try {
            callback(val);
            $('#input-node-label').val(value);
            $('#kb-modal-prompt').modal('hide');
          } catch(error) {
            $('#kb-modal-prompt .text-error').html(error);
          }
        } else {
          $('#input-node-label').addClass('is-invalid');
          // console.log('invalid');
        }
      }
      $('#kb-modal-prompt .bt-ok').off('click').on('click', submitLabel);
      $('#input-node-label').off('keypress').on('keypress', function (e) {
        if ($('#input-node-label').val().toString().trim().length)
          $('#input-node-label').removeClass('is-invalid');
        if (e.which == 13) submitLabel(); // Kalau ditekan ENTER
      });


    } else {
      $('#kb-modal-prompt .bt-ok').hide();
    }

    if (settings.closeCallback) {
      $('#kb-modal-prompt .bt-close').on('click', settings.closeCallback);
    }

    $('#kb-modal-prompt').modal('show');
  }

  newNode(type, options, callback) { // console.log('new node')

    let defaults = {
      callback: null,
      closeCallback: null,
      action: 'New'
    };
    let settings = Object.assign({}, defaults, options);
    this.promptNode(type, '', settings, callback);
  }

  editNode(node, options, callback) {

    let defaults = {
      callback: null,
      closeCallback: null
    };
    let settings = Object.assign({}, defaults, options);
    let type = node.data('type');
    let name = node.data('name');

    this.promptNode(type, name.trim(), settings, callback);
  }

  dialog(text, options) {

    let dialog = $('#kb-modal-dialog');

    let defaults = {
      'text-positive': 'Yes',
      'text-negative': 'No',
      'text': 'Do you want to continue?',
      'positive-callback': undefined,
      'negative-callback': undefined,
      'shown-callback': undefined,
      'hidden-callback': undefined,
      // 'backdrop': 'static',
      // 'keyboard': false,
      'width': 'normal',
      'selector': '.modal-dialog-dialog'
    }

    if (options == undefined) {
      $('.btn-negative').hide();
      defaults["text-positive"] = 'OK';
      defaults['positive-callback'] = function () {
        dialog.modal('hide');
      }
    } else $('.btn-negative').show();

    // console.log(text);
    let message = text || defaults['text'];
    // console.log(text, message);
    let settings = Object.assign({}, defaults, options);

    // this.setModalWidth(settings);

    dialog.find('.btn-positive').text(settings['text-positive']);
    dialog.find('.btn-negative').text(settings['text-negative']);
    dialog.find('.modal-dialog-content').html(message);

    if (settings['negative-callback'] == undefined) {
      dialog.find('.btn-negative')
        .off('click')
        .on('click', function () {
          dialog.modal('hide');
        }); //.hide();
    } else dialog.find('.btn-negative')
      .off('click')
      .on('click', settings['negative-callback'])
      .show();
    if (settings['positive-callback'] != undefined) {
      dialog.find('.btn-positive')
        .off('click')
        .on('click', settings['positive-callback']);
    } else dialog.find('.btn-positive')
      .off('click');

    if (settings['shown-callback'] != undefined) {
      dialog.on('shown.bs.modal', settings['shown-callback']);
    }
    if (settings['hidden-callback'] != undefined) {
      dialog.on('hidden.bs.modal', settings['hidden-callback']);
    }
    // console.log(settings);
    return dialog.modal(settings);

  }

  hide(dialog) {
    dialog.modal('hide');
  }

  showSnapshot(base64uri) {

    $('#kb-modal-snapshot .kb-snapshot').html('<img>');
    $('#kb-modal-snapshot .kb-snapshot').css('text-align', 'center');
    $('#kb-modal-snapshot .kb-snapshot img').css('max-width', '100%');
    $('#kb-modal-snapshot .kb-snapshot img').css('max-height', '400px');
    $('#kb-modal-snapshot .kb-snapshot img').attr('src', base64uri);

    $('#kb-modal-snapshot .btn-download').off('click').on('click', function () {
      let download = function (content, fileName, contentType) {
        var a = document.createElement("a");
        a.href = content;
        a.download = fileName;
        a.click();
      }
      download(base64uri, 'concept-map.png', 'image/png');
    });

    $('#kb-modal-snapshot .btn-close').off('click').on('click', function () {
      $('#kb-modal-snapshot').modal('hide');
    });

    $('#kb-modal-snapshot').modal('show');
  }

}