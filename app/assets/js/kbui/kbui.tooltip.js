class Tooltip {

  constructor(canvasId) {
    this.canvasElement = $('#' + canvasId);
    this.tooltipElement = $('<div class="canvas-tooltip">' +
      '  <div class="canvas-tooltip-text">-</div>' +
      '</div>');
    // this.arrow = $('<div class="canvas-tooltip-arrow"><svg height="250" width="500">' +
    //   '<polygon points="0,0 16,0 8,8 0,0" style="fill:green;" />' +
    //   '</svg></div>');
    this.canvasElement.parent().append(this.tooltipElement);
    this.tooltipElement.hide();
  }

  show(hoveredTool, label) {
    $('.canvas-tooltip-text').html(label);
    let offset = this.canvasElement.offset(); // console.log(hoveredTool, offset);
    let tooltip = this.tooltipElement.fadeIn(100);
    this.tooltipElement.offset({
      top: hoveredTool.y + offset.top - tooltip.outerHeight() - 30,
      left: hoveredTool.x + offset.left - tooltip.outerWidth() / 2
    });
    this.tooltipElement.css('border-color', hoveredTool.color + "77");
  }

  hide() {
    this.tooltipElement.hide();
  }

  fadeOut(speed) {
    this.tooltipElement.fadeOut(speed);
  }

}