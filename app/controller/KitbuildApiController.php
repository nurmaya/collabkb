<?php

class KitbuildApiController extends CoreController {

  public function registerUser() {
    $name     = isset($_POST['name']) ? $_POST['name'] : null;
    $username = isset($_POST['username']) ? $_POST['username'] : null;
    $password = isset($_POST['password']) ? $_POST['password'] : null;
    $rid      = isset($_POST['rid']) ? $_POST['rid'] : null;
    $gid      = isset($_POST['gid']) ? $_POST['gid'] : null;
    try {
      $userService  = new UserService();
      $uid          = $userService->insertUser($username, $password, $name);
      $result       = $userService->assignRoleToUser($uid, $rid);
      $groupService = new GroupService();
      $result       = $groupService->addUserToGroup($uid, $gid);
      CoreResult::instance(true)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function signIn() {
    $username = trim($_POST['username']);
    $password = trim($_POST['password']);
    try {
      $userService = new UserService();
      $users       = $userService->signIn($username, $password);
      if (count($users)) {
        CoreResult::instance($users[0])->show();
      } else {
        CoreError::instance('Invalid username and/or password.')->show();
      }
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function signInRole() {
    $username = trim($_POST['username']);
    $password = trim($_POST['password']);
    $role_id  = trim($_POST['role_id']);
    try {
      $userService = new UserService();
      $user        = $userService->signInRole($username, $password, $role_id);
      if ($user) {
        $sessionService = new SessionService();
        $session        = $sessionService->getSessionData($user->uid);
        if ($session) {
          $user->session = $session;
          $sessionData = json_decode($session->data);
          if($sessionData !== null)
            foreach ($sessionData as $key => $value) {
              $_SESSION[$key] = $value;
            }
          else $sessionService->setSessionData($user->uid, $_SESSION);
        } else {
          $sessionService->setSessionData($user->uid, $_SESSION);
        }
        CoreResult::instance($user)->show();
      } else {
        CoreError::instance('Invalid username and/or password.')->show();
      }
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function signOut($uid) {
    try {
      $uid            = $uid ? $uid : $_POST['uid'];
      $sessionService = new SessionService();
      $sessionService->clearSessionData($uid);
      CoreResult::instance(true)->show();
    } catch (Exception $ex) {
      CoreError::instance(false)->show();
    }
  }

  public function loadUser($uid) {
    try {
      $userService = new UserService();
      $user        = $userService->loadUser($uid);
      CoreResult::instance($user)->show();
    } catch (Exception $ex) {
      CoreError::instance(false)->show();
    }
  }

  public function getRoomByRid($rid) {
    try {
      $roomService = new RoomService();
      $room        = $roomService->selectRoom($rid);
      CoreResult::instance($room)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getMaterials($all = false) {
    try {
      $materialService = new MaterialService();
      $materials       = $materialService->getMaterials($all);
      CoreResult::instance($materials)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getMaterialsWithGids() {
    $gids = $_POST['gids'];
    try {
      $materialService = new MaterialService();
      $materials       = $materialService->getMaterialsWithGids($gids);
      CoreResult::instance($materials)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getMaterialByMid($mid) {
    try {
      $materialService = new MaterialService();
      $materials       = $materialService->getMaterialCollabByMid($mid); // because content is in table material_collab
      if (count($materials)) {
        CoreResult::instance($materials[0])->show();
      } else {
        CoreError::instance(null)->show();
      }
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getMaterialCollabByMid($mid) {
    try {
      $materialService = new MaterialService();
      $materials       = $materialService->getMaterialCollabByMid($mid);
      if (count($materials)) {
        CoreResult::instance($materials[0])->show();
      } else {
        CoreError::instance(null)->show();
      }
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function saveGoalmap() {
    // var_dump($_POST);
    // exit;
    try {
      $gmid = isset($_POST['gmid']) ? $_POST['gmid'] : null;
      if(!$gmid) {
        $name       = $_POST['name'];
        $type       = $_POST['type'];
        $mid        = $_POST['mid'];
        $creator_id = $_POST['creator_id'];
        $updater_id = $_POST['updater_id'];
        $concepts   = isset($_POST['concepts']) ? $_POST['concepts'] : [];
        $links      = isset($_POST['links']) ? $_POST['links'] : [];
        $goalmapService = new GoalmapService();
        $result         = $goalmapService->insertGoalmap($name, $type, $mid, $creator_id, $updater_id, $concepts, $links);
        CoreResult::instance($result)->show();
      } else { // overwrite
        $updater_id = $_POST['updater_id'];
        $concepts   = isset($_POST['concepts']) ? $_POST['concepts'] : [];
        $links      = isset($_POST['links']) ? $_POST['links'] : [];
        $type       = $_POST['type'];
        $goalmapService = new GoalmapService();
        $result         = $goalmapService->overwriteGoalmap($gmid, $updater_id, $concepts, $links, $type);
        CoreResult::instance($result)->show();
      }
      
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function saveGoalmapCollab() {
    // var_dump($_POST);
    $name       = $_POST['name'];
    $type       = $_POST['type'];
    $mid        = $_POST['mid'];
    $creator_id = $_POST['creator_id'];
    $updater_id = $_POST['updater_id'];
    $concepts   = isset($_POST['concepts']) ? $_POST['concepts'] : [];
    $links      = isset($_POST['links']) ? $_POST['links'] : [];
    $rid        = $_POST['rid'];
    // exit;
    try {
      $goalmapCollabService = new GoalmapCollabService();
      $result               = $goalmapCollabService->insertGoalmap($name, $type, $mid, $creator_id, $updater_id, $concepts, $links, $rid);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getLastDraftGoalmap($mid, $rid) {
    try {
      $goalmapCollabService = new GoalmapCollabService();
      $result               = $goalmapCollabService->getLastDraftGoalmap($mid, $rid);
      if (count($result)) {
        CoreResult::instance($result[0])->show();
      } else {
        CoreResult::instance(null)->show();
      }
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function loadLastDraftGoalmap($mid, $uid) {
    try {
      $goalmapService = new GoalmapService();
      $result         = $goalmapService->getLastDraftGoalmap($mid, $uid);
      var_dump($result);exit;
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function loadLastUserGoalmap($mid, $uid, $type = null) {
    try {
      $goalmapService = new GoalmapService();
      $result         = $goalmapService->getLastUserGoalmap($mid, $uid, $type);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function loadLastDraftGoalmapCollab($mid, $rid) {
    try {
      $goalmapCollabService = new GoalmapCollabService();
      $result               = $goalmapCollabService->getLastDraftGoalmap($mid, $rid);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getGoalmaps($mid, $type = null) {
    try {
      $goalmapService = new GoalmapService();
      $result         = $goalmapService->getGoalmaps($mid, $type);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getUserGoalmaps($mid, $uid) {
    try {
      $goalmapService = new GoalmapService();
      $result         = $goalmapService->getUserGoalmaps($mid, $uid);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getCollabGoalmaps($mid, $rid) {
    try {
      $goalmapCollabService = new GoalmapCollabService();
      $result               = $goalmapCollabService->getGoalmaps($mid, $rid);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getUserLearnermaps($gmid, $uid) {
    try {
      $learnermapService = new LearnermapService();
      $result               = $learnermapService->getUserLearnermaps($gmid, $uid);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getCollabLearnermaps($gmid, $rid) {
    try {
      $learnermapCollabService = new LearnermapCollabService();
      $result               = $learnermapCollabService->getLearnermaps($gmid, $rid);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function openKit($gmid) {
    try {
      $goalmapService = new GoalmapService();
      $result         = $goalmapService->getGoalmapKit($gmid);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function saveLearnermap() {
    // var_dump($_POST);
    $gmid     = $_POST['gmid'];
    $type     = $_POST['type'];
    $uid      = $_POST['uid'];
    $concepts = isset($_POST['concepts']) ? $_POST['concepts'] : [];
    $links    = isset($_POST['links']) ? $_POST['links'] : [];
    try {
      $learnermapService = new LearnermapService();
      $result            = $learnermapService->insertLearnermap($gmid, $uid, $concepts, $links, $type);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function saveLearnermapOverwrite() {
    // var_dump($_POST);
    // exit;
    $gmid     = $_POST['gmid'];
    $type     = $_POST['type'];
    $uid      = $_POST['uid'];
    $concepts = isset($_POST['concepts']) ? $_POST['concepts'] : [];
    $links    = isset($_POST['links']) ? $_POST['links'] : [];
    $lmid     = isset($_POST['lmid']) && !empty($_POST['lmid']) ? $_POST['lmid'] : null;
    // var_dump($lmid);
    // exit;
    try {
      $learnermapService = new LearnermapService();
      $lmid              = $learnermapService->insertLearnermapOverwrite($gmid, $uid, $concepts, $links, $type, $lmid);
      $learnermap        = $learnermapService->getLearnermap($lmid);
      CoreResult::instance($learnermap)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function saveLearnermapCollab() {
    // var_dump($_POST);
    $gmid     = $_POST['gmid'];
    $type     = $_POST['type'];
    $uid      = $_POST['uid'];
    $concepts = isset($_POST['concepts']) ? $_POST['concepts'] : [];
    $links    = isset($_POST['links']) ? $_POST['links'] : [];
    $rid      = $_POST['rid'];
    try {
      $learnermapCollabService = new LearnermapCollabService();
      $result                  = $learnermapCollabService->insertLearnermap($gmid, $uid, $concepts, $links, $type, $rid);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function loadLastDraftLearnermap($gmid, $uid) {
    try {
      $learnermapService = new LearnermapService();
      $result            = $learnermapService->getLastDraftLearnermap($gmid, $uid);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function loadLastDraftLearnermapCollab($gmid, $rid) {
    try {
      $learnermapCollabService = new LearnermapCollabService();
      $result                  = $learnermapCollabService->getLastDraftLearnermap($gmid, $rid);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getLearnermaps($gmid) {
    try {
      $learnermapService = new LearnermapService();
      $result            = $learnermapService->getLearnermaps($gmid);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getLearnermap($lmid) {
    try {
      $learnermapService = new LearnermapService();
      $result            = $learnermapService->getLearnermap($lmid);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getLastLearnermap($gmid, $uid, $type = 'draft') {
    try {
      $learnermapService = new LearnermapService();
      $result            = $learnermapService->getLastLearnermap($gmid, $uid, $type);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getEachUserLastGoalmapOfRoom($mid, $rid, $type = null) {
    try {
      $goalmapService = new GoalmapService();
      $result         = $goalmapService->getEachUserLastGoalmapOfRoom($mid, $rid, $type);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getAllUserLastGoalmapOfRoom($mid, $rid, $type = null) {
    try {
      $goalmapService = new GoalmapService();
      $result         = $goalmapService->getEachUserLastGoalmapOfRoom($mid, $rid, $type);
      $goalmaps = [];
      foreach($result as $g) {
        $goalmaps[] = $goalmapService->getGoalmapKit($g->gmid);
      }
      CoreResult::instance($goalmaps)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

}
