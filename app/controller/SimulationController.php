<?php

class SimulationController extends CoreController {

  function index() {
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('cytoscape');
    $this->ui->addPlugin('kbui', '?t='.time());

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');

    $this->ui->addScript('js/simulation/simulation.kitbuild.js', '?t='.time());
    $this->ui->addScript('js/simulation/simulation.js', '?t='.time());
    $this->ui->addScript($this->ui->location('simulation/script'), '?t='.time());
    $this->ui->addStyle('css/simulation.css', '?t='.time());

    
    $logService = new LogService();
    $data['goalmapIds'] = $logService->getGoalmapIds();
    $this->ui->view('simulation.php', $data);
  }

  function script() { //var_dump($_SESSION);
    header('Content-Type: text/javascript');
    echo '$(function(){'."\n";
    echo '})';
  }

  function getUserIdsWithGmid($gmid) {
    $logService = new LogService();
    $kitService = new KitService();
    $data['userIds']  = $logService->getUserIdsWithGmid($gmid);
    $data['kit']      = $kitService->getKitById($gmid);
    $this->ui->json($data);
  }

  function getActivityLogs($gmid, $uid) {
    $logService = new LogService();
    $data       = $logService->getActivityLogs($gmid, $uid);
    $this->ui->json($data);
  }

}