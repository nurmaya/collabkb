<?php

class E1Controller extends ExperimentController {

  public function __construct() {
    parent::__construct('e1');
  }

  public function index() {
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/e1/e1.js', '?t=' . time());
    $this->ui->addStyle('css/experiment/base.css');
    $this->ui->view('e1/e1.home.php');
  }

  function p1() {

    $this->_check('p1', 'p1');

    $user              = (object) $_SESSION['user'];
    $materialService   = new MaterialService();
    $roomService       = new RoomService();
    $data['materials'] = $materialService->getMaterialsWithGids($user->gids);
    $data['rooms']     = $roomService->selectRoomsByGids($user->gids);
    $data['user']      = $user;

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/general/logger.js');

    $this->ui->addStyle('css/experiment/base.css');

    $this->ui->addScript('js/e1/e1.student.p1.js', '?t=' . time());
    $this->ui->view('e1/e1.student.p1.php', $data);

  }

  function read() {
    $this->_check('read', 'read');

    $user            = (object) $_SESSION['user'];
    $materialService = new MaterialService();
    // $roomService       = new RoomService();
    $materials = $materialService->getMaterialCollabByMid($_SESSION['mid']);
    if (count($materials)) {
      $data['material'] = $materials[0];
    }

    // $data['rooms']     = $roomService->selectRoomsByGids($user->gids);

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/general/logger.js');

    // $this->ui->addStyle('css/read.css');

    $this->ui->addScript('js/e1/e1.student.read.js', '?t=' . time());
    $this->ui->view('e1/e1.student.read.php', $data);

  }

  function pretest() {
    $this->_check('pretest', 'pretest');

    $user = (object) $_SESSION['user'];

    $gid         = $user->gids[0];
    $type        = 'pre';
    $qsetService = new QsetService();
    $qset        = $qsetService->selectQsetWithQuestionsByGidAndType($gid, $type);

    $data['qset'] = $qset;

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/general/logger.js');

    // $this->ui->addStyle('css/read.css');

    $this->ui->addScript('js/e1/e1.student.pretest.js', '?t=' . time());
    $this->ui->addScript($this->ui->location('e1/pretestscript/' . $qset->qsid), '?t=' . time());
    $this->ui->view('e1/e1.student.pretest.php', $data);
  }

  function pretestscript($qsid) {
    header('Content-Type: text/javascript');
    echo '$(function(){' . "\n";
    if (isset($_SESSION['user'])) {
      $user  = (object) $_SESSION['user'];
      $gids  = $user->gids;
      $grups = $user->grups;
      echo "BRIDGE.studentPretest.gid = $gids[0]\n";
      echo "BRIDGE.studentPretest.grup = '$grups[0]'\n";
      echo "BRIDGE.studentPretest.qsid = $qsid\n";
    }
    echo '})';
  }

  function presb() {

    $this->_check('presb', 'presb');
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/e1/e1.student.presb.js');
    $this->ui->addStyle('css/experiment/base.css');
    $this->ui->view('e1/e1.student.presb.php');

  }

  function sb() {
    $this->_check('sb', 'sb');

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('cytoscape');
    $this->ui->addPlugin('kbui', '?t=' . time());

    $this->ui->addScript('vendors/socket.io.slim.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.channel.tool.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.message.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.kit.js', '?t=' . time());

    $this->ui->addStyle('css/chat/chat.css', '?t=' . time());

    $this->ui->addStyle('css/general/three-dots.css', '?t=' . time());
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');

    $this->ui->addScript('js/e1/e1.student.sb.app.js', '?t=' . time());
    $this->ui->addScript('js/e1/e1.student.sb.js', '?t=' . time());
    $this->ui->addScript($this->ui->location('e1/sbscript'), '?t=' . time());

    $this->ui->addStyle('css/e1/e1.cmap.css', '?t=' . time());
    $this->ui->view('e1/e1.sb.php');
  }

  function sbscript() {
    header('Content-Type: text/javascript');
    echo '/*' . "\n";
    echo print_r($_SESSION);
    echo "\n" . '*/' . "\n";
    echo '$(function(){' . "\n";
    if (isset($_SESSION['user'])) {
      $u    = (object) $_SESSION['user'];
      $gids = [];
      foreach ($u->gids as $gid) {
        $gids[] = "'$gid'";
      }
      echo "let gids = [" . implode(",", $gids) . "];\n";
      echo "BRIDGE.app.setUser('$u->uid','$u->username', '$u->role_id', gids);\n";
      echo "BRIDGE.app.signIn($u->uid,'$u->username', '$u->role_id', gids);\n";
    }
    if (isset($_SESSION['mid'])) {
      echo "BRIDGE.app.loadMaterial('$_SESSION[mid]');\n";
      echo "BRIDGE.logger.setMid('$_SESSION[mid]');\n";
    }
    echo '})';
  }

  function postsb() {

    $this->_check('postsb', 'postsb');
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/e1/e1.student.postsb.js');
    $this->ui->addScript($this->ui->location('e1/postsbscript'), '?t=' . time());
    $this->ui->addStyle('css/experiment/base.css');
    $this->ui->view('e1/e1.student.postsb.php');

  }

  function postsbscript() {
    header('Content-Type: text/javascript');
    echo '$(function(){' . "\n";
    if (isset($_SESSION['mid'])) {
      echo "BRIDGE.studentPostSB.mid = '$_SESSION[mid]';\n";
    }
    if (isset($_SESSION['rid'])) {
      echo "BRIDGE.studentPostSB.rid = '$_SESSION[rid]';\n";
    }
    echo '})';
  }

  function kb() {

    $this->_check('kb', 'kb');

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('cytoscape');
    $this->ui->addPlugin('kbui', '?t=' . time());

    $this->ui->addScript('vendors/socket.io.slim.js', '?t=' . time());

    $this->ui->addScript('js/collab/collab.channel.tool.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.message.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.kit.js', '?t=' . time());

    $this->ui->addStyle('css/chat/chat.css', '?t=' . time());

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');

    $this->ui->addScript('js/e1/e1.student.kb.app.js', '?t=' . time());
    $this->ui->addScript('js/e1/e1.student.kb.js', '?t=' . time());
    $this->ui->addScript($this->ui->location('e1/kbscript'), '?t=' . time());

    $this->ui->addStyle('css/kb.css', '?t=' . time());
    $this->ui->view('e1/e1.kb.php');
  }

  function kbscript() {
    header('Content-Type: text/javascript');
    echo '/*' . "\n";
    echo print_r($_SESSION);
    echo "\n" . '*/' . "\n";
    echo '$(function(){' . "\n";
    if (isset($_SESSION['user'])) {
      $u    = (object) $_SESSION['user'];
      $gids = [];
      foreach ($u->gids as $gid) {
        $gids[] = "'$gid'";
      }
      echo "let gids = [" . implode(",", $gids) . "];\n";
      echo "BRIDGE.app.setUser('$u->uid','$u->username', '$u->role_id', gids);\n";
      // echo "BRIDGE.app.signIn($u->uid,'$u->username', '$u->role_id', gids);\n";
    }
    // if (isset($_SESSION['chat-window-open']) && $_SESSION['chat-window-open']) {
    //   echo "BRIDGE.collabKit.chatWindowOpen = " . $_SESSION['chat-window-open'] . ";\n";
    // }

    if (isset($_SESSION['mid'])) {
      echo "BRIDGE.app.loadMaterial('$_SESSION[mid]');\n";
      // echo "BRIDGE.logger.setMid('$_SESSION[mid]');\n";
    }

    if (isset($_SESSION['gmid'])) {
      echo "BRIDGE.app.loadKit('$_SESSION[gmid]');\n";
      echo "BRIDGE.logger.setGmid('$_SESSION[gmid]');\n";
    }

    // if (isset($_SESSION['rid'])) {
    //   echo "BRIDGE.app.kit.loadRoom('$_SESSION[rid]', function(room){
    //     BRIDGE.app.kit.joinRoom(room);
    //     BRIDGE.logger.setRid('$_SESSION[rid]');
    //   });\n";
    // }

    echo '})';
  }

  function compare() {
    $this->_check('compare', 'compare');
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate'); // required for bs-notify
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('cytoscape');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('cy');
    $this->ui->addPlugin('kbui', '?t=' . time());

    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/analyzer/analyzer.lib.js');
    $this->ui->addScript('js/e1/e1.student.compare.app.js');
    $this->ui->addScript('js/e1/e1.student.compare.js');
    $this->ui->addScript($this->ui->location('e1/comparescript'), '?t=' . time());
    $this->ui->addStyle('css/experiment/base.css');

    $user               = (object) $_SESSION['user'];
    $goalmapService     = new GoalmapService();
    $experimentService  = new ExperimentService();
    $data['goalmap']    = $experimentService->getPairGoalmaps($_SESSION['rid'], $_SESSION['mid']);
    // $data['goalmap']    = $goalmapService->getGoalmapKit($_SESSION['gmid']);
    // $data['learnermap'] = $experimentService->getPairLearnermap($_SESSION['gmid'], $user->uid);

    $this->ui->addStyle('css/kb.css');
    $this->ui->view('e1/e1.student.compare.php', $data);
  }

  function comparescript() {
    header('Content-Type: text/javascript');
    echo '/*' . "\n";
    echo print_r($_SESSION);
    echo "\n" . '*/' . "\n";
    echo '$(function(){' . "\n";
    if (isset($_SESSION['user'])) {
      $u    = (object) $_SESSION['user'];
      $gids = [];
      foreach ($u->gids as $gid) {
        $gids[] = "'$gid'";
      }
      echo "let gids = [" . implode(",", $gids) . "];\n";
      echo "BRIDGE.app.setUser('$u->uid','$u->username', '$u->role_id', gids);\n";
      // echo "BRIDGE.app.signIn($u->uid,'$u->username', '$u->role_id', gids);\n";
    }
    // if (isset($_SESSION['chat-window-open']) && $_SESSION['chat-window-open']) {
    //   echo "BRIDGE.collabKit.chatWindowOpen = " . $_SESSION['chat-window-open'] . ";\n";
    // }

    if (isset($_SESSION['mid'])) {
      echo "BRIDGE.app.loadMaterial('$_SESSION[mid]');\n";
      // echo "BRIDGE.logger.setMid('$_SESSION[mid]');\n";
    }

    if (isset($_SESSION['gmid'])) {
      echo "BRIDGE.app.loadGoalmap('$_SESSION[gmid]', function() {
          $('#bt-fit').click();
        });\n";
      echo "BRIDGE.logger.setGmid('$_SESSION[gmid]');\n";
    }

    // if (isset($_SESSION['rid'])) {
    //   echo "BRIDGE.app.kit.loadRoom('$_SESSION[rid]', function(room){
    //     BRIDGE.app.kit.joinRoom(room);
    //     BRIDGE.logger.setRid('$_SESSION[rid]');
    //   });\n";
    // }

    echo '})';
  }

  function postkb() {

    $this->_check('postkb', 'postkb');
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate'); // required for bs-notify
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('cytoscape');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('cy');
    $this->ui->addPlugin('kbui', '?t=' . time());

    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/analyzer/analyzer.lib.js');
    $this->ui->addScript('js/e1/e1.student.postkb.js');
    $this->ui->addScript($this->ui->location('e1/postkbscript'), '?t=' . time());
    $this->ui->addStyle('css/experiment/base.css');

    $user               = (object) $_SESSION['user'];
    $goalmapService     = new GoalmapService();
    $experimentService  = new ExperimentService();
    $data['goalmap']    = $goalmapService->getGoalmapKit($_SESSION['gmid']);
    $data['learnermap'] = $experimentService->getPairLearnermap($_SESSION['gmid'], $user->uid);

    $this->ui->view('e1/e1.student.postkb.php', $data);

  }

  function postkbscript() {
    header('Content-Type: text/javascript');
    echo '$(function(){' . "\n";
    // echo 'console.log(BRIDGE);';
    if (isset($_SESSION['rid'])) echo "BRIDGE.app.rid = '$_SESSION[rid]';\n";
    if (isset($_SESSION['gmid'])) {
      $user = (object) $_SESSION['user'];
      echo "BRIDGE.app.loadGoalmap('$_SESSION[gmid]', function(goalmap) {
          BRIDGE.app.analyzerLib.showGoalmapKit(BRIDGE.app.getCanvas().getCy(), 
            goalmap.concepts, goalmap.links, function() {
            BRIDGE.app.getLearnermap('$_SESSION[gmid]', '$user->uid', function() {
              $('#bt-fit').click();
              });
            });
          });\n";
    }
    // if (isset($_SESSION['rid'])) {
    //   echo "BRIDGE.studentPostKB.rid = '$_SESSION[rid]';\n";
    //   echo "console.log(BRIDGE.studentPostKB);\n";
    // }
    echo '})';
  }

  function postcompare() {
    $this->_check('postcompare', 'postcompare');
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate'); // required for bs-notify
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('cytoscape');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('cy');
    $this->ui->addPlugin('kbui', '?t=' . time());

    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/analyzer/analyzer.lib.js');
    $this->ui->addScript('js/e1/e1.student.postcompare.js');
    $this->ui->addScript($this->ui->location('e1/postcomparescript'), '?t=' . time());
    $this->ui->addStyle('css/experiment/base.css');

    $user               = (object) $_SESSION['user'];
    $goalmapService     = new GoalmapService();
    $experimentService  = new ExperimentService();
    $data['goalmap']    = $goalmapService->getGoalmapKit($_SESSION['gmid']);

    // if(in_array('E1A', $user->gids))
    // $data['learnermap'] = $experimentService->getPairLearnermap($_SESSION['gmid'], $user->uid);

    $this->ui->view('e1/e1.student.postcompare.php', $data);
  }

  function postcomparescript() {
    header('Content-Type: text/javascript');
    echo '$(function(){' . "\n";
    // echo 'console.log(BRIDGE);';
    if (isset($_SESSION['rid'])) echo "BRIDGE.app.rid = '$_SESSION[rid]';\n";
    if (isset($_SESSION['mid'])) echo "BRIDGE.app.mid = '$_SESSION[mid]';\n";
    if (isset($_SESSION['rid']) && isset($_SESSION['mid']) ) {
      echo "BRIDGE.app.loadPairGoalmaps('$_SESSION[rid]','$_SESSION[mid]')";
    }
    echo "});";
  }

  function sbcollab() {
    $this->_check('sbcollab', 'sbcollab');

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('cytoscape');
    $this->ui->addPlugin('kbui', '?t=' . time());

    $this->ui->addScript('vendors/socket.io.slim.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.channel.tool.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.message.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.kit.js', '?t=' . time());

    $this->ui->addStyle('css/chat/chat.css', '?t=' . time());

    $this->ui->addStyle('css/general/three-dots.css', '?t=' . time());
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/analyzer/analyzer.lib.js');

    $this->ui->addScript('js/e1/e1.student.sb.collab.app.js', '?t=' . time());
    $this->ui->addScript('js/e1/e1.student.sb.collab.js', '?t=' . time());
    $this->ui->addScript($this->ui->location('e1/sbcollabscript'), '?t=' . time());

    $this->ui->addStyle('css/e1/e1.cmap.css', '?t=' . time());
    $this->ui->view('e1/e1.sb.collab.php');
  }

  function sbcollabscript() {
    header('Content-Type: text/javascript');
    echo '/*' . "\n";
    echo print_r($_SESSION);
    echo "\n" . '*/' . "\n";
    echo '$(function(){' . "\n";
    if (isset($_SESSION['user'])) {
      $u    = (object) $_SESSION['user'];
      $gids = [];
      foreach ($u->gids as $gid) {
        $gids[] = "'$gid'";
      }
      foreach ($u->grups as $grup) {
        $grups[] = "'$grup'";
      }
      echo "BRIDGE.app.gids = [" . implode(",", $gids) . "];\n";
      echo "BRIDGE.app.grups = [" . implode(",", $grups) . "];\n";
      echo "BRIDGE.app.kit.setUser('$u->uid','$u->username', '$u->role_id', BRIDGE.app.gids);\n";      
    }
    if (isset($_SESSION['chat-window-open']) && $_SESSION['chat-window-open']) {
      echo "BRIDGE.collabKit.chatWindowOpen = " . $_SESSION['chat-window-open'] . ";\n";
    }

    if (isset($_SESSION['mid'])) {
      echo "BRIDGE.logger.setMid('$_SESSION[mid]');\n";
      echo "BRIDGE.app.material.mid = '$_SESSION[mid]';\n";
      echo "BRIDGE.app.loadMaterial('$_SESSION[mid]', function(material){\n";
      if (isset($_SESSION['rid'])) {
        echo "BRIDGE.app.kit.loadRoom('$_SESSION[rid]', function(room){
          BRIDGE.app.kit.joinRoom(room);
          BRIDGE.app.kit.enableMessage();
          BRIDGE.logger.setRid('$_SESSION[rid]');
        });\n";
      }
      echo "  });\n"; 
    }
    echo '})';
  }

  function postsbcollab() {

    $this->_check('postsbcollab', 'postsbcollab');
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/e1/e1.student.postsb.collab.js');
    $this->ui->addStyle('css/experiment/base.css');
    $this->ui->view('e1/e1.student.postsb.collab.php');

  }

  function posttest() {
    $this->_check('posttest', 'posttest');

    $user = (object) $_SESSION['user'];

    $gid         = $user->gids[0];
    $type        = 'post';
    $qsetService = new QsetService();
    $qset        = $qsetService->selectQsetWithQuestionsByGidAndType($gid, $type);

    $data['qset'] = $qset;

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/general/logger.js');

    // $this->ui->addStyle('css/read.css');

    $this->ui->addScript('js/e1/e1.student.posttest.js', '?t=' . time());
    $this->ui->addScript($this->ui->location('e1/posttestscript/' . $qset->qsid), '?t=' . time());
    $this->ui->view('e1/e1.student.posttest.php', $data);
  }

  function posttestscript($qsid) {
    header('Content-Type: text/javascript');
    echo '$(function(){' . "\n";
    if (isset($_SESSION['user'])) {
      $user  = (object) $_SESSION['user'];
      $gids  = $user->gids;
      $grups = $user->grups;
      echo "BRIDGE.studentPosttest.gid = $gids[0]\n";
      echo "BRIDGE.studentPosttest.grup = '$grups[0]'\n";
      echo "BRIDGE.studentPosttest.qsid = $qsid\n";
    }
    echo '})';
  }

  function finish() {
    $this->_check('finish', 'finish');
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addStyle('css/experiment/base.css', '?t=' . time());
    $this->ui->view('e1/e1.finish.php');
  }

}