<?php

class AdminController extends CoreController {

  function index() {
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('tinymce');
    $this->ui->addScript('vendors/md5.min.js');
    $this->ui->addScript('vendors/socket.io.slim.js', '?t='.time());
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');

    $this->ui->addScript('js/admin/admin.collab.js', '?t='.time());
    $this->ui->addScript('js/admin/admin.js', '?t='.time());
    $this->ui->addScript($this->ui->location('admin/script'), '?t='.time());
    $this->ui->addStyle('css/admin.css', '?t='.time());
    
    $this->ui->view('admin.php');
  }

  public function test() {
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('jqui');
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/admin/test.js', '?t='.time());
    $this->ui->addStyle('css/admin.css', '?t='.time());
    $this->ui->view('admin/test/test.php');
  }

  public function analyzer() {
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate'); // required for bs-notify
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('rangeslider');
    $this->ui->addPlugin('cytoscape');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('kbui', '?t=' . time());
    $this->ui->addStyle('css/analyzer.css');

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/ajax.js');

    $this->ui->addScript('js/analyzer/analyzer.lib.js');
    $this->ui->addScript('js/analyzer/analyzer.app.js');
    $this->ui->addScript('js/analyzer/analyzer.ui.js');
    $this->ui->addScript('js/analyzer/analyzer.kitbuild.js');
    $this->ui->addScript('js/analyzer/analyzer.toolbar.js');
    $this->ui->addScript('js/analyzer/analyzer.js');
    $this->ui->view('analyzer/analyzer.php');
  }

  function global() {
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('tinymce');
    $this->ui->addScript('vendors/md5.min.js');
    $this->ui->addScript('vendors/socket.io.slim.js', '?t='.time());
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');

    $this->ui->addScript('js/admin/admin.global.js', '?t='.time());
    $this->ui->addStyle('css/admin.css', '?t='.time());
    
    $this->ui->view('admin/global/admin.global.php');
  }

  function supportscript() {
    header('Content-Type: text/javascript');
    echo '/*' . "\n";
    echo print_r($_SESSION);
    echo "\n" . '*/' . "\n";
    echo '$(function(){' . "\n";
      if (isset($_SESSION['user'])) {
        $u    = (object) $_SESSION['user'];
        $gids = [];
        foreach ($u->gids as $gid) {
          $gids[] = "'$gid'";
        }
        echo "let gids = [" . implode(",", $gids) . "];\n";
        echo "if(BRIDGE.app && BRIDGE.app.support) BRIDGE.app.support.setUser('$u->uid','$u->username', '$u->name', '$u->role_id', gids);\n";
      }
    echo '})';
  }

  function map() {
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('jqui');
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/admin/admin.map.js', '?t='.time());
    $this->ui->addStyle('css/admin.css', '?t='.time());
    $this->ui->view('admin/map/admin.map.php');
  }

  function script() { //var_dump($_SESSION);
    header('Content-Type: text/javascript');
    echo '$(function(){'."\n";
    echo '})';
  }

}