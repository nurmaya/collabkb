<?php

class TestApiController extends CoreController
{

  public function getUsersTakingQset($qsid, $customId = null) {
    try {
      $experimentService = new ExperimentService();
      $users = $experimentService->getUsersTakingQset($qsid, $customId);
      CoreResult::instance($users)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  }

  public function getTest($mid, $type) {
    try {
      $qsetService = new QsetService();
      $test = $qsetService->getQsetOfMaterialAndType($mid, $type);
      CoreResult::instance($test)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  }

  public function getTestByCustomId($mid, $customId) {
    try {
      $qsetService = new QsetService();
      $test = $qsetService->getQsetOfMaterialAndCustomId($mid, $customId);
      CoreResult::instance($test)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  }

  public function getAttempts()
  {
    $uid = $_POST['uid'];
    $qsid = isset($_POST['qsid']) ? $_POST['qsid'] : null;
    $customId = isset($_POST['customid']) ? $_POST['customid'] : null;
    $customId = empty($customId) ? null : $customId;
    if(!empty($qsid)) $customId = null;
    try {
      $experimentService = new ExperimentService();
      $result = $experimentService->checkAttempts($uid, $qsid, $customId);
      CoreResult::instance($result)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  }

  public function attemptTest($uid, $qsid) {
    try {
      $experimentService = new ExperimentService();
      $result = $experimentService->attemptTest($uid, $qsid);
      CoreResult::instance($result)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  }
  
}
