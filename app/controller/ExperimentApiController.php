<?php

class ExperimentApiController extends CoreController
{

  public function md5($data = '') {
    header('Content-Type: application/json');
    echo md5($data);
  }
  
  public function uid($username = ''){
    header('Content-Type:text/plain');
    $userService = new UserService();
    $uid = $userService->getUidFromUsername($username);
    echo $uid;
  }

  public function rid($roomname = ''){
    header('Content-Type:text/plain');
    $roomService = new RoomService();
    $roomname = urldecode($roomname);
    $rid = $roomService->getRidFromRoomName($roomname);
    echo $rid;
  }

  public function loadTest($gid, $type)
  {
    $qsetService = new QsetService();
    try {
      $qset = $qsetService->selectQsetWithQuestionsByGidAndType($gid, $type);
      CoreResult::instance($qset)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  }

  public function saveUserAnswers()
  {
    $uid = $_POST['uid'];
    $qsid = $_POST['qsid'];
    $gid = $_POST['gid'];
    $answers = $_POST['answers'];
    $userAnswerService = new UserAnswerService();
    try {
      $status = $userAnswerService->saveAnswers($uid, $qsid, $gid, $answers);
      CoreResult::instance($status)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  }

  public function saveUserAnswersEssay()
  {
    $uid = $_POST['uid'];
    $qsid = $_POST['qsid'];
    $gid = $_POST['gid'];
    $answers = $_POST['answers'];
    $userAnswerService = new UserAnswerService();
    try {
      $status = $userAnswerService->saveAnswersEssay($uid, $qsid, $gid, $answers);
      CoreResult::instance($status)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  }

  public function saveUserAnswersMulti()
  {
    $uid = $_POST['uid'];
    $qsid = $_POST['qsid'];
    $gid = $_POST['gid'];
    $answers = isset($_POST['answers']) ? $_POST['answers'] : [];
    $userAnswerService = new UserAnswerService();
    try {
      $status = $userAnswerService->saveAnswersMulti($uid, $qsid, $gid, $answers);
      CoreResult::instance($status)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  }

  public function saveAnswers()
  {
    $uid = $_POST['uid'];
    $qsid = $_POST['qsid'];
    $answers = isset($_POST['answers']) ? $_POST['answers'] : [];
    $answerService = new AnswerService();
    try {
      $status = $answerService->saveAnswers($uid, $qsid, $answers);
      CoreResult::instance($status)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  }

  public function saveAnswersEssay()
  {
    $uid = $_POST['uid'];
    $qsid = $_POST['qsid'];
    $answers = isset($_POST['answers']) ? $_POST['answers'] : [];
    $answerService = new AnswerService();
    try {
      $status = $answerService->saveAnswersEssay($uid, $qsid, $answers);
      CoreResult::instance($status)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  }

  public function saveAnswersMulti()
  {
    $uid = $_POST['uid'];
    $qsid = $_POST['qsid'];
    $answers = isset($_POST['answers']) ? $_POST['answers'] : [];
    $answerService = new AnswerService();
    try {
      $status = $answerService->saveAnswersMulti($uid, $qsid, $answers);
      CoreResult::instance($status)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  }

  public function joinRoom()
  {
    $uid = $_POST['uid'];
    $rid = $_POST['rid'];
    try {
      $experimentService = new ExperimentService();
      $result = $experimentService->joinRoom($uid, $rid);
      CoreResult::instance($result)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  }

  public function getPairGmid($uid, $rid, $mid)
  {
    try {
      $experimentService = new ExperimentService();
      $result = $experimentService->getPairGmid($uid, $rid, $mid);
      CoreResult::instance($result)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  }

  public function openKit($gmid)
  {
    try {
      $experimentService = new ExperimentService();
      $result         = $experimentService->getGoalmapKit($gmid);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getPairLearnermap($gmid, $uid)
  {
    try {
      $experimentService = new ExperimentService();
      $result = $experimentService->getPairLearnermap($gmid, $uid);
      CoreResult::instance($result)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  }

  public function getPairGoalmaps($rid, $mid)
  {
    try {
      $experimentService = new ExperimentService();
      $result = $experimentService->getPairGoalmaps($rid, $mid);
      CoreResult::instance($result)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  }

  public function getPairworkLearnermaps()
  {
    $gmids = $_POST['gmids'];
    try {
      $experimentService = new ExperimentService();
      $result = $experimentService->getPairworkLearnermaps($gmids);
      CoreResult::instance($result)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  }

  public function checkTestTaken() {
    $type = $_POST['type'];
    $uid = $_POST['uid'];
    $gid = $_POST['gid'];
    
    try {
      $qsetService = new QsetService();
      $qset = $qsetService->selectQsetWithQuestionsByGidAndType($gid, $type);
      if(!$qset) throw CoreError::instance('Invalid test type');
      $experimentService = new ExperimentService();
      $result = $experimentService->checkTestTaken($type, $uid, $qset->qsid, $gid);
      CoreResult::instance($result)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  }

  public function checkTestTakenByCustomIdAndUid() {
    $type = $_POST['type'];
    $customId = $_POST['customid'];
    $uid = $_POST['uid'];
    try {
      $qsetService = new QsetService();
      $qset = $qsetService->selectQsetWithQuestionsByCustomIdAndType($customId, $type);
      if(!$qset) throw CoreError::instance('Invalid test type');
      $experimentService = new ExperimentService();
      $result = $experimentService->checkTaken($uid, $qset->qsid);
      CoreResult::instance($result)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  }

  public function getUserRoom($uid) {
    try {
      $roomService = new RoomService();
      $room = $roomService->selectRoomByUid($uid);
      CoreResult::instance($room)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  }

  public function getMaterialCollabByFriendlyId($fid) {
    try {
      $materialService = new MaterialService();
      $material = $materialService->getMaterialCollabByFid($fid);
      CoreResult::instance($material)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  }

  public function getMaterialCollabByMidOrFid($id) {
    try {
      $materialService = new MaterialService();
      $material = $materialService->getMaterialCollabByMidOrFid($id);
      CoreResult::instance($material)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  }

  public function getUserGoalmaps($mid, $uid, $type = null) {
    try {
      $goalmapService = new GoalmapService();
      $goalmaps = $goalmapService->getUserGoalmaps($mid, $uid, $type);
      CoreResult::instance($goalmaps)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  }

  public function getRoomGoalmapsCollab($mid, $rid, $type = null) {
    try {
      $goalmapCollabService = new GoalmapCollabService();
      $goalmaps = $goalmapCollabService->getGoalmaps($mid, $rid, $type);
      CoreResult::instance($goalmaps)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  }

  public function setExperimentData($uid) {
    try {
      $data = isset($_POST['data']) ? $_POST['data'] : null;
      $experimentService = new ExperimentService();
      $goalmaps = $experimentService->setExperimentData($uid, $data);
      CoreResult::instance($goalmaps)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  }

  public function beginIndividualCmappingWithKit($uid) {
    try {
      $experimentService = new ExperimentService();
      $data = $experimentService->getExperimentData($uid);
      if($data == null) $data = new stdClass;
      $key = 'begin-icmapkit';
      $data->$key = time();
      $_SESSION[$key] = time();
      $result = $experimentService->setExperimentData($uid, $data);
      CoreResult::instance($data)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  }

  public function getBeginTimeIndividualCmappingWithKit($uid) {
    try {
      $experimentService = new ExperimentService();
      $data = $experimentService->getExperimentData($uid);
      $key = 'begin-icmapkit';
      $result = new stdClass;
      $result->begin = $data ? (isset($data->{$key}) ? $data->{$key} : null) : null;
      $result->now = time();
      CoreResult::instance($result)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  }

  public function getBeginTimeReciprocalCmappingWithKit($uid) {
    try {
      $experimentService = new ExperimentService();
      $data = $experimentService->getExperimentData($uid);
      $key = 'begin-rcmapkit';
      $result = new stdClass;
      $result->begin = $data ? (isset($data->{$key}) ? $data->{$key} : null) : null;
      $result->now = time();
      CoreResult::instance($result)->show();
    } catch (Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }
  } 

}
