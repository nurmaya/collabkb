<?php

class HanifahController extends GeneralController {

  function __construct()
  {
    parent::__construct('hanifah');
  }

  function index() {
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');

    $this->ui->addScript('js/general/logger.js');

    $this->ui->addScript('js/hanifah/home.js', '?t=' . time());
    $this->ui->addStyle('css/general/base.css');
    $this->ui->view('hanifah/home.php');
  }

  // function postlogin() {
  //   $this->_check('postlogin', 'postlogin');
  //   $this->ui->addPlugin('jquery');
  //   $this->ui->addPlugin('jqui');
  //   $this->ui->addPlugin('bootstrap');
  //   $this->ui->addPlugin('animate');
  //   $this->ui->addPlugin('bs-notify');
  //   // $this->ui->addPlugin('tippy');
  //   // $this->ui->addPlugin('cytoscape');
  //   $this->ui->addScript('js/hanifah/postlogin.js', '?t=' . time());
  //   $this->ui->addStyle('css/general/base.css');

  //   if (isset($_SESSION['user'])) {
  //     $u    = (object) $_SESSION['user']; //var_dump($u);
  //     // $roomService = new RoomService();
  //     // $data['room'] = $roomService->selectRoomByUid($u->uid);
  //     $data['materials'] = [];
  //     if(isset($u->gids)) {
  //       $materialService = new MaterialService();
  //       $data['materials'] = $materialService->getMaterialsWithGids($u->gids);
  //     }
  //   }

  //   $this->ui->view('hanifah/postlogin.php', $data);
  // }

  function lobby() { // exit;

    $this->_check('lobby', 'lobby');

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    // $this->ui->addPlugin('tippy');
    // $this->ui->addPlugin('cytoscape');
    // $this->ui->addPlugin('kbui', '?t=' . time());

    $this->ui->addScript('vendors/socket.io.slim.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.channel.tool.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.message.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.kit.js', '?t=' . time());

    $this->ui->addStyle('css/chat/chat.css', '?t=' . time());

    $this->ui->addStyle('css/general/three-dots.css', '?t=' . time());
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');

    

    $this->ui->addStyle('css/general/base.css', '?t=' . time());

    $data['room'] = null;
    if (isset($_SESSION['user'])) {
      $u    = (object) $_SESSION['user'];
      $roomService = new RoomService();
      $data['room'] = $roomService->selectRoomByUid($u->uid);
      $data['materials'] = [];
      // var_dump($u);
      if(isset($u->gids)) {
        $materialService = new MaterialService();
        $materials = $materialService->getMaterialsWithGids($u->gids);
        $groupService = new GroupService();
        $groups = $groupService->selectGroupsByIds($u->gids);
        foreach($materials as $m) {
          // var_dump($m->fid);
          if(!$m->fid) $data['materials'][] = $m;
          else {
            // var_dump(strpos($m->fid, '-G-'));
            if(strpos($m->fid, '-G-') === false) {
              $data['materials'][] = $m;
              continue;
            }
            foreach($groups as $g) { // var_dump($m->fid, strtoupper($g->fid . $_SESSION['activity']));
              if(strpos($m->fid, strtoupper($g->fid . $_SESSION['activity'])) !== false)
              $data['materials'][] = $m;
            }
          }
        }
        // var_dump($data['materials']);
        // var_dump($groups);
        // var_dump($_SESSION['activity']);
      }
    }

    switch($_SESSION['activity']) {
      case 'cmap':
        $this->ui->addScript('js/hanifah/lobby.js', '?t=' . time());
        $this->ui->addScript($this->ui->location('hanifah/lobbyscript'), '?t=' . time());
        $this->ui->view('hanifah/lobby.php', $data);
        break;
      case 'kb':
        $this->ui->addScript('js/hanifah/lobbykb.js', '?t=' . time());
        $this->ui->addScript($this->ui->location('hanifah/lobbyscript'), '?t=' . time());
        $this->ui->view('hanifah/lobbykb.php', $data);
        break;
      case 'icmap':
        $this->ui->addScript('js/hanifah/lobbyicmap.js', '?t=' . time());
        //$this->ui->addScript($this->ui->location('hanifah/lobbyscript'), '?t=' . time());
        $this->ui->view('hanifah/lobbyicmap.php', $data);
        break;
      case 'ikb':
        $this->ui->addScript('js/hanifah/lobbyikb.js', '?t=' . time());
        //$this->ui->addScript($this->ui->location('hanifah/lobbyscript'), '?t=' . time());
        $this->ui->view('hanifah/lobbyikb.php', $data);
        break;
    }
    
  }

  function lobbyscript($page = 'lobby') {
    header('Content-Type: text/javascript');
    echo '/*' . "\n";
    echo print_r($_SESSION);
    echo "\n" . '*/' . "\n";
    echo '$(function(){' . "\n";
    if (isset($_SESSION['user'])) {
      $u    = (object) $_SESSION['user'];
      $gids = [];
      if(isset($u->gids))
        foreach ($u->gids as $gid) {
          $gids[] = "'$gid'";
        }
      echo "let gids = [" . implode(",", $gids) . "];\n";
      echo "BRIDGE.app.kit.setUserWithName('$u->uid','$u->username','$u->name','$u->role_id', gids, '$page');\n";
      echo "BRIDGE.app.signIn($u->uid,'$u->username','$u->name','$u->role_id', gids);\n";
      echo "BRIDGE.app.joinRoom($u->uid);\n";
    }
    echo '})';
  }

  // function lobbykb() { // exit; 

  //   $this->_check('lobbykb', 'lobbykb');

  //   $this->ui->addPlugin('jquery');
  //   $this->ui->addPlugin('jqui');
  //   $this->ui->addPlugin('bootstrap');
  //   $this->ui->addPlugin('animate');
  //   $this->ui->addPlugin('bs-notify');
  //   $this->ui->addPlugin('tippy');
  //   $this->ui->addPlugin('cytoscape');
  //   $this->ui->addPlugin('kbui', '?t=' . time());

  //   $this->ui->addScript('vendors/socket.io.slim.js', '?t=' . time());
  //   $this->ui->addScript('js/collab/collab.channel.tool.js', '?t=' . time());
  //   $this->ui->addScript('js/collab/collab.message.js', '?t=' . time());
  //   $this->ui->addScript('js/collab/collab.kit.js', '?t=' . time());

  //   $this->ui->addStyle('css/chat/chat.css', '?t=' . time());

  //   $this->ui->addStyle('css/general/three-dots.css', '?t=' . time());
  //   $this->ui->addScript('js/general/notification.js');
  //   $this->ui->addScript('js/general/logger.js');
  //   $this->ui->addScript('js/general/eventlistener.js');
  //   $this->ui->addScript('js/general/gui.js');
  //   $this->ui->addScript('js/general/session.js');
  //   $this->ui->addScript('js/general/ajax.js');

  //   $this->ui->addScript('js/hanifah/lobbykb.js', '?t=' . time());
  //   $this->ui->addScript($this->ui->location('hanifah/lobbykbscript'), '?t=' . time());

  //   $this->ui->addStyle('css/general/base.css', '?t=' . time());

  //   $data['room'] = null;
  //   if (isset($_SESSION['user'])) {
  //     $u    = (object) $_SESSION['user'];
  //     $roomService = new RoomService();
  //     $data['room'] = $roomService->selectRoomByUid($u->uid);
  //   }

  //   $this->ui->view('hanifah/lobbykb.php', $data);
  // }

  // function lobbykbscript($page = 'lobbykb') {
  //   header('Content-Type: text/javascript');
  //   echo '/*' . "\n";
  //   echo print_r($_SESSION);
  //   echo "\n" . '*/' . "\n";
  //   echo '$(function(){' . "\n";
  //   if (isset($_SESSION['user'])) {
  //     $u    = (object) $_SESSION['user'];
  //     $gids = [];
  //     if(isset($u->gids))
  //       foreach ($u->gids as $gid) {
  //         $gids[] = "'$gid'";
  //       }
  //     // $mid = $_SESSION['mid'];
  //     // $goalmapService = new GoalmapService();
  //     // $goalmaps = $goalmapService->getGoalmaps($mid, 'fix');
  //     // $data['goalmaps'] = count($goalmaps) ? $goalmaps : [];
  //     echo "let gids = [" . implode(",", $gids) . "];\n";
  //     echo "BRIDGE.app.kit.setUserWithName('$u->uid','$u->username','$u->name','$u->role_id', gids, '$page');\n";
  //     echo "BRIDGE.app.signIn($u->uid,'$u->username','$u->name','$u->role_id', gids);\n";
  //     echo "BRIDGE.app.joinRoom($u->uid);\n";
  //     // echo "BRIDGE.app.loadMaterial('PAPER-PROTOTYPE-TESTING');\n";
  //   }
  //   echo '})';
  // }

  function postlobby() {
    $this->_check('postlobby', 'postlobby');

    $materialService = new MaterialService();
    if(!isset($_SESSION['mid'])) 
      die('Invalid session. <a href="'.$this->location($this->controller . '/signOut').'">Sign Out</a>');
    $material = $materialService->getMaterialByMid($_SESSION['mid']);
    $fid = $material->fid;
    if(!$fid || strpos($fid, '-WPRE') === false) {
      $next = $_SESSION['activity'];
      $_SESSION['page'] = $next;
      $this->redirect($this->controller . '/' . $next);
      return;
    }

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/hanifah/postlobby.js', '?t=' . time());
    $this->ui->addScript($this->ui->location('hanifah/postlobbyscript/' . $material->mid . '/' . $material->fid . '/'), '?t=' . time());
    $this->ui->addStyle('css/general/base.css', '?t=' . time());

    $data['material'] = $material;

    $this->ui->view('hanifah/postlobby.php',$data);
  }

  function postlobbyscript($mid, $fid) {
    header('Content-Type: text/javascript');
    echo '/*' . "\n";
    echo print_r($_SESSION);
    echo "\n" . '*/' . "\n";
    echo '$(function(){' . "\n";
    echo "BRIDGE.app.mid = $mid;\n";
    echo "BRIDGE.app.fid = '$fid';\n";
    echo "BRIDGE.app.logger.log('pre-pretest-page', {mid: $mid, fid: '$fid'});\n";
    echo '})';
  }

  function pretest() {
    $this->_check('pretest', 'pretest');

    $user = (object) $_SESSION['user'];
    $remaining = (15 * 60);
    if(!isset($_SESSION['prebegin'])) {
      $_SESSION['prebegin'] = time();
      $_SESSION['preend'] = time() + (15 * 60);
    } else {
      $remaining = $_SESSION['preend'] - time();
    }

    // $customId = 'PPT-POST';
    $mid = $_SESSION['mid'];
    $type = 'pre';
    $qsetService = new QsetService();
    $qset = $qsetService->getQsetOfMaterialAndType($mid, $type);
    $qset = $qsetService->selectQsetWithQuestions($qset->qsid);
    // $qsetService->takeAttempt($qset->qsid, $user->uid);

    $data['qset'] = $qset;

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/general/logger.js');

    // $this->ui->addStyle('css/read.css');

    $this->ui->addScript('js/hanifah/pretest.js', '?t=' . time());
    $this->ui->addScript($this->ui->location('hanifah/pretestscript/' . $qset->qsid . '/' . $remaining), '?t=' . time());
    $this->ui->view('hanifah/pretest.php', $data);
  }

  function pretestscript($qsid, $remaining) {
    header('Content-Type: text/javascript');
    echo '$(function(){' . "\n";
    if (isset($_SESSION['user'])) {
      echo "BRIDGE.app.qsid = $qsid\n";
      echo "BRIDGE.app.countdown($remaining);\n";
    }
    echo '})';
  }

  function premapping() { // exit;

    $this->_check('premapping', 'premapping');

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    // $this->ui->addPlugin('tippy');
    // $this->ui->addPlugin('cytoscape');
    // $this->ui->addPlugin('kbui', '?t=' . time());

    switch($_SESSION['activity']) {
      case 'cmap':
      case 'kb':
        $this->ui->addScript('vendors/socket.io.slim.js', '?t=' . time());
        $this->ui->addScript('js/collab/collab.channel.tool.js', '?t=' . time());
        $this->ui->addScript('js/collab/collab.message.js', '?t=' . time());
        $this->ui->addScript('js/collab/collab.kit.js', '?t=' . time());
        $this->ui->addStyle('css/chat/chat.css', '?t=' . time());
      break;
    }

    $this->ui->addStyle('css/general/three-dots.css', '?t=' . time());
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');
    switch($_SESSION['activity']) {
      case 'icmap':
      case 'ikb':
        $this->ui->addScript('js/hanifah/premapping.js', '?t=' . time());
        // $this->ui->addScript($this->ui->location('hanifah/premappingscript'), '?t=' . time());
        break;
      default:
        $this->ui->addScript('js/hanifah/premapping.js', '?t=' . time());
        // $this->ui->addScript($this->ui->location('hanifah/premappingscript'), '?t=' . time());
        break;
    }

    $this->ui->addStyle('css/general/base.css', '?t=' . time());

    $data['room'] = null;
    if (isset($_SESSION['user'])) {
      $u    = (object) $_SESSION['user'];
      $roomService = new RoomService();
      $data['room'] = $roomService->selectRoomByUid($u->uid);
      $data['materials'] = [];
      //var_dump($u);
      if(isset($u->gids)) {
        $materialService = new MaterialService();
        $data['materials'] = $materialService->getMaterialsWithGids($u->gids);
      }
    }

    $this->ui->view('hanifah/premapping.php', $data);
  }

  // function premappingscript($page = 'premapping') {
  //   header('Content-Type: text/javascript');
  //   echo '/*' . "\n";
  //   echo print_r($_SESSION);
  //   echo "\n" . '*/' . "\n";
  //   echo '$(function(){' . "\n";
  //   if (isset($_SESSION['user'])) {
  //     $u    = (object) $_SESSION['user'];
  //     $gids = [];
  //     if(isset($u->gids))
  //       foreach ($u->gids as $gid) {
  //         $gids[] = "'$gid'";
  //       }
  //     echo "let gids = [" . implode(",", $gids) . "];\n";
  //     echo "BRIDGE.app.kit.setUserWithName('$u->uid','$u->username','$u->name','$u->role_id', gids, '$page');\n";
  //     echo "BRIDGE.app.signIn($u->uid,'$u->username','$u->name','$u->role_id', gids);\n";
  //     echo "BRIDGE.app.joinRoom($u->uid);\n";
  //     // echo "BRIDGE.app.loadMaterial('H-PRACTICE');\n";
  //   }
  //   echo '})';
  // }

  function cmap() { // exit;

    $this->_check('cmap', 'cmap');

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('cytoscape');
    $this->ui->addPlugin('kbui', '?t=' . time());

    $this->ui->addScript('vendors/socket.io.slim.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.channel.tool.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.message.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.kit.js', '?t=' . time());

    $this->ui->addStyle('css/chat/chat.css', '?t=' . time());

    $this->ui->addStyle('css/general/three-dots.css', '?t=' . time());
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');

    $this->ui->addScript('js/hanifah/cmap.app.js', '?t=' . time());
    $this->ui->addScript('js/hanifah/cmap.js', '?t=' . time());
    $this->ui->addScript($this->ui->location('hanifah/cmapscript'), '?t=' . time());

    $this->ui->addStyle('css/hanifah/cmap.css', '?t=' . time());
    $this->ui->view('hanifah/cmap.php');
  }

  function cmapscript($page = 'cmap') {
    header('Content-Type: text/javascript');
    echo '/*' . "\n";
    echo print_r($_SESSION);
    echo "\n" . '*/' . "\n";
    echo '$(function(){' . "\n";
    if (isset($_SESSION['user'])) {
      $u    = (object) $_SESSION['user'];
      $gids = [];
      if(isset($u->gids))
      foreach ($u->gids as $gid) {
        $gids[] = "'$gid'";
      }
      echo "let gids = [" . implode(",", $gids) . "];\n";
      echo "BRIDGE.app.kit.setUserWithName('$u->uid','$u->username','$u->name','$u->role_id', gids, '$page');\n";
      echo "BRIDGE.app.signIn($u->uid,'$u->username','$u->role_id', gids);\n";
    }
    if (isset($_SESSION['chat-window-open']) && $_SESSION['chat-window-open']) {
      echo "BRIDGE.collabKit.chatWindowOpen = " . $_SESSION['chat-window-open'] . ";\n";
    }

    if (isset($_SESSION['mid'])) {
      echo "BRIDGE.app.loadMaterial('$_SESSION[mid]');\n";
      echo "BRIDGE.logger.setMid('$_SESSION[mid]');\n";
    }

    if (isset($_SESSION['rid'])) {
      echo "BRIDGE.app.kit.loadRoom('$_SESSION[rid]', function(room){
        BRIDGE.app.kit.joinRoom(room);
        BRIDGE.logger.setRid('$_SESSION[rid]');
      });\n";
    }

    // if (isset($_SESSION['room'])) {
    //   $r = $_SESSION['room'];
    //   echo "BRIDGE.collabKit.joinRoom({rid:'$r[rid]',name:'$r[name]'});\n";
    // }
    echo '})';
  }

  function kb() {

    $this->_check('kb', 'kb');

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('cytoscape');
    $this->ui->addPlugin('kbui', '?t=' . time());
    
    $this->ui->addScript('js/analyzer/analyzer.lib.js', '?t=' . time());

    $this->ui->addScript('vendors/socket.io.slim.js', '?t=' . time());
    
    $this->ui->addScript('js/collab/collab.channel.tool.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.message.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.kit.js', '?t=' . time());

    $this->ui->addStyle('css/chat/chat.css', '?t=' . time());

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');

    $this->ui->addScript('js/hanifah/kb.js', '?t=' . time());
    $this->ui->addScript($this->ui->location('hanifah/sscript'), '?t=' . time());

    $this->ui->addStyle('css/kb.css', '?t=' . time());
    $this->ui->view('hanifah/kb.php');
  }

  function sscript($page = 'kb') {
    header('Content-Type: text/javascript');
    echo '/*' . "\n";
    echo print_r($_SESSION);
    echo "\n" . '*/' . "\n";
    echo '$(function(){' . "\n";
    if (isset($_SESSION['user'])) {
      $u    = (object) $_SESSION['user'];
      $gids = [];
      if(property_exists($u, 'gids'))
        foreach ($u->gids as $gid) {
          $gids[] = "'$gid'";
        }
      echo "let gids = [" . implode(",", $gids) . "];\n";
      echo "BRIDGE.app.kit.setUserWithName('$u->uid','$u->username','$u->name','$u->role_id', gids, '$page');\n";
      echo "BRIDGE.app.signIn($u->uid,'$u->username','$u->name','$u->role_id', gids);\n";
    }
    if (isset($_SESSION['chat-window-open']) && $_SESSION['chat-window-open']) {
      echo "BRIDGE.collabKit.chatWindowOpen = " . $_SESSION['chat-window-open'] . ";\n";
    }

    if (isset($_SESSION['mid'])) {
      echo "BRIDGE.app.loadMaterial('$_SESSION[mid]');\n";
      // echo "BRIDGE.logger.setMid('$_SESSION[mid]');\n";
    }

    if (isset($_SESSION['gmid'])) {
      echo "BRIDGE.app.loadKit('$_SESSION[gmid]');\n";
      echo "BRIDGE.logger.setGmid('$_SESSION[gmid]');\n";
    }

    if (isset($_SESSION['rid'])) {
      echo "BRIDGE.app.kit.loadRoom('$_SESSION[rid]', function(room){
        BRIDGE.app.kit.joinRoom(room);
        BRIDGE.logger.setRid('$_SESSION[rid]');
      });\n";
    }

    echo '})';
  }

  function icmap() { // exit;

    $this->_check('icmap', 'icmap');

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('cytoscape');
    $this->ui->addPlugin('kbui', '?t=' . time());

    // $this->ui->addScript('vendors/socket.io.slim.js', '?t=' . time());
    // $this->ui->addScript('js/collab/collab.channel.tool.js', '?t=' . time());
    // $this->ui->addScript('js/collab/collab.message.js', '?t=' . time());
    // $this->ui->addScript('js/collab/collab.kit.js', '?t=' . time());

    // $this->ui->addStyle('css/chat/chat.css', '?t=' . time());

    $this->ui->addStyle('css/general/three-dots.css', '?t=' . time());
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');

    $this->ui->addScript('js/hanifah/icmap.js', '?t=' . time());
    // $this->ui->addScript($this->ui->location('hanifah/icmapscript'), '?t=' . time());

    $this->ui->addStyle('css/hanifah/cmap.css', '?t=' . time());
    $this->ui->view('hanifah/icmap.php');
  }

  // function icmapscript($page = 'cmap') {
  //   header('Content-Type: text/javascript');
  //   echo '/*' . "\n";
  //   echo print_r($_SESSION);
  //   echo "\n" . '*/' . "\n";
  //   echo '$(function(){' . "\n";
  //   if (isset($_SESSION['user'])) {
  //     $u    = (object) $_SESSION['user'];
  //     $gids = [];
  //     if(isset($u->gids))
  //     foreach ($u->gids as $gid) {
  //       $gids[] = "'$gid'";
  //     }
  //     echo "let gids = [" . implode(",", $gids) . "];\n";
  //     echo "BRIDGE.app.kit.setUserWithName('$u->uid','$u->username','$u->name','$u->role_id', gids, '$page');\n";
  //     echo "BRIDGE.app.signIn($u->uid,'$u->username','$u->role_id', gids);\n";
  //   }
  //   if (isset($_SESSION['chat-window-open']) && $_SESSION['chat-window-open']) {
  //     echo "BRIDGE.collabKit.chatWindowOpen = " . $_SESSION['chat-window-open'] . ";\n";
  //   }

  //   if (isset($_SESSION['mid'])) {
  //     echo "BRIDGE.app.loadMaterial('$_SESSION[mid]');\n";
  //     echo "BRIDGE.logger.setMid('$_SESSION[mid]');\n";
  //   }

  //   if (isset($_SESSION['rid'])) {
  //     echo "BRIDGE.app.kit.loadRoom('$_SESSION[rid]', function(room){
  //       BRIDGE.app.kit.joinRoom(room);
  //       BRIDGE.logger.setRid('$_SESSION[rid]');
  //     });\n";
  //   }

  //   // if (isset($_SESSION['room'])) {
  //   //   $r = $_SESSION['room'];
  //   //   echo "BRIDGE.collabKit.joinRoom({rid:'$r[rid]',name:'$r[name]'});\n";
  //   // }
  //   echo '})';
  // }

  function ikb() {

    $this->_check('ikb', 'ikb');

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('cytoscape');
    $this->ui->addPlugin('kbui', '?t=' . time());

    $this->ui->addScript('js/analyzer/analyzer.lib.js', '?t=' . time());

    // $this->ui->addScript('vendors/socket.io.slim.js', '?t=' . time());
    // $this->ui->addScript('js/collab/collab.channel.tool.js', '?t=' . time());
    // $this->ui->addScript('js/collab/collab.message.js', '?t=' . time());
    // $this->ui->addScript('js/collab/collab.kit.js', '?t=' . time());

    $this->ui->addStyle('css/chat/chat.css', '?t=' . time());

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');

    $this->ui->addScript('js/hanifah/ikb.js', '?t=' . time());
    // $this->ui->addScript($this->ui->location('hanifah/sscript'), '?t=' . time());

    $this->ui->addStyle('css/kb.css', '?t=' . time());
    $this->ui->view('hanifah/ikb.php');
  }

  // function sscript($page = 'kb') {
  //   header('Content-Type: text/javascript');
  //   echo '/*' . "\n";
  //   echo print_r($_SESSION);
  //   echo "\n" . '*/' . "\n";
  //   echo '$(function(){' . "\n";
  //   if (isset($_SESSION['user'])) {
  //     $u    = (object) $_SESSION['user'];
  //     $gids = [];
  //     if(property_exists($u, 'gids'))
  //       foreach ($u->gids as $gid) {
  //         $gids[] = "'$gid'";
  //       }
  //     echo "let gids = [" . implode(",", $gids) . "];\n";
  //     echo "BRIDGE.app.kit.setUserWithName('$u->uid','$u->username','$u->name','$u->role_id', gids, '$page');\n";
  //     echo "BRIDGE.app.signIn($u->uid,'$u->username','$u->name','$u->role_id', gids);\n";
  //   }
  //   if (isset($_SESSION['chat-window-open']) && $_SESSION['chat-window-open']) {
  //     echo "BRIDGE.collabKit.chatWindowOpen = " . $_SESSION['chat-window-open'] . ";\n";
  //   }

  //   if (isset($_SESSION['mid'])) {
  //     echo "BRIDGE.app.loadMaterial('$_SESSION[mid]');\n";
  //     // echo "BRIDGE.logger.setMid('$_SESSION[mid]');\n";
  //   }

  //   if (isset($_SESSION['gmid'])) {
  //     echo "BRIDGE.app.loadKit('$_SESSION[gmid]');\n";
  //     echo "BRIDGE.logger.setGmid('$_SESSION[gmid]');\n";
  //   }

  //   if (isset($_SESSION['rid'])) {
  //     echo "BRIDGE.app.kit.loadRoom('$_SESSION[rid]', function(room){
  //       BRIDGE.app.kit.joinRoom(room);
  //       BRIDGE.logger.setRid('$_SESSION[rid]');
  //     });\n";
  //   }

  //   echo '})';
  // }

  function postmapping() {
    $this->_check('postmapping', 'postmapping');

    $materialService = new MaterialService();
    $material = $materialService->getMaterialByMid($_SESSION['mid']);
    $fid = $material->fid;
    if(!$fid || strpos($fid, '-WPOST') === false) {
      $next = 'finish';
      $_SESSION['page'] = $next;
      $this->redirect($this->controller . '/' . $next);
      return;
    }

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/hanifah/postmapping.js', '?t=' . time());
    $this->ui->addScript($this->ui->location('hanifah/postmappingscript/' . $material->mid . '/' . $material->fid . '/'), '?t=' . time());
    $this->ui->addStyle('css/general/base.css', '?t=' . time());

    $data['material'] = $material;

    $this->ui->view('hanifah/postmapping.php',$data);
  }

  function postmappingscript($mid, $fid) {
    header('Content-Type: text/javascript');
    echo '/*' . "\n";
    echo print_r($_SESSION);
    echo "\n" . '*/' . "\n";
    echo '$(function(){' . "\n";
    echo "BRIDGE.app.mid = $mid;\n";
    echo "BRIDGE.app.fid = '$fid';\n";
    echo "BRIDGE.app.logger.log('pre-posttest-page', {mid: $mid, fid: '$fid'});\n";
    echo '})';
  }

  function posttest() {
    $this->_check('posttest', 'posttest');

    $user = (object) $_SESSION['user'];
    $remaining = (15 * 60);
    if(!isset($_SESSION['postbegin'])) {
      $_SESSION['postbegin'] = time();
      $_SESSION['postend'] = time() + (15 * 60);
    } else {
      $remaining = $_SESSION['postend'] - time();
    }

    $mid = $_SESSION['mid'];
    $type = 'post';
    $qsetService = new QsetService();
    $qset = $qsetService->getQsetOfMaterialAndType($mid, $type);
    $qset = $qsetService->selectQsetWithQuestions($qset->qsid);

    $data['qset'] = $qset;

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/general/logger.js');

    // $this->ui->addStyle('css/read.css');

    $this->ui->addScript('js/hanifah/posttest.js', '?t=' . time());
    $this->ui->addScript($this->ui->location('hanifah/posttestscript/' . $qset->qsid . '/' . $remaining), '?t=' . time());
    $this->ui->view('hanifah/posttest.php', $data);
  }

  function posttestscript($qsid, $remaining) {
    header('Content-Type: text/javascript');
    echo '$(function(){' . "\n";
    if (isset($_SESSION['user'])) {
      echo "BRIDGE.app.qsid = $qsid\n";
      echo "BRIDGE.app.countdown($remaining);\n";
    }
    echo '})';
  }

  function finish() {
    $this->_check('finish', 'finish');
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addStyle('css/experiment/base.css', '?t=' . time());
    $this->ui->view('hanifah/finish.php');
  }


  function delay($topicfid) {
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/hanifah/delay.login.js', '?t=' . time());
    $this->ui->addStyle('css/experiment/base.css');

    $materialService = new MaterialService();
    $material = $materialService->getMaterialCollabByFid($topicfid);
    if(!$material) die('Invalid ID');
    $qsetService = new QsetService();
    $qset = $qsetService->getQsetOfMaterialAndType($material->mid, 'delay');
    $this->ui->view('hanifah/delay.login.php', array(
      'topic' => $material,
      'qset' => $qset
    ));
  }


  function delaytest() {
    $this->_check('delaytest', 'delaytest');

    $user = (object) $_SESSION['user'];
    $remaining = (15 * 60);
    if(!isset($_SESSION['delaybegin'])) {
      $_SESSION['delaybegin'] = time();
      $_SESSION['delayend'] = time() + (15 * 60);
    } else {
      $remaining = $_SESSION['delayend'] - time();
    }

    $mid = $_SESSION['mid'];
    $type = 'delay';
    $qsetService = new QsetService();
    $qset = $qsetService->getQsetOfMaterialAndType($mid, $type);
    $qset = $qsetService->selectQsetWithQuestions($qset->qsid);

    $experimentService = new ExperimentService();
    $result = $experimentService->attemptTest($user->uid, $qset->qsid);

    $data['qset'] = $qset;

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/general/logger.js');

    // $this->ui->addStyle('css/read.css');

    $this->ui->addScript('js/hanifah/delay.test.js', '?t=' . time());
    $this->ui->addScript($this->ui->location('hanifah/delaytestscript/' . $qset->qsid . '/' . $remaining), '?t=' . time());
    $this->ui->view('hanifah/delay.test.php', $data);
  }

  function delaytestscript($qsid, $remaining) {
    header('Content-Type: text/javascript');
    echo '$(function(){' . "\n";
    if (isset($_SESSION['user'])) {
      echo "BRIDGE.app.qsid = $qsid\n";
      echo "BRIDGE.app.countdown($remaining);\n";
    }
    echo '})';
  }

  function delayfinish() {
    $this->_check('delayfinish', 'delayfinish');
    $user = isset($_SESSION['user']) ? (object) $_SESSION['user'] : null;
    if ($user and $uid = $user->uid) {
      $sessionService = new SessionService();
      $rows = $sessionService->clearSessionData($uid);
      session_destroy();
    }
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addStyle('css/experiment/base.css', '?t=' . time());
    $this->ui->view('hanifah/delay.finish.php');
  }

}