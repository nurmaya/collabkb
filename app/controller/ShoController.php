<?php

class ShoController extends GeneralController {

  function __construct()
  {
    parent::__construct('home');
  }

  function cmap() { // exit;

    // $this->_check('cmap', 'cmap');

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('cytoscape');
    $this->ui->addPlugin('kbui', '?t=' . time());

    $this->ui->addScript('vendors/socket.io.slim.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.channel.tool.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.message.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.kit.js', '?t=' . time());

    $this->ui->addStyle('css/chat/chat.css', '?t=' . time());

    $this->ui->addStyle('css/general/three-dots.css', '?t=' . time());
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');

    $this->ui->addScript('js/sho/cmap.js', '?t=' . time());
    // $this->ui->addScript($this->ui->location('home/tscript'), '?t=' . time());
    $this->ui->addStyle('css/general/base.css', '?t=' . time());
    $this->ui->addStyle('css/sho/cmap.css', '?t=' . time());
    $this->ui->view('sho/cmap.php');
  }

  function tscript() {
    header('Content-Type: text/javascript');
    echo '/*' . "\n";
    echo print_r($_SESSION);
    echo "\n" . '*/' . "\n";
    echo '$(function(){' . "\n";
    if (isset($_SESSION['user'])) {
      $u    = (object) $_SESSION['user'];
      $gids = [];
      foreach ($u->gids as $gid) {
        $gids[] = "'$gid'";
      }
      echo "let gids = [" . implode(",", $gids) . "];\n";
      echo "BRIDGE.app.kit.setUserWithName('$u->uid','$u->username','$u->name','$u->role_id', gids);\n";
      echo "BRIDGE.app.signIn($u->uid,'$u->username', '$u->role_id', gids);\n";
    }
    if (isset($_SESSION['chat-window-open']) && $_SESSION['chat-window-open']) {
      echo "BRIDGE.collabKit.chatWindowOpen = " . $_SESSION['chat-window-open'] . ";\n";
    }

    if (isset($_SESSION['mid'])) {
      echo "BRIDGE.app.loadMaterial('$_SESSION[mid]');\n";
      echo "BRIDGE.logger.setMid('$_SESSION[mid]');\n";
    }

    if (isset($_SESSION['rid'])) {
      echo "BRIDGE.app.kit.loadRoom('$_SESSION[rid]', function(room){
        BRIDGE.app.kit.joinRoom(room);
        BRIDGE.logger.setRid('$_SESSION[rid]');
      });\n";
    }

    // if (isset($_SESSION['room'])) {
    //   $r = $_SESSION['room'];
    //   echo "BRIDGE.collabKit.joinRoom({rid:'$r[rid]',name:'$r[name]'});\n";
    // }
    echo '})';
  }

}