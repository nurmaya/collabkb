<?php

class ChannelService extends CoreService {

  public function openChannel($rid, $node_id, $node_type, $node_label, $name, $extra) {
    $db      = $this->getInstance('kb-collab');
    $qb      = QB::instance($db);
    $result  = new stdClass;
    $node_id = substr($node_id, 2);
    $channel = $qb->table('channel')
      ->select(array('cid', 'name', 'date_create', 'rid', 'node_id', 'node_type', 'node_label', 'extra'))
      ->where('rid', $rid)
      ->where('node_id', $node_id)
      ->where('node_type', $node_type)
      ->executeQuery(true);
    if (!count($channel)) {
      $insert['rid']        = QB::esc($rid);
      $insert['node_id']    = QB::esc($node_id);
      $insert['node_type']  = QB::esc($node_type);
      $insert['node_label'] = QB::esc($node_label);
      $insert['name']       = QB::esc(substr($name, 0, 90));
      $insert['extra']      = $extra !== null ? QB::esc(json_encode($extra)) : null;
      $qb->clear();
      $qb->table('channel')
        ->insert($insert)
        ->execute();
      $qb->clear();
      $channel = $qb->table('channel')
        ->select(array('cid', 'name', 'date_create', 'rid', 'node_id', 'node_type', 'node_label', 'extra'))
        ->where('rid', $rid)
        ->where('node_id', $node_id)
        ->where('node_type', $node_type)
        ->executeQuery(true);
    }
    $qb->clear();
    $messages = $qb->table('messages m')
      ->leftJoin('messages_channel mc', 'm.mid', 'mc.mid')
      ->leftJoin('channel c', 'mc.cid', 'c.cid')
      ->leftJoin('users u', 'm.uid', 'u.uid')
      ->select(array('m.mid', 'm.message', 'm.mdate', 'm.rid', 'm.uid', 'mc.cid', 'u.uid', 'u.username', 'u.name'))
      ->where('c.rid', $rid)
      ->where('c.node_id', $node_id)
      ->where('c.node_type', $node_type)
      ->executeQuery(true);
    $result  = count($channel) ? $channel[0] : null;
    if($result) $result->messages = $messages;
    return $result;
  }

}