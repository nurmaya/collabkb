<?php

class GoalmapCollabService extends GoalmapService {

  public function getLastDraftGoalmap($mid, $rid) {
    $db       = $this->getInstance('kb-collab');
    $qb       = QB::instance($db);
    $result   = new stdClass;
    $goalmaps = $qb->table('goalmaps g')
      ->leftJoin('goalmaps_collab gc', 'gc.gmid', 'g.gmid')
      ->select(array('g.gmid', 'name', 'type', 'mid', 'creator_id', 'create_time', 'updater_id', 'update_time'))
      ->where('g.type', 'draft')
      ->where('g.mid', QB::esc($mid))
      ->where('gc.rid', QB::esc($rid))
      ->orderBy('update_time', QB::ORDER_DESC)
      ->limit(1)
      ->executeQuery(true);
    $gmid = null;
    if ($goalmaps) {
      $goalmap         = $goalmaps[0];
      $gmid            = $goalmap->gmid;
      $result->goalmap = $goalmap;
    } else {
      return null;
    }

    $qb->clear();
    $concepts = $qb->table('goalmaps_concepts c')
      ->leftJoin('goalmaps_concepts_extras e', array('c.cid' => 'e.cid', 'c.gmid' => 'e.gmid'))
      ->select(array('c.cid', 'c.gmid', 'c.label', 'c.locx', 'c.locy', 'e.extra'))
      ->where('c.gmid', $gmid)
      ->executeQuery(true);
    $result->concepts = $concepts ? $concepts : [];
    $qb->clear();
    $links = $qb->table('goalmaps_links l')
      ->leftJoin('goalmaps_links_extras e', array('l.lid' => 'e.lid', 'l.gmid' => 'e.gmid'))
      ->select(array('l.lid', 'l.gmid', 'l.label', 'l.locx', 'l.locy', 'l.source', 'l.target', 'e.extra'))
      ->where('l.gmid', $gmid)
      ->executeQuery(true);
    $result->links = $links ? $links : [];
    return $result;
  }

  public function insertGoalmap($name, $type, $mid, $creator_id, $updater_id, $concepts, $links, $rid) {
    $db                     = $this->getInstance('kb-collab');
    $qb                     = QB::instance($db);
    $goalmaps['name']       = QB::esc($name);
    $goalmaps['type']       = QB::esc($type);
    $goalmaps['mid']        = QB::esc($mid);
    $goalmaps['creator_id'] = QB::esc($creator_id);
    $goalmaps['updater_id'] = QB::esc($updater_id);
    $goalmaps_collab['rid'] = QB::esc($rid);
    try {
      $qb->begin();
      if($type == 'auto') {
        $gmid = $qb->table('goalmaps g')
          ->leftJoin('goalmaps_collab gc', 'g.gmid', 'gc.gmid')
          ->delete()
          ->where('mid', QB::esc($mid))
          ->where('type', QB::esc($type))
          ->where('creator_id', QB::esc($creator_id))
          ->where('gc.rid', QB::esc($rid))
          ->execute();
        $qb->clear();
      }
      $gmid = $qb->table('goalmaps')
        ->insert($goalmaps)
        ->execute()
        ->getInsertId();
      $cs  = [];
      $ls  = [];
      foreach ($concepts as $k => $v) {
        $concepts[$k]['gmid']  = $gmid;
        $concepts[$k]['label'] = QB::esc($concepts[$k]['label']);
        $concepts[$k]['extra'] = isset($concepts[$k]['extra']) ? QB::esc(json_encode($concepts[$k]['extra'])) : null;
        $cs[]                  = (object) $concepts[$k];
      }
      foreach ($links as $k => $v) {
        $links[$k]['gmid']  = $gmid;
        $links[$k]['label'] = QB::esc($links[$k]['label']);
        $links[$k]['extra'] = isset($links[$k]['extra']) ? QB::esc(json_encode($links[$k]['extra'])) : null;
        if ($links[$k]['source'] == "") {
          $links[$k]['source'] = null;
        }

        if ($links[$k]['target'] == "") {
          $links[$k]['target'] = null;
        }

        $ls[] = (object) $links[$k];
      }
      if (count($cs)) {
        foreach ($cs as $c) {
          $e = [];
          $e['cid'] = $c->cid;
          $e['gmid']  = $c->gmid;
          $e['extra'] = $c->extra;
          unset($c->extra);
          $qb->clear();
          $qb->table('goalmaps_concepts')
            ->insertModel($c)
            ->execute();
          $qb->clear();
          $qb->table('goalmaps_concepts_extras')
            ->insert($e)
            ->execute();
        }
      }
      if (count($ls)) {
        foreach ($ls as $l) {
          $e = [];
          $e['lid'] = $l->lid;
          $e['gmid']  = $l->gmid;
          $e['extra'] = $l->extra;
          unset($l->extra);
          $qb->clear();
          $qb->table('goalmaps_links')
            ->insertModel($l)
            ->execute();
          $qb->clear();
          $qb->table('goalmaps_links_extras')
            ->insert($e)
            ->execute();
        }
      }
      $qb->clear();
      $goalmaps_collab['gmid'] = $gmid;
      $result                  = $qb->table('goalmaps_collab')
        ->insert($goalmaps_collab)
        ->execute()->getAffectedRows();
      // if ($type == 'fix') {
      //   $qb->clear();
      //   $qb->table('goalmaps g')
      //     ->leftJoin('goalmaps_collab gc', 'g.gmid', 'gc.gmid')
      //     ->delete()
      //     ->where('g.type', 'draft')
      //     ->where('g.mid', $mid)
      //     ->where('gc.rid', $rid)
      //     ->execute();
      // }
      if ($result) {
        $qb->commit();
      } else {
        throw CoreError::instance('Unable to insert into `goalmaps_collab` inherited table.');
      }

      return $gmid;
    } catch (Exception $ex) {
      $qb->rollback();
      throw new Exception($ex->getMessage() . ". " . $qb->get());
    }
  }

  public function getGoalmaps($mid, $rid, $type = null) {
    $db       = $this->getInstance('kb-collab');
    $qb       = QB::instance($db);
    $qb       = $qb->table('goalmaps g')
      ->leftJoin('goalmaps_collab gc', 'g.gmid', 'gc.gmid')
      ->leftJoin('users u', 'u.uid', 'g.creator_id')
      ->select(array('g.gmid', 'g.name', 'g.type', 'g.mid', 'g.creator_id', 'g.create_time', 'g.updater_id', 'g.update_time'))
      ->select(array('u.uid', 'u.username'))
      ->selectRaw('(SELECT COUNT(*) FROM goalmaps_concepts gcs WHERE gcs.gmid = g.gmid) AS concepts_count')
      ->selectRaw('(SELECT COUNT(*) FROM goalmaps_links gl WHERE gl.gmid = g.gmid) AS links_count');
    if($type) $qb->where('g.type', QB::esc($type));
    $goalmaps = $qb->where('g.mid', QB::esc($mid))
      ->where('gc.rid', QB::esc($rid))
      ->orderBy('update_time', QB::ORDER_DESC)
      ->executeQuery(true);
    return $goalmaps;
  }

}
