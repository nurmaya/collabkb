<?php

class RoomService extends CoreService {

  public function getRidFromRoomName($roomname) {
    $db = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('rooms')
      ->select(array('rid'))
      ->where('name',$roomname)
      ->executeQuery(true);
    return count($result) ? $result[0]->rid : '';
  }

  public function selectRooms() {
    $db = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('rooms')
      ->select(array('rid', 'name', 'open', 'date_create', 'gid'))
      ->executeQuery(true);
    return $result;
  }

  public function selectRoomsWithExtras() {
    $db = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('rooms r')
      ->leftJoin('room_users ru', 'ru.rid', 'r.rid')
      ->select(array('r.rid', 'r.name', 'r.open', 'r.date_create', 'r.gid'))
      ->selectRaw('COUNT(ru.uid) AS users_count')
      ->groupBy(array('r.rid'))
      ->executeQuery(true);
    return $result;
  }

  public function selectRoom($rid) {
    $db = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('rooms')
      ->select(array('rid', 'name', 'open', 'date_create', 'gid'))
      ->where('rid', $rid)
      ->executeQuery(true);
    if(count($result)) return $result[0];
    return $result;
  }

  public function selectRoomByUid($uid) {
    $db = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('rooms r')
      ->leftJoin('room_users ru', 'r.rid', 'ru.rid')
      ->select(array('r.rid', 'r.name', 'r.open', 'r.date_create', 'r.gid'))
      ->where('ru.uid', $uid)
      ->executeQuery(true);
    if(count($result)) return $result[0];
    return $result;
  }

  public function selectRoomsByGids($gids) {
    if(!count($gids)) return array();
    $db = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('rooms')
      ->select(array('rid', 'name', 'open', 'date_create', 'gid'))
      ->distinct()
      ->whereIn('gid', $gids)
      ->executeQuery(true);
    return $result;
  }

  public function insertRoom($name) {
    $db = $this->getInstance('kb-collab');
    $cols['name'] = QB::esc($name);
    $qb = QB::instance($db)
      ->table('rooms')
      ->insert($cols)
      ->execute(true);
    return $qb->insertId();
  }

  public function deleteRoom($rid) {
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db)
      ->table('rooms')
      ->delete()
      ->where('rid', QB::esc($rid))
      ->execute(true);
    return $qb->getAffectedRows();
  }

  public function closeRoom($rid) {
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db)
      ->table('rooms')
      ->update(array('open' => 0))
      ->where('rid', QB::esc($rid))
      ->execute(true);
    return $qb->getAffectedRows();
  }

  public function openRoom($rid) {
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db)
      ->table('rooms')
      ->update(array('open' => 1))
      ->where('rid', QB::esc($rid))
      ->execute(true);
    return $qb->getAffectedRows();
  }

  public function updateRoom($rid, $name) {
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db)
      ->table('rooms')
      ->update(array('name' => $name))
      ->where('rid', QB::esc($rid))
      ->execute(true);
    return $qb->getAffectedRows();
  }

  // User Room

  public function getUserRoom($rid)
  {
    $db        = $this->getInstance('kb-collab');
    $rid       = QB::esc($rid);
    $qb        = QB::instance($db);
    $users     = new stdClass;
    $users->in = $qb->table('users u')
      ->selectRaw('u.uid, u.name, u.username')
      ->whereRaw("u.uid IN (SELECT ru.uid FROM room_users ru WHERE ru.rid = '$rid')")
      ->executeQuery(true);
    $qb->clear();
    $users->notin = $qb->table('users u')
      ->selectRaw('u.uid, u.name, u.username')
      ->whereRaw("u.uid NOT IN (SELECT ru.uid FROM room_users ru WHERE ru.rid = '$rid')")
      ->executeQuery(true);
    return $users;
  }

  public function addUserToRoom($uid, $rid)
  {
    $db        = $this->getInstance('kb-collab');
    $uid       = QB::esc($uid);
    $rid       = QB::esc($rid);
    $qb        = QB::instance($db);
    $insert['uid'] = $uid;
    $insert['rid'] = $rid;
    $qb->table('room_users ru')
      ->insert($insert, true)
      ->execute();
    return $qb->getAffectedRows();
  }

  public function removeUserFromRoom($uid, $rid)
  {
    $db        = $this->getInstance('kb-collab');
    $uid       = QB::esc($uid);
    $rid       = QB::esc($rid);
    $qb        = QB::instance($db);
    $res       = $qb->table('room_users ru')
      ->delete()
      ->where('uid', $uid)
      ->where('rid', $rid)
      ->execute();
    return $qb->getAffectedRows();
  }

}