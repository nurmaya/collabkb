<?php

class AnswerService extends CoreService
{

  public function saveAnswers($uid, $qsid, $answers)
  {
    $db     = $this->getInstance('kb-collab');
    $qb = QB::instance($db);
    $insert = [];
    foreach($answers as $a) {
      $insert['uid'] = $uid;
      $insert['qsid'] = $qsid;
      $insert['qid'] = $a['qid'];
      $insert['qoid'] = $a['qoid'];
      $update['qoid'] = $a['qoid'];
      $update['answer_time'] = QB::raw('NOW()');
      $qb->clear();
      $qb->table('answers_multiple_choice')
      ->insertUpdate($insert, $update)
      ->execute();
    }
    return true;
  }

  public function saveAnswersEssay($uid, $qsid, $answers)
  {
    $db     = $this->getInstance('kb-collab');
    $qb = QB::instance($db);
    $insert = [];
    foreach($answers as $a) {
      if(empty(QB::esc($a['answer']))) continue;
      $insert['uid'] = $uid;
      $insert['qsid'] = $qsid;
      $insert['qid'] = $a['qid'];
      $insert['essay'] = QB::esc($a['answer']);
      $update['essay'] = QB::esc($a['answer']);
      $qb->clear();
      $qb->table('answers_essay')
      ->insertUpdate($insert, $update)
      ->execute();
    }
    return true;
  }

  public function saveAnswersMulti($uid, $qsid, $answers)
  {
    $db     = $this->getInstance('kb-collab');
    $qb = QB::instance($db);
    $insert = [];
    $lastQid = null;
    foreach($answers as $a) {
      $insert['uid'] = $uid;
      $insert['qsid'] = $qsid;
      $insert['qid'] = $a['qid'];
      $insert['qoid'] = $a['qoid'];
      if($lastQid != $a['qid']) {
        $qb->clear();
        $qb->table('answers_multiple_answer')
          ->delete()
          ->where('uid', $uid)
          ->where('qid', $a['qid'])
          ->execute();
        $lastQid = $a['qid'];
      }
      $qb->clear();
      $qb->table('answers_multiple_answer')
      ->insert($insert)
      ->execute();
    }
    return true;
  }

}