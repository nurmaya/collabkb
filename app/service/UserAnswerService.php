<?php

class UserAnswerService extends CoreService
{

  public function saveAnswers($uid, $qsid, $gid, $answers)
  {
    $db     = $this->getInstance('kb-collab');
    $qb = QB::instance($db);
    $insert = [];
    foreach($answers as $a) {
      $insert['uid'] = $uid;
      $insert['qsid'] = $qsid;
      $insert['gid'] = $gid;
      $insert['qid'] = $a['qid'];
      $insert['qoid'] = $a['qoid'];
      $update['qoid'] = $a['qoid'];
      $qb->clear();
      $qb->table('user_answers')
      ->insertUpdate($insert, $update)
      ->execute();
    }
    return true;
  }

  public function saveAnswersEssay($uid, $qsid, $gid, $answers)
  {
    $db     = $this->getInstance('kb-collab');
    $qb = QB::instance($db);
    $insert = [];
    foreach($answers as $a) {
      $insert['uid'] = $uid;
      $insert['qsid'] = $qsid;
      $insert['gid'] = $gid;
      $insert['qid'] = $a['qid'];
      $insert['essay'] = QB::esc($a['answer']);
      $update['essay'] = QB::esc($a['answer']);
      $qb->clear();
      $qb->table('user_answers_essay')
      ->insertUpdate($insert, $update)
      ->execute();
    }
    return true;
  }

  public function saveAnswersMulti($uid, $qsid, $gid, $answers)
  {
    $db     = $this->getInstance('kb-collab');
    $qb = QB::instance($db);
    $insert = [];
    $lastQid = null;
    foreach($answers as $a) {
      $insert['uid'] = $uid;
      $insert['qsid'] = $qsid;
      $insert['gid'] = $gid;
      $insert['qid'] = $a['qid'];
      $insert['qoid'] = $a['qoid'];
      if($lastQid != $a['qid']) {
        $qb->clear();
        $qb->table('user_answers_multi')
          ->delete()
          ->where('uid', $uid)
          ->where('qsid', $qsid)
          ->where('gid', $gid)
          ->where('qid', $a['qid'])
          ->execute();
        $lastQid = $a['qid'];
      }
      $qb->clear();
      $qb->table('user_answers_multi')
      ->insert($insert)
      ->execute();
    }
    return true;
  }

}