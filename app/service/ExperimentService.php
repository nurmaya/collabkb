<?php

class ExperimentService extends CoreService
{

  public function joinRoom($uid, $rid)
  {
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db);
    $result = $qb->table('room_users')
      ->insertUpdate(array('uid' => $uid, 'rid' => $rid), array('rid' => $rid))
      ->execute();
    return $result;
  }

  public function getPairGmid($uid, $rid, $mid)
  {
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db);
    $result = $qb->table('room_users')
      ->select(array('uid'))
      ->where('rid', $rid)
      ->where('uid', '<>', $uid)
      ->limit(1)
      ->executeQuery(true);
    if (!count($result)) {
      throw CoreError::instance('No pair found.');
    }

    $row = $result[0];
    $pairUid = $row->uid;
    $qb->clear();
    $goalmaps = $qb->table('goalmaps')
      ->select(array('gmid'))
      ->where('creator_id', $pairUid)
      ->where('mid', $mid)
      ->orderBy('gmid', QB::ORDER_DESC)
      ->limit(1)
      ->executeQuery(true);
    if (!count($goalmaps)) {
      throw CoreError::instance('No goalmaps found.');
    }

    $goalmap = $goalmaps[0];
    return $goalmap->gmid;
  }

  public function getGoalmapKit($gmid)
  {
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db);
    $result  = new stdClass;
    $goalmap = $qb->table('goalmaps g')
      ->leftJoin('goalmaps_collab gc', 'g.gmid', 'gc.gmid')
      ->select(array('g.gmid', 'name', 'type', 'mid', 'creator_id', 'create_time', 'updater_id', 'update_time', 'gc.rid'))
      ->where('g.gmid', QB::esc($gmid))
      ->limit(1)
      ->executeQuery(true);
    $qb->clear();
    $concepts = $qb->table('goalmaps_concepts c')
      ->select(array('cid', 'label', 'gmid', 'locx', 'locy'))
      ->where('gmid', QB::esc($gmid))
      ->executeQuery(true);
    $qb->clear();
    $links = $qb->table('goalmaps_links l')
      ->leftJoin('goalmaps_concepts gcs', array('l.source' => 'gcs.cid', 'l.gmid' => 'gcs.gmid'))
      ->leftJoin('goalmaps_concepts gct', array('l.target' => 'gct.cid', 'l.gmid' => 'gct.gmid'))
      ->select(array('l.lid', 'l.label', 'l.gmid', 'l.locx', 'l.locy', 'l.source', 'l.target', 'gcs.label AS slabel', 'gct.label AS tlabel'))
      ->where('l.gmid', QB::esc($gmid))
      ->executeQuery(true);
    $result->goalmap  = count($goalmap) ? $goalmap[0] : null;
    $result->concepts = $concepts;
    $result->links    = $links;
    return $result;
  }

  public function getPairLearnermap($gmid, $uid)
  {
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db);
    $learnermaps = $qb->table('learnermaps')
      ->select(array('lmid', 'gmid', 'uid', 'type'))
      ->where('uid', $uid)
      ->where('gmid', $gmid)
      ->where('type', 'fix')
      ->orderBy('gmid', QB::ORDER_DESC)
      ->limit(1)
      ->executeQuery(true);
    if (!count($learnermaps)) {
      throw CoreError::instance('Could not find learnermaps.');
    }

    $learnermap = $learnermaps[0];
    $qb->clear();
    $concepts = $qb->table('learnermaps_concepts lc')
      ->leftJoin('goalmaps_concepts gc', array('lc.cid' => 'gc.cid', 'lc.gmid' => 'gc.gmid'))
      ->select(array('lc.cid', 'lc.lmid', 'lc.gmid', 'lc.locx', 'lc.locy', 'gc.label'))
      ->where('lc.lmid', $learnermap->lmid)
      ->executeQuery(true);
    $learnermap->concepts = $concepts ? $concepts : [];
    $qb->clear();
    $links = $qb->table('learnermaps_links ll')
      ->leftJoin('goalmaps_links gl', array('ll.lid' => 'gl.lid', 'll.gmid' => 'gl.gmid'))
      ->leftJoin('learnermaps_concepts lcs', array('ll.lmid' => 'lcs.lmid', 'll.gmid' => 'lcs.gmid', 'll.source' => 'lcs.cid'))
      ->leftJoin('goalmaps_concepts gcs', array('lcs.cid' => 'gcs.cid', 'lcs.gmid' => 'gcs.gmid'))
      ->leftJoin('learnermaps_concepts lct', array('ll.lmid' => 'lct.lmid', 'll.gmid' => 'lct.gmid', 'll.target' => 'lct.cid'))
      ->leftJoin('goalmaps_concepts gct', array('lct.cid' => 'gct.cid', 'lct.gmid' => 'gct.gmid'))
      ->select(array('ll.lid', 'll.lmid', 'll.gmid', 'll.locx', 'll.locy', 'll.source', 'll.target', 'gl.label', 'gcs.label AS slabel', 'gct.label AS tlabel'))
      ->where('ll.lmid', $learnermap->lmid)
      ->executeQuery(true);
    $learnermap->links = $links ? $links : [];
    return $learnermap;
  }

  public function getPairGoalmaps($rid, $gmid)
  {
    $db   = $this->getInstance('kb-collab');
    $qb   = QB::instance($db);
    $gmid = QB::esc($gmid);
    $roomUsers = $qb->table('room_users u')
      ->select(array('u.uid'))
      ->selectRaw("(SELECT gm.gmid FROM goalmaps gm WHERE gm.creator_id = u.uid AND gm.type = 'fix' AND gm.mid = '$gmid' ORDER BY create_time DESC LIMIT 1) AS gmid")
      ->where('rid', QB::esc($rid))
      ->executeQuery(true);
    for ($i = 0; $i < count($roomUsers); $i++) {
      $roomUser = $roomUsers[$i];
      $qb->clear();
      $goalmap = $qb->table('goalmaps g')
        ->leftJoin('goalmaps_collab gc', 'g.gmid', 'gc.gmid')
        ->select(array('g.gmid', 'name', 'type', 'mid', 'creator_id', 'create_time', 'updater_id', 'update_time', 'gc.rid'))
        ->where('g.gmid', QB::esc($roomUser->gmid))
        ->limit(1)
        ->executeQuery(true);
      $qb->clear();
      $concepts = $qb->table('goalmaps_concepts c')
        ->select(array('cid', 'label', 'gmid', 'locx', 'locy'))
        ->where('gmid', QB::esc($roomUser->gmid))
        ->executeQuery(true);
      $qb->clear();
      $links = $qb->table('goalmaps_links l')
        ->leftJoin('goalmaps_concepts gcs', array('l.source' => 'gcs.cid', 'l.gmid' => 'gcs.gmid'))
        ->leftJoin('goalmaps_concepts gct', array('l.target' => 'gct.cid', 'l.gmid' => 'gct.gmid'))
        ->select(array('l.lid', 'l.label', 'l.gmid', 'l.locx', 'l.locy', 'l.source', 'l.target', 'gcs.label AS slabel', 'gct.label AS tlabel'))
        ->where('l.gmid', QB::esc($roomUser->gmid))
        ->executeQuery(true);
      $roomUsers[$i]->goalmap  = count($goalmap) ? $goalmap[0] : null;
      $roomUsers[$i]->concepts = $concepts;
      $roomUsers[$i]->links    = $links;
    }
    return $roomUsers;
  }

  public function getPairworkLearnermaps($gmids)
  {
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db);
    $fromTable = $qb->table('learnermaps')
      ->select(array('lmid', 'gmid', 'uid', 'type'))
      ->whereIn('gmid', $gmids)
      ->where('type', 'fix')->get();
    $qb->clear();
    $learnermaps = $qb->table(QB::raw('(' . $fromTable . ') x'))
      ->selectRaw('uid, ANY_VALUE(lmid) AS lmid, ANY_VALUE(gmid) AS gmid, ANY_VALUE(type) AS type')
      ->groupBy(array('uid'))
      ->orderBy('lmid', QB::ORDER_DESC)
      ->executeQuery(true);
    if (!count($learnermaps))
      throw CoreError::instance('Could not find learnermaps.');

    for ($i = 0; $i < count($learnermaps); $i++) {
      $learnermap = $learnermaps[$i];
      $qb->clear();
      $concepts = $qb->table('learnermaps_concepts lc')
        ->leftJoin('goalmaps_concepts gc', array('lc.cid' => 'gc.cid', 'lc.gmid' => 'gc.gmid'))
        ->select(array('lc.cid', 'lc.lmid', 'lc.gmid', 'lc.locx', 'lc.locy', 'gc.label'))
        ->where('lc.lmid', $learnermap->lmid)
        ->executeQuery(true);
      $learnermaps[$i]->concepts = $concepts ? $concepts : [];
      $qb->clear();
      $links = $qb->table('learnermaps_links ll')
        ->leftJoin('goalmaps_links gl', array('ll.lid' => 'gl.lid', 'll.gmid' => 'gl.gmid'))
        ->leftJoin('learnermaps_concepts lcs', array('ll.lmid' => 'lcs.lmid', 'll.gmid' => 'lcs.gmid', 'll.source' => 'lcs.cid'))
        ->leftJoin('goalmaps_concepts gcs', array('lcs.cid' => 'gcs.cid', 'lcs.gmid' => 'gcs.gmid'))
        ->leftJoin('learnermaps_concepts lct', array('ll.lmid' => 'lct.lmid', 'll.gmid' => 'lct.gmid', 'll.target' => 'lct.cid'))
        ->leftJoin('goalmaps_concepts gct', array('lct.cid' => 'gct.cid', 'lct.gmid' => 'gct.gmid'))
        ->select(array('ll.lid', 'll.lmid', 'll.gmid', 'll.locx', 'll.locy', 'll.source', 'll.target', 'gl.label', 'gcs.label AS slabel', 'gct.label AS tlabel'))
        ->where('ll.lmid', $learnermap->lmid)
        ->executeQuery(true);
      $learnermaps[$i]->links = $links ? $links : [];
    }
    return $learnermaps;
  }

  public function checkTestTaken($type, $uid, $qsid, $gid = null)
  {
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db);
    $ua1 = $qb->table('user_answers ua')
      ->leftJoin('grups_has_qsets gq', array('ua.qsid' => 'gq.qsid', 'ua.gid' => 'gq.gid'))
      ->leftJoin('question_sets qs', 'qs.qsid', 'gq.qsid')
      ->selectRaw('COUNT(*) as answered')
      ->where('ua.uid', $uid)
      ->where('ua.qsid', $qsid);
    if ($gid) $ua1->where('ua.gid', $gid);
    $ua1 = $ua1->where('qs.type', $type)
      ->executeQuery(true);
    if (count($ua1)) {
      $ua = $ua1[0];
      if ($ua->answered) return true;
    }
    $qb->clear();
    $ua2 = $qb->table('user_answers_essay ua')
      ->leftJoin('grups_has_qsets gq', array('ua.qsid' => 'gq.qsid', 'ua.gid' => 'gq.gid'))
      ->leftJoin('question_sets qs', 'qs.qsid', 'gq.qsid')
      ->selectRaw('COUNT(*) as answered')
      ->where('ua.uid', $uid)
      ->where('ua.qsid', $qsid);
    if ($gid) $ua2->where('ua.gid', $gid);
    $ua2 = $ua2->where('qs.type', $type)
      ->where('qs.type', $type)
      ->executeQuery(true);
    if (count($ua2)) {
      $ua = $ua2[0];
      if ($ua->answered) return true;
    }
    return false;
  }

  public function checkTaken($uid, $qsid)
  {
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db);
    $uamc = $qb->table('answers_multiple_choice amc')
      ->selectRaw('COUNT(*) AS answered')
      ->whereRaw('amc.uid = \'' . QB::esc($uid) . '\' AND amc.qid IN (
        SELECT qid FROM qsets_has_questions qq 
        LEFT JOIN question_sets qs ON qs.qsid = qq.qsid
        WHERE qs.qsid = \'' . QB::esc($qsid) . '\')')
      ->executeQuery(true);
    if (count($uamc)) {
      $ua = $uamc[0];
      if ($ua->answered) return true;
    }
    $qb->clear();
    $uae = $qb->table('answers_essay ae')
      ->selectRaw('COUNT(*) AS answered')
      ->whereRaw('ae.uid = \'' . QB::esc($uid) . '\' AND ae.qid IN (
        SELECT qid FROM qsets_has_questions qq 
        LEFT JOIN question_sets qs ON qs.qsid = qq.qsid
        WHERE qs.qsid = \'' . QB::esc($qsid) . '\')')
      ->executeQuery(true);
    if (count($uae)) {
      $ua = $uae[0];
      if ($ua->answered) return true;
    }
    return false;
  }

  public function getUsersTakingQset($qsid = null, $customId = null) {
    if($qsid == null && $customId == null) 
      throw CoreError::instance('Invalid ID');
      
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db);
    if ($customId !== null) {
      $users = $qb->table('test_attempt a')
        ->leftJoin('question_sets qs', 'qs.qsid', 'a.qsid')
        ->leftJoin('users u', 'u.uid', 'a.uid')
        ->select(array('a.uid', 'a.qsid', 'a.attempt_time', 'u.username', 'u.name'))
        ->where('qs.customid', QB::esc($customId))
        ->executeQuery(true);
      return $users && count($users) ? $users : [];
    }

    $users = $qb->table('test_attempt a')
      ->leftJoin('users u', 'u.uid', 'a.uid')
      ->select(array('a.uid', 'a.qsid', 'a.attempt_time', 'u.username', 'u.name'))
      ->where('a.qsid', QB::esc($qsid))
      ->executeQuery(true);
    return $users && count($users) ? $users : [];
  }

  public function checkAttempts($uid, $qsid = null, $customId = null)
  {
    if($qsid == null && $customId == null) 
      throw CoreError::instance('Invalid ID');
      
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db);

    if($qsid !== null && !empty($qsid)) {
      $attempts = $qb->table('test_attempt a')
      ->leftJoin('question_sets qs', 'qs.qsid', 'a.qsid')
      ->leftJoin('users u', 'u.uid', 'a.uid')
      ->select(array('a.uid', 'a.qsid', 'a.attempt_time', 'u.username', 'u.name'))
      ->where('a.uid', QB::esc($uid))
      ->where('a.qsid', QB::esc($qsid))
      ->executeQuery(true);
      return $attempts && count($attempts) ? $attempts : [];
    }
    if($customId !== null && !empty($customId)) {
      $attempts = $qb->table('test_attempt a')
        ->leftJoin('question_sets qs', 'qs.qsid', 'a.qsid')
        ->leftJoin('users u', 'u.uid', 'a.uid')
        ->select(array('a.uid', 'a.qsid', 'a.attempt_time', 'u.username', 'u.name'))
        ->where('a.uid', QB::esc($uid))
        ->where('qs.customid', QB::esc($customId))
        ->executeQuery(true);
      return $attempts && count($attempts) ? $attempts : [];
    }
    return [];
  }

  public function attemptTest($uid, $qsid) {
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db);
    $result = $qb->table('test_attempt')
      ->insert(array('uid' => $uid, 'qsid' => $qsid))
      ->execute();
    return $result;
  }

  public function getExperimentData($uid) {
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db);
    $result = $qb->table('experiment_data')
      ->select(array('data'))
      ->where('uid', $uid)
      ->executeQuery(true);
    return count($result) ? json_decode(($result[0])->data) : null;
  }

  public function setExperimentData($uid, $data) {
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db);
    $result = $qb->table('experiment_data')
      ->insertUpdate(array(
        'uid' => $uid,
        'data' => json_encode($data)
      ), array(
        'data' => json_encode($data)
      ))
      ->execute();
    return $result;
  }
}
