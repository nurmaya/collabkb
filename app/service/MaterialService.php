<?php

class MaterialService extends CoreService {

  public function selectMaterials() {
    $db     = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('materials m')
      ->select(array('m.mid', 'm.fid', 'm.name', 'm.creator_id', 'm.create_time', 'm.enabled'))
      ->executeQuery(true);
    return $result;
  }

  public function selectMaterialsWithQsetExtras() {
    $db     = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('materials m')
      ->select(array('m.mid', 'm.fid', 'm.name', 'm.creator_id', 'm.create_time', 'm.enabled'))
      ->selectRaw('(SELECT COUNT(*) FROM question_sets qs WHERE qs.mid = m.mid) AS cqsets')
      ->executeQuery(true);
    return $result;
  }

  public function getMaterials($all) {
    $db     = $this->getInstance('kb-collab');
    $qb     = QB::instance($db)
      ->table('materials m')
      ->select(array('m.mid', 'm.fid', 'm.name', 'm.creator_id', 'm.create_time', 'm.enabled'));
    if(!$all) $qb->where('m.enabled', 1);
    $result = $qb->executeQuery(true);
    return $result;
  }

  public function getMaterialByMid($mid, $enabledOnly = 1) {
    $db     = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('materials m')
      ->leftJoin('materials_collab mc', 'm.mid', 'mc.mid')
      ->select(array('m.mid', 'm.fid', 'm.name', 'm.creator_id', 'm.create_time', 'm.enabled'))
      ->select(array('mc.content'))
      ->where('m.mid', QB::esc($mid));
    if($enabledOnly) $result->where('m.enabled', $enabledOnly);
    $result = $result->limit(1)
      ->executeQuery(true);
    return count($result) ? $result[0] : null;
  }

  public function getMaterialCollabByMid($mid) {
    $db     = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('materials m')
      ->leftJoin('materials_collab mc', 'm.mid', 'mc.mid')
      ->select(array('m.mid', 'm.fid', 'm.name', 'm.creator_id', 'm.create_time', 'm.enabled'))
      ->select(array('mc.content'))
      ->where('m.mid', QB::esc($mid))
      ->where('m.enabled', 1)
      ->limit(1)
      ->executeQuery(true);
    return $result;
  }

  public function getMaterialCollabByFid($fid) {
    $db     = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('materials m')
      ->leftJoin('materials_collab mc', 'm.mid', 'mc.mid')
      ->select(array('m.mid', 'm.fid', 'm.name', 'm.creator_id', 'm.create_time', 'm.enabled'))
      ->select(array('mc.content'))
      ->where('m.fid', QB::esc($fid))
      ->where('m.enabled', 1)
      ->limit(1)
      ->executeQuery(true);
    return count($result) ? $result[0] : null;
  }

  public function getMaterialCollabByMidOrFid($id) {
    $db     = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('materials m')
      ->leftJoin('materials_collab mc', 'm.mid', 'mc.mid')
      ->select(array('m.mid', 'm.fid', 'm.name', 'm.creator_id', 'm.create_time', 'm.enabled'))
      ->select(array('mc.content'))
      ->whereRaw("(m.fid = '".QB::esc($id)."' OR m.mid = '".QB::esc($id)."')")
      ->where('m.enabled', 1)
      ->limit(1)
      ->executeQuery(true);
    return count($result) ? $result[0] : null;
  }

  public function getMaterialsWithGids($gids, $enabledOnly = 1) {
    if(!count($gids)) return array();
    $db     = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('materials m')
      ->leftJoin('grups_has_materials gm', 'gm.mid', 'm.mid')
      ->select(array('m.mid', 'm.fid', 'm.name', 'm.creator_id', 'm.create_time', 'm.enabled'))
      ->distinct();
    if($enabledOnly) $result = $result->where('m.enabled', '1');
    $result = $result->whereIn('gm.gid', $gids)
      ->executeQuery(true);
    return $result;
  }

  public function insertMaterial($name, $fid, $content) {
    $db                          = $this->getInstance('kb-collab');
    $materials['name']           = QB::esc($name);
    $materials['fid']            = QB::esc($fid);
    $materials_collab['content'] = QB::esc($content);
    $qb                          = QB::instance($db);
    try {
      $qb->begin()
        ->table('materials')
        ->insert($materials)
        ->execute(true);
      $materials_collab['mid'] = $qb->insertId();
      $qb->clear()
        ->table('materials_collab')
        ->insert($materials_collab)
        ->execute(true)
        ->commit();
    } catch (Exception $ex) {
      $qb->rollback();
      throw $ex;
    }
  }

  public function deleteMaterial($mid) {
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db)
      ->table('materials')
      ->delete()
      ->where('mid', QB::esc($mid))
      ->execute(true);
    return $qb->getAffectedRows();
  }

  public function disableMaterial($mid) {
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db)
      ->table('materials')
      ->update(array('enabled' => 0))
      ->where('mid', QB::esc($mid))
      ->execute(true);
    return $qb->getAffectedRows();
  }

  public function enableMaterial($mid) {
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db)
      ->table('materials')
      ->update(array('enabled' => 1))
      ->where('mid', QB::esc($mid))
      ->execute(true);
    return $qb->getAffectedRows();
  }

  public function updateMaterial($mid, $fid, $name, $content) {
    $db                = $this->getInstance('kb-collab');
    $insert['mid']     = QB::esc($mid);
    $insert['content'] = QB::esc($content);
    $update['content'] = QB::esc($content);
    $qb                = QB::instance($db);
    try {
      $qb->begin()
        ->table('materials')
        ->update(array('name' => QB::esc($name),'fid' => QB::esc($fid)))
        ->where('mid', QB::esc($mid))
        ->execute(true)
        ->clear()
        ->table('materials_collab')
        ->insertUpdate($insert, $update)
        ->execute(true)
        ->commit();
    } catch (Exception $ex) {
      $qb->rollback();
      throw $ex;
    }
  }

  // Material Content

  public function getCollabContent($mid) {
    $db     = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('materials_collab m')
      ->select(array('m.content'))
      ->where('mid', $mid)
      ->executeQuery(true);
    return $result;
  }

}