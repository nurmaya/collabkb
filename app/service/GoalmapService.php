<?php

class GoalmapService extends CoreService
{
  public function getGoalmaps($mid, $type = null) {
    $db       = $this->getInstance('kb-collab');
    $qb       = QB::instance($db);
    $qb = $qb->table('goalmaps g')
      ->select(array('g.gmid', 'name', 'type', 'mid', 'creator_id', 'create_time', 'updater_id', 'update_time'))
      ->selectRaw('(SELECT COUNT(*) FROM goalmaps_concepts gc WHERE gc.gmid = g.gmid) AS concepts_count')
      ->selectRaw('(SELECT COUNT(*) FROM goalmaps_links gl WHERE gl.gmid = g.gmid) AS links_count')
      ->selectRaw('(SELECT COUNT(*) FROM learnermaps l WHERE l.type = \'fix\' AND l.gmid = g.gmid) AS learnermaps_count');
    if($type) $qb->where('g.type', QB::esc($type));
    $goalmaps = $qb->where('g.mid', QB::esc($mid))
      ->orderBy('update_time', QB::ORDER_DESC)
      ->executeQuery(true);
    return $goalmaps;
  }

  public function getUserGoalmaps($mid, $uid, $type = null) {
    $db       = $this->getInstance('kb-collab');
    $qb       = QB::instance($db);
    $qb = $qb->table('goalmaps g')
      ->select(array('g.gmid', 'name', 'type', 'mid', 'creator_id', 'create_time', 'updater_id', 'update_time'))
      ->selectRaw('(SELECT COUNT(*) FROM goalmaps_concepts gc WHERE gc.gmid = g.gmid) AS concepts_count')
      ->selectRaw('(SELECT COUNT(*) FROM goalmaps_links gl WHERE gl.gmid = g.gmid) AS links_count')
      ->selectRaw('(SELECT COUNT(*) FROM learnermaps l WHERE l.type = \'fix\' AND l.gmid = g.gmid) AS learnermaps_count');
    if($type) $qb->where('g.type', QB::esc($type));
    $goalmaps = $qb->where('g.mid', QB::esc($mid))
      ->where('g.creator_id', QB::esc($uid))
      ->orderBy('update_time', QB::ORDER_DESC)
      ->executeQuery(true);
    return $goalmaps;
  }

  public function getLastUserGoalmap($mid, $uid, $type = null) {
    $db       = $this->getInstance('kb-collab');
    $qb       = QB::instance($db);
    $qb = $qb->table('goalmaps g')
      ->select(array('g.gmid', 'name', 'type', 'mid', 'creator_id', 'create_time', 'updater_id', 'update_time'))
      ->selectRaw('(SELECT COUNT(*) FROM goalmaps_concepts gc WHERE gc.gmid = g.gmid) AS concepts_count')
      ->selectRaw('(SELECT COUNT(*) FROM goalmaps_links gl WHERE gl.gmid = g.gmid) AS links_count')
      ->selectRaw('(SELECT COUNT(*) FROM learnermaps l WHERE l.type = \'fix\' AND l.gmid = g.gmid) AS learnermaps_count');
    if($type) $qb->where('g.type', QB::esc($type));
    $goalmaps = $qb->where('g.mid', QB::esc($mid))
      ->where('g.creator_id', QB::esc($uid))
      ->orderBy('update_time', QB::ORDER_DESC)
      ->limit(1)
      ->executeQuery(true);
    return count($goalmaps) ? $goalmaps[0] : null;
  }

  public function getEachUserLastGoalmapOfRoom($mid, $rid, $type = null) {
    $db       = $this->getInstance('kb-collab');
    $qb       = QB::instance($db);
    $goalmaps = $qb->table('goalmaps g')
      ->select(array('g.gmid', 'name', 'type', 'mid', 'creator_id', 'create_time', 'updater_id', 'update_time'))
      ->selectRaw('(SELECT COUNT(*) FROM goalmaps_concepts gc WHERE gc.gmid = g.gmid) AS concepts_count')
      ->selectRaw('(SELECT COUNT(*) FROM goalmaps_links gl WHERE gl.gmid = g.gmid) AS links_count')
      ->selectRaw('(SELECT COUNT(*) FROM learnermaps l WHERE l.type = \'fix\' AND l.gmid = g.gmid) AS learnermaps_count')
      ->whereRaw("g.gmid IN (
      SELECT
        (SELECT gmid FROM goalmaps gm WHERE gm.creator_id = ru.uid AND mid = '$mid' AND type = '$type' ORDER BY create_time DESC LIMIT 1) AS gmid
        FROM room_users ru WHERE rid = '$rid'
      )")
      ->executeQuery(true);
    return count($goalmaps) ? $goalmaps : [];
  }

  public function getGoalmapKit($gmid) {
    $db       = $this->getInstance('kb-collab');
    $qb       = QB::instance($db);
    $result   = new stdClass;
    $goalmap  = $qb->table('goalmaps g')
      ->leftJoin('goalmaps_collab gc', 'g.gmid', 'gc.gmid')
      ->select(array('g.gmid', 'name', 'type', 'mid', 'creator_id', 'create_time', 'updater_id', 'update_time', 'gc.rid'))
      ->where('g.gmid', QB::esc($gmid))
      ->limit(1)
      ->executeQuery(true);
    $qb->clear();
    $concepts = $qb->table('goalmaps_concepts c')
      ->select(array('cid', 'label', 'gmid', 'locx', 'locy'))
      ->where('gmid', QB::esc($gmid))
      ->executeQuery(true);
    $qb->clear();
    $links = $qb->table('goalmaps_links l')
      ->select(array('lid', 'label', 'gmid', 'locx', 'locy', 'source', 'target'))
      ->where('gmid', QB::esc($gmid))
      ->executeQuery(true);
    $result->goalmap = count($goalmap) ? $goalmap[0]: null;
    $result->concepts = $concepts;
    $result->links = $links;
    return $result;
  } 

  public function insertGoalmap($name, $type, $mid, $creator_id, $updater_id, $concepts, $links) {
    $db                     = $this->getInstance('kb-collab');
    $qb                     = QB::instance($db);
    $goalmaps['name']       = QB::esc($name);
    $goalmaps['type']       = QB::esc($type);
    $goalmaps['mid']        = QB::esc($mid);
    $goalmaps['creator_id'] = QB::esc($creator_id);
    $goalmaps['updater_id'] = QB::esc($updater_id);
    try {
      $qb->begin();
      $gmid = $qb->table('goalmaps')
        ->insert($goalmaps)
        ->execute()
        ->getInsertId();
      $cs = [];
      $ls = [];
      foreach ($concepts as $k => $v) {
        $concepts[$k]['gmid']  = $gmid;
        $concepts[$k]['label'] = QB::esc($concepts[$k]['label']);
        $cs[]                  = (object) $concepts[$k];
      }
      foreach ($links as $k => $v) {
        $links[$k]['gmid']  = $gmid;
        $links[$k]['label'] = QB::esc($links[$k]['label']);
        if ($links[$k]['source'] == "") {
          $links[$k]['source'] = null;
        }

        if ($links[$k]['target'] == "") {
          $links[$k]['target'] = null;
        }

        $ls[] = (object) $links[$k];
      }
      if (count($cs)) {
        $qb->clear();
        $qb->table('goalmaps_concepts')
          ->insertModel($cs)
          ->execute();
      }
      if (count($ls)) {
        $qb->clear();
        $qb->table('goalmaps_links')
          ->insertModel($ls)
          ->execute();
      }
      // if ($type == 'fix') {
      //   $qb->clear();
      //   $qb->table('goalmaps g')
      //     ->delete()
      //     ->where('g.type', 'draft')
      //     ->where('g.mid', $mid)
      //     ->execute();
      // }
      $qb->commit();
      return $gmid;
    } catch (Exception $ex) {
      $qb->rollback();
      throw new Exception($ex->getMessage() . ". " . $qb->get());
    }
  }

  public function overwriteGoalmap($gmid, $updater_id, $concepts, $links, $type = 'draft') {
    $db                     = $this->getInstance('kb-collab');
    $qb                     = QB::instance($db);
    $goalmaps['updater_id'] = QB::esc($updater_id);
    $goalmaps['type']       = QB::esc($type);
    $goalmaps['update_time'] = QB::raw('NOW()');
    try {
      $qb->begin();
      $qb->table('goalmaps_links')
          ->delete()
          ->where('gmid', $gmid)
          ->execute();
      $qb->clear();
      $qb->table('goalmaps_concepts')
          ->delete()
          ->where('gmid', $gmid)
          ->execute();
      $qb->clear();
      $qb->table('goalmaps')
          ->update($goalmaps)
          ->where('gmid', $gmid)
          ->execute();
      $qb->clear();
      $cs = [];
      $ls = [];
      foreach ($concepts as $k => $v) {
        $concepts[$k]['gmid']  = $gmid;
        $concepts[$k]['label'] = QB::esc($concepts[$k]['label']);
        $cs[]                  = (object) $concepts[$k];
      }
      foreach ($links as $k => $v) {
        $links[$k]['gmid']  = $gmid;
        $links[$k]['label'] = QB::esc($links[$k]['label']);
        if ($links[$k]['source'] == "") {
          $links[$k]['source'] = null;
        }

        if ($links[$k]['target'] == "") {
          $links[$k]['target'] = null;
        }

        $ls[] = (object) $links[$k];
      }
      if (count($cs)) {
        $qb->clear();
        $qb->table('goalmaps_concepts')
          ->insertModel($cs)
          ->execute();
      }
      if (count($ls)) {
        $qb->clear();
        $qb->table('goalmaps_links')
          ->insertModel($ls)
          ->execute();
      }
      // if ($type == 'fix') {
      //   $qb->clear();
      //   $qb->table('goalmaps g')
      //     ->delete()
      //     ->where('g.type', 'draft')
      //     ->where('g.mid', $mid)
      //     ->execute();
      // }
      $qb->commit();
      return $gmid;
    } catch (Exception $ex) {
      $qb->rollback();
      throw new Exception($ex->getMessage() . ". " . $qb->get());
    }
  }

  public function updateGoalmap($gmid, $name, $type) {
    $db       = $this->getInstance('kb-collab');
    $qb       = QB::instance($db);
    $result = $qb->table('goalmaps g')
      ->update(array('g.name' => QB::esc($name), 'g.type' => QB::esc($type)))
      ->where('g.gmid', QB::esc($gmid))
      ->execute();
    return $result;
  }

  public function deleteGoalmap($gmid) {
    $db       = $this->getInstance('kb-collab');
    $qb       = QB::instance($db);
    $result = $qb->table('goalmaps g')
      ->delete()
      ->where('g.gmid', QB::esc($gmid))
      ->execute();
    return $result;
  }

  public function getLastDraftGoalmap($mid, $uid) {
    $db       = $this->getInstance('kb-collab');
    $qb       = QB::instance($db);
    $result   = new stdClass;
    $goalmaps = $qb->table('goalmaps g')
      ->select(array('g.gmid', 'name', 'type', 'mid', 'creator_id', 'create_time', 'updater_id', 'update_time'))
      ->where('g.type', 'draft')
      ->where('g.mid', QB::esc($mid))
      ->where('g.creator_id', QB::esc($uid))
      ->orderBy('update_time', QB::ORDER_DESC)
      ->limit(1)
      ->executeQuery(true);
    $gmid = null;
    if ($goalmaps) {
      $goalmap         = $goalmaps[0];
      $gmid            = $goalmap->gmid;
      $result->goalmap = $goalmap;
    } else {
      return null;
    }

    $qb->clear();
    $concepts = $qb->table('goalmaps_concepts')
      ->select(array('cid', 'gmid', 'label', 'locx', 'locy'))
      ->where('gmid', $gmid)
      ->executeQuery(true);
    $result->concepts = $concepts ? $concepts : [];
    $qb->clear();
    $links = $qb->table('goalmaps_links')
      ->select(array('lid', 'gmid', 'label', 'locx', 'locy', 'source', 'target'))
      ->where('gmid', $gmid)
      ->executeQuery(true);
    $result->links = $links ? $links : [];
    return $result;
  }

  function setGoalmapAsKit($gmid) {
    $db       = $this->getInstance('kb-collab');
    $qb       = QB::instance($db);
    $result = $qb->table('goalmaps g')
      ->update(array('g.type' => 'kit'))
      ->where('g.gmid', QB::esc($gmid))
      ->execute();
    return $result;
  }
}
