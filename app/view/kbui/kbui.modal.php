<!-- New Node Modal -->
<div class="modal" id="kb-modal-prompt" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">New Concept</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="form-inline">
          <div class="form-group mb-2">
            <span class="modal-input-label">Concept Name</span>
          </div>
          <div class="form-group mx-sm-3 mb-2" style="position:relative">
            <input type="text" class="form-control" id="input-node-label" style="width:300px;">
            <div class="invalid-tooltip">
              Please provide a label.
            </div>
          </div>
        </div>
        <small class="modal-helper-text">Concept name should be less than 6 words and cannot empty.</small>
        

      </div>
      <div class="modal-footer">
        <p class="text-error text-danger mr-5">Error</p>
        <button type="button" class="btn btn-sm btn-primary bt-ok bt-dialog">OK</button>
        <button type="button" class="btn btn-sm btn-secondary bt-close bt-dialog" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Dialog Modal -->

<div class="modal" id="kb-modal-dialog" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="modal-dialog-content"></div>
        <hr>
        <div style="text-align:right">
          <button type="button" class="btn btn-sm btn-primary btn-positive bt-dialog">Yes</button>
          <button type="button" class="btn btn-sm btn-secondary btn-negative bt-dialog">No</button>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Snapshot Modal -->

<div class="modal" id="kb-modal-snapshot" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" style="max-width:800px" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div style="text-align:right">
          <button type="button" class="btn btn-sm btn-primary btn-download bt-dialog">
            <i class="fas fa-save"></i>&nbsp; Download</button>
          <button type="button" class="btn btn-sm btn-secondary btn-close bt-dialog">Close</button>
        </div>
        <hr>
        <div class="kb-snapshot" style="padding:1em"></div>
        <hr>
        <div style="text-align:right">
          <button type="button" class="btn btn-sm btn-primary btn-download bt-dialog">
            <i class="fas fa-save"></i>&nbsp; Download</button>
          <button type="button" class="btn btn-sm btn-secondary btn-close bt-dialog">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>