    <div id="cy-toolbar">
      <div class="btn-group">
        <button class="btn btn-sm btn-outline-secondary" disabled>
          <i class="fas fa-plus"></i></button>
        <button id="bt-new-concept" class="btn btn-sm btn-outline-primary"
          data-tippy-content="New Concept">Concept</button>
        <button id="bt-new-link" class="btn btn-sm btn-outline-info" 
          data-tippy-content="New Link">Link</button>
      </div>
      <div class="btn-group" style="margin-left: 1em;">
        <button id="bt-undo" class="btn btn-sm btn-outline-primary" 
          data-tippy-content="Undo last action">
          <i class="fas fa-undo"></i> Undo</button>
        <button id="bt-redo" class="btn btn-sm btn-outline-primary" 
          data-tippy-content="Redo last action">
          <i class="fas fa-redo"></i> Redo</button>
      </div>
      <div class="btn-group" style="margin-left: 1em;">
        <button class="btn btn-sm btn-outline-secondary" disabled>Zoom</button>
        <button id="bt-zoom-in" class="btn btn-sm btn-outline-primary" 
          data-tippy-content="Zoom In">
          <i class="fas fa-plus"></i>
        </button>
        <button id="bt-zoom-out" class="btn btn-sm btn-outline-primary" 
          data-tippy-content="Zoom Out">
          <i class="fas fa-minus"></i>
        </button>
        <button id="bt-fit" class="btn btn-sm btn-outline-primary" 
          data-tippy-content="Zoom fit to screen">
          <i class="fas fa-expand"></i>
        </button>
      </div>
      <div class="btn-group" style="margin-left: 1em;">
        <button class="btn btn-sm btn-outline-secondary" disabled>Map</button>
        <button id="bt-center" class="btn btn-sm btn-outline-primary" 
          data-tippy-content="Center map">
          <i class="fas fa-compress"></i>
        </button>
        <button id="bt-relayout" class="btn btn-sm btn-outline-primary" 
          data-tippy-content="Auto-layout map elements">
          <i class="fas fa-bezier-curve"></i>
        </button>
        <button id="bt-save-image" class="btn btn-sm btn-outline-primary" 
          data-tippy-content="Save map as image">
          <i class="fas fa-image"></i>
        </button>
        <button id="bt-clear-canvas" class="btn btn-sm btn-outline-primary" 
          data-tippy-content="Clear canvas">
          <i class="fas fa-trash-alt"></i>
        </button>
      </div>
    </div>