<?php $this->view('base/header.php'); ?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
      <!-- <h3 class="masthead-brand">Kit-Build</h3> -->
      <nav class="nav nav-masthead justify-content-center">
        <!-- <a class="btn btn-danger" href="<?php echo $this->location('home/signOut'); ?>">Sign Out</a> -->
        <!-- <a class="nav-link" href="#">Features</a>
        <a class="nav-link" href="#">Contact</a> -->
      </nav>
    </div>
  </header>

  <main role="main" class="inner cover text-left mx-auto" style="width: 42em;">
    <h1 class="h3">Thank You!</h1> 
    <h1 class="h4 cover-heading text-info">ありがとうございました！</h1>
    <hr>
    <p>Terima kasih Anda telah membangun peta konsep dari kit yang diberikan. Selanjutnya Anda akan diminta untuk menjawab kembali pertanyaan-pertanyaan dalam post-test terkait materi/topik yang sama dengan sebelumnya untuk mengevaluasi pemahaman Anda terkait materi yang diberikan.</p>
    <p>Anda diberi waktu <strong class="text-danger">10 menit</strong> untuk menjawab seluruh pertanyaan yang diberikan. Anda tidak diperbolehkan untuk melihat dan membaca materi yang diberikan.</p>
    <!-- <?php 
      // var_dump($_SESSION['user']); 
      // $user = (object) $_SESSION['user'];
      // $grups = $user->grups;
      // $nextPage = 'kb';
      // if(in_array('E2B', $grups)) : 
      //   $nextPage = 'kbcollab';
      $nextPage = 'posttest';
    ?>
    <p>Dalam membangun peta konsep dengan menggunakan kit nantinya. Anda akan membangun peta konsep dengan berkolaborasi <strong>secara berpasangan</strong> dengan rekan Anda. Kolaborasi yang dilakukan ketika membangun peta konsep dilakukan secara real-time, dimana perubahan peta konsep yang Anda lakukan akan langsung direfleksikan dalam layar rekan kolaborasi Anda. Silakan memanfaatkan fitur chat/diskusi yang melekat pada link/concept untuk berdiskusi terkait konsep dan link yang sedang didiskusikan.</p>
    <p>Percakapan yang dilakukan dalam sistem selama membangun peta konsep akan direkam untuk dianalisis.</p>
    <?php // endif; ?> -->
    <hr>
    <p>Tunggu instruksi yang diberikan sebelum Anda melanjutkan ke tahap selanjutnya.</p>
    <button id="bt-continue" class="btn btn-primary btn-lg" data-next="<?php echo $nextPage; ?>">Kerjakan Post-Test</button>
  </main>

  <footer class="mastfoot mt-auto">
    <div class="inner text-center">
      &nbsp;
    </div>
  </footer>
</div>

<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>