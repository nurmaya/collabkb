<?php $this->view('base/header.php'); ?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column mt-5" style="max-width: 42em;">
  <main role="main" class="inner cover">
    
    <p>Silakan baca dan pahami topik bahasan berikut ini dalam waktu <strong class="text-danger">10 menit</strong>.<br>Selanjutnya, Anda akan diminta untuk menjawab beberapa pertanyaan berdasarkan bacaan ini.</p>

    <hr>

    <?php // var_dump($material); ?>

    <h1 class="cover-heading mb-3"><?php echo $material->name; ?></h1>
    
    <p><?php echo ($material->content); ?></p>

    <hr>

    <div class="row mb-5">
      <div class="col">
        <small>Setelah Anda melanjutkan ke halaman berikutnya, Anda tidak dapat kembali lagi ke halaman ini. Namun pada saat Anda nanti diminta untuk membuat peta konsep, Anda diperbolehkan untuk membuat peta konsep sambil membaca bacaan ini.</small>
        <hr>
        <a href="#" id="bt-continue" class="btn btn-lg btn-primary">Continue</a>
      </div>
    </div>
  </main>
</div>

<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>