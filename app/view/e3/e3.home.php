<?php $this->view('base/header.php');?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
    </div>
  </header>

  <main role="main" class="inner cover text-center mx-auto">
    <h1 class="cover-heading">Kit-Build Concept Map</h1>
    <p>Kit-Build Concept Map is a digital tool for supporting a concept map strategy to represent instructor’s
      expectation and assess learners' current understanding as well as for formative assessment and visualizing the
      gaps between the understanding of learners and instructor’s expectation.</p>
    <!-- <hr style="border-color: #ccc">
    <p class="lead" style="font-weight: 500">This is an experimental page of the implementation of Collaborative
      Kit-Build Concept in collaborative context where learners and/or teacher collaborate in concept mapping activities
      with Kit-Build Concept Map Framework.</p> -->
    <hr style="border-color: #ccc">
    <h3 class="text-danger">E3</h3>
    <!-- <p class="lead font-italic text-info" style="font-weight: 500">So, who are you?</p> -->
    <div class="row">
      <!-- <div class="col ml-5">
        <a href="#" id="bt-teacher" class="btn btn-lg btn-primary">I am a teacher</a>
      </div> -->
      <div class="col">
        <a href="#" id="bt-student" class="btn btn-lg btn-primary">Start</a>
      </div>
    </div>
    <p class="mt-3">Please prepare username and password given to you and use any of the following recommended web browser for full compatibility.</p>
    <p>
      <a href="https://www.google.com/chrome/"><img src="<?php echo $this->assets('images/chrome.png'); ?>" style="height: 64px"></a>
      <a href="https://www.microsoft.com/en-us/edge"><img src="<?php echo $this->assets('images/edge.png'); ?>" style="height: 64px"></a>
      <a href="https://www.mozilla.org/en-US/firefox/new/"><img src="<?php echo $this->assets('images/firefox.png'); ?>" style="height: 64px"></a>
    </p>
  </main>

  <footer class="mastfoot mt-auto" style="max-width: 42em;">
    <div class="inner text-center">
      <p>This page is a modified template page from <a href="https://getbootstrap.com/">Bootstrap</a>, by <a
          href="https://twitter.com/mdo">@mdo</a>. Part of the implementation of this system is made beautifully using
        <a href="https://js.cytoscape.org/">Cytoscape.js</a>. <a href="https://opensource.org/licenses/MIT">MIT</a>
        licensed.</p>
    </div>
  </footer>
</div>

<?php $this->view('e2/e2.modal.php');?>
<?php $this->view('general/general.ui.php');?>
<?php $this->view('base/footer.php');?>