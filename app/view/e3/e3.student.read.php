<?php $this->view('base/header.php'); ?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column mt-5" style="max-width: 42em;">
  <main role="main" class="inner cover">
    
    <p>Please read the following passage and try to understand of what are the passage is talking about in <strong class="text-danger">10 minutes</strong>.</p>

    <hr>

    <?php // var_dump($material); ?>

    <h1 class="cover-heading mb-3"><?php echo $material->name; ?></h1>
    
    <p><?php echo $material->content; ?></p>

    <hr>

    <div class="row mb-5">
      <div class="col">
        <p><strong class="text-danger">Warning</strong><br>
        The next step will be a pre test that have some questions for you to answer. When you are answering the questions, you are <strong class="text-danger">not allowed</strong> to read the text or read any notes you have made. Once you leave this page, you cannot return to this page anymore.</p>
        <hr>
        <a href="#" id="bt-continue" class="btn btn-lg btn-primary">Continue</a>
      </div>
    </div>
  </main>
</div>

<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>