<?php $this->view('base/header.php'); ?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
      <!-- <h3 class="masthead-brand">Kit-Build</h3> -->
      <nav class="nav nav-masthead justify-content-center">
        <!-- <a class="btn btn-danger" href="<?php echo $this->location('home/signOut'); ?>">Sign Out</a> -->
        <!-- <a class="nav-link" href="#">Features</a>
        <a class="nav-link" href="#">Contact</a> -->
      </nav>
    </div>
  </header>

  <main role="main" class="inner cover text-center" style="width:48em;">
    <h1 class="h1">Thank You!</h1> 
    <h1 class="cover-heading text-info">ありがとうございました！</h1>
    <p>Thank you for participating in this experiment.<br>Hope you are enjoying the experience in concept mapping using Kit-Build.</p>
    <hr>
    <p>You can now safely close this window. Thank you.</p>
    <a href="<?php echo $this->location('e3/signOut'); ?>" class="btn btn-primary btn-lg">Sign Out</a>
  </main>

  <footer class="mastfoot mt-auto">
    <div class="inner text-center">
      <!-- <p>This page is a modified template page from <a href="https://getbootstrap.com/">Bootstrap</a>, by <a
          href="https://twitter.com/mdo">@mdo</a>. Part of the implementation of this system is made beautifully using <a href="https://js.cytoscape.org/">Cytoscape.js</a>. <a href="https://opensource.org/licenses/MIT">MIT</a> licensed.</p> -->
    </div>
  </footer>
</div>

<?php $this->view('home/home.modal.php'); ?>
<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>