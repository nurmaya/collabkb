<?php $this->view('base/header.php'); ?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
      <nav class="nav nav-masthead justify-content-center"></nav>
    </div>
  </header>

  <main role="main" class="inner cover text-left mx-auto" style="width: 42em;">
    <h1 class="h3">Thank You!</h1> 
    <h1 class="h4 cover-heading text-info">ありがとうございました!</h1>
    <hr>
    <p>Thank you for your effort in constructing the the concept map. The next activity will be the <strong class="text-danger">Post Test session</strong>, where you will be asked again similar questions regarding to the material <strong class="text-danger"><?php echo $material->name; ?></strong>.</p>
    <p class="alert alert-danger"><em>Test ini ditujukan untuk mengukur sejauh mana pemahaman Anda terhadap materi yang telah diberikan setelah Anda melakukan concept mapping/kit-building.<br>Hasil dari pre-test ini <strong class="text-danger">tidak digunakan</strong> sebagai nilai Quiz/Evaluasi mahasiswa untuk matakuliah ini.</em></p>
    <p class="alert alert-info"><em>Rangkaian aktivitas Anda untuk materi ini akan selesai setelah Anda menjawab dan menyelesaikan soal post-test.</em></p>
    <p>You will be given 20 multiple choices questions and 1 essay question related with the topic. You have <strong class="text-danger">15 minutes</strong> to answer all of the questions as quick and as much as possible. During answering the test, You <strong class="text-danger">are not allowed</strong> to use any notes you have made or accessing the internet.</p>
    <p>Any answers you have made will be recorded automatically.</p>
    <?php 
      $nextPage = 'posttest';
    ?>
    <hr>
    <p class="alert alert-danger"><strong class="text-danger">This is a one time test. You can ONLY take the test ONE time.</strong></p>
    <p class="alert alert-warning"><em>Jika Anda sebelumnya telah menyelesaikan soal post test untuk materi ini, silakan klik tombol [<strong>Start Post-Test</strong>] dan lakukan konfirmasi untuk melewati post test dan menyelesaikan rangkaian aktivitas ini.</em></p>
    <div class="form-check"><input type="checkbox" id="check-understand" /> <label for="check-understand">&nbsp; I understand, let me take the test.</label></div>
    <hr>
    <button id="bt-logout" class="btn btn-outline-danger btn-lg" data-next="signOut">Sign Out</button>
    <button id="bt-continue" class="btn btn-primary btn-lg ml-5" data-next="<?php echo $nextPage; ?>">Start Post-Test</button>
    
  </main>

  <footer class="mastfoot mt-auto">
    <div class="inner text-center">
      &nbsp;
    </div>
  </footer>
</div>

<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>