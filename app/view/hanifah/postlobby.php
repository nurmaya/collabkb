<?php $this->view('base/header.php'); ?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
      <nav class="nav nav-masthead justify-content-center"></nav>
    </div>
  </header>

  <main role="main" class="inner cover text-left mx-auto" style="width: 42em;">
    <h1 class="h3">Welcome to Kit-Build!</h1> 
    <h1 class="h4 cover-heading text-info">キットビルドへようこそ!</h1>
    <hr>
    <p>The next activity will be the <strong class="text-danger">pre-test session</strong>, where you will be asked some questions regarding to the material <strong class="text-danger"><?php echo $material->name; ?></strong>.</p> 
    <p class="alert alert-danger"><em>Test ini ditujukan untuk mengukur sejauh mana pemahaman Anda terhadap materi yang telah diberikan Sebelum Anda melakukan concept mapping/kit-building. Hasil dari pre-test ini <strong class="text-danger">tidak digunakan</strong> sebagai nilai Quiz/Evaluasi mahasiswa untuk matakuliah ini.</em></p>
    <p class="alert alert-info"><em>Anda diharapkan <strong>telah membaca dan mencoba memahami</strong> isi dari materi <strong class="text-danger"><?php echo $material->name; ?></strong> yang telah diberikan. Jika Anda belum membaca dan mencoba memahami materi yang telah diberikan, maka silakan Anda <a href="<?php echo $this->location($this->controller->getControllerName() . "/signOut"); ?>">keluar dari sistem ini</a> sebelum Anda melanjutkan ke tahap selanjutnya.</em><br><em>Setelah Anda menyelesaikan soal pre-test, Anda dengan rekan Anda satu grup akan diminta untuk melakukan concept mapping/kit-building terkait materi <strong class="text-danger"><?php echo $material->name; ?></strong> secara bersama-sama.</em></p>
    <p class="alert alert-info"><em>Anda diberi waktu 1x24 jam untuk menyelesaikan seluruh tahapan dalam aktivitas ini. <strong class="text-danger">Batas waktu yang diberikan terhitung mulai dari Anda memulai pre-test.</strong></em></p>
    <p>You will be given 20 multiple choices questions and 1 essay question related with the topic. You have <strong class="text-danger">15 minutes</strong> to answer all of the questions as quick and as much as possible. During answering the test, You <strong class="text-danger">are not allowed</strong> to use any notes you have made or accessing the internet.</p>
    <p>Any answers you have made will be recorded automatically.</p>
    <?php 
      $nextPage = 'posttest';
    ?>
    <hr>
    <p class="alert alert-danger"><strong class="text-danger">This is a one time test. You can ONLY take the test ONE time.</strong></p>
    <p class="alert alert-warning"><em>Jika Anda sebelumnya telah menyelesaikan soal pretest untuk materi ini, silakan klik tombol [<strong>Start Pretest</strong>] dan lakukan konfirmasi untuk melewati proses pretest dan menuju tahap selanjutnya.</em></p>
    <div class="form-check"><input type="checkbox" id="check-understand" /> <label for="check-understand">&nbsp; I understand, let me take the test.</label></div>
    <hr>
    <button id="bt-logout" class="btn btn-outline-danger btn-lg" data-next="signOut">Sign Out</button>
    <button id="bt-continue" class="btn btn-primary btn-lg ml-5" data-next="<?php echo $nextPage; ?>">Start Pre-Test</button>
    
  </main>

  <footer class="mastfoot mt-auto">
    <div class="inner text-center">
      &nbsp;
    </div>
  </footer>
</div>

<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>