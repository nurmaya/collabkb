<?php $this->view('base/header.php');?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
      <!-- <h3 class="masthead-brand">Kit-Build</h3>
      <nav class="nav nav-masthead justify-content-center">

      </nav> -->
    </div>
  </header>

  <main role="main" class="inner cover text-center mx-auto" style="max-width: 42em;">
    <h1 class="cover-heading">Kit-Build Concept Map</h1>
    <p>Kit-Build Concept Map is a digital tool for supporting a concept map strategy to represent instructor’s
      expectation and assess learners' current understanding as well as for formative assessment and visualizing the
      gaps between the understanding of learners and instructor’s expectation.</p>
    <hr style="border-color: #ccc">
    <div class="row">
      <div class="col">
        <h3 class="text-danger">E1</h3>
        <a href="#" id="bt-student" class="btn btn-lg btn-primary">Begin</a>
      </div>
    </div>
    <p class="mt-3">Please prepare username and password given to you and use any of the following recommended web browser for full compatibility.</p>
    <p>
      <a href="https://www.google.com/chrome/"><img src="<?php echo $this->assets('images/chrome.png'); ?>" style="height: 64px"></a>
      <a href="https://www.microsoft.com/en-us/edge"><img src="<?php echo $this->assets('images/edge.png'); ?>" style="height: 64px"></a>
      <a href="https://www.mozilla.org/en-US/firefox/new/"><img src="<?php echo $this->assets('images/firefox.png'); ?>" style="height: 64px"></a>
    </p>
  </main>

  <footer class="mastfoot mt-auto" style="max-width: 42em;">
    <div class="inner text-center">
      <p>This page is a modified template page from <a href="https://getbootstrap.com/">Bootstrap</a>, by <a
          href="https://twitter.com/mdo">@mdo</a>. Part of the implementation of this system is made beautifully using
        <a href="https://js.cytoscape.org/">Cytoscape.js</a>. <a href="https://opensource.org/licenses/MIT">MIT</a>
        licensed.</p>
    </div>
  </footer>
</div>

<?php $this->view('e1/e1.modal.php');?>
<?php $this->view('general/general.ui.php');?>
<?php $this->view('base/footer.php');?>