<?php $this->view('base/header.php');?>

<div id="main-container">
  <div id="app-toolbar">

    <div class="separator"></div>

    <div class="btn-group">
      <button id="bt-show-material" data-tippy-content="Show Material" class="btn btn-sm btn-outline-primary"
        disabled><i class="fas fa-eye"></i> <span class="d-none d-sm-inline-block">Material</span></button>
    </div>

    <div class="btn-group">
      <button id="bt-init" class="btn btn-sm btn-outline-danger" data-tippy-content="Initialize map">
        <i class="fas fa-undo"></i> <span class="d-none d-sm-inline-block">Initialize Map</span>
      </button>
    </div>

    <div class="btn-group">
      <button id="bt-sync" class="btn btn-sm btn-outline-primary" data-tippy-content="Synchronize map" disabled>
        <i class="fas fa-sync"></i> <span class="d-none d-sm-inline-block">Sync</span>
      </button>
    </div>

    <div class="btn-group">
      <button id="bt-save" class="btn btn-sm btn-outline-info" data-tippy-content="Save map" disabled>
        <i class="fas fa-save"></i> <span class="d-none d-sm-inline-block">Save</span>
      </button>
      <button id="bt-load-draft" class="btn btn-sm btn-outline-info" data-tippy-content="Load draft map" disabled>
        <i class="fas fa-upload"></i>
      </button>
    </div>
    <div class="btn-group">
      <button id="bt-finalize" class="btn btn-sm btn-danger" data-tippy-content="Finalize map" disabled>
        <i class="fas fa-save"></i> Finalize
      </button>
    </div>

  </div>

  <!-- Material Modal -->
  <div class="modal" id="modal-kit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <h5>Open Material</h4>
            <hr>
            <div class="row">
              <div class="col">
                <h6>Material</h6>
                <hr>
                <div class="material-list list-container"></div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-primary bt-dialog bt-open">Open</button>
          <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-cancel"
            data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
  <!-- /Material Modal -->

  <!-- Map name Modal -->
  <div class="modal" id="modal-map-name" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <div class="form-row pr-2 pl-2">
            <label>Please give your map a name</label>
            <input type="text" class="form-control map-name-input">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok pl-5 pr-5">OK</button>
          <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-cancel pl-5 pr-5"
            data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
  <!-- /Map name Modal -->

  <?php $this->view('kbui/kbui.canvas.php');?>
  <?php $this->view('chat/chat.window.php');?>
  
  <?php $user = (object) $_SESSION['user']; 
    if(in_array('E1A', $user->grups)) :
  ?>
  <div style="position: absolute; top: 7em; left: 3em; cursor:pointer;">
    <input type="checkbox" id="show-excessive-links" style="cursor:pointer;" checked>
    <label for="show-excessive-links" style="cursor:pointer;"><small>Show pair propositions</small></label>
  </div>
  <?php endif; ?>

  <div id="popup-material" class="popup" style="display: none">
    <div class="row justify-content-between pl-3 pr-3">
      <span class="material-title h4"></span>
      <span class="text-secondary align-items-center">
        <input id="popup-material-check-open" type="checkbox" class="check-open mr-2">
        <label for="popup-material-check-open"><small><i class="fas fa-eye-slash"></i> Keep open </small></label>
        <span class="bt-close badge badge-danger ml-3">
          <i class="fas fa-times"></i> Close
        </span>
      </span>
    </div>
    <hr>
    <div class="material-content"></div>
    <button class="btn btn-sm btn-primary mt-3"><i class="fas fa-chevron-down"></i> More</button>
  </div>

  <div id="popup-map" class="popup p-3" style="display: none; min-width: 500px; background: #fff">
    <div class="row justify-content-between pl-3 pr-3">
      <span class="material-title">Concept Map</span>
      <button class="bt-cancel btn btn-danger btn-sm"><i class="fas fa-times"></i> Cancel Finalize</button>
    </div>
    <hr>
    <div class="text-center">
      <img class="cmap" style="margin: inherit auto; max-width: 800px; max-height:400px;">
    </div>
    <hr>
    <div class="row justify-content-between align-items-center p-3">
      <button class="bt-download btn btn-secondary mt-3"><i class="fas fa-save"></i> Download as image</button>
      <button class="bt-continue btn btn-primary mt-3 ml-3">OK, Continue Finalize &nbsp; <i
          class="fas fa-chevron-right"></i></button>
    </div>
  </div>

</div> <!-- /Main Container -->

<?php $this->view('general/general.ui.php');?>
<?php $this->view('base/footer.php');?>
<script>
  $(document).mouseup(function (e) {
    var container = $("#popup-material");
    var button = $('#bt-show-material')
    if ($('#popup-material .check-open').prop('checked')) return;
    if (!container.is(e.target) && container.has(e.target).length === 0 &&
      !button.is(e.target) && button.has(e.target).length === 0) {
      if (container.is(':visible')) {
        container.fadeOut();
        BRIDGE.kitbuild.eventListeners.forEach(listener => {
          if (listener && typeof listener.onAppEvent == 'function') {
            listener.onAppEvent('hide-material');
          }
        })
      }
    }
  });
</script>