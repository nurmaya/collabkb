<?php $this->view('base/header.php'); ?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
      <!-- <h3 class="masthead-brand">Kit-Build</h3> -->
      <nav class="nav nav-masthead justify-content-center">
        <!-- <a class="btn btn-danger" href="<?php echo $this->location('home/signOut'); ?>">Sign Out</a> -->
        <!-- <a class="nav-link" href="#">Features</a>
        <a class="nav-link" href="#">Contact</a> -->
      </nav>
    </div>
  </header>

  <main role="main" class="inner cover text-left mx-auto">
    <div class="mx-auto" style="width: 42em;">
      <h1 class="h3">Thank You!</h1>
      <h1 class="h4 cover-heading text-info">ありがとうございました！</h1>
      <hr>
      <p>Terima kasih Anda telah mencoba memahami peta konsep yang dibuat oleh rekan Anda.<br>Berikut ini adalah overlay
        map yang memperlihatkan peta konsep yang telah Anda dan rekan Anda buat.</p>
    </div>
    <div id="panel-container" class="row m-0 mb-5" style="flex: 1; height: 1000px;">
      <div class="pl-0 mx-auto" style="flex: 1; width: 10px; display: flex;">
        <?php $this->view('kbui/kbui.canvas.php'); ?>
      </div>
    </div> <!-- /panel-container -->
    <div class="mx-auto" style="width: 42em;">
      <p>Selanjutnya Anda diminta untuk membangun sebuah peta konsep <strong>berkolaborasi dengan rekan Anda</strong>
        dalam satu canvas yang sama secara real-time. Komponen peta konsep (concept dan link) yang telah Anda buat
        sebelumnya akan diberikan untuk memudahkan Anda dalam membuat peta konsep yang menggambarkan pemahaman Anda
        terkait topik yang sedang dipelajari.</p>
      <p>Anda diberi waktu <strong class="text-danger">30 menit</strong> untuk membangun peta konsep bersama rekan Anda.
        Selama membangun peta konsep, Anda diperbolehkan untuk melihat dan membaca materi yang diberikan.</p>
      <hr>
      <p>Tunggu instruksi yang diberikan sebelum Anda melanjutkan ke tahap selanjutnya.</p>
      <button id="bt-continue" class="btn btn-primary btn-lg" data-next="<?php echo $nextPage; ?>">Let's Go!</button>
    </div>
  </main>

  <footer class="mastfoot mt-auto">
    <div class="inner text-center">
      &nbsp;
    </div>
  </footer>
</div>

<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>