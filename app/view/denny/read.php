<?php $this->view('base/header.php'); ?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column mt-5" style="max-width: 42em;">
  <main role="main" class="inner cover">

    <div id="remaining" class="h4 text-danger" style="position: fixed; top: 1rem; left: 1rem"></div>    
      
    <hr>

    <h1 class="cover-heading mb-3"><?php echo $material->name; ?></h1>
    <div class="content">
      <?php echo $material->content; ?>
    </div>

    <hr>

    <div class="row mb-5 alert alert-warning">
      <div class="col">
        <strong class="text-danger">Warning</strong><br>
        Setelah Anda melanjutkan ke tahap selanjutnya, Anda tidak dapat kembali ke halaman ini lagi.
        <hr>
        <button id="bt-continue" class="btn btn-lg btn-primary">Continue</button>
      </div>
    </div>
  </main>
</div>

<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>