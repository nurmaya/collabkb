<?php $this->view('base/header.php'); ?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
      <nav class="nav nav-masthead justify-content-center">
      </nav>
    </div>
  </header>

  <main role="main" class="inner cover text-center" style="width:48em;">
    <h1 class="h1">Thank You!</h1> 
    <h1 class="cover-heading text-info">ありがとうございました！</h1>
    <p>Thank you for answers.<br>Your effort will greatly help you and your teacher's towards a better learning process.</p>
    <hr>
    <p>You can now safely close this window. Thank you.</p>
    <a href="<?php echo $this->location('denny'); ?>" class="btn btn-primary btn-lg">Finish</a>
  </main>

  <footer class="mastfoot mt-auto">
    <div class="inner text-center">
      
    </div>
  </footer>
</div>

<?php $this->view('home/home.modal.php'); ?>
<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>