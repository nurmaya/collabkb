<?php $this->view('base/header.php'); ?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
      <?php // var_dump($room); ?>
      <h1 class="text-center">Kit-Build &rsaquo; Lobby</h1>
    </div>
  </header>

  <main role="main" class="inner cover text-center justify-content-center">
    <h1 class="text-center"><small class="text-info">Material</small></h1>
    <div id="material-list" class="mx-auto" style="width: 30em;">
      <?php if(count($materials)) {
        foreach($materials as $m) {
          echo '<div data-mid="'.$m->mid.'" class="row list pt-2 pb-2 pl-5 pr-5 text-center d-flex align-items-center justify-content-between mx-auto list-hover list-pointer"><span class="material-name">'.$m->name.'</span><i class="fas fa-check text-success" style="display:none"></i></div>';
        }
      } ?>
    </div>
    <hr>
    <h1 class="text-center"><small class="text-info">Kit</small></h1>
    <div id="kit-list" class="mb-5 mx-auto" style="width: 30em;"></div>
    <button id="bt-logout" class="btn btn-lg btn-outline-danger mr-5 mt-5">Sign Out</button>
    <button id="bt-continue" class="btn btn-lg btn-primary mt-5">Continue</button>
  </main>

  <footer class="mastfoot mt-auto">
    <div class="inner text-center">
      &nbsp;
    </div>
  </footer>
</div>

<?php $this->view('home/home.modal.php'); ?>
<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>