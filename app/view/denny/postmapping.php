<?php $this->view('base/header.php'); ?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
      <nav class="nav nav-masthead justify-content-center"></nav>
    </div>
  </header>

  <main role="main" class="inner cover text-left mx-auto" style="width: 42em;">
    <h1 class="h3">Thank You!</h1> 
    <hr>
    <p>Terima kasih atas pembuatan peta konsep yang telah Anda lakukan. Aktivitas selanjutnya adalah <strong class="text-danger">sesi Post Test</strong>. Anda akan diberikan pertanyaan yang terkait dengan materi <strong class="text-danger"><?php echo $material->name; ?></strong>.</p>
    <p class="alert alert-info"><em>Test ini ditujukan untuk mengukur sejauh mana pemahaman Anda terhadap materi yang telah diberikan setelah Anda melakukan concept mapping/kit-building.<br>Hasil dari post-test ini <strong class="text-danger">TIDAK digunakan</strong> sebagai nilai Quiz/Evaluasi mahasiswa untuk matakuliah ini.</em></p>
    <p>Anda diberi waktu <strong class="text-danger">10 menit</strong> untuk menjawab seluruh pertanyaan yang diberikan. Selama mengerjakan soal, Anda <strong class="text-danger">TIDAK DIPERBOLEHKAN</strong> untuk melihat catatan dalam bentuk apapun. Jawaban yang telah Anda berikan akan disimpan secara otomatis oleh sistem.</p>
    <?php 
      $nextPage = 'posttest';
    ?>
    <hr>
    <p class="alert alert-danger"><strong class="text-danger">Anda hanya dapat melakukan post test SATU kali.</strong></p>
    <p class="alert alert-warning"><em>Jika Anda sebelumnya telah menyelesaikan soal post test untuk materi ini, silakan klik tombol [<strong>Start Post-Test</strong>] dan lakukan konfirmasi untuk melewati post test dan menyelesaikan rangkaian aktivitas ini.</em></p>
    <p class="alert alert-info">Rangkaian aktivitas Anda untuk materi ini akan selesai setelah Anda menjawab dan menyelesaikan soal post-test.</p>
    <div class="form-check"><input type="checkbox" id="check-understand" /> <label for="check-understand">&nbsp; Saya mengerti, biarkan saya mengambil post test.</label></div>
    <hr>
    <button id="bt-logout" class="btn btn-outline-danger btn-lg" data-next="signOut">Sign Out</button>
    <button id="bt-continue" class="btn btn-primary btn-lg ml-5" data-next="<?php echo $nextPage; ?>">Start Post-Test</button>
    
  </main>

  <footer class="mastfoot mt-auto">
    <div class="inner text-center">
      &nbsp;
    </div>
  </footer>
</div>

<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>