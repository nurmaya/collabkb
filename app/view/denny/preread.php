<?php $this->view('base/header.php'); ?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
      <nav class="nav nav-masthead justify-content-center"></nav>
    </div>
  </header>

  <main role="main" class="inner cover text-left mx-auto" style="width: 42em;">
    <h1 class="h3">Instruksi</h1> 
    <hr>
    <p>Langkah selanjutnya Anda akan diminta untuk membaca materi terkait <strong class="text-danger"><?php echo $material->name; ?></strong>. Setelah Anda selesai membaca, Anda akan diberi instruksi terkait pre-test dan bagaimana Anda menyelesaikan soal pre-test yang akan diberikan.</p> 
    <p class="alert alert-info">Soal pretest yang diberikan akan terkait dengan materi yang akan Anda baca.</p>
    <?php 
      $nextPage = 'read';
    ?>
    <hr>
    <p class="alert alert-danger"><strong class="text-danger">Anda diberi kesempatan untuk membaca materi <?php echo $material->name; ?> yang akan diberikan ini selama 10 menit.</strong></p>
    <hr>
    <button id="bt-logout" class="btn btn-outline-danger btn-lg" data-next="signOut">Sign Out</button>
    <button id="bt-continue" class="btn btn-primary btn-lg ml-5" data-next="<?php echo $nextPage; ?>">Start Reading &rsaquo;</button>
    
  </main>

  <footer class="mastfoot mt-auto">
    <div class="inner text-center">
      &nbsp;
    </div>
  </footer>
</div>

<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>