<?php $this->view('base/header.php'); ?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
      <?php // var_dump($room); ?>
      <h3 class="text-center">Kit-Build &rsaquo; Pre-mapping Lobby</h3>
    </div>
  </header>



  <main role="main" class="inner cover text-center justify-content-center">
    <hr>
    <h2>Instruksi</h2>
    <p class="text-left">Di tahap selanjutnya adalah Anda diminta untuk membuat peta konsep (kit-building) berdasarkan
      materi/topik <strong class="text-danger"><?php echo $material->name; ?></strong>.</p>

    <?php if(in_array($_SESSION['activity'], array('rcmapkit'))) : ?>
      <p class="text-left">Pertemuan kali ini menggunakan metode <strong>reciprocal</strong> dimana Anda bersama rekan Anda akan bekerja sama untuk membuat satu peta konsep yang merupakan <strong>gabungan</strong> dari peta konsep yang sebelumnya dibuat oleh masing-masing anggota kelompok secara individu.</p>
      <p class="text-left">Oleh karena peta konsep yang akan dibuat kali ini merupakan gabungan dari masing-masing anggota kelompok, dimungkinkan adanya duplikasi proposisi, ataupun adanya proposisi-proposisi yang mungkin memiliki pemahaman yang berbeda antara anggota kelompok. Oleh karena itu, tugas evaluasi kali ini adalah menyempurnakan peta konsep gabungan yang diberikan menjadi <strong>satu peta konsep yang disepakati oleh seluruh anggota kelompok</strong>.</p>
      <p class="text-left"><strong class="text-danger">Silakan diskusikan dan sepakati secara bersama-sama, ubah proposisi yang tidak sesuai, hapus proposisi yang tidak diperlukan, dan tambahkan proposisi lain yang dirasa perlu</strong>.  Penyempurnaan peta konsep dalam aktivitas ini dilakukan sedemikian rupa sehingga setiap proposisi yang terdapat dalam peta konsep ini akan menggambarkan relasi yang "masuk akal" dan "benar" antara dua buah concept dan menggambarkan pemahaman seluruh anggota kelompok berdasarkan materi yang diberikan.</p>
      <p class="alert alert-danger">Anda bersama rekan kelompok Anda diberi waktu <strong>maksimal 120 menit</strong> untuk berdiskusi secara bersama-sama dalam membangun peta konsep. Waktu dihitung mulai Anda dan rekan Anda memulai aktivitas pembuatan peta konsep, yaitu setelah Anda melanjutkan proses menuju ke halaman selanjutnya.</p>
    <?php else: ?>
      <p class="text-left">Anda akan diberi sebagian kit peta konsep (hanya komponen concept yang disediakan) dari peta
      konsep yang dibuat oleh dosen. Anda diminta untuk membuat dan merangkai kembali peta konsep dengan menggunakan kit
      yang disediakan, namun Anda diberi kebebasan untuk menentukan sendiri linking-words (link) yang digunakan untuk
      membentuk proposisi antara dua concept. Linking words (link) dapat Anda buat berdasarkan pemahaman Anda terkait
      materi yang diberikan. Dimana setiap link yang Anda buat dan Anda hubungkan dengan concept akan menggambarkan
      relasi yang "masuk akal" dan "benar" antara dua concept berdasarkan materi yang diberikan.</p>
    <?php endif; ?>

    <h4 class="m-3"><strong class="text-danger">Bagaimana peta konsep Anda akan dinilai?</strong></h4>
    <p class="text-left">Pemahaman Anda akan dinilai berdasarkan kebenaran dan setiap proposisi (concept-link-concept)
      dari peta konsep yang Anda buat. Banyak proposisi yang Anda buat dalam sebuah peta konsep, akan menggambarkan
      seberapa dalam pemahaman Anda terhadap materi yang diberikan. Nilai peta konsep diberikan atas banyaknya proposisi
      (jumlah) yang dibuat serta tingkat validitas "kebenaran" (kualitas) dari proposisi yang dibuat.</p>

    <?php if(in_array($_SESSION['activity'], array('cmap','kb','cmapkit','rcmapkit'))) { ?>
    <p class="text-left">Selama membangun peta konsep, percakapan Anda akan dicatat oleh sistem. Diskusi dan kontribusi
      dari anggota kelompok akan sangat menentukan kesesuaian peta konsep yang dibangun serta akan mempengaruhi sejauh
      mana pemahaman Anda terkait materi yang diberikan. Oleh karena itu, <strong class="text-danger">kontribusi
        aktivitas setiap anggota kelompok dalam membangun peta konsep, percakapan yang dilakukan, dan diskusi antar
        anggota kelompok akan dinilai</strong>.</p>
    <p class="text-left text-danger">Lakukan diskusi pada Channel Discuss Concept/Link untuk mendiskusikan hal yang terkait concept/link tersebut dan dan lakukan diskusi pada kolom Message untuk berdiskusi terkait hal umum lainnya.</p>
    <hr>
    <h1 class="text-center"><small class="text-info"><?php echo $room->name; ?></small></h1>
    <p class="lead" style="font-weight: 500">Room participants:</p>
    <div id="participant-list" class="mx-auto"></div>
    <hr>
    <p>Silakan tunggu anggota kelompok Anda yang lain untuk bergabung dalam room yang sama. Jika rekan Anda telah
      bergabung dalam room, halaman ini akan diupdate secara otomatis. Atau klik tombol [<strong>Refresh</strong>] untuk
      memeriksa secara manual.</p>
    <?php } ?>
    <div class="mb-5">
      <button id="bt-logout" class="btn btn-lg btn-outline-danger mt-2">Sign Out</button>
      <?php if(in_array($_SESSION['activity'], array('cmap','kb','cmapkit'))) { ?>
      <button id="bt-refresh" class="btn btn-lg btn-outline-info mt-2">Refresh</button>
      <?php } ?>
      <button id="bt-continue" class="btn btn-lg btn-primary ml-5 mt-2"
        data-next="<?php echo $_SESSION['activity']; ?>">Continue</button>
    </div>
    <?php 
      if(!in_array($_SESSION['activity'], array('icmap', 'ikb', 'icmapkit'))) { 
        $this->view('chat/chat.window.php');
      }
    ?>
  </main>
  <footer class="mastfoot mt-auto">
    <div class="inner text-center">
      &nbsp;
    </div>
  </footer>
</div>

<?php $this->view('home/home.modal.php'); ?>
<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>