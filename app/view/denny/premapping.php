<?php $this->view('base/header.php'); ?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
      <?php // var_dump($room); ?>
      <h1 class="text-center">Kit-Build &rsaquo; Pre-mapping Lobby</h1>
    </div>
  </header>

  

  <main role="main" class="inner cover text-center justify-content-center">
    <hr>
    <h3>Instruksi</h3>
    <p class="text-left">Di tahap selanjutnya adalah Anda diminta untuk membuat peta konsep (kit-building) dengan menggunakan kit (komponen peta konsep) berdasarkan materi/topik yang telah ditentukan.</p>
    <p class="text-left">Anda akan diberi kit peta konsep (link dan concept) dari peta konsep yang dibuat oleh dosen. Anda diminta untuk merangkai kembali peta konsep dengan menggunakan kit tersebut berdasarkan pemahaman Anda terkait materi yang diberikan.</p>
    <p class="alert alert-warning">Peta konsep yang Anda buat sedapat mungkin mencakup seluruh pembahasan materi yang diberikan serta menggambarkan pemahaman Anda terhadap materi. Kesesuaian materi dengan peta konsep yang Anda buat akan membantu Anda dalam memahami materi yang diberikan serta membantun Anda dalam menjawab post test yang akan menguji tingkat pemahaman Anda terhadap materi ini.</p>
    <h4 class="m-5"><strong class="text-danger">Bagaimana peta konsep Anda akan dinilai?</strong></h4>
    <p class="text-left">Pemahaman Anda akan dinilai berdasarkan kesamaan proposisi peta konsep yang Anda rangkai dengan proposisi yang ada dalam peta konsep dosen. Selain itu, jika terdapat perbedaan dalam peta konsep Anda dengan peta konsep dosen, maka perbedaan proposisi yang ada akan dinilai berdasarkan kebenaran dari tiap proposisi dalam peta konsep yang Anda buat.</p>

    <p class="alert alert-warning">Anda diberi waktu <strong>maksimal 120 menit</strong> untuk menyelesaikan peta konsep. Waktu mulai dihitung dari memulai akses ke halaman selanjutnya. Waktu yang tersisa untuk membuat peta konsep akan terus berjalan walaupun Anda keluar/logout dari sistem di tengah-tengah pembuatan peta konsep.</p>

    <p class="alert alert-danger">Anda <strong class="text-danger">WAJIB</strong> untuk melakukan <strong>finalisasi</strong> setelah Anda selesai merangkai peta konsep. Jika tidak, maka Anda tidak dapat melanjutkan sesi concept mapping secara kolaboratif di UAS tahap ke-2 yang telah dijadwalkan untuk dilaksanakan esok hari.</p>

    <?php if($_SESSION['activity'] == 'cmap' || $_SESSION['activity'] == 'kb') { ?>
    <p class="text-left">Selama membangun peta konsep, percakapan Anda akan dicatat oleh sistem. Diskusi dan kontribusi dari anggota kelompok akan sangat menentukan kesesuaian peta konsep yang dibangun serta akan mempengaruhi sejauh mana pemahaman Anda terkait materi yang diberikan. Oleh karena itu, <strong class="text-danger">kontribusi aktivitas setiap anggota kelompok dalam membangun peta konsep, percakapan yang dilakukan, dan diskusi antar anggota kelompok akan dinilai</strong>.</p>
    <hr>
    <h1 class="text-center"><small class="text-info"><?php echo $room->name; ?></small></h1>
    <p class="lead" style="font-weight: 500">Room participants:</p>
    <div id="participant-list" class="mx-auto"></div>
    <hr>
    <p>Silakan tunggu anggota kelompok Anda yang lain untuk bergabung dalam room yang sama. Jika rekan Anda telah bergabung dalam room, halaman ini akan diupdate secara otomatis. Atau klik tombol [<strong>Refresh</strong>] untuk memeriksa secara manual.</p>
    <button id="bt-logout" class="btn btn-lg btn-outline-danger mt-5">Sign Out</button>
    <button id="bt-refresh" class="btn btn-lg btn-outline-info mt-5">Refresh</button>
    <?php } else { ?>
    <button id="bt-logout" class="btn btn-lg btn-outline-danger mt-5">Sign Out</button>
    <?php } ?>
    <button id="bt-continue" class="btn btn-lg btn-primary ml-5 mt-5"
      data-next="<?php echo $_SESSION['activity']; ?>">Continue</button>
    <?php 
    if($_SESSION['activity'] != 'icmap' && $_SESSION['activity'] != 'ikb') { 
      $this->view('chat/chat.window.php');
    }
    ?>
  </main>

  <footer class="mastfoot mt-auto">
    <div class="inner text-center">
      &nbsp;
    </div>
  </footer>
</div>

<?php $this->view('home/home.modal.php'); ?>
<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>