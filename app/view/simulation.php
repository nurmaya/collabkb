<?php $this->view('base/header.php'); ?>

<div id="main-container">
  <div id="app-toolbar">
    <div class="row justify-content-center">
    <div class="input-group input-group-sm col-sm-8">
      <select id="select-gmid" class="form-control-sm">
<?php if($goalmapIds): ?>
<?php foreach($goalmapIds as $gm): ?>
        <option value="<?php echo $gm->gmid; ?>"><?php echo $gm->name; ?></option>
<?php endforeach; ?>
<?php endif; ?>
      </select>
      <select id="select-uid" class="form-control-sm"></select>
      <!-- <div class="separator"></div> -->
      <!-- <input id="simulation-delay" class="form-control-sm col-sm-1" type="text" value="100" /> -->
      <!-- <div class="col-sm-3"> -->
      <!-- <div class="input-group-sm"> -->
        <input id="simulation-delay" value="200" type="text" class="form-control" placeholder="Delay" aria-label="Delay" aria-describedby="basic-addon1">
        <div class="input-group-append">
          <span class="input-group-text" id="basic-addon1">ms</span>
        </div>
        <input id="simulation-break" value="0" type="text" class="form-control" placeholder="Delay" aria-label="Delay" aria-describedby="basic-addon1">
      <!-- </div> -->
      <!-- </div> -->
      <button id="bt-simulate" class="btn btn-sm btn-outline-primary">Simulate</button>
      <button id="bt-step" class="btn btn-sm btn-outline-primary">Step</button>
    </div>
    </div>
    <div class="row justify-content-center">
      <div class="col-sm-6">
        <div class="progress">
          <div id="simulation-progress-bar" class="progress-bar" style="width: 0%" aria-valuemin="" aria-valuemax=""
            aria-valuenow="">0%</div>
        </div>
      </div>
    </div>
  </div>
  <?php $this->view('kbui/kbui.canvas.php'); ?>
</div>

<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>