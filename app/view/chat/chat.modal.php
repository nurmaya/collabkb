<!-- Modal Login -->
<div class="modal" id="modal-chat-login" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form">
          <div class="form-group row">
            <label for="inputEmail3" class="col-sm-4 col-form-label text-right">Username</label>
            <div class="col-sm-6">
              <input type="email" class="form-control input-username" placeholder="Username">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputPassword3" class="col-sm-4 col-form-label text-right">Password</label>
            <div class="col-sm-6">
              <input type="password" class="form-control input-password" placeholder="Password">
            </div>
          </div>
        </div>
        <div class="text-sm-center font-italic text-secondary"><span class="modal-helper-text text-sm">Enter username
            and password.</span></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok">&nbsp; &nbsp; Sign-In &nbsp;
          &nbsp;</button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-close" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Create Group -->
<div class="modal" id="modal-create-room" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Create New Room</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-inline">
          <div class="form-group mb-2">
            <span class="modal-input-label">Room Name</span>
          </div>
          <div class="form-group mx-sm-3 mb-2" style="position:relative">
            <input type="text" class="form-control" id="input-room-name" style="width:300px;" required>
            <div class="invalid-tooltip">
              Please provide a room name.
            </div>
          </div>
        </div>
        <small class="modal-helper-text">Enter a room name. Room name cannot empty.</small>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok">&nbsp; &nbsp; OK &nbsp; &nbsp;</button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-close" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- <div class="modal" id="modal-channel" tabindex="-1" role="dialog" aria-hidden="true"> -->
  <!-- <div id="modal-channel" class="modal-dialog modal-dialog-centered" role="document"> -->
  <div id="modal-channel" class="channel-dialog" role="document" style="display: none">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title h6">Channel: <span class="channel-name"></span></h5>
        <button type="button" class="bt-close close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="channel-container">
          <div class="channel-message-container"></div>
        </div>
        <div class="channel-message-input-container">
          <input type="text" class="form-control form-control-sm channel-message-input" placeholder="Enter message...">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="bt-close btn btn-sm btn-secondary bt-dialog" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
<!-- </div> -->