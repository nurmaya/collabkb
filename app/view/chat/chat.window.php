<div id="chat">

  <div id="chat-toolbar" class="btn-group">
    <button id="bt-error" data-toggle="tooltip" class="btn btn-sm btn-outline-secondary">
      <i class="fas fa-check"></i>
    </button>
    <button id="bt-room" class="btn btn-sm btn-outline-primary">Room</button>
    <button id="bt-channel" class="btn btn-sm btn-outline-primary">Channel</button>
    <button id="bt-message" class="btn btn-sm btn-outline-info">Message</button>
  </div>


  <div id="popup-chat" class="popup" style="display: none">
    <div id="popup-chat-header">
      <span id="room-name"></span>
      <span class="badge badge-danger" id="bt-close-popup-chat"><i class="fas fa-times"></i></span>
    </div>
    <div id="chat-container">
      <div id="chat-message-container"></div>
    </div>
    <div id="chat-message-input-container">
      <input type="text" class="form-control form-control-sm" id="chat-message-input" placeholder="Enter message...">
    </div>
  </div>

  <div id="popup-rooms" class="popup" style="display: none">
    <div style="margin: 0.5em 1em; font-size:smaller"><strong>Room List</strong></div>
    <div id="room-list"></div>
    <hr style="margin: 0.5em 0">
    <div style="padding: 0 0.5em">
      <!-- <button id="bt-create-room" class="btn btn-sm btn-outline-primary">Create Room</button> -->
      <button id="bt-leave-room" class="btn btn-sm btn-outline-secondary" disabled>Leave Room</button>
    </div>
  </div>

  <div id="popup-users" class="popup" style="display: none">
    <div style="margin: 0.5em 1em; font-size:smaller"><strong class="text-info">Users</strong></div>
    <div id="user-list" class="mb-2"></div>
  </div>

  <div id="popup-channel" class="popup" style="display: none; max-width: 230px;">
    <div style="margin: 0.5em 1em; font-size:smaller"><strong class="text-info">Discussion Channel</strong></div>
    <div id="channel-list" class="list-container" style="overflow:scroll"></div>
  </div>

  <?php $this->view('chat/chat.modal.php'); ?>

</div>