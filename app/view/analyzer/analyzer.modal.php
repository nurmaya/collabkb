<!-- Material Modal -->
<div class="modal" id="modal-kit" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h5>Open Kit</h5>
          <hr>
          <div class="row">
            <div class="col">
              <h6>Material</h6>
              <hr>
              <div class="material-list list-container"></div>
            </div>
            <div class="col">
              <h6>Kit</h6>
              <hr>
              <div class="goalmap-list list-container"></div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-open">Open</button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-cancel" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- /Material Modal -->