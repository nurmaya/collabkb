<div id="analyzer-sidebar-container" style="padding:0.5em ;">
  <div class="text-secondary" style="margin-bottom:0.5em;display: flex; justify-content: space-between;">
    <div class="title">
      <i class="fas fa-user"></i> + <i class="fas fa-users"></i>
      Groups Map
    </div>
    <div class="control">
      <div id="bt-sidebar-show-hide" class="bt" style="padding: 0 0.5em;"><i class="fas fa-sort"></i></div>
    </div>
  </div>
  <div id="sidebar-content">
    <!-- <div class="form-check form-control-sm">
    <input type="checkbox" class="form-check-input" id="preset-layout" checked>
    <label class="form-check-label" for="preset-layout">Use preset positioning</label>
  </div> -->
    <div class="custom-control custom-switch form-control-sm">
      <input type="checkbox" class="custom-control-input" id="preset-layout" checked>
      <label class="custom-control-label" for="preset-layout">Use preset positioning</label>
    </div>
    <div class="custom-control custom-switch form-control-sm">
      <input type="checkbox" class="custom-control-input" id="bt-matching-links" checked>
      <label class="custom-control-label" for="bt-matching-links">Matching links</label>
    </div>
    <div class="custom-control custom-switch form-control-sm">
      <input type="checkbox" class="custom-control-input" id="bt-excessive-links" checked>
      <label class="custom-control-label" for="bt-excessive-links">Excessive links</label>
    </div>
    <div class="custom-control custom-switch form-control-sm">
      <input type="checkbox" class="custom-control-input" id="bt-missing-links" checked>
      <label class="custom-control-label" for="bt-missing-links">Missing (Lacking) links</label>
    </div>
    <div class="custom-control custom-switch form-control-sm">
      <input type="checkbox" class="custom-control-input" id="bt-leaving-links" checked>
      <label class="custom-control-label" for="bt-leaving-links">Leaving links</label>
    </div>
    <!-- <div class="form-check form-control-sm">
    <input type="checkbox" class="form-check-input" id="lack-match" checked>
    <label class="form-check-label" for="preset-layout">Show lacking/matching links</label>
  </div> -->

    <div class="btn-group btn-group-sm mt-2 mb-1">
      <button id="bt-show-teacher-map" class="btn btn-outline-primary btn-sm">Teacher Map</button>  
      <button id="bt-show-groups-map" class="btn btn-outline-primary btn-sm">Groups Map</button>
      <button class="btn btn-sm btn-outline-secondary" disabled>Range: </button>
    </div>
    <div style="display: flex; flex-direction: row"><input data-id="min-range" type="range" min="1" max="1" value="1"
        step="1" style="flex-grow: 1;"><span class="min-val" style="width: 35px;text-align: center;">1</span></div>
    <div style="display: flex; flex-direction: row"><input data-id="max-range" type="range" min="1" max="1" value="1"
        step="1" style="flex-grow: 1;"><span class="max-val" style="width: 35px;text-align: center;">1</span></div>
    <div>
      <hr>
    </div>
    <div class="text-secondary"><i class="fas fa-user-friends"></i> Learners Maps</div>
    <div id="learner-maps-heading"
      style="display:flex; justify-content:space-between; border-bottom: 1px solid #cccccc">
      <span><small>Name</small></span>
      <span><small>Score</small></span>
    </div>
    <div id="learner-maps-container">
      <div id="learner-maps-list-container" class="container" style="max-height: 300px; overflow-y:scroll"></div>
    </div>
  </div>
</div>