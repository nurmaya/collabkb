<!-- Modal Edit Goalmap -->
<div class="modal" id="modal-edit-goalmap" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Goalmap</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cancel">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-inline">
          <div class="form-goalmap mb-2 col-sm-4">
            <span class="modal-input-label">Name</span>
          </div>
          <div class="form-goalmap mx-sm-3 mb-2 col-sm-8" style="position:relative">
            <input type="text" class="form-control input-goalmap-name" style="width:300px;" required>
            <div class="invalid-tooltip">
              Please provide a goalmap name.
            </div>
          </div>
        </div>
        <!-- <div class="form-inline">
          <div class="form-goalmap mb-2 col-sm-4">
            <span class="modal-input-label">Friendly ID</span>
          </div>
          <div class="form-goalmap mx-sm-3 mb-2 col-sm-8" style="position:relative">
            <input type="text" class="form-control input-goalmap-fid" style="width:300px;">
          </div>
        </div> -->
        <small class="modal-helper-text">Enter a goalmap name. Goalmap name cannot empty.</small>
        <hr>
        <div class="form">
          <div class="form-goalmap mb-2">
            <span class="modal-input-label">Goalmap Type</span>
          </div>
          <div class="form-goalmap mx-sm-3 mb-2" style="position:relative">
            <div class="form-check form-check-inline">
              <input class="form-check-input input-goalmap-type input-goalmap-type-pilot" type="radio"
                name="input-goalmap-type" id="inlineRadio1" value="draft">
              <label class="form-check-label" for="inlineRadio1">Draft</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input input-goalmap-type input-goalmap-type-practice" type="radio"
                name="input-goalmap-type" id="inlineRadio2" value="auto">
              <label class="form-check-label" for="inlineRadio2">Auto</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input input-goalmap-type input-goalmap-type-experimental" type="radio"
                name="input-goalmap-type" id="inlineRadio3" value="fix">
              <label class="form-check-label" for="inlineRadio3">Final (Fix)</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input input-goalmap-type input-goalmap-type-regular" type="radio"
                name="input-goalmap-type" id="inlineRadio4" value="kit">
              <label class="form-check-label" for="inlineRadio4">Kit</label>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok">OK</button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-close" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Edit Goalmap -->