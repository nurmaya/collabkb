<!-- Modal Assign Question to Qset -->
<div class="modal" id="modal-assign-qset" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Assign Question to: <span class="qs-name"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cancel">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="list-question"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Assign Question to Qset -->

<!-- Modal Question Order -->
<div class="modal" id="modal-question-order" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Questions Ordering: <span class="qs-name"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cancel">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="question-order-list" class="list-question"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok">Save Order</button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-close" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Question Order -->

<!-- Modal Question Set Taken By User -->
<div class="modal" id="modal-qset-taken" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">User attempting to: <span class="qs-name"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cancel">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="list-user"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Question Set Taken By User  -->