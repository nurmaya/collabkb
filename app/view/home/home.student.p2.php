<?php $this->view('base/header.php'); ?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">

    </div>
  </header>

  <main role="main" class="inner cover text-center justify-content-center" style="width: 42em;">
    <p class="lead" style="font-weight: 500">Please select a material topic:</p>
    <div class="row justify-content-center">
      <div id="list-material" class="list col-sm-6">
        <?php if(count($materials)) {
          foreach($materials as $m) :
            $display = (isset($_SESSION['mid']) and $m->mid == $_SESSION['mid']) ? 'block' : 'none';
            ?>
        <div class="material row" data-mid="<?php echo $m->mid; ?>"><?php echo $m->name; ?> <i
            class="fas fa-check text-success" style="display: <?php echo $display; ?>"></i></div>
        <?php
          endforeach;
        } ?>
      </div>
    </div>

    <div id="section-kit" class="mt-5">
      <p class="lead" style="font-weight: 500;">Please select a kit based on selected topic:</p>
      <div class="row justify-content-center">
        <div id="list-goalmap" class="list col-sm-6">
          <em class="pl-5 text-secondary">Select a material topic...</em>
        </div>
      </div>
    </div>

    <p class="lead mt-5" style="font-weight: 500">Please select a destined collaboration room:</p>
    <div class="row justify-content-center">
      <div id="list-room" class="list col-sm-6">
        <?php if(count($rooms)) {
          foreach($rooms as $r) :
            $display = (isset($_SESSION['mid']) and $r->rid == $_SESSION['rid']) ? 'block' : 'none';
            ?>
        <div class="room row" data-rid="<?php echo $r->rid; ?>"><?php echo $r->name; ?> <i
            class="fas fa-check text-success" style="display: <?php echo $display; ?>"></i></div>
        <?php
          endforeach;
        } ?>
      </div>
    </div>
    <button id="bt-continue" class="btn btn-lg btn-primary mt-5"
      <?php if(isset($_SESSION['mid'])) echo 'data-mid="' . $_SESSION['mid'] . '"'; ?>
      <?php if(isset($_SESSION['rid'])) echo 'data-rid="' . $_SESSION['rid'] . '"'; ?>>Continue</button>
    <a id="bt-signout" class="btn btn-lg btn-danger mt-5 ml-5" href="<?php echo $this->location('home/signOut'); ?>">Sign Out</a>
  </main>

  <footer class="mastfoot mt-auto">
    <div class="inner text-center">

    </div>
  </footer>
</div>

<?php $this->view('home/home.modal.php'); ?>
<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>