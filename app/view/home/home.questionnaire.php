<?php $this->view('base/header.php'); ?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column mt-5" style="max-width: 42em;">
  <main role="main" class="inner cover">

    <p>Silakan berikan pendapat/jawaban Anda pada pertanyaan questionnaire berikut ini sesuai dengan
      pemahaman/pandangan/pendapat Anda secara pribadi.</p>

    <hr>

    <!-- <h1 class="cover-heading mb-3"><?php echo $material->name; ?></h1> -->
    <?php if(!count($qset->questions)) : echo '<p class="text-center">No questions is set.</p>'; 
      else :
    ?>
    <ol>
      <?php foreach($qset->questions as $q) : ?>
      <li class="mb-4 question" data-type="<?php echo $q->type; ?>">
        <p class="mb-2"><?php echo $q->question; ?></p>
        <?php if($q->type == 'essay') : 
          $essay = '';
          if(isset($_SESSION['qansweressay'])) {
            $qanswersessay = $_SESSION['qansweressay'];
            foreach($qanswersessay as $a) {
              $a = (object) $a;
              if($a->qid == $q->qid) {
                $essay = $a->answer;
                break;
              }
            }
          }
        ?>
        <textarea class="input-free form-control" data-qid="<?php echo $q->qid; ?>"
          rows="5"><?php echo $essay; ?></textarea>
        <?php endif; ?>
        <?php 
          if($q->type == 'multi' || $q->type == 'multianswer') : 
          $inputType = $q->type == 'multianswer' ? 'checkbox' : 'radio'; 
        ?>
        <ol type="A">
          <?php foreach($q->options as $o) : ?>
          <?php $checked = '';
            if(isset($_SESSION['qanswer'])) {
              $qanswers = $_SESSION['qanswer'];
              foreach($qanswers as $a) {
                $a = (object) $a;
                if($a->qid == $q->qid && $a->qoid == $o->qoid) {
                  $checked = 'checked="checked"';
                }
                if($checked) break;
              }
            }
          ?>
          <li class="mb-2 pl-2"><input type="<?php echo $inputType; ?>" id="<?php echo $q->qid."-".$o->qoid; ?>"
              name="<?php echo $q->qid; ?>" data-qid="<?php echo $q->qid; ?>" data-qoid="<?php echo $o->qoid; ?>"
              <?php echo $checked; ?> /> &nbsp;
            <label class="mb-0" for="<?php echo $q->qid."-".$o->qoid; ?>"><?php echo $o->option; ?></label></li>

          <?php endforeach; ?>
        </ol>
        <?php endif; ?>
      </li>
      <?php endforeach; ?>
    </ol>
    <?php endif; ?>

    <hr>

    <div class="row mb-5">
      <div class="col">
        <small>Setelah Anda menyelesaikan, Anda tidak dapat kembali lagi ke halaman ini.</small>
        <hr>
        <button id="bt-save" class="btn btn-lg btn-primary">Save</button>
        <button id="bt-finish" class="btn btn-lg btn-danger">Finish</button>
      </div>
    </div>
  </main>
</div>

<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>