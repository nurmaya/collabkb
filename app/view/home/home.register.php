<?php $this->view('base/header.php'); ?>

<div id="main-container" class="container">

  <div class="row mx-auto mt-5" style="flex-direction: column; width: 42rem">

    <h1>User Registration<br><small class="text-info"><?php echo $role->name . " &rsaquo; " . $group->name; ?></small>
    <br>
    <hr>
    </h1>

    <p>In this page, you will be registering for the <strong><?php echo $group->name; ?></strong> group as <strong><?php echo $role->name; ?></strong>. Please register your desired username and password to use the system.</p>
    <p>The password will be stored in a save and hashed form. It will not be human-readable.</p>
    <div id="form-register" class="form">
    <hr>
      <div class="form-group row">
        <label for="inputUsername" class="col-sm-4 col-form-label text-right">Your Name</label>
        <div class="col-sm-6">
          <input type="text" autocomplete="new-password" class="form-control input-name" placeholder="Name">
        </div>
      </div>
      <div class="form-group row">
        <label for="inputUsername" class="col-sm-4 col-form-label text-right">Username</label>
        <div class="col-sm-6">
          <input type="text" autocomplete="new-password" class="form-control input-username" placeholder="Username">
        </div>
      </div>
      <div class="form-group row">
        <label for="inputPassword" autocomplete="new-password" class="col-sm-4 col-form-label text-right">Password</label>
        <div class="col-sm-6">
          <input type="password" class="form-control input-password" placeholder="Password">
        </div>
      </div>
      <div class="form-group row">
        <label for="inputPassword2" class="col-sm-4 col-form-label text-right">Password (Repeat)</label>
        <div class="col-sm-6">
          <input type="password" autocomplete="new-password" class="form-control input-password input-password-2" placeholder="Password (Repeat)">
        </div>
      </div>
      <hr>
    </div>

    <?php // var_dump($group); ?>
    
    <div class="row">
    <div class="col btn-group" style="width: 20rem;">
      <button id="bt-register" class="btn btn-lg btn-primary" data-rid="<?php echo $role->rid; ?>" data-gid="<?php echo $group->gid; ?>"><i class="fas fa-plus"></i>
        Register</button>
    </div>
    <div class="col btn-group" style="width: 20rem;">
      <a class="btn btn-lg btn-secondary" href="<?php echo $this->location(''); ?>">
        Return to Home</a>
    </div>
    </div>
    

  </div>

</div>

<?php $this->view('admin/test/test.modal.php'); ?>
<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>