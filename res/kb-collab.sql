/*
 Navicat Premium Data Transfer

 Source Server         : mac-localhost
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : localhost:3306
 Source Schema         : kb-collab

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 09/03/2020 16:52:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ability
-- ----------------------------
DROP TABLE IF EXISTS `ability`;
CREATE TABLE `ability` (
  `aid` varchar(200) COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for channel
-- ----------------------------
DROP TABLE IF EXISTS `channel`;
CREATE TABLE `channel` (
  `cid` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `date_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rid` int unsigned NOT NULL,
  `node_id` int unsigned NOT NULL,
  `node_type` enum('concept','link') COLLATE utf8mb4_general_ci NOT NULL,
  `node_label` text COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`cid`),
  UNIQUE KEY `u_node_channel` (`rid`,`node_id`,`node_type`),
  KEY `fk_channel_rooms_idx` (`rid`),
  CONSTRAINT `fk_channel_rooms` FOREIGN KEY (`rid`) REFERENCES `rooms` (`rid`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for goalmaps
-- ----------------------------
DROP TABLE IF EXISTS `goalmaps`;
CREATE TABLE `goalmaps` (
  `gmid` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `type` enum('draft','fix','auto') COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'draft',
  `mid` int unsigned NOT NULL,
  `creator_id` int unsigned DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updater_id` int unsigned DEFAULT NULL,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`gmid`),
  KEY `fk_goalmaps_materials_idx` (`mid`),
  KEY `fk_goalmaps_creator_idx` (`creator_id`),
  KEY `fk_goalmaps_updater_idx` (`updater_id`),
  CONSTRAINT `fk_goalmaps_creator` FOREIGN KEY (`creator_id`) REFERENCES `users` (`uid`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_goalmaps_materials` FOREIGN KEY (`mid`) REFERENCES `materials` (`mid`) ON UPDATE CASCADE,
  CONSTRAINT `fk_goalmaps_updater` FOREIGN KEY (`updater_id`) REFERENCES `users` (`uid`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for goalmaps_collab
-- ----------------------------
DROP TABLE IF EXISTS `goalmaps_collab`;
CREATE TABLE `goalmaps_collab` (
  `gmid` int unsigned NOT NULL,
  `rid` int unsigned NOT NULL,
  PRIMARY KEY (`gmid`),
  KEY `fk_goalmaps_collab_rooms_idx` (`rid`),
  CONSTRAINT `fk_goalmaps_collab_goalmaps` FOREIGN KEY (`gmid`) REFERENCES `goalmaps` (`gmid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_goalmaps_collab_rooms` FOREIGN KEY (`rid`) REFERENCES `rooms` (`rid`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for goalmaps_concepts
-- ----------------------------
DROP TABLE IF EXISTS `goalmaps_concepts`;
CREATE TABLE `goalmaps_concepts` (
  `cid` int unsigned NOT NULL,
  `gmid` int unsigned NOT NULL,
  `label` text COLLATE utf8mb4_general_ci NOT NULL,
  `locx` int NOT NULL DEFAULT '0',
  `locy` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`cid`,`gmid`),
  KEY `fk_goalmaps_concepts_goalmaps_idx` (`gmid`),
  KEY `fk_goalmaps_links_concept_source_target` (`gmid`,`cid`),
  CONSTRAINT `fk_goalmaps_concepts_goalmaps` FOREIGN KEY (`gmid`) REFERENCES `goalmaps` (`gmid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for goalmaps_links
-- ----------------------------
DROP TABLE IF EXISTS `goalmaps_links`;
CREATE TABLE `goalmaps_links` (
  `lid` int unsigned NOT NULL,
  `gmid` int unsigned NOT NULL,
  `label` text COLLATE utf8mb4_general_ci NOT NULL,
  `locx` int NOT NULL DEFAULT '0',
  `locy` int NOT NULL DEFAULT '0',
  `source` int unsigned DEFAULT NULL,
  `target` int unsigned DEFAULT NULL,
  PRIMARY KEY (`lid`,`gmid`),
  KEY `fk_goalmaps_links_goalmaps_idx` (`gmid`),
  KEY `fk_goalmaps_links_goalmaps_concepts_target_idx` (`gmid`,`target`),
  KEY `fk_goalmaps_links_goalmaps_concepts_source_idx` (`gmid`,`source`),
  CONSTRAINT `fk_goalmaps_links_goalmaps` FOREIGN KEY (`gmid`) REFERENCES `goalmaps` (`gmid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_goalmaps_links_goalmaps_concepts_source` FOREIGN KEY (`gmid`, `source`) REFERENCES `goalmaps_concepts` (`gmid`, `cid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_goalmaps_links_goalmaps_concepts_target` FOREIGN KEY (`gmid`, `target`) REFERENCES `goalmaps_concepts` (`gmid`, `cid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for goalmaps_score_bartel
-- ----------------------------
DROP TABLE IF EXISTS `goalmaps_score_bartel`;
CREATE TABLE `goalmaps_score_bartel` (
  `gmid` int unsigned NOT NULL,
  `expert_id` int unsigned NOT NULL,
  `ct` int NOT NULL DEFAULT '0',
  `kr` int NOT NULL DEFAULT '0',
  `ac` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`gmid`,`expert_id`),
  KEY `fk_goalmaps_score_bartel_users_idx` (`expert_id`),
  CONSTRAINT `fk_goalmaps_score_mueller_goalmaps10` FOREIGN KEY (`gmid`) REFERENCES `goalmaps` (`gmid`) ON UPDATE CASCADE,
  CONSTRAINT `fk_goalmaps_score_mueller_users10` FOREIGN KEY (`expert_id`) REFERENCES `users` (`uid`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for goalmaps_score_mueller
-- ----------------------------
DROP TABLE IF EXISTS `goalmaps_score_mueller`;
CREATE TABLE `goalmaps_score_mueller` (
  `gmid` int unsigned NOT NULL,
  `expert_id` int unsigned NOT NULL,
  `le` int NOT NULL DEFAULT '0',
  `ac` int NOT NULL DEFAULT '0',
  `co` int NOT NULL DEFAULT '0',
  `so` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`gmid`,`expert_id`),
  KEY `fk_goalmaps_score_mueller_users_idx` (`expert_id`),
  CONSTRAINT `fk_goalmaps_score_mueller_goalmaps` FOREIGN KEY (`gmid`) REFERENCES `goalmaps` (`gmid`) ON UPDATE CASCADE,
  CONSTRAINT `fk_goalmaps_score_mueller_users` FOREIGN KEY (`expert_id`) REFERENCES `users` (`uid`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for grups
-- ----------------------------
DROP TABLE IF EXISTS `grups`;
CREATE TABLE `grups` (
  `gid` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `type` enum('pilot','practice','experimental','regular') COLLATE utf8mb4_general_ci DEFAULT 'regular',
  `grade` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `class` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_general_ci,
  `creator_id` int unsigned DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`gid`),
  KEY `fk_grups_creator_idx` (`creator_id`),
  CONSTRAINT `fk_grups_creator` FOREIGN KEY (`creator_id`) REFERENCES `users` (`uid`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for grups_has_materials
-- ----------------------------
DROP TABLE IF EXISTS `grups_has_materials`;
CREATE TABLE `grups_has_materials` (
  `gid` int unsigned NOT NULL,
  `mid` int unsigned NOT NULL,
  PRIMARY KEY (`gid`,`mid`),
  KEY `fk_grups_has_materials_materials_idx` (`mid`),
  KEY `fk_grups_has_materials_grups_idx` (`gid`),
  CONSTRAINT `fk_grups_has_materials_grups` FOREIGN KEY (`gid`) REFERENCES `grups` (`gid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_grups_has_materials_materials` FOREIGN KEY (`mid`) REFERENCES `materials` (`mid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for grups_has_qsets
-- ----------------------------
DROP TABLE IF EXISTS `grups_has_qsets`;
CREATE TABLE `grups_has_qsets` (
  `qsid` int unsigned NOT NULL,
  `gid` int unsigned NOT NULL,
  PRIMARY KEY (`qsid`,`gid`),
  KEY `fk_question_sets_has_grups_grups_idx` (`gid`),
  KEY `fk_question_sets_has_grups_question_sets_idx` (`qsid`),
  CONSTRAINT `fk_question_sets_has_grups_grups` FOREIGN KEY (`gid`) REFERENCES `grups` (`gid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_question_sets_has_grups_question_sets` FOREIGN KEY (`qsid`) REFERENCES `question_sets` (`qsid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for learnermaps
-- ----------------------------
DROP TABLE IF EXISTS `learnermaps`;
CREATE TABLE `learnermaps` (
  `lmid` int unsigned NOT NULL AUTO_INCREMENT,
  `gmid` int unsigned NOT NULL,
  `uid` int unsigned NOT NULL,
  `type` enum('draft','fix','auto') COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'draft',
  PRIMARY KEY (`lmid`),
  KEY `fk_learnermaps_goalmaps_idx` (`gmid`),
  KEY `fk_learnermaps_users_idx` (`uid`),
  CONSTRAINT `fk_learnermaps_goalmaps` FOREIGN KEY (`gmid`) REFERENCES `goalmaps` (`gmid`) ON UPDATE CASCADE,
  CONSTRAINT `fk_learnermaps_users` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for learnermaps_collab
-- ----------------------------
DROP TABLE IF EXISTS `learnermaps_collab`;
CREATE TABLE `learnermaps_collab` (
  `lmid` int unsigned NOT NULL,
  `rid` int unsigned NOT NULL,
  PRIMARY KEY (`lmid`),
  KEY `fk_learnermaps_collab_rooms_idx` (`rid`),
  CONSTRAINT `fk_learnermaps_collab_learnermaps` FOREIGN KEY (`lmid`) REFERENCES `learnermaps` (`lmid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_learnermaps_collab_rooms` FOREIGN KEY (`rid`) REFERENCES `rooms` (`rid`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for learnermaps_concepts
-- ----------------------------
DROP TABLE IF EXISTS `learnermaps_concepts`;
CREATE TABLE `learnermaps_concepts` (
  `lmid` int unsigned NOT NULL,
  `cid` int unsigned NOT NULL,
  `gmid` int unsigned NOT NULL,
  `locx` int NOT NULL DEFAULT '0',
  `locy` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`gmid`,`cid`,`lmid`),
  KEY `fk_learnermaps_concepts_learnermaps_idx` (`lmid`),
  KEY `fk_learnermaps_concepts_goalmaps_concepts_idx` (`cid`,`gmid`),
  KEY `fk_learnermaps_links_concepts_source_target` (`lmid`,`gmid`,`cid`),
  CONSTRAINT `fk_learnermaps_concepts_goalmaps_concepts` FOREIGN KEY (`cid`, `gmid`) REFERENCES `goalmaps_concepts` (`cid`, `gmid`) ON UPDATE CASCADE,
  CONSTRAINT `fk_learnermaps_concepts_learnermaps` FOREIGN KEY (`lmid`) REFERENCES `learnermaps` (`lmid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for learnermaps_links
-- ----------------------------
DROP TABLE IF EXISTS `learnermaps_links`;
CREATE TABLE `learnermaps_links` (
  `lmid` int unsigned NOT NULL,
  `lid` int unsigned NOT NULL,
  `gmid` int unsigned NOT NULL,
  `locx` int NOT NULL DEFAULT '0',
  `locy` int NOT NULL DEFAULT '0',
  `source` int unsigned DEFAULT NULL,
  `target` int unsigned DEFAULT NULL,
  PRIMARY KEY (`lmid`,`lid`,`gmid`),
  KEY `fk_learnermaps_links_learnermaps_idx` (`lmid`),
  KEY `fk_learnermaps_links_goalmaps_links_idx` (`lid`,`gmid`),
  KEY `fk_learnermaps_links_learnermaps_concepts_source_idx` (`lmid`,`gmid`,`source`),
  KEY `fk_learnermaps_links_learnermaps_concepts_target_idx` (`lmid`,`gmid`,`target`),
  CONSTRAINT `fk_learnermaps_links_goalmaps_links` FOREIGN KEY (`lid`, `gmid`) REFERENCES `goalmaps_links` (`lid`, `gmid`) ON UPDATE CASCADE,
  CONSTRAINT `fk_learnermaps_links_learnermaps` FOREIGN KEY (`lmid`) REFERENCES `learnermaps` (`lmid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_learnermaps_links_learnermaps_concepts_source` FOREIGN KEY (`lmid`, `gmid`, `source`) REFERENCES `learnermaps_concepts` (`lmid`, `gmid`, `cid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_learnermaps_links_learnermaps_concepts_target` FOREIGN KEY (`lmid`, `gmid`, `target`) REFERENCES `learnermaps_concepts` (`lmid`, `gmid`, `cid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for logs_cmap_state
-- ----------------------------
DROP TABLE IF EXISTS `logs_cmap_state`;
CREATE TABLE `logs_cmap_state` (
  `lid` int unsigned NOT NULL,
  `concepts` text COLLATE utf8mb4_general_ci,
  `links` text COLLATE utf8mb4_general_ci,
  `edges` text COLLATE utf8mb4_general_ci,
  `propositions` text COLLATE utf8mb4_general_ci,
  `partial_propositions` text COLLATE utf8mb4_general_ci,
  `no_propositions` text COLLATE utf8mb4_general_ci,
  `nc` int NOT NULL DEFAULT '0',
  `nl` int NOT NULL DEFAULT '0',
  `ne` int NOT NULL DEFAULT '0',
  `np` int NOT NULL DEFAULT '0',
  `npp` int NOT NULL DEFAULT '0',
  `nnp` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`lid`),
  CONSTRAINT `fk_logs_cmap_state_logs_cmapping` FOREIGN KEY (`lid`) REFERENCES `logs_cmapping` (`lid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for logs_cmapping
-- ----------------------------
DROP TABLE IF EXISTS `logs_cmapping`;
CREATE TABLE `logs_cmapping` (
  `lid` int unsigned NOT NULL AUTO_INCREMENT,
  `uid` int unsigned NOT NULL,
  `seq` int NOT NULL DEFAULT '1',
  `action` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `time_client` datetime DEFAULT NULL,
  `data` text COLLATE utf8mb4_general_ci,
  `phpsessid` varchar(128) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`lid`),
  KEY `fk_logs_cmapping_users_idx` (`uid`),
  CONSTRAINT `fk_logs_cmapping_users` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2194 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for logs_goalmapping
-- ----------------------------
DROP TABLE IF EXISTS `logs_goalmapping`;
CREATE TABLE `logs_goalmapping` (
  `lid` int unsigned NOT NULL,
  PRIMARY KEY (`lid`),
  CONSTRAINT `fk_logs_goalmapping_logs_cmapping` FOREIGN KEY (`lid`) REFERENCES `logs_cmapping` (`lid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for logs_goalmapping_collab
-- ----------------------------
DROP TABLE IF EXISTS `logs_goalmapping_collab`;
CREATE TABLE `logs_goalmapping_collab` (
  `lid` int unsigned NOT NULL,
  `rid` int unsigned NOT NULL,
  `mid` int unsigned NOT NULL,
  PRIMARY KEY (`lid`),
  KEY `fk_logs_goalmapping_collab_rooms1_idx` (`rid`),
  KEY `fk_logs_goalmapping_collab_materials1_idx` (`mid`),
  CONSTRAINT `fk_logs_goalmapping_collab_logs_cmapping` FOREIGN KEY (`lid`) REFERENCES `logs_cmapping` (`lid`),
  CONSTRAINT `fk_logs_goalmapping_collab_materials` FOREIGN KEY (`mid`) REFERENCES `materials` (`mid`),
  CONSTRAINT `fk_logs_goalmapping_collab_rooms` FOREIGN KEY (`rid`) REFERENCES `rooms` (`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for logs_kitbuilding
-- ----------------------------
DROP TABLE IF EXISTS `logs_kitbuilding`;
CREATE TABLE `logs_kitbuilding` (
  `lid` int unsigned NOT NULL,
  `lmid` int unsigned NOT NULL,
  PRIMARY KEY (`lid`),
  KEY `fk_logs_kitbuilding_logs_cmapping_idx` (`lid`),
  KEY `fk_logs_kitbuilding_learnermaps_idx` (`lmid`),
  CONSTRAINT `fk_logs_kitbuilding_learnermaps` FOREIGN KEY (`lmid`) REFERENCES `learnermaps` (`lmid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_logs_kitbuilding_logs_cmapping` FOREIGN KEY (`lid`) REFERENCES `logs_cmapping` (`lid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for logs_kitbuilding_collab
-- ----------------------------
DROP TABLE IF EXISTS `logs_kitbuilding_collab`;
CREATE TABLE `logs_kitbuilding_collab` (
  `lid` int unsigned NOT NULL,
  `rid` int unsigned NOT NULL,
  `gmid` int unsigned NOT NULL,
  PRIMARY KEY (`lid`),
  KEY `fk_logs_kitbuilding_collab_logs_cmapping1_idx` (`lid`),
  KEY `fk_logs_kitbuilding_collab_goalmaps1_idx` (`gmid`),
  KEY `fk_logs_kitbuilding_collab_rooms` (`rid`),
  CONSTRAINT `fk_logs_kitbuilding_collab_goalmaps` FOREIGN KEY (`gmid`) REFERENCES `goalmaps` (`gmid`),
  CONSTRAINT `fk_logs_kitbuilding_collab_logs_cmapping` FOREIGN KEY (`lid`) REFERENCES `logs_cmapping` (`lid`),
  CONSTRAINT `fk_logs_kitbuilding_collab_rooms` FOREIGN KEY (`rid`) REFERENCES `rooms` (`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for materials
-- ----------------------------
DROP TABLE IF EXISTS `materials`;
CREATE TABLE `materials` (
  `mid` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `creator_id` int unsigned DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`mid`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `fk_materials_creator_idx` (`creator_id`),
  CONSTRAINT `fk_materials_creator` FOREIGN KEY (`creator_id`) REFERENCES `users` (`uid`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for materials_collab
-- ----------------------------
DROP TABLE IF EXISTS `materials_collab`;
CREATE TABLE `materials_collab` (
  `mid` int unsigned NOT NULL,
  `content` text COLLATE utf8mb4_general_ci,
  PRIMARY KEY (`mid`),
  CONSTRAINT `fk_materials_collab_materials` FOREIGN KEY (`mid`) REFERENCES `materials` (`mid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for materials_efl
-- ----------------------------
DROP TABLE IF EXISTS `materials_efl`;
CREATE TABLE `materials_efl` (
  `mid` int unsigned NOT NULL,
  `content` text COLLATE utf8mb4_general_ci,
  `nlp` text COLLATE utf8mb4_general_ci,
  PRIMARY KEY (`mid`),
  CONSTRAINT `fk_efl_parent` FOREIGN KEY (`mid`) REFERENCES `materials` (`mid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for materials_online
-- ----------------------------
DROP TABLE IF EXISTS `materials_online`;
CREATE TABLE `materials_online` (
  `mid` int unsigned NOT NULL,
  `url` text COLLATE utf8mb4_general_ci,
  PRIMARY KEY (`mid`),
  CONSTRAINT `fk_online_parent` FOREIGN KEY (`mid`) REFERENCES `materials` (`mid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for messages
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `mid` int unsigned NOT NULL AUTO_INCREMENT,
  `message` text COLLATE utf8mb4_general_ci NOT NULL,
  `mdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rid` int unsigned NOT NULL,
  `uid` int unsigned NOT NULL,
  PRIMARY KEY (`mid`),
  KEY `fk_messages_rooms_idx` (`rid`),
  KEY `fk_messages_users_idx` (`uid`),
  CONSTRAINT `fk_messages_rooms` FOREIGN KEY (`rid`) REFERENCES `rooms` (`rid`) ON UPDATE CASCADE,
  CONSTRAINT `fk_messages_users` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for messages_channel
-- ----------------------------
DROP TABLE IF EXISTS `messages_channel`;
CREATE TABLE `messages_channel` (
  `mid` int unsigned NOT NULL,
  `cid` int unsigned NOT NULL,
  PRIMARY KEY (`mid`),
  KEY `fk_messages_channel_channel_idx` (`cid`),
  CONSTRAINT `fk_messages_channel_channel` FOREIGN KEY (`cid`) REFERENCES `channel` (`cid`) ON UPDATE CASCADE,
  CONSTRAINT `fk_messages_channel_messages` FOREIGN KEY (`mid`) REFERENCES `messages` (`mid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for qsets_has_questions
-- ----------------------------
DROP TABLE IF EXISTS `qsets_has_questions`;
CREATE TABLE `qsets_has_questions` (
  `qsid` int unsigned NOT NULL,
  `qid` int unsigned NOT NULL,
  PRIMARY KEY (`qid`,`qsid`),
  KEY `fk_question_sets_has_questions_questions_idx` (`qid`),
  KEY `fk_question_sets_has_questions_question_sets_idx` (`qsid`),
  CONSTRAINT `fk_question_sets_has_questions_question_sets` FOREIGN KEY (`qsid`) REFERENCES `question_sets` (`qsid`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_question_sets_has_questions_questions` FOREIGN KEY (`qid`) REFERENCES `questions` (`qid`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for question_options
-- ----------------------------
DROP TABLE IF EXISTS `question_options`;
CREATE TABLE `question_options` (
  `qoid` int unsigned NOT NULL AUTO_INCREMENT,
  `option` text COLLATE utf8mb4_general_ci,
  `qid` int unsigned NOT NULL,
  PRIMARY KEY (`qoid`),
  KEY `fk_question_options_questions_idx` (`qid`),
  CONSTRAINT `fk_question_options_questions` FOREIGN KEY (`qid`) REFERENCES `questions` (`qid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for question_sets
-- ----------------------------
DROP TABLE IF EXISTS `question_sets`;
CREATE TABLE `question_sets` (
  `qsid` int unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8mb4_general_ci NOT NULL,
  `type` enum('pre','post','delay','generic') COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'generic',
  PRIMARY KEY (`qsid`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for questions
-- ----------------------------
DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions` (
  `qid` int unsigned NOT NULL AUTO_INCREMENT,
  `question` text COLLATE utf8mb4_general_ci,
  `answer_qoid` int unsigned DEFAULT NULL,
  PRIMARY KEY (`qid`),
  KEY `fk_questions_question_options_idx` (`answer_qoid`),
  CONSTRAINT `fk_questions_question_options` FOREIGN KEY (`answer_qoid`) REFERENCES `question_options` (`qoid`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `rid` varchar(15) COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for roles_has_ability
-- ----------------------------
DROP TABLE IF EXISTS `roles_has_ability`;
CREATE TABLE `roles_has_ability` (
  `rid` varchar(15) COLLATE utf8mb4_general_ci NOT NULL,
  `aid` varchar(200) COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`rid`,`aid`),
  KEY `fk_roles_has_ability_ability_idx` (`aid`),
  KEY `fk_roles_has_ability_roles_idx` (`rid`),
  CONSTRAINT `fk_roles_has_ability_ability` FOREIGN KEY (`aid`) REFERENCES `ability` (`aid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_roles_has_ability_roles` FOREIGN KEY (`rid`) REFERENCES `roles` (`rid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for room_users
-- ----------------------------
DROP TABLE IF EXISTS `room_users`;
CREATE TABLE `room_users` (
  `uid` int unsigned NOT NULL,
  `rid` int unsigned NOT NULL,
  PRIMARY KEY (`uid`),
  KEY `fk_room_users_users1_idx` (`uid`),
  KEY `fk_room_users_rooms1_idx` (`rid`),
  CONSTRAINT `fk_room_users_rooms` FOREIGN KEY (`rid`) REFERENCES `rooms` (`rid`),
  CONSTRAINT `fk_room_users_users` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for rooms
-- ----------------------------
DROP TABLE IF EXISTS `rooms`;
CREATE TABLE `rooms` (
  `rid` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `open` tinyint(1) NOT NULL DEFAULT '0',
  `date_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `gid` int unsigned DEFAULT NULL,
  PRIMARY KEY (`rid`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `fk_rooms_grups_idx` (`gid`),
  CONSTRAINT `fk_rooms_grups` FOREIGN KEY (`gid`) REFERENCES `grups` (`gid`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for user_answers
-- ----------------------------
DROP TABLE IF EXISTS `user_answers`;
CREATE TABLE `user_answers` (
  `uid` int unsigned NOT NULL,
  `qsid` int unsigned NOT NULL,
  `gid` int unsigned NOT NULL,
  `qid` int unsigned NOT NULL,
  `qoid` int unsigned NOT NULL,
  PRIMARY KEY (`uid`,`qsid`,`gid`,`qid`),
  KEY `fk_question_answers_questions_idx` (`qid`),
  KEY `fk_question_answers_question_options_idx` (`qoid`),
  KEY `fk_user_answers_users_idx` (`uid`),
  KEY `fk_user_answers_grups_has_qsets1_idx` (`qsid`,`gid`),
  CONSTRAINT `fk_question_answers_question_options` FOREIGN KEY (`qoid`) REFERENCES `question_options` (`qoid`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_question_answers_questions` FOREIGN KEY (`qid`) REFERENCES `questions` (`qid`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_user_answers_grups_has_qsets1` FOREIGN KEY (`qsid`, `gid`) REFERENCES `grups_has_qsets` (`qsid`, `gid`),
  CONSTRAINT `fk_user_answers_users` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(32) COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `role_id` varchar(15) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `creator_id` int unsigned DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `fk_users_roles_idx` (`role_id`),
  KEY `fk_users_creator_idx` (`creator_id`),
  CONSTRAINT `fk_users_creator` FOREIGN KEY (`creator_id`) REFERENCES `users` (`uid`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_users_roles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`rid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for users_in_grups
-- ----------------------------
DROP TABLE IF EXISTS `users_in_grups`;
CREATE TABLE `users_in_grups` (
  `uid` int unsigned NOT NULL,
  `gid` int unsigned NOT NULL,
  PRIMARY KEY (`uid`,`gid`),
  KEY `fk_users_has_grups_grups_idx` (`gid`),
  KEY `fk_users_has_grups_users_idx` (`uid`),
  CONSTRAINT `fk_users_in_grups_grups` FOREIGN KEY (`gid`) REFERENCES `grups` (`gid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_users_in_grups_users` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for users_sessions
-- ----------------------------
DROP TABLE IF EXISTS `users_sessions`;
CREATE TABLE `users_sessions` (
  `uid` int unsigned NOT NULL,
  `data` text COLLATE utf8mb4_general_ci,
  PRIMARY KEY (`uid`),
  CONSTRAINT `fk_users_sessions_users` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

SET FOREIGN_KEY_CHECKS = 1;
